-- MySQL dump 10.13  Distrib 5.7.24, for Linux (x86_64)
--
-- Host: localhost    Database: cms
-- ------------------------------------------------------
-- Server version	5.7.24-0ubuntu0.18.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_permission_user`
--

DROP TABLE IF EXISTS `auth_permission_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `permission` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission_user`
--

LOCK TABLES `auth_permission_user` WRITE;
/*!40000 ALTER TABLE `auth_permission_user` DISABLE KEYS */;
INSERT INTO `auth_permission_user` VALUES (1,1,2,'user-list'),(2,1,1,'user-add'),(3,3,1,'user-list'),(4,3,2,'user-add'),(5,6,1,'user-list'),(6,6,2,'user-add'),(48,8,1,'user-list'),(49,8,2,'user-add'),(50,8,3,'user-permission-assign'),(53,4,1,'user-list'),(54,4,2,'user-add'),(55,4,3,'user-permission-assign');
/*!40000 ALTER TABLE `auth_permission_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permissions`
--

DROP TABLE IF EXISTS `auth_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `display_name` varchar(255) NOT NULL,
  `code_name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permissions`
--

LOCK TABLES `auth_permissions` WRITE;
/*!40000 ALTER TABLE `auth_permissions` DISABLE KEYS */;
INSERT INTO `auth_permissions` VALUES (1,'Can list users','user-list'),(2,'Can add user','user-add'),(3,'Can assign permission','user-permission-assign');
/*!40000 ALTER TABLE `auth_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloads_categories`
--

DROP TABLE IF EXISTS `downloads_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloads_categories` (
  `downloads_category_id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`downloads_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloads_categories`
--

LOCK TABLES `downloads_categories` WRITE;
/*!40000 ALTER TABLE `downloads_categories` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloads_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `downloads_items`
--

DROP TABLE IF EXISTS `downloads_items`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `downloads_items` (
  `download_items_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text,
  `file` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `is_trashed` int(11) NOT NULL,
  `unique_key` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`download_items_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `downloads_items`
--

LOCK TABLES `downloads_items` WRITE;
/*!40000 ALTER TABLE `downloads_items` DISABLE KEYS */;
/*!40000 ALTER TABLE `downloads_items` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery`
--

DROP TABLE IF EXISTS `gallery`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_name` varchar(100) DEFAULT NULL,
  `description` text,
  `is_trashed` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery`
--

LOCK TABLES `gallery` WRITE;
/*!40000 ALTER TABLE `gallery` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gallery_images`
--

DROP TABLE IF EXISTS `gallery_images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gallery_images` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `gallery_id` int(11) NOT NULL,
  `image_name` varchar(200) NOT NULL,
  `is_trashed` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `trashed_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gallery_images`
--

LOCK TABLES `gallery_images` WRITE;
/*!40000 ALTER TABLE `gallery_images` DISABLE KEYS */;
/*!40000 ALTER TABLE `gallery_images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `medias`
--

DROP TABLE IF EXISTS `medias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `medias` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `size` varchar(50) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_trashed` int(11) NOT NULL DEFAULT '0',
  `gallery_id` int(11) DEFAULT NULL,
  `file_type` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `trashed_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `medias`
--

LOCK TABLES `medias` WRITE;
/*!40000 ALTER TABLE `medias` DISABLE KEYS */;
/*!40000 ALTER TABLE `medias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menu_post`
--

DROP TABLE IF EXISTS `menu_post`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menu_post` (
  `menu_post_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `menu_display_name` varchar(255) DEFAULT NULL,
  `menu_icon` varchar(255) DEFAULT NULL,
  `menu_description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`menu_post_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menu_post`
--

LOCK TABLES `menu_post` WRITE;
/*!40000 ALTER TABLE `menu_post` DISABLE KEYS */;
INSERT INTO `menu_post` VALUES (1,4,1,NULL,NULL,'2018-11-10 06:55:42','2018-11-10 06:55:42',NULL,NULL,NULL),(2,12,1,NULL,NULL,'2018-11-10 06:58:05','2018-11-10 06:58:05',NULL,NULL,NULL);
/*!40000 ALTER TABLE `menu_post` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(50) NOT NULL,
  `menu_type` varchar(50) NOT NULL,
  `is_selected` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'Main menu','nav_menu',1,'2018-11-10 06:55:31','2018-11-10 06:55:36',1);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (3,'2014_10_12_000000_create_users_table',1),(4,'2014_10_12_100000_create_password_resets_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_category`
--

DROP TABLE IF EXISTS `post_category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `category_title` varchar(255) DEFAULT NULL,
  `category_subtitle` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_category`
--

LOCK TABLES `post_category` WRITE;
/*!40000 ALTER TABLE `post_category` DISABLE KEYS */;
INSERT INTO `post_category` VALUES (1,'Plain White T\'s','Plain White T\'s (read as \"plain white tees\") are an American rock band from Lombard, Illinois, formed in 1997 by high school friends Tom Higgenson, Dave Tirio and Ken Fletcher. They were joined a short time later by Steve Mast. The group had a mostly underground following in Chicago basements, clubs and bars in its early years.[6][7]\r\n\r\nThe band is best known in the U.S. for the number-one hit song \"Hey There Delilah\", which achieved platinum status in 2007 and earned two Grammy nominations,[8] as well as \"1234\" and \"Rhythm of Love\", which were certified platinum in 2009 and 2011.',1,'2018-11-07 01:54:45','2018-11-07 01:54:45');
/*!40000 ALTER TABLE `post_category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `post_meta`
--

DROP TABLE IF EXISTS `post_meta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `post_meta` (
  `post_meta_id` int(11) NOT NULL AUTO_INCREMENT,
  `post_id` int(11) DEFAULT NULL,
  `meta_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  PRIMARY KEY (`post_meta_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `post_meta`
--

LOCK TABLES `post_meta` WRITE;
/*!40000 ALTER TABLE `post_meta` DISABLE KEYS */;
INSERT INTO `post_meta` VALUES (1,4,1,'2018-11-07 02:05:49','2018-11-07 02:05:49','category');
/*!40000 ALTER TABLE `post_meta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `post_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `post_content` longtext CHARACTER SET utf8,
  `post_slug` varchar(50) NOT NULL,
  `post_status` varchar(20) NOT NULL,
  `post_type` varchar(20) NOT NULL,
  `post_parent` int(11) NOT NULL DEFAULT '0',
  `unique_key` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `post_url` varchar(200) NOT NULL,
  `is_trashed` int(11) DEFAULT '0',
  `gallery_id` int(11) DEFAULT NULL,
  `tags` text,
  `cover_image` varchar(200) DEFAULT NULL,
  `post_subtitle` text,
  `cover_icon` varchar(200) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `button_type` varchar(100) DEFAULT NULL,
  `button_text` varchar(45) DEFAULT NULL,
  `url_target` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (1,'Nuttin\' for Christmas','<p>\"<strong>Nuttin&rsquo; for Christmas</strong>\" (also known as \"<strong>Nothing for Christmas</strong>\") is a novelty Christmas song written by&nbsp;<a title=\"Sid Tepper\" href=\"https://en.wikipedia.org/wiki/Sid_Tepper\">Sid Tepper</a>&nbsp;and&nbsp;<a title=\"Roy C. Bennett\" href=\"https://en.wikipedia.org/wiki/Roy_C._Bennett\">Roy C. Bennett</a>. It became a hit during the 1955 Christmas season when it appeared in&nbsp;<em><a title=\"Billboard (magazine)\" href=\"https://en.wikipedia.org/wiki/Billboard_(magazine)\">Billboard</a></em>&rsquo;s pop charts by five different artists. The highest-charting of the five recordings was released by&nbsp;<a title=\"Art Mooney\" href=\"https://en.wikipedia.org/wiki/Art_Mooney\">Art Mooney</a>&nbsp;and His Orchestra, with six-year-old&nbsp;<a title=\"Barry Gordon\" href=\"https://en.wikipedia.org/wiki/Barry_Gordon\">Barry Gordon</a>&nbsp;as lead vocalist; this version peaked at #6 and became a million-seller. Another notable version was performed by&nbsp;<a title=\"Stan Freberg\" href=\"https://en.wikipedia.org/wiki/Stan_Freberg\">Stan Freberg</a>&nbsp;(with&nbsp;<a title=\"Daws Butler\" href=\"https://en.wikipedia.org/wiki/Daws_Butler\">Daws Butler</a>&nbsp;appearing as a burglar helped by the kid at the end). Other charting versions were recorded by&nbsp;<a title=\"The Fontane Sisters\" href=\"https://en.wikipedia.org/wiki/The_Fontane_Sisters\">The Fontane Sisters</a>, Joe Ward, and Ricky Zahnd and the Blue Jeaners.</p>\r\n<p>The song was revived on the&nbsp;<a title=\"Bigtop Records\" href=\"https://en.wikipedia.org/wiki/Bigtop_Records\">Big Top</a>&nbsp;label by Kenny and Corky and entered the&nbsp;<em>Cashbox</em>&nbsp;Top 100 in 1959.<sup id=\"cite_ref-1\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Nuttin%27_for_Christmas#cite_note-1\">[1]</a></sup></p>\r\n<p>Other artists who have recorded the song include&nbsp;<a title=\"Less Than Jake\" href=\"https://en.wikipedia.org/wiki/Less_Than_Jake\">Less Than Jake</a>,&nbsp;<a title=\"Spike Jones\" href=\"https://en.wikipedia.org/wiki/Spike_Jones\">Spike Jones</a>,&nbsp;<a title=\"Eartha Kitt\" href=\"https://en.wikipedia.org/wiki/Eartha_Kitt\">Eartha Kitt</a>,&nbsp;<a title=\"Homer and Jethro\" href=\"https://en.wikipedia.org/wiki/Homer_and_Jethro\">Homer and Jethro</a>,&nbsp;<a title=\"\" href=\"https://en.wikipedia.org/wiki/Relient_K\">Relient K</a>,&nbsp;<a title=\"Smash Mouth\" href=\"https://en.wikipedia.org/wiki/Smash_Mouth\">Smash Mouth</a>&nbsp;(featuring&nbsp;<a title=\"Rosie O\'Donnell\" href=\"https://en.wikipedia.org/wiki/Rosie_O%27Donnell\">Rosie O\'Donnell</a>),&nbsp;<a title=\"Sugarland\" href=\"https://en.wikipedia.org/wiki/Sugarland\">Sugarland</a>,&nbsp;<a class=\"mw-redirect\" title=\"Tonic Sol-fa\" href=\"https://en.wikipedia.org/wiki/Tonic_Sol-fa\">Tonic Sol-fa</a>&nbsp;and&nbsp;<a title=\"The Vindictives\" href=\"https://en.wikipedia.org/wiki/The_Vindictives\">The Vindictives</a>. In November 2011, it was covered by&nbsp;<a title=\"Plain White T\'s\" href=\"https://en.wikipedia.org/wiki/Plain_White_T%27s\">The Plain White T\'s</a>.</p>\r\n<p>In 2009, the rap artist&nbsp;<a title=\"Tony Yayo\" href=\"https://en.wikipedia.org/wiki/Tony_Yayo\">Tony Yayo</a>&nbsp;sampled the original track on his mixtape&nbsp;<em>The Swine Flu</em>&nbsp;on a track titled \"Somebody Snitched On Me\" as a \"diss\" track for the rap artist&nbsp;<a title=\"Rick Ross\" href=\"https://en.wikipedia.org/wiki/Rick_Ross\">Rick Ross</a>.</p>\r\n<p>Bobby Stewart, circa 1956, Peter Pan Records with the Peter Pan Orchestra and Chorus.</p>','nuttin\'-for-christmas','publish','category_item',0,'SWlsjAyvgblSLZUl96lcOxfdZct3oaU9HIoCwi4qyXfZ2bIZ5o351thuje3NMWBJzk6tPCc4wYcImAvjR4gLxME17sP977jQp2xI',1,NULL,'2018-11-07 01:59:16','2018-11-07 01:59:16','http://localhost:8000/nuttin\'-for-christmas',0,NULL,NULL,'','\"Nuttin’ for Christmas\" (also known as \"Nothing for Christmas\") is a novelty Christmas song written by Sid Tepper and Roy C. Bennett.','',1,'btn btn-success  ',NULL,NULL),(2,'Rhythm of Love','<table class=\"infobox vevent\">\r\n<tbody>\r\n<tr>\r\n<th class=\"summary\" colspan=\"2\">\"Rhythm of Love\"</th>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\"><a class=\"image\" href=\"https://en.wikipedia.org/wiki/File:Plain_white_ts_-_Rhythm_of_love.jpg\"><img src=\"https://upload.wikimedia.org/wikipedia/en/thumb/7/75/Plain_white_ts_-_Rhythm_of_love.jpg/220px-Plain_white_ts_-_Rhythm_of_love.jpg\" srcset=\"//upload.wikimedia.org/wikipedia/en/7/75/Plain_white_ts_-_Rhythm_of_love.jpg 1.5x\" alt=\"Plain white ts - Rhythm of love.jpg\" width=\"220\" height=\"219\" data-file-width=\"317\" data-file-height=\"315\" /></a></td>\r\n</tr>\r\n<tr class=\"description\">\r\n<th class=\"description\" colspan=\"2\"><a title=\"Single (music)\" href=\"https://en.wikipedia.org/wiki/Single_(music)\">Single</a>&nbsp;by&nbsp;<a title=\"Plain White T\'s\" href=\"https://en.wikipedia.org/wiki/Plain_White_T%27s\">Plain White T\'s</a></th>\r\n</tr>\r\n<tr class=\"description\">\r\n<th class=\"description\" colspan=\"2\">from the album&nbsp;<em><a title=\"Wonders of the Younger\" href=\"https://en.wikipedia.org/wiki/Wonders_of_the_Younger\">Wonders of the Younger</a></em></th>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\">Released</th>\r\n<td class=\"plainlist\">October&nbsp;3,&nbsp;2010</td>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\">Format</th>\r\n<td class=\"hlist\"><a title=\"CD single\" href=\"https://en.wikipedia.org/wiki/CD_single\">CD single</a>,&nbsp;<a title=\"Music download\" href=\"https://en.wikipedia.org/wiki/Music_download\">digital download</a></td>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\">Recorded</th>\r\n<td class=\"plainlist\">2010</td>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\">Length</th>\r\n<td class=\"plainlist\"><span class=\"duration\"><span class=\"min\">3</span>:<span class=\"s\">21</span></span>&nbsp;<small>(single version)</small><br /><span class=\"duration\"><span class=\"min\">3</span>:<span class=\"s\">08</span></span>&nbsp;<small>(radio edit)</small></td>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\"><a title=\"Record label\" href=\"https://en.wikipedia.org/wiki/Record_label\">Label</a></th>\r\n<td class=\"hlist\"><a title=\"Hollywood Records\" href=\"https://en.wikipedia.org/wiki/Hollywood_Records\">Hollywood</a></td>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\"><span class=\"nowrap\"><a title=\"Songwriter\" href=\"https://en.wikipedia.org/wiki/Songwriter\">Songwriter(s)</a></span></th>\r\n<td class=\"hlist\">Tim Lopez</td>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\"><span class=\"nowrap\"><a title=\"Record producer\" href=\"https://en.wikipedia.org/wiki/Record_producer\">Producer(s)</a></span></th>\r\n<td class=\"hlist\"><a class=\"mw-redirect\" title=\"Ian Kirkpatrick (producer)\" href=\"https://en.wikipedia.org/wiki/Ian_Kirkpatrick_(producer)\">Ian Kirkpatrick</a>, Nick Rucker</td>\r\n</tr>\r\n<tr>\r\n<th class=\"description\" colspan=\"2\"><a title=\"Plain White T\'s\" href=\"https://en.wikipedia.org/wiki/Plain_White_T%27s\">Plain White T\'s</a>&nbsp;singles chronology</th>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>\"<a title=\"1, 2, 3, 4 (Plain White T\'s song)\" href=\"https://en.wikipedia.org/wiki/1,_2,_3,_4_(Plain_White_T%27s_song)\">1, 2, 3, 4</a>\"<br />(2008)</td>\r\n<td>\"<strong>Rhythm of Love</strong>\"<br />(2010)</td>\r\n<td>\"Boomerang\"<br />(2011)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>\"<strong>Rhythm of Love</strong>\" is a song by American&nbsp;<a title=\"Rock music\" href=\"https://en.wikipedia.org/wiki/Rock_music\">rock</a>&nbsp;band&nbsp;<a title=\"Plain White T\'s\" href=\"https://en.wikipedia.org/wiki/Plain_White_T%27s\">Plain White T\'s</a>. It is the first single from their sixth studio album&nbsp;<em><a title=\"Wonders of the Younger\" href=\"https://en.wikipedia.org/wiki/Wonders_of_the_Younger\">Wonders of the Younger</a></em>. The song debuted at number 96 on the&nbsp;<a title=\"Billboard Hot 100\" href=\"https://en.wikipedia.org/wiki/Billboard_Hot_100\"><em>Billboard</em>&nbsp;Hot 100</a>&nbsp;and peaked at number 38.<sup id=\"cite_ref-1\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Rhythm_of_Love_(Plain_White_T%27s_song)#cite_note-1\">[1]</a></sup>&nbsp;The song was featured in a fall 2010 promo for the season two premiere of&nbsp;<a title=\"Parenthood (2010 TV series)\" href=\"https://en.wikipedia.org/wiki/Parenthood_(2010_TV_series)\"><em>Parenthood</em></a>&nbsp;on NBC, and also during the closing credits of the 2011 film&nbsp;<a title=\"No Strings Attached (film)\" href=\"https://en.wikipedia.org/wiki/No_Strings_Attached_(film)\"><em>No Strings Attached</em></a>. As of April 2011, the single has sold over 1,000,000 copies.<sup id=\"cite_ref-2\" class=\"reference\"><a href=\"https://en.wikipedia.org/wiki/Rhythm_of_Love_(Plain_White_T%27s_song)#cite_note-2\">[2]</a></sup></p>','rhythm-of-love','publish','category_item',0,'jFDIhl3g5dZN1EGD9U1bg2GOUTihVL0k90c9ipwiPGaiTS2fUfJaGQN78I3Gc8wpuZGDNh7CwyvUbEuoYBDcEF1qRHpkoCPuIVdC',1,NULL,'2018-11-07 02:00:44','2018-11-07 02:00:44','http://localhost:8000/rhythm-of-love',0,NULL,NULL,'','\"Rhythm of Love\" is a song by American rock band Plain White T\'s. It is the first single from their sixth studio album Wonders of the Younger.','',1,'btn btn-success  ',NULL,NULL),(3,'Our Time Now','<table class=\"infobox vevent\">\r\n<tbody>\r\n<tr>\r\n<th class=\"summary\" colspan=\"2\">\"Our Time Now\"</th>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\"><a class=\"image\" href=\"https://en.wikipedia.org/wiki/File:Our_Time_Now.jpg\"><img src=\"https://upload.wikimedia.org/wikipedia/en/thumb/9/9e/Our_Time_Now.jpg/220px-Our_Time_Now.jpg\" srcset=\"//upload.wikimedia.org/wikipedia/en/9/9e/Our_Time_Now.jpg 1.5x\" alt=\"Our Time Now.jpg\" width=\"220\" height=\"221\" data-file-width=\"315\" data-file-height=\"316\" /></a></td>\r\n</tr>\r\n<tr class=\"description\">\r\n<th class=\"description\" colspan=\"2\"><a title=\"Single (music)\" href=\"https://en.wikipedia.org/wiki/Single_(music)\">Single</a>&nbsp;by&nbsp;<a title=\"Plain White T\'s\" href=\"https://en.wikipedia.org/wiki/Plain_White_T%27s\">Plain White T\'s</a></th>\r\n</tr>\r\n<tr class=\"description\">\r\n<th class=\"description\" colspan=\"2\">from the album&nbsp;<em><a title=\"Every Second Counts (album)\" href=\"https://en.wikipedia.org/wiki/Every_Second_Counts_(album)\">Every Second Counts</a></em></th>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\">Released</th>\r\n<td class=\"plainlist\">November 6, 2007</td>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\">Format</th>\r\n<td class=\"hlist\"><a title=\"CD single\" href=\"https://en.wikipedia.org/wiki/CD_single\">CD single</a></td>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\">Recorded</th>\r\n<td class=\"plainlist\">2006</td>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\"><a title=\"Music genre\" href=\"https://en.wikipedia.org/wiki/Music_genre\">Genre</a></th>\r\n<td class=\"category hlist\"><a title=\"Pop punk\" href=\"https://en.wikipedia.org/wiki/Pop_punk\">Pop punk</a></td>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\">Length</th>\r\n<td class=\"plainlist\"><span class=\"duration\"><span class=\"min\">2</span>:<span class=\"s\">50</span></span></td>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\"><a title=\"Record label\" href=\"https://en.wikipedia.org/wiki/Record_label\">Label</a></th>\r\n<td class=\"hlist\"><a title=\"Hollywood Records\" href=\"https://en.wikipedia.org/wiki/Hollywood_Records\">Hollywood</a>,&nbsp;<a title=\"Fearless Records\" href=\"https://en.wikipedia.org/wiki/Fearless_Records\">Fearless</a></td>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\"><span class=\"nowrap\"><a title=\"Songwriter\" href=\"https://en.wikipedia.org/wiki/Songwriter\">Songwriter(s)</a></span></th>\r\n<td class=\"hlist\">Tom Higgenson, Mia Post, Mike Daly</td>\r\n</tr>\r\n<tr>\r\n<th scope=\"row\"><span class=\"nowrap\"><a title=\"Record producer\" href=\"https://en.wikipedia.org/wiki/Record_producer\">Producer(s)</a></span></th>\r\n<td class=\"hlist\"><a title=\"Johnny K\" href=\"https://en.wikipedia.org/wiki/Johnny_K\">Johnny K</a></td>\r\n</tr>\r\n<tr>\r\n<th class=\"description\" colspan=\"2\"><a title=\"Plain White T\'s\" href=\"https://en.wikipedia.org/wiki/Plain_White_T%27s\">Plain White T\'s</a>&nbsp;singles chronology</th>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\">\r\n<table>\r\n<tbody>\r\n<tr>\r\n<td>\"<a title=\"Hey There Delilah\" href=\"https://en.wikipedia.org/wiki/Hey_There_Delilah\">Hey There Delilah</a>\"<br />(re-release)<br />(2007)</td>\r\n<td>\"<strong>Our Time Now</strong>\"<br />(2007)</td>\r\n<td>\"<a title=\"Natural Disaster (Plain White T\'s song)\" href=\"https://en.wikipedia.org/wiki/Natural_Disaster_(Plain_White_T%27s_song)\">Natural Disaster</a>\"<br />(2008)</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n<p>\"<strong>Our Time Now</strong>\" is the third single from the&nbsp;<a title=\"Plain White T\'s\" href=\"https://en.wikipedia.org/wiki/Plain_White_T%27s\">Plain White T\'s</a>&nbsp;from their album&nbsp;<em><a title=\"Every Second Counts (album)\" href=\"https://en.wikipedia.org/wiki/Every_Second_Counts_(album)\">Every Second Counts</a></em>. It was shipped to Pop Radio on November 6, 2007. It peaked at number 90 on the&nbsp;<a title=\"Billboard Hot 100\" href=\"https://en.wikipedia.org/wiki/Billboard_Hot_100\"><em>Billboard</em>&nbsp;Hot 100</a>&nbsp;and number 29 on US Modern Rock charts.</p>\r\n<p>The song was featured during a montage of people getting approved to go to Hollywood during&nbsp;<em><a title=\"American Idol\" href=\"https://en.wikipedia.org/wiki/American_Idol\">American Idol</a></em>&nbsp;on January 29, 2008. The promo caused the song to jump 20 spots on iTunes from 75 to 55 overnight.</p>\r\n<p>\"Our Time Now\" was written by Tom Higgenson, Mia Post (Mia Koo), and Michael Daly and produced by Johnny K.</p>','our-time-now','publish','category_item',0,'YQ1aNcKvUdK0v3k4DXPFAWLcgZcHdcaQGqChp0yWGvn5GlBmZRYjTkLNp51xOOdj7O8XKQ3E0KB4b0PtFjFQRK8lE1QjNR7VeFPq',1,NULL,'2018-11-07 02:01:41','2018-11-07 02:01:41','http://localhost:8000/our-time-now',0,NULL,NULL,'','\"Our Time Now\" is the third single from the Plain White T\'s from their album Every Second Counts. It was shipped to Pop Radio on November 6, 2007. It peaked at number 90 on the Billboard Hot 100 and number 29 on US Modern Rock charts.','',1,'btn btn-success  ',NULL,NULL),(4,'Hey There Delilah chords by Plain White T\'s','<pre>\r\n<img alt=\"\" src=\"http://localhost:8000/storage/media/image/1/download.jpeg\" style=\"height:183px; width:276px\" />\r\n[Intro]\r\nD  F#m  D  F#m\r\n\r\n[Verse 1]\r\nD                            F#m\r\nHey there Delilah, What&rsquo;s it like in New York City? \r\n      D                               F#m\r\nI&rsquo;m a thousand miles away, But girl tonight you look so pretty, \r\n        Bm  G                 A                  Bm\r\nYes you do, Time Square can&rsquo;t shine as bright as you, \r\n             A\r\nI swear it&rsquo;s true. \r\nD                            F#m\r\nHey there Delilah, Don&rsquo;t you worry about the distance, \r\n          D                                  F#m\r\nI&rsquo;m right there if you get lonely, Give this song another listen, \r\n           Bm    G            A                Bm \r\nClose your eyes, Listen to my voice it&rsquo;s my disguise, \r\n            A\r\nI&rsquo;m by your side. \r\n\r\n[Chorus]\r\nD                      Bm  D                      Bm     A\r\nOh it&rsquo;s what you do to me, Oh it&rsquo;s what you do to me, \r\nD                      Bm  D                      Bm             \r\nOh it&rsquo;s what you do to me, Oh it&rsquo;s what you do to me, \r\n               D\r\nWhat you do to me. \r\n\r\n[Verse 2]\r\nD                         F#m\r\nHey there Delilah, I know times are getting hard, \r\n           D                                    F#m\r\nBut just believe me girl some day, I&#39;ll pay the bills with this guitar, \r\n              Bm    G              A               Bm\r\nWe&#39;ll have it good, We&#39;ll have the life we knew we would, \r\n           A\r\nMy word is good. \r\nD                           F#m   \r\nHey there Delilah, I&rsquo;ve got so much left to say, \r\n         D                                 F#m\r\nIf every simple song I wrote to you, Would take your breath away, \r\n             Bm   G            A                  Bm\r\nI&rsquo;d write it all, Even more in love with me you&rsquo;d fall, \r\n             A\r\nWe&rsquo;d have it all. \r\n\r\n[Chorus]\r\nD                      Bm  D                      Bm    A\r\nOh it&rsquo;s what you do to me, Oh it&rsquo;s what you do to me, \r\nD                      Bm  D                      Bm    A       \r\nOh it&rsquo;s what you do to me, Oh it&rsquo;s what you do to me, \r\n\r\n[Bridge]\r\nG                                      A\r\nA thousand miles seems pretty far, But they&rsquo;ve got planes and trains and cars, \r\nD                                 Bm  \r\nI&rsquo;d walk to you if I had no other way \r\nG                                         A\r\nOur friends would all make fun of us, And we&#39;ll just laugh along because, \r\n   D                                     Bm\r\nWe know that none of them have felt this way, \r\nG                               A            \r\nDelilah I can promise you, That by the time that we get through, \r\n    Bm                                               A\r\nThe world will never ever be the same, And you&rsquo;re to blame. \r\n\r\n[Verse 3]\r\nD                        F#m\r\nHey there Delilah you be good, And don&rsquo;t you miss me, \r\n         D                                         F#m\r\nTwo more years and you&rsquo;ll be done with school, And I&#39;ll be making history, \r\n       Bm  G                A              Bm\r\nLike I do, You&rsquo;ll know it&#39;s all because of you, \r\nG             A            Bm\r\nWe can do whatever we want to, \r\nG           A               Bm                  A\r\nHey there Delilah here&#39;s to you, This one&rsquo;s for you. \r\n\r\n[Final Chorus]\r\nD                      Bm  D                      Bm    A\r\nOh it&rsquo;s what you do to me, Oh it&rsquo;s what you do to me, \r\nD                      Bm  D                      Bm\r\nOh it&rsquo;s what you do to me, Oh it&rsquo;s what you do to me, \r\n               D\r\nWhat you do to me. \r\n   Bm   D    Bm    D    Bm   D    Bm   D D\r\nOhhh</pre>\r\n\r\n<p>By helping UG you make the world better... and earn IQ</p>','hey-there-delilah-chords-by-plain-white-t\'s','publish','page',0,'RTHgzAnuMqY3nDVeUF7FUOAZc8PROMqrMXVn5OMnBUrdx3jXp4y7X5yIxHoDExHHlOatxzz3rbqJvvfvo1fkiomLMdvYjMqDKM0k',1,NULL,'2018-11-07 02:05:48','2018-11-07 02:05:48','http://localhost:8000/hey-there-delilah-chords-by-plain-white-t\'s',0,NULL,NULL,'pages/T6P42nHwQVk0BnbAXy6mFk0FGhj5M47PEYjldkVkrk4Tg.jpeg','\"Hey There Delilah\" is a song by American rock band Plain White T\'s. It was released in May 2006 as the third single from their third studio album All That We Needed. It received radio play over the following year and eventually reached No. 1 on the Billboard Hot 100 in July 2007. The song has received immense popularity since its release and has been featured in television shows such as Journeyman, I Love the New Millennium, Orange Is the New Black, and Family Guy.',NULL,NULL,NULL,NULL,NULL),(5,'Google',NULL,'google','publish','custom_link',0,'w2PLG38BhrRn0deh0lq64EmaT3lhPXlpl5WBofGf6jLeIj1ZToyX6sJAkoz4F3DYqt1KdtvNnlzOY9iuKyI3fPGUQ7M1ChRjF19D',1,NULL,'2018-11-07 02:07:14','2018-11-07 02:07:14','https://google.com',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(6,'Home',NULL,'home','publish','custom_link',0,'u7CstX3KJmBreYE4RrlFSoIQgGN7kwDrPyDQhC1gLfTeWEt8QXv1iTTXyl0Og65WYn2wyvdGTxmjm8YJSGcuMEb1R2VCZFr4KxBM',1,NULL,'2018-11-09 11:42:53','2018-11-09 11:42:53','http://localhost:8000/',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(7,'Check Mail',NULL,'check-mail','publish','custom_link',0,'tmjasdbtvvvcQPHnQi9QsThVGyVWjnFFQ34KDLQme4eEG0GOhiR4nbGgOzyZuICxB9PJW1CGTU4HKineShfuhX36sCJpqhb0McbI',1,NULL,'2018-11-10 01:33:15','2018-11-10 01:33:15','https://mail.ufl.com.np/owa/auth/logon.aspx?replaceCurrent=1&url=https%3a%2f%2fmail.ufl.com.np%2fowa%2f',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(8,'Downloads',NULL,'downloads','publish','custom_link',0,'k22Wv9UrZJeuhwpL1lUDjA9vHGYu5EeGDnKHfV662z5pmsNy7TEXF1RR7f7owRxPabn5N6k7jzO3vt3uyq1AUP57qlnz0VwnCszS',1,NULL,'2018-11-10 01:34:35','2018-11-10 01:34:35','https://google.com',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(9,'Networks',NULL,'networks','publish','custom_link',0,'K1In7G3GUcmxWoRi5KMCIDxYClYvUeTpMw3WtY8d6ICT0L1RIkYxMDwOohEXBMFjQBAphkcGACZXN1eE7Aqg1LGaTT86VDJevOeq',1,NULL,'2018-11-10 01:34:52','2018-11-10 01:34:52','https://google.com',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(10,'Career',NULL,'career','publish','custom_link',0,'i8hF4yXLniWyJNLbhHHTm02QxxJ36UBDMCI1RKuribEgMRpx44oUdUt983mihyluX5G2xiZg9uRevcUcczDqgjOGpv4rjUJBTxDF',1,NULL,'2018-11-10 01:35:08','2018-11-10 01:35:08','https://google.com',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(11,'Contact Us',NULL,'contact-us','publish','custom_link',0,'cs2t3m3IIoEnv1N3AeORQrEoK0cGfZEQ63SIb7cmFzG9PhLdKvGSZ8rwTQrHX0X1ZqPBuNmlXRRxpAUR8tuaanTlTpjUIlxa60lL',1,NULL,'2018-11-10 01:35:20','2018-11-10 01:35:20','https://google.com',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(12,'Google',NULL,'google','publish','custom_link',0,'97590TUYgVGJao9h7jirSoDgGqQeoOWBtqyZ5oqb0aA705LDAk3ojXnnrRLG4Np94DvX1uDxVR8K5tfF0nkDFt4GUcgLbrWSUlEo',1,NULL,'2018-11-10 06:58:05','2018-11-10 06:58:05','https://google.com',0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seo`
--

DROP TABLE IF EXISTS `seo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seo` (
  `seo_id` int(11) NOT NULL AUTO_INCREMENT,
  `page_id` int(11) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` text,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`seo_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seo`
--

LOCK TABLES `seo` WRITE;
/*!40000 ALTER TABLE `seo` DISABLE KEYS */;
INSERT INTO `seo` VALUES (1,4,'Hey There Delilah chords by Plain White T\'s',NULL,'Hey There Delilah chords by Plain White T\'s','2018-11-07 02:05:48','2018-11-07 02:05:48');
/*!40000 ALTER TABLE `seo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `settings`
--

DROP TABLE IF EXISTS `settings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `url` varchar(255) DEFAULT NULL,
  `slug` varchar(200) NOT NULL,
  `value` int(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `text` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `settings`
--

LOCK TABLES `settings` WRITE;
/*!40000 ALTER TABLE `settings` DISABLE KEYS */;
INSERT INTO `settings` VALUES (9,NULL,'image_post_max_size',11264,'2018-10-30 04:16:23','2018-11-04 23:54:20',NULL,NULL),(10,NULL,'file_post_max_size',10240,'2018-10-30 04:16:29','2018-11-04 23:53:31',NULL,NULL),(13,'http://localhost:8000/','site_logo',NULL,'2018-11-10 01:30:26','2018-11-10 01:30:26','logo/KTrpbpaFtlTKJ0Rjk6sQvj2DdJSmts5dTeTy9MZ8vWrqM.png',NULL),(14,NULL,'pre_nav_contact_info',NULL,'2018-11-10 02:54:49','2018-11-10 02:54:49',NULL,'01-4241648, 01-4241645 | IJ plaza, Durbar Marga, Kathmandu, Nepal');
/*!40000 ALTER TABLE `settings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `slider_type`
--

DROP TABLE IF EXISTS `slider_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `slider_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `slug` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `slider_type`
--

LOCK TABLES `slider_type` WRITE;
/*!40000 ALTER TABLE `slider_type` DISABLE KEYS */;
INSERT INTO `slider_type` VALUES (1,'Uncategorized',0,'uncategorized','2018-11-07 02:15:36','2018-11-07 02:15:36');
/*!40000 ALTER TABLE `slider_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sliders`
--

DROP TABLE IF EXISTS `sliders`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sliders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `button_text` varchar(255) DEFAULT NULL,
  `target` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sliders`
--

LOCK TABLES `sliders` WRITE;
/*!40000 ALTER TABLE `sliders` DISABLE KEYS */;
INSERT INTO `sliders` VALUES (1,'sliders/Y09efC7wwWSXCLphjQqDwlo3pgyDNvfwwXyE6zPb.jpeg',1,'What is Lorem Ipsum?','Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.','https://www.lipsum.com/',1,1,'2018-11-07 02:15:36','2018-11-09 12:47:11','Read more','_blank'),(2,'sliders/sL9xdsduhLgHZTKIEPi4Xk7MB9n9MTrh6NdoJ3Ss.jpeg',1,'Why do we use it?','It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using.','https://www.lipsum.com/',1,1,'2018-11-07 02:17:22','2018-11-09 12:55:34','Read more','_self'),(3,'sliders/cPnLJW5zt0NFabqJDeu4lreI5SH1H4jHqwa6npp0.jpeg',1,NULL,NULL,'https://',1,1,'2018-11-09 13:03:59','2018-11-09 13:04:02',NULL,'_self');
/*!40000 ALTER TABLE `sliders` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `is_super` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Bishal','Khatri','9856325412','Kathmandu','bishal.khatri343@gmail.com',NULL,'$2y$10$yEwJnd.EbF7tcTTS/oSnMelgbvH7tMLbUHNTxdabMTlElMUh4m3MW','2018-11-10 00:39:32',NULL,1,'iV1az4jbtvFrur4wtDJYxWqLOF3bwmmVtGsfbQdY1R76VTOsEUgNpPHmsXgO','2018-09-10 23:29:31','2018-11-10 00:39:32'),(4,'Bishal','Khatri','9856325412','Kathmandu','editor11@gmail.com',NULL,'$2y$10$0TmobgwkWPmZUSkZA5uDHu8vL4O97jGC.5/IgXIoiADLH2tVWY/dy','2018-09-11 23:24:14',NULL,0,'8jQFZNP8grhap4BtV8rbXZKsHotetlTqIqrsEnmISluUggBNpgr037e5S79s','2018-09-11 23:24:14','2018-10-06 11:06:16'),(7,'Editor','User','9856325412','Kathmandu','consultancy1@gmail.com',NULL,'$2y$10$TOpU1SQzIoZAh20yu.I5YucnWExnQD5DryxB62oJ.9JMIdW7bKbzu','2018-09-11 23:30:23',NULL,0,'OKmGhQ4lntT3UuB8yIsheiPOcyUAUwuXrkBJz7IUOcKtimzWg0CYOidKKudd','2018-09-11 23:30:23','2018-09-11 23:30:23'),(8,'Editor','employee','9856325412','Kathmandu','overseer1@gmail.com',NULL,'$2y$10$t9K/PKp7VtzhHd0tF2qrQumb7uG2p./quqw7tEH4Enm0zsGq2bn2W','2018-09-12 00:37:04',NULL,0,NULL,'2018-09-11 23:33:51','2018-09-12 00:37:04');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-10 23:06:08
