-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 28, 2019 at 04:22 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.1.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `deltacms`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_permissions`
--

CREATE TABLE `auth_permissions` (
  `id` int(11) NOT NULL,
  `display_name` varchar(255) NOT NULL,
  `code_name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permissions`
--

INSERT INTO `auth_permissions` (`id`, `display_name`, `code_name`) VALUES
(1, 'Can list users', 'user-list'),
(2, 'Can add user', 'user-add'),
(3, 'Can assign permission', 'user-permission-assign'),
(4, 'Can edit user', 'user-edit'),
(5, 'Can suspend/delete user', 'user-delete'),
(6, 'Can add permissions', 'permission-add'),
(7, 'Can delete permissions', 'permission-delete'),
(8, 'Can view setting', 'setting-list'),
(9, 'Can change setting', 'setting-change'),
(10, 'Can list slider', 'slider-list'),
(11, 'Can add slider', 'slider-add'),
(12, 'Can delete slider', 'slider-delete'),
(13, 'Can list pages', 'pages-list'),
(14, 'Can create page', 'pages-add'),
(15, 'Can edit page', 'pages-edit'),
(16, 'Can delete pages', 'pages-delete'),
(17, 'Can list menu', 'menu-list'),
(18, 'Can add menu', 'menu-add'),
(19, 'Can list gallery', 'gallery-list'),
(20, 'Can create gallery', 'gallery-add'),
(21, 'Can delete gallery', 'gallery-delete'),
(22, 'Can edit gallery', 'gallery-edit'),
(23, 'Permission-one', 'Test-permission-code'),
(24, 'Can create category', 'category-add'),
(25, 'Can list category', 'category-list'),
(26, 'Can list jobs', 'jobs-list'),
(27, 'Can add job', 'jobs-add'),
(28, 'Can edit jobs', 'jobs-edit'),
(29, 'Can delete jobs', 'jobs-delete'),
(30, 'Can view job application list', 'jobs-list-application');

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission_user`
--

CREATE TABLE `auth_permission_user` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  `permission` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission_user`
--

INSERT INTO `auth_permission_user` (`id`, `user_id`, `permission_id`, `permission`) VALUES
(1, 1, 2, 'user-list'),
(2, 1, 1, 'user-add'),
(3, 3, 1, 'user-list'),
(4, 3, 2, 'user-add'),
(5, 6, 1, 'user-list'),
(6, 6, 2, 'user-add'),
(53, 4, 1, 'user-list'),
(54, 4, 2, 'user-add'),
(55, 4, 3, 'user-permission-assign'),
(56, 8, 1, 'user-list'),
(57, 8, 2, 'user-add'),
(58, 8, 3, 'user-permission-assign'),
(59, 8, 4, 'user-edit'),
(60, 8, 5, 'user-delete'),
(61, 8, 6, 'permission-add'),
(62, 8, 7, 'permission-delete'),
(63, 8, 8, 'setting-list'),
(64, 8, 9, 'setting-change'),
(65, 8, 10, 'slider-list'),
(66, 8, 11, 'slider-add'),
(67, 8, 12, 'slider-delete'),
(68, 8, 13, 'pages-list'),
(69, 8, 14, 'pages-add'),
(70, 8, 15, 'pages-edit'),
(71, 8, 16, 'pages-delete'),
(72, 8, 17, 'menu-list'),
(73, 8, 18, 'menu-add'),
(74, 8, 19, 'gallery-list'),
(75, 8, 20, 'gallery-add'),
(76, 8, 21, 'gallery-delete'),
(77, 8, 22, 'gallery-edit'),
(148, 9, 1, 'user-list'),
(149, 9, 2, 'user-add'),
(150, 9, 3, 'user-permission-assign'),
(151, 9, 4, 'user-edit'),
(152, 9, 5, 'user-delete'),
(153, 9, 6, 'permission-add'),
(154, 9, 7, 'permission-delete'),
(155, 9, 8, 'setting-list'),
(156, 9, 9, 'setting-change'),
(157, 9, 10, 'slider-list'),
(158, 9, 11, 'slider-add'),
(159, 9, 12, 'slider-delete'),
(160, 9, 13, 'pages-list'),
(161, 9, 14, 'pages-add'),
(162, 9, 15, 'pages-edit'),
(163, 9, 16, 'pages-delete'),
(164, 9, 19, 'gallery-list'),
(165, 9, 20, 'gallery-add'),
(166, 9, 21, 'gallery-delete'),
(167, 9, 22, 'gallery-edit'),
(168, 9, 23, 'Test-permission-code'),
(169, 9, 24, 'category-add'),
(170, 9, 25, 'category-list');

-- --------------------------------------------------------

--
-- Table structure for table `downloads_categories`
--

CREATE TABLE `downloads_categories` (
  `downloads_category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `has_group` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `downloads_categories`
--

INSERT INTO `downloads_categories` (`downloads_category_id`, `title`, `description`, `created_by`, `created_at`, `updated_at`, `slug`, `has_group`) VALUES
(5, 'News and Notices', NULL, 1, '2019-02-07 18:18:02', '2019-02-07 18:18:02', 'downloads-item-news-and-notices', 1);

-- --------------------------------------------------------

--
-- Table structure for table `downloads_items`
--

CREATE TABLE `downloads_items` (
  `download_items_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` text,
  `file` varchar(255) NOT NULL,
  `size` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `is_trashed` int(11) NOT NULL,
  `unique_key` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_slug` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `downloads_items`
--

INSERT INTO `downloads_items` (`download_items_id`, `category_id`, `title`, `body`, `file`, `size`, `url`, `is_trashed`, `unique_key`, `created_by`, `created_at`, `updated_at`, `group_slug`) VALUES
(2, 2, 'I\'m a product', NULL, 'file_downloads/s6Z2MyfYtWgSmV3eHsNjyDwB1WuF16Bm2HfJjqPT.jpeg', '7.22 KiB', 'http://172.16.1.131:85/deltacms/file_downloads/s6Z2MyfYtWgSmV3eHsNjyDwB1WuF16Bm2HfJjqPT.jpeg', 0, 'W0jiRy8Yt715k2BuFM5926QzOMEaTlA3veV0vaAZzRjbXBCjRidcf8PaxDw4ehR96ghQpC0wjWO', 1, '2019-01-07 05:47:40', '2019-01-07 05:47:40', NULL),
(3, 3, 'What is Lorem Ipsum?', NULL, 'file_downloads/BT4IbLWB903nek5OokgJyXql5xTUo7PCVVqKz5Mk.jpeg', '28.64 KiB', 'http://localhost/deltacms/file_downloads/BT4IbLWB903nek5OokgJyXql5xTUo7PCVVqKz5Mk.jpeg', 0, 'QqqNxW2mMnEfwQCc8ZcTjxMDYxfojtlQtmvfIuT7ckOA5ZX0mI39c7wu7vPWuTMjkjAoTGLoFBh', 1, '2019-02-07 16:56:43', '2019-02-07 16:56:43', NULL),
(4, 4, 'What is Lorem Ipsum?', NULL, 'file_downloads/54GUUZZfZL3380CRbSqmgFCNNHdYxzu00fcA9vtQ.jpeg', '28.64 KiB', 'http://localhost/deltacms/file_downloads/54GUUZZfZL3380CRbSqmgFCNNHdYxzu00fcA9vtQ.jpeg', 0, 'U9rKpzov0Aw3KHloVElYgqSGOXwXCZjweIibR4x301nRtggpLOKEppdi8EQdAqW3LGSE2K68djr', 1, '2019-02-07 17:12:29', '2019-02-07 17:12:29', 'FISCAL YEAR 2075/76'),
(5, 4, 'Why do we use it?', NULL, 'file_downloads/SRSbgmuj2wOvcd50vKGTfyMKyj0ySLvEiw6LVOOC.jpeg', '83.49 KiB', 'http://localhost/deltacms/file_downloads/SRSbgmuj2wOvcd50vKGTfyMKyj0ySLvEiw6LVOOC.jpeg', 0, 'SMlmzTUrJg8I92Y7JdiYfVUMIgExsYX7zu4ycy0oZv3nwr3WytcHa4SFkQHJ8Z1WSutNjX37Qgz', 1, '2019-02-07 17:14:54', '2019-02-07 17:14:54', 'FISCAL YEAR 2075/76'),
(6, 4, 'What is Lorem Ipsum?', NULL, 'file_downloads/kVkoSp0WcwGJFKVs0gnqQc51xkRLCuO5kb5jIbgQ.jpeg', '54.25 KiB', 'http://localhost/deltacms/file_downloads/kVkoSp0WcwGJFKVs0gnqQc51xkRLCuO5kb5jIbgQ.jpeg', 0, '5nt5izEjMg5QDUkhtrj5d3QTygsRtDYMRl5NguISBpfQowNnEelbZoh6heXRCoD8Nu3L3pKDial', 1, '2019-02-07 17:25:17', '2019-02-07 17:25:17', 'FISCAL YEAR 2074/75'),
(8, 5, 'News and Notices', NULL, 'file_downloads/Tq7AxDdNmxy6931LrdFe5jueYwkhaXyeHzYLOAJy.jpeg', '73.14 KiB', 'http://localhost/deltacms/file_downloads/Tq7AxDdNmxy6931LrdFe5jueYwkhaXyeHzYLOAJy.jpeg', 0, '3kIGo6mo06XIqxRIOpv898a0d2OnkpGhAVPSHDEJbHqmo9cClMMbejlW5QCpMmF6oZa9i9gxmSg', 1, '2019-02-07 18:18:13', '2019-02-07 18:18:13', 'FISCAL YEAR 2075/76'),
(9, 5, 'News and Notices23', NULL, 'file_downloads/IPpdb74MH7GRYDzqiLy86PKeJhmcdJGUcKfvB0zd.jpeg', '73.14 KiB', 'http://localhost/deltacms/file_downloads/IPpdb74MH7GRYDzqiLy86PKeJhmcdJGUcKfvB0zd.jpeg', 0, 'DkfPr0efNk2PapDnrDeJejXTcec09w2BXaZcYl5dAiXg2gFmip3npVYJIsj1dRZF1iKyZyUT8fm', 1, '2019-02-07 18:18:25', '2019-02-07 18:18:25', 'FISCAL YEAR 2075/76'),
(10, 5, 'Why do we use it?', NULL, 'file_downloads/dxRN1Nkil9VhFJv0jMBXWnoWurN8XPycau3PHsWh.jpeg', '74.96 KiB', 'http://localhost/deltacms/file_downloads/dxRN1Nkil9VhFJv0jMBXWnoWurN8XPycau3PHsWh.jpeg', 0, 'CuJSSwpWoU4WSR6iI6Wx0FMr5E5L4kgjuqMkG6cBiA11Ql3QLC5TtkHVB1fYsxYrBPimTOOflpt', 1, '2019-02-07 18:18:34', '2019-02-07 18:18:34', 'FISCAL YEAR 2074/75'),
(11, 5, 'Aenean ultricies ultrices mauris ac congue edited again', NULL, 'file_downloads/f2AMbGC4Ihx4Zntia54quX4oYuixktE2rYodT38h.jpeg', '3.71 KiB', 'http://localhost/deltacms/file_downloads/f2AMbGC4Ihx4Zntia54quX4oYuixktE2rYodT38h.jpeg', 0, 'J5oCepPqSmEcbzUiHxP9Oos4mYawkEfAfaWEMt8ylMVkS40krzItay7Ci0qlDGojFLISdHRJVHT', 1, '2019-02-07 18:18:44', '2019-02-07 18:18:44', 'FISCAL YEAR 2074/75');

-- --------------------------------------------------------

--
-- Table structure for table `gallery`
--

CREATE TABLE `gallery` (
  `id` int(11) NOT NULL,
  `gallery_name` varchar(100) DEFAULT NULL,
  `description` text,
  `is_trashed` int(11) DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `gallery_images`
--

CREATE TABLE `gallery_images` (
  `id` int(11) NOT NULL,
  `gallery_id` int(11) NOT NULL,
  `image_name` varchar(200) NOT NULL,
  `is_trashed` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `trashed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` int(11) NOT NULL,
  `title` text NOT NULL,
  `description` text,
  `is_published` int(11) NOT NULL,
  `deadline` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `category` varchar(255) DEFAULT NULL,
  `designation` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `jobs`
--

INSERT INTO `jobs` (`id`, `title`, `description`, `is_published`, `deadline`, `category`, `designation`, `created_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(5, 'Aenean ultricies ultrices mauris ac congue edited', '<hr />\r\n<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<h2>Why do we use it?</h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 1, '2018-12-29 08:00:00', NULL, NULL, 1, NULL, '2018-12-22 22:57:51', '2018-12-22 22:57:51'),
(6, 'What is Lorem Ipsum?', '<hr />\r\n<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<h2>Why do we use it?</h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 0, '2018-12-28 08:00:00', NULL, NULL, 1, NULL, '2018-12-22 23:00:32', '2018-12-22 23:00:32'),
(7, 'Why do we use it?', '<h2>What is Lorem Ipsum?</h2>\r\n\r\n<p><strong>Lorem Ipsum</strong>&nbsp;is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry&#39;s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p>\r\n\r\n<h2>Why do we use it?</h2>\r\n\r\n<p>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using &#39;Content here, content here&#39;, making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for &#39;lorem ipsum&#39; will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).</p>', 1, '2019-01-01 08:00:00', NULL, NULL, 1, NULL, '2018-12-22 23:01:27', '2018-12-22 23:01:43');

-- --------------------------------------------------------

--
-- Table structure for table `job_application`
--

CREATE TABLE `job_application` (
  `id` int(11) NOT NULL,
  `site_user_id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `cover_letter` text CHARACTER SET utf8,
  `cv` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `job_application`
--

INSERT INTO `job_application` (`id`, `site_user_id`, `job_id`, `cover_letter`, `cv`, `created_at`, `updated_at`) VALUES
(7, 5, 7, NULL, NULL, '2019-01-29 16:34:33', '2018-12-22 23:03:24'),
(8, 6, 7, NULL, NULL, '2018-12-22 23:03:33', '2018-12-22 23:03:33');

-- --------------------------------------------------------

--
-- Table structure for table `medias`
--

CREATE TABLE `medias` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `size` varchar(50) DEFAULT NULL,
  `url` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `is_trashed` int(11) NOT NULL DEFAULT '0',
  `gallery_id` int(11) DEFAULT NULL,
  `file_type` varchar(45) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `trashed_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menus`
--

CREATE TABLE `menus` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `menu_type` varchar(50) NOT NULL,
  `is_selected` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menus`
--

INSERT INTO `menus` (`id`, `title`, `menu_type`, `is_selected`, `created_at`, `updated_at`, `is_active`) VALUES
(1, 'Main menu', 'nav_menu', 1, '2018-11-25 09:42:31', '2019-01-30 14:53:16', 1),
(2, 'Secondary menu', 'footer', 0, '2018-11-25 22:03:24', '2018-12-01 17:17:30', 0),
(3, 'prenav', 'prenav_menu', 0, '2018-11-26 05:37:53', '2018-12-21 10:20:44', 1),
(4, 'Footer menu', 'footer_menu', 0, '2018-11-26 08:25:13', '2018-12-02 09:14:46', 1);

-- --------------------------------------------------------

--
-- Table structure for table `menu_post`
--

CREATE TABLE `menu_post` (
  `menu_post_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `order` int(11) DEFAULT NULL,
  `parent` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `menu_display_name` varchar(255) DEFAULT NULL,
  `menu_icon` varchar(255) DEFAULT NULL,
  `menu_description` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_post`
--

INSERT INTO `menu_post` (`menu_post_id`, `post_id`, `menu_id`, `order`, `parent`, `created_at`, `updated_at`, `menu_display_name`, `menu_icon`, `menu_description`) VALUES
(35, 62, 1, 1, 0, '2019-02-07 18:19:30', '2019-03-24 08:41:50', NULL, NULL, NULL),
(36, 40, 1, 2, 0, '2019-03-24 08:41:41', '2019-03-24 08:42:27', 'News', NULL, NULL),
(37, 41, 1, 1, 36, '2019-03-24 08:41:41', '2019-03-24 08:41:50', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(3, '2014_10_12_000000_create_users_table', 1),
(4, '2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `post_title` varchar(255) CHARACTER SET utf8 NOT NULL,
  `post_content` longtext CHARACTER SET utf8,
  `post_slug` text CHARACTER SET utf8 NOT NULL,
  `post_status` varchar(20) NOT NULL,
  `post_type` varchar(20) NOT NULL,
  `post_parent` int(11) NOT NULL DEFAULT '0',
  `unique_key` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `post_url` varchar(200) CHARACTER SET utf8 NOT NULL,
  `is_trashed` int(11) DEFAULT '0',
  `gallery_id` int(11) DEFAULT NULL,
  `tags` text,
  `cover_image` varchar(200) DEFAULT NULL,
  `post_subtitle` text CHARACTER SET utf8,
  `cover_icon` varchar(200) DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `button_type` varchar(100) DEFAULT NULL,
  `button_text` varchar(45) DEFAULT NULL,
  `url_target` varchar(45) DEFAULT NULL,
  `downloads_id` int(11) DEFAULT NULL,
  `facebook_share` int(11) DEFAULT '0',
  `twitter_share` int(11) DEFAULT '0',
  `is_featured` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `post_title`, `post_content`, `post_slug`, `post_status`, `post_type`, `post_parent`, `unique_key`, `created_by`, `updated_by`, `created_at`, `updated_at`, `post_url`, `is_trashed`, `gallery_id`, `tags`, `cover_image`, `post_subtitle`, `cover_icon`, `category_id`, `button_type`, `button_text`, `url_target`, `downloads_id`, `facebook_share`, `twitter_share`, `is_featured`) VALUES
(2, 'Result of Written Test of Management Trainee', '<p>sadfasdf</p>', 'fdsafasdf', 'publish', 'page', 0, 'ymDofQMSaaMbQRmQqX3aoLckvtq5j7ocpoWUbsPAZvdweDNcWhzURVTcn85o5KyTaWu7zDFBhd4B5Meb6pJwR8AY99kqCC12W2af', 1, 1, '2018-11-23 06:45:36', '2019-01-13 09:27:55', 'http://172.16.1.131:85/deltacms/fdsafasdf', 0, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0),
(3, 'Safe Deposit Locker', '<p>Security Deposit and annual locker rental.</p>\r\n\r\n<table border=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Size</td>\r\n			<td>Security Deposit (NPR)</td>\r\n			<td>Rental (NPR)</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Small</td>\r\n			<td>Rs.5000.00</td>\r\n			<td>Rs.1000.00</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 'safe-deposit-locker', 'publish', 'page', 0, 'AipMweisKfYpZsqqiaJtuF2aXJxQj729BUkvP3d3GwwXsXOfkupXAucLrg6oxSaYVOCnuvZAoqI2yPjBZtAwnua26LpqtmIUkSLx', 1, 1, '2018-11-25 08:43:27', '2019-01-24 16:53:40', 'http://172.16.1.131:85/deltacms/safe-deposit-locker', 0, NULL, NULL, '', 'UFL offers safe deposit locker to its valued customers to provide securities to their valuable goods. The locker facility is available from its Head Office, Durbar Marga.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(4, 'Mobile Banking', '<p><strong>Welcome to United Mobile Banking Service</strong><br />\r\nUnited Mobile Banking is a smart, easy and cost efficient alternative module for executing banking transactions through your bank accounts. It allows you to do the banking transaction 24/7, directly from your smart mobile phone.<br />\r\n<br />\r\n<strong><u>Features of United Mobile Banking:</u></strong><br />\r\nYou can perform following service via United Mobile Banking:<br />\r\n<br />\r\n<strong>Enquiry &amp; Requests</strong></p>\r\n\r\n<ul>\r\n	<li>Balance Enquiry</li>\r\n	<li>Mini Statements</li>\r\n	<li>Available Limit</li>\r\n	<li>Account Information</li>\r\n	<li>Cheque Book Request</li>\r\n	<li>Statement Request</li>\r\n	<li>Banking Hours</li>\r\n</ul>\r\n\r\n<p><br />\r\n<br />\r\n<strong>Alerts &amp; Notifications</strong></p>\r\n\r\n<ul>\r\n	<li>Transaction Alerts</li>\r\n	<li>Loan Repayment Alerts</li>\r\n	<li>General Notifications</li>\r\n</ul>\r\n\r\n<p><br />\r\n<br />\r\n<strong>Fund Transfers</strong></p>\r\n\r\n<ul>\r\n	<li>Other Account in same bank</li>\r\n	<li>Account in other bank</li>\r\n	<li>To Registered Mobile</li>\r\n</ul>\r\n\r\n<p><br />\r\n<br />\r\n<strong>Payments</strong></p>\r\n\r\n<ul>\r\n	<li>Recharge Cards(NT GSM/CDMA, Broadlink, Dish Home)</li>\r\n	<li>Top-ups(NT Prepaid/Postpaid/Landline/ADSL), Ncell</li>\r\n	<li>Credit Card Bill Payments(Global IME, Nabil, NMB, Laxmi Bank)</li>\r\n	<li>Merchant Payments (fonepay)</li>\r\n	<li>e-Sewa</li>\r\n</ul>\r\n\r\n<p><br />\r\n<br />\r\n<strong>United Mobile Banking Charges</strong><br />\r\nRegistration: NPR. 200 per annum<br />\r\nRenewal: NPR. 200 per annum<br />\r\nPin-Regenerate: NPR. 100<br />\r\n<br />\r\n<strong>Frequently Asked Questions</strong><br />\r\n<strong>1. What is United Mobile Banking Service? What services are available?</strong><br />\r\nUnited Mobile Banking is a smart, easy and cost efficient alternative for doing financial transactions through your bank accounts. It allows you to do the banking transaction 24/7, directly from your smart mobile phone.<br />\r\nYou can perform following service via United Mobile Banking:<br />\r\nBalance Enquiry<br />\r\nMini Statement<br />\r\nCheque Book Request<br />\r\nBanking Hour<br />\r\nStatement Request<br />\r\nFund Transfer<br />\r\nMobile Top-up<br />\r\nRecharge Cards<br />\r\nExchange Rate<br />\r\nLoad e-sewa<br />\r\n<br />\r\n<strong>2. Who is eligible for this service?</strong><br />\r\nIf you are a United Finance Limited customer with at least one account, you are eligible for this service. Both Saving and Current Account can be used for the service.<br />\r\n<br />\r\n<strong>3. What are the charges associated with mobile banking?</strong><br />\r\nThe charges associated with Mobile Banking are as mentioned below: Registration: NPR. 200 per annum Renewal: NPR. 200 per annum Pin-Regenerate: NPR. 100<br />\r\n<br />\r\n<strong>4. How can I know I have been registered in United mobile banking service?</strong><br />\r\nAs soon as your account is registered with your mobile number in United Mobile Banking Service, you will get SMS with a 4 digit MPIN.<br />\r\n<br />\r\n<strong>5. What is MPIN?</strong><br />\r\nM- PIN is a 4 digit unique Personal Identification Number which you need to dial each time while performing mobile transactions.<br />\r\n<br />\r\n<strong>6. How many times can I change my PIN? Should I change the PIN? If I forget Pin, What do I do?</strong><br />\r\nNo limitation to change pin. System generated pin is difficult to remember and it is better and safe for you to change the MPIN. Please visit nearest branches and fill up the pin re-generate form to receive new pin.<br />\r\n<br />\r\n<strong>7. Can I transfer fund from my account to another customer&rsquo;s account?</strong><br />\r\nYes, you can directly send money to another customer&rsquo;s account.<br />\r\n<br />\r\n<strong>8. What happens if I lose my mobile phone or Stolen? Can someone take money out of my account via United mobile banking if mobile lost/stolen?</strong><br />\r\nPlease immediately contact to Customer Service Department for assistance or Please call us immediately at our number 01-4241648 Ext. 110/116.<br />\r\n<br />\r\nIf the person who found your mobile doesn&rsquo;t know your PIN, they would not be able to take out your money. But if they know your PIN they can misuse it.<br />\r\n<br />\r\n<strong>9. Which mobile network carrier does it work with?</strong><br />\r\nIt works with NTC and Ncell network only.<br />\r\n<br />\r\n<strong>10. Can I conduct transaction through this service if I am outside of the country?</strong><br />\r\nNo, this service is limited within network coverage area of Nepal.<br />\r\n<br />\r\n<strong>11. Do I have to maintain Balance in my account?</strong><br />\r\nYes, to perform any transaction, you need to have sufficient balance in your account.<br />\r\n<br />\r\n<strong>12. Can I operate two or more accounts into same mobile banking registration?</strong><br />\r\nYes! You can operate maximum five accounts from single mobile banking registration.<br />\r\n<br />\r\n<strong>13. Can a single mobile number be used for registration of two different customers?</strong><br />\r\nNo! two different registrations of two separate customers cannot be done through single mobile number.<br />\r\n<br />\r\n<strong>14. I am a Power of Attorney (POA) holder of an account maintained at United Finance. Can I apply for UnitedMobile-Banking to be operated through my own mobile number?</strong><br />\r\nNo! POA holders are not allowed to apply for United Mobile Banking. Account holder&rsquo;s presence is mandatory to apply for United Mobile Banking Service.<br />\r\n<br />\r\n<strong>15. Is United Mobile Banking available for Joint accounts?</strong><br />\r\nYes! But Joint account holders can get only inquiry of account balance, mini statement (last five transactions of your linked account), SMS alert (transaction alerts each time transaction is done in your account) service.<br />\r\n<br />\r\n<strong>16. Can a guardian of a minor apply for United Mobile Banking on any minor account?</strong><br />\r\nYes! A guardian can apply for mobile banking for minor account. However, the request cannot be merged with registration of his/her personal account.<br />\r\n<br />\r\n<strong>17. I have maintained a corporate account at United Finance. What are the mobile banking facilities available for corporate account?</strong><br />\r\nFor corporate accounts, customer can get facilities such as- inquiry of account balance, mini statement (last five transactions of registered account) and transaction alerts each time transaction is done in that particular account.<br />\r\n<br />\r\n<strong>18. How can I get app for my mobile phone for using United Mobile banking service?</strong><br />\r\nAndroid users can simply download the app from play store and IOS users can download it from Appstore. <strong>19. Is there any limitation for Fund transfer?</strong><br />\r\nYes! As per bank&rsquo;s rule there are certain limits on a/c to a/c fund transfer which is as mentioned below:<br />\r\n1. Fund Transfer per transaction maximum limit &ndash; 10,000.00<br />\r\n2. Fund Transfer daily limit- 50,000.00<br />\r\n3. Utility Payment (per transaction) -10,000.00<br />\r\n4. Utility Payment daily limit -50,000.00<br />\r\n5. Total number of transaction (daily count) -10(all combined)<br />\r\n<br />\r\n<strong>20. What are the transaction charges for Inter Bank fund transfers?<br />\r\nThe transaction charges for Inter Bank Fund transfer are as mentioned below:Rs. 100 to Rs 1,000 = Rs 10<br />\r\nRs. 1,001 to Rs 10,000 = Rs 20<br />\r\nRs 10,001 to Rs 20,000 = Rs 30<br />\r\nRs 20,001 to Rs 30,000 = Rs 40<br />\r\nRs 30,001 to Rs 40,000 = Rs 50<br />\r\nRs 40,001 to Rs 50,000 = Rs 60<br />\r\n<br />\r\n(NOTE: for Inter Bank Fund Transfer maximum per transaction limit is NPR. 10,000)<br />\r\n<br />\r\n<strong>Security Measures for M-Banking</strong></strong></p>\r\n\r\n<ul>\r\n	<li><strong>Please treat your mobile is as cash and do not allow others to use.</strong></li>\r\n	<li><strong>Change your password from time to time</strong></li>\r\n	<li><strong>Always logout when your task is completed</strong></li>\r\n	<li><strong>Monitor your account regularly</strong></li>\r\n	<li><strong>Protect your device with password so that if you have lost or left a device unattended, access shall be safe</strong></li>\r\n	<li><strong>Be suspicious of free download offer in your Device</strong></li>\r\n	<li><strong>As far as possible your device should have wipe function which helps to delete data remotely if the device is lost or stolen.</strong></li>\r\n	<li><strong>Your device should have anti-virous software for protecting device from malware attack.</strong></li>\r\n	<li><strong>Do not use public Wi-Fi at least for banking transaction as they may not be secured.</strong></li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>', 'mobile-banking', 'publish', 'page', 0, 'z8qtLschNrM2lhPc1lOO0GUgmbschXjAPIqrH6JWx5m6I9N8sROg6WpbrQYPlJpP1RjHTBq5UIwsm7vWHH1W4uud1JMrIAZQdPqP', 1, NULL, '2018-11-25 08:45:30', '2019-01-13 09:27:51', 'http://172.16.1.131:85/deltacms/mobile-banking', 0, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0),
(5, 'Anubhabi Jeeban Bachat Khata', '<p>This scheme is designed for senior citizens of the nation aged 45 years and above to salute them for their contribution to the nation and the society with an attractive interest rate and other benefits:<br />\r\n<br />\r\n<strong>Features:</strong></p>\r\n\r\n<ol>\r\n	<li>Minimum balance : Rs 100.00</li>\r\n	<li>Free Any Branch Banking Service</li>\r\n	<li>SMS facility</li>\r\n	<li>Locker Facility</li>\r\n	<li>Free Issuance of Cheque Book</li>\r\n	<li>Unlimited deposit and withdrawal</li>\r\n</ol>', 'anubhabi-jeeban-bachat-khata', 'draft', 'page', 0, 'y9nobGRXwPFLMn71ndlJ896Mnnli3NZnEazyZy9a2LpLIvoZycJ70z7w6lZOB6jHshHAB2mDITHFKqBW7PuSEStubBQ10JyDmzlX', 1, 1, '2018-11-25 09:10:43', '2019-01-24 15:28:48', 'http://172.16.1.131:85/deltacms/anubhabi-jeeban-bachat-khata', 0, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(6, 'Avasar Bachat Khata', '<p>This Scheme is designed to facilitate the individuals for easy and regular deposit of funds as and when required and simultaneously earning of income on the balance kept in the account with an attractive interest rate:<br />\r\n<br />\r\n<strong>Features:</strong></p>\r\n\r\n<ul>\r\n	<li>Minimum balance : Rs 100.00</li>\r\n	<li>Free Any Branch Banking Service</li>\r\n	<li>SMS facility</li>\r\n	<li>Locker Facility</li>\r\n	<li>Free Issuance of Cheque Book</li>\r\n	<li>Unlimited deposit and withdrawal</li>\r\n</ul>', 'avasar-bachat-khata', 'publish', 'page', 0, 'GJfapYOyeIslJXt97SMoKCRPNNQCnJm2gSBZ4oQXvEH0lyivTXghwhcZKOxsfcRCxBBmniQxDVp4HUfuyVEIzWjSzd6fpfH6OoWU', 1, NULL, '2018-11-25 09:13:01', '2019-01-13 09:27:41', 'http://172.16.1.131:85/deltacms/avasar-bachat-khata', 0, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0),
(7, 'Komal Jeevan Bachat Khata', '<p>This scheme is especially designed for minor who belonging to the age group of 16 years and below. It is a recurring type of deposit scheme which cultivate the saving habit from childhood:<br />\r\n<br />\r\n<strong>Features:</strong></p>\r\n\r\n<ul>\r\n	<li>Minimum amount : Rs 500.00 per month</li>\r\n	<li>SMS facility</li>\r\n	<li>Locker Facility</li>\r\n	<li>Tenure of account minimum of 1 year and maximum of 16 years</li>\r\n	<li>90% of the deposit can be withdrawn as advance with better interest rate</li>\r\n</ul>', 'komal-jeevan-bachat-khata', 'publish', 'page', 0, '572Y7tpU0xrVXXmYggfKQgagfIC0mSJDYReX5BRo0zpxEekLEKAaFLITZ26iHZTRwQPkZaZdsFB8eDOPqq5lXi5HSe16AS77AuE6', 1, 1, '2018-11-25 09:13:19', '2019-01-24 15:59:57', 'http://172.16.1.131:85/deltacms/komal-jeevan-bachat-khata', 0, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0),
(8, 'Introduction', '<p>United Finance Limited is promoted by prominent and dynamic businessmen of Nepal, commenced its operation in 2051 as National Level Finance Company with its registered office at Durbarmarga, Kathmandu.<img alt=\"\" src=\"http://172.16.1.131:85/deltacms/public/storage/media/image/1/img-about.png\" style=\"float:right; height:263px; width:434px\" /><br />\r\n<br />\r\nThe Company has emerged as one of the leading Finance Companies by providing the best financial solution to the different customer segments and thriving healthy growth with consistency in profit. The Company is dedicated to maintain the highest level of ethical standards, professional integrity, corporate governance and regulatory compliance. As a result, United Finance is perceived as a strong financial institution and player in the Nepalese banking industry. United Finance is committed to meet the customers&#39; expectations by offering wide range of banking products and financial services in all area of its business along with continuous improvement in its service.<br />\r\n<br />\r\n<strong>Vision</strong><br />\r\nTo be a trusted and one of the leading Finance Company<br />\r\n<br />\r\n<strong>Mission</strong><br />\r\nThe Company&#39;s mission is to provide one stop solution to all the banking need of the customers through highly motivated, professional and efficient human resource team and be continuously innovative and keep up with the changes to satisfy the investors, staff, customers, regulators and the people of the community we serve.<br />\r\n<br />\r\n<strong>Core Values</strong></p>\r\n\r\n<ul>\r\n	<li>Teamwork</li>\r\n	<li>Efficiency</li>\r\n	<li>Customer Care</li>\r\n	<li>Compliance</li>\r\n	<li>Trustworthy (TECCT)</li>\r\n</ul>\r\n\r\n<p><strong>Capital Structure</strong></p>\r\n\r\n<table border=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Authorized Capital</td>\r\n			<td>Rs. 100 crores</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Issued Capital</td>\r\n			<td>Rs. 80.72 crores</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Paid up Capital</td>\r\n			<td>Rs. 80.05 crores</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p><br />\r\n<br />\r\n<strong>Shareholding Pattern</strong></p>\r\n\r\n<table border=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Promoters</td>\r\n			<td>51.00%</td>\r\n		</tr>\r\n		<tr>\r\n			<td>General Public</td>\r\n			<td>49.00%</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n\r\n<p><br />\r\n<br />\r\n<strong>Corporate Information</strong></p>\r\n\r\n<table border=\"0\" cellspacing=\"0\" style=\"width:100%\">\r\n	<tbody>\r\n		<tr>\r\n			<td>Full Name of the Company</td>\r\n			<td>United Finance Limited</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Legal form</td>\r\n			<td>A Public Limited Company incorporated at the office of Company Registrar, Kathmandu, Nepal on 2049-11-11 B.S. under the license no 06-049/50 and classified as &quot;C&quot; class licensed institution on 2051-06-09 B.S.under license no 8/051from Nepal Rastra Bank</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Commencement of business</td>\r\n			<td>2051 B.S.</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Permanent Account Number</td>\r\n			<td>500058602</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Registered Office</td>\r\n			<td>Kathmandu Metro-Politancity Ward No. 1, Kathmandu</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Internal Auditor</td>\r\n			<td>Suresh Shrestha &amp; Associates, Naxal, Kathmandu</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Company Secretary</td>\r\n			<td>Mr. Shesh Kumar Dhungana</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Statutory Auditor</td>\r\n			<td>B.K Agrawal &amp; Company, Sifal, Kathmandu</td>\r\n		</tr>\r\n		<tr>\r\n			<td>Legal Advisor</td>\r\n			<td>Mr. Ramkrishna Nirala, Manu Law Firm, Anamnagar, Kathmandu</td>\r\n		</tr>\r\n	</tbody>\r\n</table>', 'introduction', 'publish', 'page', 0, 'xRPaV32mEpr8U6j8wNrCjvNyGxruzEP6fkmvP56N0gBU9KbjJVnWn8RP7VFhtL6MgDBjNSYzCCHm61t6ZFKlB6aySVLiETllMOKt', 1, 1, '2018-11-25 09:38:35', '2019-01-13 10:36:33', 'http://172.16.1.131:85/deltacms/introduction', 0, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 0),
(9, 'Home', NULL, 'home', 'publish', 'custom_link', 0, 'W0ICzDzGb5Sd3n7A3N6RDIl7YGjE0HhpFIQF6lfklSVxoNE9kLibGDMXDS2bLEpdS4sQmYtl157IRYYHVS6FuR3TpVg3UU4ehqCy', 1, NULL, '2018-11-25 09:43:05', '2018-11-25 09:43:05', '/weblink', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(10, 'About Us', NULL, 'about-us', 'publish', 'custom_link', 0, '3pND3M5hcn9Gl5y365v0HHay9xZmkh46oYeWNFfGYb1qRPsLj0tfV0exylzJ6E1ejAgAbNuJ4NqADhA3m90S8RbUKj6OYQg3pFtT', 1, NULL, '2018-11-25 09:43:26', '2019-01-13 08:58:13', '#', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1),
(11, 'Products', NULL, 'products', 'publish', 'custom_link', 0, 'i6kFhN61i5yDEniJiZD7IALvdrBIabICLf7YgJTkcP5Yp8qjdBPyC5UuesuvVKGuZ3R0lY6N9TysZG8ifS5to4fJqz0C1hYUVHa3', 1, NULL, '2018-11-25 09:43:43', '2018-11-25 09:43:43', '#', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(12, 'Branch Networks', NULL, 'branch-networks', 'publish', 'custom_link', 0, 'Bde7ECPrDaCyIeGuF07lYJ52P5Y42vsMAEvzYINFrt1RyBvqNXvRsvmBf61tOCnYMggSNShpoIMEH7T4LbvifRWA2fyCV86tzINJ', 1, NULL, '2018-11-25 09:44:12', '2018-11-25 09:44:12', '#', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(13, 'E-Banking', NULL, 'e-banking', 'publish', 'custom_link', 0, 'tDnG5FWKtSqXuYtRb5G0zw5RTumSyqtcVsoeQJ9DAFpLDf1Q1qB6erVVSLY5hxIcjQJcOG43Wmo8XN9Yh7HW4W44eowYPrAr3yYA', 1, NULL, '2018-11-25 09:44:33', '2018-11-25 09:44:33', '#', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(14, 'Rate', NULL, 'rate', 'publish', 'custom_link', 0, 'CuuagAmY7VyGzESzRJSqzAx1SkwyCoWJUQ6oOUVSPIXZMAXs4StPVLfUwRw0qRahrbn1Bxl6bktvbGBPwCBLbZPIL9kQmO6s0ZRf', 1, NULL, '2018-11-25 09:44:49', '2018-11-25 09:44:49', '#', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(15, 'Reports', NULL, 'reports', 'publish', 'custom_link', 0, 'pp8AR6tIvyRYZSYZhSIxlHpQDe5x0t2DfStpX5fsPqgqDBeyg9w14l7HQoRsU29gaR4Ob4EaICd75tiyOSrIjYEhc3y7o2p5aI2f', 1, NULL, '2018-11-25 09:45:18', '2018-11-25 09:45:18', '#', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(16, 'Loan & Advances', NULL, 'loan-&-advances', 'publish', 'custom_link', 0, '2UULU5v8jpATjhGxu2f9p2wSbbOoRMm4J555xvkcW15jFUI4jdo4RS9kY3SHdmuqQfNry7mAtQVwQFzDBam0mvPIMCci4hPjaENp', 1, NULL, '2018-11-25 09:47:07', '2018-11-25 09:47:07', '#', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(17, 'Google', NULL, 'google', 'publish', 'custom_link', 0, '5hp9zKLszYZoBu2IS8T2DEz6hGyqH1KdhvoNY4HzIcqYFZUaL3HwEsZMFzRQcGZOfKNHEPuhYCXu3ki1dn07tv3haFf8JbugEBKY', 1, NULL, '2018-11-26 05:38:50', '2018-11-26 05:38:50', 'https://www.google.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(18, 'Home', NULL, 'home', 'publish', 'custom_link', 0, 'T185YnvBC2GADAUohmpbFjRLq1mq2JPFpk4W3iHUTXI4FVfhsECvmgxC26WqBq0FA3UCkf7itRwe7ZH4MyPqC29trDW5ZYiTKGEI', 1, NULL, '2018-11-26 08:59:40', '2018-11-26 08:59:40', '/deltacms', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(19, 'जग्गा बिक्री गर्ने बारेको सिलबन्दी बोलपत्र आह्वानको सूचना', '<p><span style=\"font-size:14px\">यस युनाईटेड फाईनान्स लि. को आफ्नो स्वामित्वमा रहेको का.म.न.पा. वडा नं. १, नक्साल स्थित तपसिलमा उल्लेखित जग्गा &ldquo;जहाँ जे जस्तो अवस्थामा छ&rdquo; सोहि अवस्थामा गोप्य सिलबन्दी बोलपत्रद्वारा सार्वजनिक रूपमा संगठीत संस्था&divide;फर्म&divide;कम्पनी वा व्यक्तिलाई बिक्री गरिने हुँदा सो जग्गा खरिद गर्न इच्छुक संगठित संस्था&divide;फर्म वा व्यक्तिले के कति मुल्य तिरी लिने हो मुल्य खोलि यो सूचना प्रथम पटक प्रकाशित भएको मितिले १५ दिन भित्र अर्थात मिति २०७३/०७/२५ गते कार्यालय समय भित्र र सो दिन बिदा परेमा सो को भोलिपल्ट सोही समयमा यस कम्पनीको प्रधान कार्यालय, दरबारमार्गबाट उपलब्ध गराईएको बोलपत्र फारम भरी प्रधान कार्यालय, प्रशासन विभागमा वोलपत्र पेश गर्नुहुन आह्वान गरिएको छ । </span></p>', 'जग्गा-बिक्री-गर्ने-बारेको-सिलबन्दी-बोलपत्र-आह्वानको-सूचना', 'publish', 'category_item', 0, 'cYLlQuZmU2mbXmXvJJUAba7JbaJI4ibK9xOxHC5wAsKZQwoQ4w7QVUEGnQiw1idnyjhnuIZUOtoCjwmo4bIAI1EBx7BeGs87vLhk', 1, NULL, '2018-11-30 09:47:04', '2018-11-30 09:47:04', 'http://172.16.1.131:85/deltacms/जग्गा-बिक्री-गर्ने-बारेको-सिलबन्दी-बोलपत्र-आह्वानको-सूचना', 0, NULL, NULL, '', 'जग्गा बिक्री गर्ने बारेको सिलबन्दी बोलपत्र आह्वानको सूचना (प्रथम पटक प्रकाशित मितिः २०७३/०७/११)', '', 1, '  ', NULL, NULL, NULL, NULL, 0, NULL),
(20, 'Capital Plan Updated as on 27th Shrawan 2073', '<p><span style=\"font-size:14px\">यस युनाईटेड फाईनान्स लि. को आफ्नो स्वामित्वमा रहेको का.म.न.पा. वडा नं. १, नक्साल स्थित तपसिलमा उल्लेखित जग्गा &ldquo;जहाँ जे जस्तो अवस्थामा छ&rdquo; सोहि अवस्थामा गोप्य सिलबन्दी बोलपत्रद्वारा सार्वजनिक रूपमा संगठीत संस्था&divide;फर्म&divide;कम्पनी वा व्यक्तिलाई बिक्री गरिने हुँदा सो जग्गा खरिद गर्न इच्छुक संगठित संस्था&divide;फर्म वा व्यक्तिले के कति मुल्य तिरी लिने हो मुल्य खोलि यो सूचना प्रथम पटक प्रकाशित भएको मितिले १५ दिन भित्र अर्थात मिति २०७३/०७/२५ गते कार्यालय समय भित्र र सो दिन बिदा परेमा सो को भोलिपल्ट सोही समयमा यस कम्पनीको प्रधान कार्यालय, दरबारमार्गबाट उपलब्ध गराईएको बोलपत्र फारम भरी प्रधान कार्यालय, प्रशासन विभागमा वोलपत्र पेश गर्नुहुन आह्वान गरिएको छ । </span></p>', 'capital-plan-updated-as-on-27th-shrawan-2073', 'publish', 'category_item', 0, 'qCU8Q7x5AQpjGJIMaV4VwGcTB9SBAE93xzI8Ef9MEDLT1MaCsoaFtWxwPO0yoyAOMD6hlb2zze1CbQJzjEKn9wrB4JygOpqio0Ko', 1, NULL, '2018-11-30 09:48:20', '2018-11-30 10:00:04', 'http://172.16.1.131:85/deltacms/capital-plan-updated-as-on-27th-shrawan-2073', 0, NULL, NULL, '', NULL, '', 1, '  ', NULL, NULL, NULL, NULL, 0, NULL),
(21, 'Notice For Share Auction Through Close Bid', '<p>यस युनाईटेड फाईनान्स लि. को आफ्नो स्वामित्वमा रहेको का.म.न.पा. वडा नं. १, नक्साल स्थित तपसिलमा उल्लेखित जग्गा &ldquo;जहाँ जे जस्तो अवस्थामा छ&rdquo; सोहि अवस्थामा गोप्य सिलबन्दी बोलपत्रद्वारा सार्वजनिक रूपमा संगठीत संस्था&divide;फर्म&divide;कम्पनी वा व्यक्तिलाई बिक्री गरिने हुँदा सो जग्गा खरिद गर्न इच्छुक संगठित संस्था&divide;फर्म वा व्यक्तिले के कति मुल्य तिरी लिने हो मुल्य खोलि यो सूचना प्रथम पटक प्रकाशित भएको मितिले १५ दिन भित्र अर्थात मिति २०७३/०७/२५ गते कार्यालय समय भित्र र सो दिन बिदा परेमा सो को भोलिपल्ट सोही समयमा यस कम्पनीको प्रधान कार्यालय, दरबारमार्गबाट उपलब्ध गराईएको बोलपत्र फारम भरी प्रधान कार्यालय, प्रशासन विभागमा वोलपत्र पेश गर्नुहुन आह्वान गरिएको छ ।</p>\r\n\r\n<p>यस युनाईटेड फाईनान्स लि. को आफ्नो स्वामित्वमा रहेको का.म.न.पा. वडा नं. १, नक्साल स्थित तपसिलमा उल्लेखित जग्गा &ldquo;जहाँ जे जस्तो अवस्थामा छ&rdquo; सोहि अवस्थामा गोप्य सिलबन्दी बोलपत्रद्वारा सार्वजनिक रूपमा संगठीत संस्था&divide;फर्म&divide;कम्पनी वा व्यक्तिलाई बिक्री गरिने हुँदा सो जग्गा खरिद गर्न इच्छुक संगठित संस्था&divide;फर्म वा व्यक्तिले के कति मुल्य तिरी लिने हो मुल्य खोलि यो सूचना प्रथम पटक प्रकाशित भएको मितिले १५ दिन भित्र अर्थात मिति २०७३/०७/२५ गते कार्यालय समय भित्र र सो दिन बिदा परेमा सो को भोलिपल्ट सोही समयमा यस कम्पनीको प्रधान कार्यालय, दरबारमार्गबाट उपलब्ध गराईएको बोलपत्र फारम भरी प्रधान कार्यालय, प्रशासन विभागमा वोलपत्र पेश गर्नुहुन आह्वान गरिएको छ ।</p>\r\n\r\n<p>यस युनाईटेड फाईनान्स लि. को आफ्नो स्वामित्वमा रहेको का.म.न.पा. वडा नं. १, नक्साल स्थित तपसिलमा उल्लेखित जग्गा &ldquo;जहाँ जे जस्तो अवस्थामा छ&rdquo; सोहि अवस्थामा गोप्य सिलबन्दी बोलपत्रद्वारा सार्वजनिक रूपमा संगठीत संस्था&divide;फर्म&divide;कम्पनी वा व्यक्तिलाई बिक्री गरिने हुँदा सो जग्गा खरिद गर्न इच्छुक संगठित संस्था&divide;फर्म वा व्यक्तिले के कति मुल्य तिरी लिने हो मुल्य खोलि यो सूचना प्रथम पटक प्रकाशित भएको मितिले १५ दिन भित्र अर्थात मिति २०७३/०७/२५ गते कार्यालय समय भित्र र सो दिन बिदा परेमा सो को भोलिपल्ट सोही समयमा यस कम्पनीको प्रधान कार्यालय, दरबारमार्गबाट उपलब्ध गराईएको बोलपत्र फारम भरी प्रधान कार्यालय, प्रशासन विभागमा वोलपत्र पेश गर्नुहुन आह्वान गरिएको छ ।</p>\r\n\r\n<p>यस युनाईटेड फाईनान्स लि. को आफ्नो स्वामित्वमा रहेको का.म.न.पा. वडा नं. १, नक्साल स्थित तपसिलमा उल्लेखित जग्गा &ldquo;जहाँ जे जस्तो अवस्थामा छ&rdquo; सोहि अवस्थामा गोप्य सिलबन्दी बोलपत्रद्वारा सार्वजनिक रूपमा संगठीत संस्था&divide;फर्म&divide;कम्पनी वा व्यक्तिलाई बिक्री गरिने हुँदा सो जग्गा खरिद गर्न इच्छुक संगठित संस्था&divide;फर्म वा व्यक्तिले के कति मुल्य तिरी लिने हो मुल्य खोलि यो सूचना प्रथम पटक प्रकाशित भएको मितिले १५ दिन भित्र अर्थात मिति २०७३/०७/२५ गते कार्यालय समय भित्र र सो दिन बिदा परेमा सो को भोलिपल्ट सोही समयमा यस कम्पनीको प्रधान कार्यालय, दरबारमार्गबाट उपलब्ध गराईएको बोलपत्र फारम भरी प्रधान कार्यालय, प्रशासन विभागमा वोलपत्र पेश गर्नुहुन आह्वान गरिएको छ ।</p>', 'notice-for-share-auction-through-close-bid', 'publish', 'category_item', 0, 'UQO8hZvzoO1IJM1JF48OH75L0FFnVf93t9TDQtW15z08hfsV9P3K5CegTOM7fmS6SNYRX1rUKa1xHK0x6Iq6L3MwpP2bK7ImzIaS', 1, NULL, '2018-11-30 09:48:47', '2018-11-30 09:48:47', 'http://172.16.1.131:85/deltacms/notice-for-share-auction-through-close-bid', 0, NULL, NULL, '', NULL, '', 1, '  ', NULL, NULL, NULL, NULL, 0, NULL),
(22, 'जग्गा बिक्री गर्ने बारेको सिलबन्दी बोलपत्र आह्वानको सूचना', '<p><span style=\"font-size:14px\">यस युनाईटेड फाईनान्स लि. को आफ्नो स्वामित्वमा रहेको का.म.न.पा. वडा नं. १, नक्साल स्थित तपसिलमा उल्लेखित जग्गा &ldquo;जहाँ जे जस्तो अवस्थामा छ&rdquo; सोहि अवस्थामा गोप्य सिलबन्दी बोलपत्रद्वारा सार्वजनिक रूपमा संगठीत संस्था&divide;फर्म&divide;कम्पनी वा व्यक्तिलाई बिक्री गरिने हुँदा सो जग्गा खरिद गर्न इच्छुक संगठित संस्था&divide;फर्म वा व्यक्तिले के कति मुल्य तिरी लिने हो मुल्य खोलि यो सूचना प्रथम पटक प्रकाशित भएको मितिले १५ दिन भित्र अर्थात मिति २०७३/०७/२५ गते कार्यालय समय भित्र र सो दिन बिदा परेमा सो को भोलिपल्ट सोही समयमा यस कम्पनीको प्रधान कार्यालय, दरबारमार्गबाट उपलब्ध गराईएको बोलपत्र फारम भरी प्रधान कार्यालय, प्रशासन विभागमा वोलपत्र पेश गर्नुहुन आह्वान गरिएको छ । </span></p>', 'जग्गा-बिक्री-गर्ने-बारेको-सिलबन्दी-बोलपत्र-आह्वानको-सूचना-1', 'publish', 'category_item', 0, 'U6QXCa3YFODFltxv1mXnwsqpT57ItdkO1zmKd1H4rCXpfusfACHdjNaUgbPu8yPFCTrsQD3lnv8UaIayqQgoALz90dHYo0TK7IVz', 1, NULL, '2018-11-30 11:21:32', '2018-11-30 11:21:32', 'http://172.16.1.131:85/deltacms/जग्गा-बिक्री-गर्ने-बारेको-सिलबन्दी-बोलपत्र-आह्वानको-सूचना-1', 0, NULL, NULL, '', 'यस युनाईटेड फाईनान्स लि. को आफ्नो स्वामित्वमा रहेको का.म.न.पा. वडा नं. १, नक्साल स्थित तपसिलमा उल्लेखित जग्गा “जहाँ जे जस्तो अवस्थामा छ” सोहि अवस्थामा गोप्य सिलबन्दी बोलपत्रद्वारा सार्वजनिक रूपमा संगठीत संस्था÷फर्म÷कम्पनी वा व्यक्तिलाई बिक्री गरिने हुँदा सो जग्गा खरिद गर्न इच्छुक संगठित संस्था÷फर्म वा व्यक्तिले के कति मुल्य तिरी लिने हो मुल्य खोलि यो सूचना प्रथम पटक प्रकाशित भएको मितिले १५ दिन भित्र अर्थात मिति २०७३/०७/२५ गते कार्यालय समय भित्र र सो दिन बिदा परेमा सो को भोलिपल्ट सोही समयमा यस कम्पनीको प्रधान कार्यालय, दरबारमार्गबाट उपलब्ध गराईएको बोलपत्र फारम भरी प्रधान कार्यालय, प्रशासन विभागमा वोलपत्र पेश गर्नुहुन आह्वान गरिएको छ ।', '', 1, '  ', NULL, NULL, NULL, NULL, 0, NULL),
(23, 'Home', NULL, 'home', 'publish', 'custom_link', 0, 'JGMBhgFNxnhLLIQfGLrpKn5QWtv55CLeIUdZiCUxLvhEzj4GkkKX9kkhMVfuQaopU3DaK2PjSE3kXeDJoWtfmxM5EyDwWHvrOhb5', 1, NULL, '2018-12-02 09:20:48', '2018-12-02 09:20:48', 'deltacms/', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(26, 'career', NULL, 'career', 'publish', 'custom_link', 0, 'udXg4kMI0GK3munkxUebGsMgxc86snEOuTH03X14NVeIupw8BCqYVKWFjXacvfNoNtgOFXOICP96JqAI4qFKObZcoHNz57stKl26', 1, NULL, '2018-12-21 10:21:15', '2018-12-21 10:21:15', '/career', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(27, 'Test link', NULL, 'test-link', 'publish', 'custom_link', 0, '0NuOCIPfKLlNTD817NlNKmlOW21bgal4D9r1HbXlHdw2qpEUSPA0U49pmFvs2xeC6In6JWxWmVIKtYQKUs4qI9GDQ2KF3jOVNr2u', 1, NULL, '2018-12-27 11:09:26', '2018-12-27 11:09:26', 'http://facebook.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(28, 'Test 2', NULL, 'test-2', 'publish', 'custom_link', 0, 'cq98UWElEIFe19UvxkgXgebfrApdawAvOcK0LtNzcksKYq3bhlS4ePPlfD70jXOAhhlaLJLsp2c00D7pNKLTvjP8Fe13ThYi9iFf', 1, NULL, '2018-12-27 11:12:07', '2018-12-27 11:12:07', 'http://google.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(29, 'Test 3', NULL, 'test-3', 'publish', 'custom_link', 0, 'rZZvzL6lBPIZJQDjTBsPECHkIYTpqFKN9zKpJFiJIebXmpmAf9zUhG02v26Zy4bS4qiIAaijcDQotfxjhtOkMn5El2rrrAKv0rb4', 1, NULL, '2018-12-27 11:12:22', '2018-12-27 11:12:22', 'http://google.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(30, 'Test 4', NULL, 'test-4', 'publish', 'custom_link', 0, 'UWJYLokbElSLAPXt4wMxgXKkFEn21j9GrjET7RW2sDL3QUsGEFjJuTV2S47ZJauyIUiivMtxrXVLWRr9t9PQhibtEp0rC8m6XR0Y', 1, NULL, '2018-12-27 11:12:42', '2018-12-27 11:12:42', 'http://google.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(31, 'Test 5', NULL, 'test-5', 'publish', 'custom_link', 0, '971ZmcYrwYb0DeaYV7zY0cJ734FKI9S0H6Otzf6qeDdRlxh7tEEZ5aNMkgfdfsdovqXdltS1MvdYgPCFMk6aNRkO77LucAlCbfkm', 1, NULL, '2018-12-27 11:12:54', '2018-12-27 11:12:54', 'http://alskdfj.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(32, 'Home', NULL, 'home', 'publish', 'custom_link', 0, '2ZpqHOH9lTO3VfJnrcNM2SbBI8wJe59IPLPHy66bMjJkveebgPKyrGIobnBKYvtEOd8lxxf6hddS5FgxnMyj93iAfvdjijNNpfH3', 1, NULL, '2018-12-27 11:13:05', '2018-12-27 11:13:05', 'http://google.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(33, 'E-Banking', NULL, 'e-banking', 'publish', 'custom_link', 0, 'cPkfH50XyYGHWBAuhOydhDiMLi3vYSyKX4KthfTjx2qknB5YNaQFAwKR2HHTFOF38GAwbIZAZ5h3o2yiRjD7MHiNbHnGFf5kqGP6', 1, NULL, '2018-12-27 11:13:15', '2018-12-27 11:13:15', 'http://facebook.com', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL),
(34, 'SDfvasdf', NULL, 'sdfvasdf', 'publish', 'page', 0, 'dvRtybd1btx4cdDqlhWkD4aj59wqQir6kxPcJKbNxigEygzypulTNXFgsJz18aXF8ZVxuHWt1UMG3KUknDHGu6u1YALIRrCtjmgC', 1, NULL, '2019-01-02 06:27:35', '2019-01-02 06:27:35', 'http://172.16.1.131:85/deltacms/sdfvasdf', 0, NULL, NULL, '', 'asdf', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(35, 'This is a post/ apple/ball ? ok', '<p>asdfasdfasdf</p>', 'this-is-a-post/-apple/ball-?-ok', 'publish', 'page', 0, 'HMOuxnY8jaLaELcKuIKHO2sSSu3OPCQfwrQjkJBzTlobHtZZbM1nkWFsvbdLSQIxfe06DL8Q1jEXkLpHoomZGYBJdQarbQVfI7bY', 1, NULL, '2019-01-11 08:50:33', '2019-01-11 08:50:33', 'http://172.16.1.131:85/deltacms/this-is-a-post/-apple/ball-?-ok', 0, NULL, NULL, '', 'This is test', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(36, 'This is a post/ apple/ball ? okfsadfwdf', '<p>asdfasdfasdf</p>', 'this-is-a-post--apple-ball---okfsadfwdf', 'publish', 'page', 0, 'XamJVhUi0ZQgMWA9s6IDjE28EZvGWX0SUWicNK1CebnJR72OZ6wZgAuoZyJrFRndK8PLL6AZu1H6BLKxD1PW4dbAvbBBG60eD5ZS', 1, NULL, '2019-01-11 08:52:43', '2019-01-11 08:52:43', 'http://172.16.1.131:85/deltacms/this-is-a-post--apple-ball---okfsadfwdf', 0, NULL, NULL, '', 'This is test', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(37, 'About United Finance Ltd?Askldf/fdkjjjsdffvsad', NULL, 'about-united-finance-ltd-askldf-fdkjjjsdf', 'publish', 'page', 0, 'hOMUlLWKuoDHCVtmzIQDfWYXrOQ2lQz6smfVnjte4tYMZ409MJfoSmwq4Ye2iRxhrMs8pqHjMozt7znQzk1YmR2s9kOrjZqsj8vg', 1, 1, '2019-01-11 08:55:19', '2019-01-24 15:59:40', 'http://172.16.1.131:85/deltacms/about-united-finance-ltd-askldf-fdkjjjsdf', 0, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1),
(38, 'About United Finance Ltd/PVT-ltd', '<pre>\r\n<code><span class=\"pln\"> </span><span class=\"pun\">(</span><span class=\"kwd\">void</span><span class=\"pun\">)</span><span class=\"pln\">webView</span><span class=\"pun\">:(</span><span class=\"typ\">WKWebView</span><span class=\"pln\"> </span><span class=\"pun\">*)</span><span class=\"pln\">webView decidePolicyForNavigationResponse</span><span class=\"pun\">:(</span><span class=\"typ\">WKNavigationResponse</span><span class=\"pln\"> </span><span class=\"pun\">*)</span><span class=\"pln\">navigationResponse decisionHandler</span><span class=\"pun\">:(</span><span class=\"kwd\">void</span><span class=\"pln\"> </span><span class=\"pun\">(^)(</span><span class=\"typ\">WKNavigationResponsePolicy</span><span class=\"pun\">))</span><span class=\"pln\">decisionHandler\r\n</span><span class=\"pun\">{</span><span class=\"pln\">\r\n    </span><span class=\"typ\">NSHTTPURLResponse</span><span class=\"pln\"> </span><span class=\"pun\">*</span><span class=\"pln\">response </span><span class=\"pun\">=</span><span class=\"pln\"> </span><span class=\"pun\">(</span><span class=\"typ\">NSHTTPURLResponse</span><span class=\"pln\"> </span><span class=\"pun\">*)</span><span class=\"pln\">navigationResponse</span><span class=\"pun\">.</span><span class=\"pln\">response</span><span class=\"pun\">;</span><span class=\"pln\">\r\n    </span><span class=\"typ\">NSArray</span><span class=\"pln\"> </span><span class=\"pun\">*</span><span class=\"pln\">cookies </span><span class=\"pun\">=[</span><span class=\"typ\">NSHTTPCookie</span><span class=\"pln\"> cookiesWithResponseHeaderFields</span><span class=\"pun\">:[</span><span class=\"pln\">response allHeaderFields</span><span class=\"pun\">]</span><span class=\"pln\"> forURL</span><span class=\"pun\">:</span><span class=\"pln\">response</span><span class=\"pun\">.</span><span class=\"pln\">URL</span><span class=\"pun\">];</span><span class=\"pln\">\r\n\r\n    </span><span class=\"typ\">NSLog</span><span class=\"pun\">(@</span><span class=\"str\">&quot;How many Cookies: %lu&quot;</span><span class=\"pun\">,</span><span class=\"pln\"> </span><span class=\"pun\">(</span><span class=\"kwd\">unsigned</span><span class=\"pln\"> </span><span class=\"kwd\">long</span><span class=\"pun\">)</span><span class=\"pln\">cookies</span><span class=\"pun\">.</span><span class=\"pln\">count</span><span class=\"pun\">);</span><span class=\"pln\"> </span><span class=\"com\">//This always returns 0</span><span class=\"pln\">\r\n\r\n    </span><span class=\"kwd\">for</span><span class=\"pln\"> </span><span class=\"pun\">(</span><span class=\"typ\">NSHTTPCookie</span><span class=\"pln\"> </span><span class=\"pun\">*</span><span class=\"pln\">cookie in cookies</span><span class=\"pun\">)</span><span class=\"pln\"> </span><span class=\"pun\">{</span><span class=\"pln\">\r\n\r\n        </span><span class=\"pun\">[[</span><span class=\"typ\">NSHTTPCookieStorage</span><span class=\"pln\"> sharedHTTPCookieStorage</span><span class=\"pun\">]</span><span class=\"pln\"> setCookie</span><span class=\"pun\">:</span><span class=\"pln\">cookie</span><span class=\"pun\">];</span><span class=\"pln\">\r\n        </span><span class=\"typ\">NSLog</span><span class=\"pun\">(@</span><span class=\"str\">&quot;Entered cookie jar!&quot;</span><span class=\"pun\">);</span><span class=\"pln\">\r\n        </span><span class=\"typ\">NSLog</span><span class=\"pun\">(@</span><span class=\"str\">&quot;The cookie is %@&quot;</span><span class=\"pun\">,</span><span class=\"pln\"> cookie</span><span class=\"pun\">);</span><span class=\"pln\">\r\n    </span><span class=\"pun\">}</span><span class=\"pln\">\r\n    decisionHandler</span><span class=\"pun\">(</span><span class=\"typ\">WKNavigationResponsePolicyAllow</span><span class=\"pun\">);</span><span class=\"pln\">\r\n</span><span class=\"pun\">}</span></code></pre>', 'about-united-finance-ltd-pvt-ltd', 'publish', 'page', 0, 'zFBaCkgQwZTaoQ81NPWRnMLr7Ihd5Bk69bPN9ek3NbuBeeusx782jd6Y5ZN3QLHFGOjZeb1vCYgkgofMy9R6Z66K9rq1x7nMzTZR', 1, 1, '2019-01-11 09:06:06', '2019-01-13 10:41:32', 'http://172.16.1.131:85/deltacms/about-united-finance-ltd-pvt-ltd', 0, NULL, NULL, 'pages/VOX2fLErPm608cKWKEQfrpb35CKGsoRhALhrD9Bg5Jnwu.jpg', 'I am not able to get cookies from the NSHTTPURLResponse, though it previously worked and the Android app can get the cookies perfectly fine. When printing out how many cookies there are, I always get 0. Is my code wrong or is something else the matter?', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1),
(39, 'About United Finance Ltd/PVT-ltd?Test', '<pre>\r\n<code><span class=\"pln\"> </span><span class=\"pun\">(</span><span class=\"kwd\">void</span><span class=\"pun\">)</span><span class=\"pln\">webView</span><span class=\"pun\">:(</span><span class=\"typ\">WKWebView</span><span class=\"pln\"> </span><span class=\"pun\">*)</span><span class=\"pln\">webView decidePolicyForNavigationResponse</span><span class=\"pun\">:(</span><span class=\"typ\">WKNavigationResponse</span><span class=\"pln\"> </span><span class=\"pun\">*)</span><span class=\"pln\">navigationResponse decisionHandler</span><span class=\"pun\">:(</span><span class=\"kwd\">void</span><span class=\"pln\"> </span><span class=\"pun\">(^)(</span><span class=\"typ\">WKNavigationResponsePolicy</span><span class=\"pun\">))</span><span class=\"pln\">decisionHandler\r\n</span><span class=\"pun\">{</span><span class=\"pln\">\r\n    </span><span class=\"typ\">NSHTTPURLResponse</span><span class=\"pln\"> </span><span class=\"pun\">*</span><span class=\"pln\">response </span><span class=\"pun\">=</span><span class=\"pln\"> </span><span class=\"pun\">(</span><span class=\"typ\">NSHTTPURLResponse</span><span class=\"pln\"> </span><span class=\"pun\">*)</span><span class=\"pln\">navigationResponse</span><span class=\"pun\">.</span><span class=\"pln\">response</span><span class=\"pun\">;</span><span class=\"pln\">\r\n    </span><span class=\"typ\">NSArray</span><span class=\"pln\"> </span><span class=\"pun\">*</span><span class=\"pln\">cookies </span><span class=\"pun\">=[</span><span class=\"typ\">NSHTTPCookie</span><span class=\"pln\"> cookiesWithResponseHeaderFields</span><span class=\"pun\">:[</span><span class=\"pln\">response allHeaderFields</span><span class=\"pun\">]</span><span class=\"pln\"> forURL</span><span class=\"pun\">:</span><span class=\"pln\">response</span><span class=\"pun\">.</span><span class=\"pln\">URL</span><span class=\"pun\">];</span><span class=\"pln\">\r\n\r\n    </span><span class=\"typ\">NSLog</span><span class=\"pun\">(@</span><span class=\"str\">&quot;How many Cookies: %lu&quot;</span><span class=\"pun\">,</span><span class=\"pln\"> </span><span class=\"pun\">(</span><span class=\"kwd\">unsigned</span><span class=\"pln\"> </span><span class=\"kwd\">long</span><span class=\"pun\">)</span><span class=\"pln\">cookies</span><span class=\"pun\">.</span><span class=\"pln\">count</span><span class=\"pun\">);</span><span class=\"pln\"> </span><span class=\"com\">//This always returns 0</span><span class=\"pln\">\r\n\r\n    </span><span class=\"kwd\">for</span><span class=\"pln\"> </span><span class=\"pun\">(</span><span class=\"typ\">NSHTTPCookie</span><span class=\"pln\"> </span><span class=\"pun\">*</span><span class=\"pln\">cookie in cookies</span><span class=\"pun\">)</span><span class=\"pln\"> </span><span class=\"pun\">{</span><span class=\"pln\">\r\n\r\n        </span><span class=\"pun\">[[</span><span class=\"typ\">NSHTTPCookieStorage</span><span class=\"pln\"> sharedHTTPCookieStorage</span><span class=\"pun\">]</span><span class=\"pln\"> setCookie</span><span class=\"pun\">:</span><span class=\"pln\">cookie</span><span class=\"pun\">];</span><span class=\"pln\">\r\n        </span><span class=\"typ\">NSLog</span><span class=\"pun\">(@</span><span class=\"str\">&quot;Entered cookie jar!&quot;</span><span class=\"pun\">);</span><span class=\"pln\">\r\n        </span><span class=\"typ\">NSLog</span><span class=\"pun\">(@</span><span class=\"str\">&quot;The cookie is %@&quot;</span><span class=\"pun\">,</span><span class=\"pln\"> cookie</span><span class=\"pun\">);</span><span class=\"pln\">\r\n    </span><span class=\"pun\">}</span><span class=\"pln\">\r\n    decisionHandler</span><span class=\"pun\">(</span><span class=\"typ\">WKNavigationResponsePolicyAllow</span><span class=\"pun\">);</span><span class=\"pln\">\r\n</span><span class=\"pun\">}</span></code></pre>', 'about-united-finance-ltd-pvt-ltd-test', 'publish', 'page', 0, '9zM7U5AwVc6gwNieg3Ox8RDuCn8i8yXhhDzssW28HJuyu1iZrplZ4l5FZ1fCmBSn1Cm4Zm3QOXt2ew3uBXxP1YNsMojD0Kdz5tR5', 1, NULL, '2019-01-11 09:07:38', '2019-01-13 09:28:20', 'http://172.16.1.131:85/deltacms/about-united-finance-ltd-pvt-ltd-test', 0, NULL, NULL, '', 'I am not able to get cookies from the NSHTTPURLResponse, though it previously worked and the Android app can get the cookies perfectly fine. When printing out how many cookies there are, I always get 0. Is my code wrong or is something else the matter?', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, 0),
(40, 'sdfasdfdsaf', NULL, 'sdfasdfdsaf', 'publish', 'page', 0, 'hvOSUqRmIewrD6G20RlKMH2W2leS4skZWIbgTBd5gOIlpBxcyaeu2dwC25lU4MZ03Ies6xsOspJQiqOsfXLzD8SrENqHf2kdPPwt', 1, NULL, '2019-01-11 09:20:47', '2019-01-11 09:20:47', 'http://172.16.1.131:85/deltacms/sdfasdfdsaf', 0, NULL, NULL, '', 'asdfasdfsdaf', NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(41, 'Notice For Share Auction Through Close Bid', '<p>laksdjfklasjdfkladsf</p>', 'notice-for-share-auction-through-close-bid-1', 'publish', 'page', 0, 'Q9sVPjdjRk6QlbW33guf0AUrfuIDy7lwNYDMfCiYx89Ixd3k8NxrCj8eaPAAtLe8z0nFFRDekoQ0OaZbyuchqvSx1gPetBL3DoeT', 1, NULL, '2019-01-24 15:42:36', '2019-01-24 15:42:36', 'http://localhost/deltacms/notice-for-share-auction-through-close-bid-1', 0, NULL, NULL, '', 'kasjdlfk', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL);
INSERT INTO `posts` (`id`, `post_title`, `post_content`, `post_slug`, `post_status`, `post_type`, `post_parent`, `unique_key`, `created_by`, `updated_by`, `created_at`, `updated_at`, `post_url`, `is_trashed`, `gallery_id`, `tags`, `cover_image`, `post_subtitle`, `cover_icon`, `category_id`, `button_type`, `button_text`, `url_target`, `downloads_id`, `facebook_share`, `twitter_share`, `is_featured`) VALUES
(49, 'Introduction', '<ul>\r\n	<li>Share:</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li><a href=\"https://www.facebook.com/dialog/send?app_id=128457007838762=https://www.impactbnd.com/blog/examples-of-calls-to-action-for-lead-generation=https://www.impactbnd.com\" target=\"_blank\" title=\"Share on Facebook Messenger\"><img alt=\"Share on Facebook Messenger\" src=\"https://www.impactbnd.com/hubfs/blog-files/facebook-messenger-01.svg\" style=\"height:20px\" /></a></li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n</ul>\r\n\r\n<p>Published on July 10th, 2017</p>\r\n\r\n<p><a href=\"javascript:window.print()\">&nbsp;Print this Page/Save it as a PDF</a></p>\r\n\r\n<p>Have you struggled to get visitors to your site to do what you want?&nbsp;Maybe you want them to sign up for your newsletter, or get a demo of your product, but they never make it to that page?</p>\r\n\r\n<p>It may be time to&nbsp;<a href=\"https://offers.impactbnd.com/generate-more-leads-from-calls-to-action\" target=\"_blank\">revisit your calls-to-action</a>.</p>\r\n\r\n<p>These 15 call-to-action examples will show you how to&nbsp;<em>really</em>&nbsp;generate leads.</p>\r\n\r\n<h2>Call-to-Action Examples</h2>\r\n\r\n<ol>\r\n	<li>Netflix</li>\r\n	<li>WordStream&nbsp;</li>\r\n	<li>Hotjar&nbsp;</li>\r\n	<li>HubSpot Sales&nbsp;</li>\r\n	<li>Campaign Monitor</li>\r\n	<li>Kissmetrics</li>\r\n	<li>Spotify</li>\r\n	<li>Trello</li>\r\n	<li>Lyft</li>\r\n	<li>Join.Me</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>A&nbsp;<a href=\"https://www.impactbnd.com/blog/hubspot-how-to-creating-call-to-action\">call-to-action&nbsp;(CTA)&nbsp;is a button or link</a>&nbsp;that you add to your website in order to guide your visitor and tell them what to do next.</p>\r\n\r\n<p>In inbound marketing, they usually lead to a landing page where the visitor can fill out a form and become a lead.</p>\r\n\r\n<p><img src=\"https://www.impactbnd.com/hubfs/IMPACT%20Live/2019/CTAs/article-below-min.png\" /></p>\r\n\r\n<p><a href=\"https://www.impactbnd.com/live\"><img alt=\"IMPACT Live 2019 Tickets\" src=\"https://www.impactbnd.com/hubfs/IMPACT%20Live/2019/CTAs/IMPACT-Live-Blog-CTA-Video--Desktop-min.png\" /></a></p>\r\n\r\n<h2>How to Use Calls-to-Action for Lead Generation</h2>\r\n\r\n<p>You can place a CTA anywhere on your site, but it&rsquo;s important to do it right.</p>\r\n\r\n<p>You want your CTA to grab the attention of your visitors and really entice them to click.</p>\r\n\r\n<p>Show your visitors an offer that they can&rsquo;t refuse. Not only does it have to be visually appealing, but&nbsp;<a href=\"https://www.impactbnd.com/blog/13-simple-tricks-to-make-your-website-more-persuasive\" target=\"_blank\">the content has to persuasive</a>&nbsp;as well.&nbsp;</p>\r\n\r\n<p>In order to ensure your calls-to-action generate leads, you also need to remember&nbsp;<a href=\"https://www.impactbnd.com/blog/what-is-the-buyers-journey\">the buyer&#39;s journey.</a></p>\r\n\r\n<p>If the call-to-action you&rsquo;re adding to your site doesn&rsquo;t fulfill the need of your visitor at the specific point in their journey, it&rsquo;s not going to resonate with them, and they&rsquo;re not likely to move forward.</p>\r\n\r\n<p>Follow these&nbsp;<a href=\"https://www.impactbnd.com/blog/cta-best-practices\">best practices for creating strong calls-to-action</a>&nbsp;that will generate leads:</p>\r\n\r\n<ol>\r\n	<li>Make them Action-Oriented</li>\r\n	<li>Use Persuasive Text</li>\r\n	<li>Include Strong Visuals</li>\r\n	<li>Create a Sense of Urgency</li>\r\n	<li>Make them Easy to Find</li>\r\n</ol>\r\n\r\n<p>Then, use these 15 call-to-action examples to inspire your next design.&nbsp;</p>\r\n\r\n<h2>1.&nbsp;<a href=\"http://netflix.com/\">Netflix</a></h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples Netflix\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20Netflix.jpg?width=600&amp;name=Call-to-Action%20Examples%20Netflix.jpg\" style=\"width:600px\" title=\"Call-to-Action Examples Netflix\" /></p>\r\n\r\n<p>Netflix uses persuasive text to guide you to their free trial. They let you know how convenient their product is with the ability to watch anywhere as well as the ability to cancel if you&rsquo;re not satisfied.</p>\r\n\r\n<h2>&nbsp;2.&nbsp;<a href=\"http://wordstream.com/\">WordStream</a>&nbsp;</h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples Wordstream\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20Wordstream.jpg?width=600&amp;name=Call-to-Action%20Examples%20Wordstream.jpg\" style=\"width:600px\" title=\"Call-to-Action Examples Wordstream\" /></p>\r\n\r\n<p>WordStream uses contrasting colors with their CTA to make it stand out against the all-blue site. They also reiterate the fact that their product is free to use twice further enticing you to try it out.</p>\r\n\r\n<h2>3.&nbsp;<a href=\"http://hotjar.com/\">Hotja</a><a href=\"http://hotjar.com/\">r</a>&nbsp;</h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples Hotjar\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20Hotjar.jpg?width=600&amp;name=Call-to-Action%20Examples%20Hotjar.jpg\" style=\"width:600px\" title=\"Call-to-Action Examples Hotjar\" /></p>\r\n\r\n<p>Hotjar&rsquo;s homepage features two calls-to-action leading you to the same place. Both CTAs are easy to find with placement and color.</p>\r\n\r\n<h2>&nbsp;4.&nbsp;<a href=\"https://www.hubspot.com/products/sales/sales-tools?__hstc=191390709.c81124620f6db2f59e3a086745d0b203.1548771565625.1548771565625.1548771565625.1&amp;__hssc=191390709.1.1548771565626&amp;__hsfp=3210458627\">HubSpot Sales</a>&nbsp;</h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples HubSpot\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20HubSpot.jpg?width=600&amp;name=Call-to-Action%20Examples%20HubSpot.jpg\" style=\"width:600px\" title=\"Call-to-Action Examples HubSpot\" /></p>\r\n\r\n<p>HubSpot uses simple persuasive content alongside relatable images of fellow salespeople to get you to try their sales tools.</p>\r\n\r\n<p>Showing relevant images behind the CTA helps people visualize themselves using the product and in turn, make clicking through more appealing.</p>\r\n\r\n<p>HubSpot also makes sure to use friction-free text on their button making it inviting to get started with their program.</p>\r\n\r\n<h2>5.&nbsp;<a href=\"http://campaignmonitor.com/\">Campaign Monitor</a></h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples CampaignMonitor\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20CampaignMonitor%20(1).jpg?width=657&amp;name=Call-to-Action%20Examples%20CampaignMonitor%20(1).jpg\" style=\"width:657px\" title=\"Call-to-Action Examples CampaignMonitor\" /></p>', 'introduction-1', 'publish', 'page', 0, 'QTA8QNKsHPx17LStTuNNGvbNa7r15KzfPIQvlS84Ni7PIvcRmRiTyHOQVJJu1od2YP0GSoI60WE6tnOtXu8T9X634du39clbO3ip', 1, NULL, '2019-01-29 14:51:59', '2019-01-29 14:51:59', 'http://localhost/deltacms/introduction-1', 0, NULL, NULL, 'pages/QjRjdXsV8ApsMWBHpbE9yn4PJLmUPb9eTU59g1cK3nHcT.jpg', 'WordStream uses contrasting colors with their CTA to make it stand out against the all-blue site. They also reiterate the fact that their product is free to use twice further enticing you to try it out.', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(50, 'Introduction', '<ul>\r\n	<li>Share:</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li><a href=\"https://www.facebook.com/dialog/send?app_id=128457007838762=https://www.impactbnd.com/blog/examples-of-calls-to-action-for-lead-generation=https://www.impactbnd.com\" target=\"_blank\" title=\"Share on Facebook Messenger\"><img alt=\"Share on Facebook Messenger\" src=\"https://www.impactbnd.com/hubfs/blog-files/facebook-messenger-01.svg\" style=\"height:20px\" /></a></li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n</ul>\r\n\r\n<p>Published on July 10th, 2017</p>\r\n\r\n<p><a href=\"javascript:window.print()\">&nbsp;Print this Page/Save it as a PDF</a></p>\r\n\r\n<p>Have you struggled to get visitors to your site to do what you want?&nbsp;Maybe you want them to sign up for your newsletter, or get a demo of your product, but they never make it to that page?</p>\r\n\r\n<p>It may be time to&nbsp;<a href=\"https://offers.impactbnd.com/generate-more-leads-from-calls-to-action\" target=\"_blank\">revisit your calls-to-action</a>.</p>\r\n\r\n<p>These 15 call-to-action examples will show you how to&nbsp;<em>really</em>&nbsp;generate leads.</p>\r\n\r\n<h2>Call-to-Action Examples</h2>\r\n\r\n<ol>\r\n	<li>Netflix</li>\r\n	<li>WordStream&nbsp;</li>\r\n	<li>Hotjar&nbsp;</li>\r\n	<li>HubSpot Sales&nbsp;</li>\r\n	<li>Campaign Monitor</li>\r\n	<li>Kissmetrics</li>\r\n	<li>Spotify</li>\r\n	<li>Trello</li>\r\n	<li>Lyft</li>\r\n	<li>Join.Me</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>A&nbsp;<a href=\"https://www.impactbnd.com/blog/hubspot-how-to-creating-call-to-action\">call-to-action&nbsp;(CTA)&nbsp;is a button or link</a>&nbsp;that you add to your website in order to guide your visitor and tell them what to do next.</p>\r\n\r\n<p>In inbound marketing, they usually lead to a landing page where the visitor can fill out a form and become a lead.</p>\r\n\r\n<p><img src=\"https://www.impactbnd.com/hubfs/IMPACT%20Live/2019/CTAs/article-below-min.png\" /></p>\r\n\r\n<p><a href=\"https://www.impactbnd.com/live\"><img alt=\"IMPACT Live 2019 Tickets\" src=\"https://www.impactbnd.com/hubfs/IMPACT%20Live/2019/CTAs/IMPACT-Live-Blog-CTA-Video--Desktop-min.png\" /></a></p>\r\n\r\n<h2>How to Use Calls-to-Action for Lead Generation</h2>\r\n\r\n<p>You can place a CTA anywhere on your site, but it&rsquo;s important to do it right.</p>\r\n\r\n<p>You want your CTA to grab the attention of your visitors and really entice them to click.</p>\r\n\r\n<p>Show your visitors an offer that they can&rsquo;t refuse. Not only does it have to be visually appealing, but&nbsp;<a href=\"https://www.impactbnd.com/blog/13-simple-tricks-to-make-your-website-more-persuasive\" target=\"_blank\">the content has to persuasive</a>&nbsp;as well.&nbsp;</p>\r\n\r\n<p>In order to ensure your calls-to-action generate leads, you also need to remember&nbsp;<a href=\"https://www.impactbnd.com/blog/what-is-the-buyers-journey\">the buyer&#39;s journey.</a></p>\r\n\r\n<p>If the call-to-action you&rsquo;re adding to your site doesn&rsquo;t fulfill the need of your visitor at the specific point in their journey, it&rsquo;s not going to resonate with them, and they&rsquo;re not likely to move forward.</p>\r\n\r\n<p>Follow these&nbsp;<a href=\"https://www.impactbnd.com/blog/cta-best-practices\">best practices for creating strong calls-to-action</a>&nbsp;that will generate leads:</p>\r\n\r\n<ol>\r\n	<li>Make them Action-Oriented</li>\r\n	<li>Use Persuasive Text</li>\r\n	<li>Include Strong Visuals</li>\r\n	<li>Create a Sense of Urgency</li>\r\n	<li>Make them Easy to Find</li>\r\n</ol>\r\n\r\n<p>Then, use these 15 call-to-action examples to inspire your next design.&nbsp;</p>\r\n\r\n<h2>1.&nbsp;<a href=\"http://netflix.com/\">Netflix</a></h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples Netflix\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20Netflix.jpg?width=600&amp;name=Call-to-Action%20Examples%20Netflix.jpg\" style=\"width:600px\" title=\"Call-to-Action Examples Netflix\" /></p>\r\n\r\n<p>Netflix uses persuasive text to guide you to their free trial. They let you know how convenient their product is with the ability to watch anywhere as well as the ability to cancel if you&rsquo;re not satisfied.</p>\r\n\r\n<h2>&nbsp;2.&nbsp;<a href=\"http://wordstream.com/\">WordStream</a>&nbsp;</h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples Wordstream\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20Wordstream.jpg?width=600&amp;name=Call-to-Action%20Examples%20Wordstream.jpg\" style=\"width:600px\" title=\"Call-to-Action Examples Wordstream\" /></p>\r\n\r\n<p>WordStream uses contrasting colors with their CTA to make it stand out against the all-blue site. They also reiterate the fact that their product is free to use twice further enticing you to try it out.</p>\r\n\r\n<h2>3.&nbsp;<a href=\"http://hotjar.com/\">Hotja</a><a href=\"http://hotjar.com/\">r</a>&nbsp;</h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples Hotjar\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20Hotjar.jpg?width=600&amp;name=Call-to-Action%20Examples%20Hotjar.jpg\" style=\"width:600px\" title=\"Call-to-Action Examples Hotjar\" /></p>\r\n\r\n<p>Hotjar&rsquo;s homepage features two calls-to-action leading you to the same place. Both CTAs are easy to find with placement and color.</p>\r\n\r\n<h2>&nbsp;4.&nbsp;<a href=\"https://www.hubspot.com/products/sales/sales-tools?__hstc=191390709.c81124620f6db2f59e3a086745d0b203.1548771565625.1548771565625.1548771565625.1&amp;__hssc=191390709.1.1548771565626&amp;__hsfp=3210458627\">HubSpot Sales</a>&nbsp;</h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples HubSpot\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20HubSpot.jpg?width=600&amp;name=Call-to-Action%20Examples%20HubSpot.jpg\" style=\"width:600px\" title=\"Call-to-Action Examples HubSpot\" /></p>\r\n\r\n<p>HubSpot uses simple persuasive content alongside relatable images of fellow salespeople to get you to try their sales tools.</p>\r\n\r\n<p>Showing relevant images behind the CTA helps people visualize themselves using the product and in turn, make clicking through more appealing.</p>\r\n\r\n<p>HubSpot also makes sure to use friction-free text on their button making it inviting to get started with their program.</p>\r\n\r\n<h2>5.&nbsp;<a href=\"http://campaignmonitor.com/\">Campaign Monitor</a></h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples CampaignMonitor\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20CampaignMonitor%20(1).jpg?width=657&amp;name=Call-to-Action%20Examples%20CampaignMonitor%20(1).jpg\" style=\"width:657px\" title=\"Call-to-Action Examples CampaignMonitor\" /></p>', 'introduction-1-2', 'publish', 'page', 0, 'YA2tEKP2vt8wdZQRRt6zqnf4aINcmtWQgbx6OCkjiyXevdKaCHQYsB7rvn0w0UDKOn9qsvBbKZ1B5qFhf5D3WMQXYtPiY6LRRwIM', 1, NULL, '2019-01-29 14:53:09', '2019-01-29 14:53:09', 'http://localhost/deltacms/introduction-1-2', 0, NULL, NULL, 'pages/ata2s3ROmhoiu8GmDME1T38oKN2lurNTGpV8hX06lpu5C.jpg', 'WordStream uses contrasting colors with their CTA to make it stand out against the all-blue site. They also reiterate the fact that their product is free to use twice further enticing you to try it out.', NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL),
(51, 'Notice For Share Auction Through Close Bid', '<ul>\r\n	<li>Share:</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li><a href=\"https://www.facebook.com/dialog/send?app_id=128457007838762=https://www.impactbnd.com/blog/examples-of-calls-to-action-for-lead-generation=https://www.impactbnd.com\" target=\"_blank\" title=\"Share on Facebook Messenger\"><img alt=\"Share on Facebook Messenger\" src=\"https://www.impactbnd.com/hubfs/blog-files/facebook-messenger-01.svg\" style=\"height:20px\" /></a></li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n	<li>&nbsp;</li>\r\n</ul>\r\n\r\n<p>Published on July 10th, 2017</p>\r\n\r\n<p><a href=\"javascript:window.print()\">&nbsp;Print this Page/Save it as a PDF</a></p>\r\n\r\n<p>Have you struggled to get visitors to your site to do what you want?&nbsp;Maybe you want them to sign up for your newsletter, or get a demo of your product, but they never make it to that page?</p>\r\n\r\n<p>It may be time to&nbsp;<a href=\"https://offers.impactbnd.com/generate-more-leads-from-calls-to-action\" target=\"_blank\">revisit your calls-to-action</a>.</p>\r\n\r\n<p>These 15 call-to-action examples will show you how to&nbsp;<em>really</em>&nbsp;generate leads.</p>\r\n\r\n<h2>Call-to-Action Examples</h2>\r\n\r\n<ol>\r\n	<li>Netflix</li>\r\n	<li>WordStream&nbsp;</li>\r\n	<li>Hotjar&nbsp;</li>\r\n	<li>HubSpot Sales&nbsp;</li>\r\n	<li>Campaign Monitor</li>\r\n	<li>Kissmetrics</li>\r\n	<li>Spotify</li>\r\n	<li>Trello</li>\r\n	<li>Lyft</li>\r\n	<li>Join.Me</li>\r\n</ol>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>A&nbsp;<a href=\"https://www.impactbnd.com/blog/hubspot-how-to-creating-call-to-action\">call-to-action&nbsp;(CTA)&nbsp;is a button or link</a>&nbsp;that you add to your website in order to guide your visitor and tell them what to do next.</p>\r\n\r\n<p>In inbound marketing, they usually lead to a landing page where the visitor can fill out a form and become a lead.</p>\r\n\r\n<p><img src=\"https://www.impactbnd.com/hubfs/IMPACT%20Live/2019/CTAs/article-below-min.png\" /></p>\r\n\r\n<p><a href=\"https://www.impactbnd.com/live\"><img alt=\"IMPACT Live 2019 Tickets\" src=\"https://www.impactbnd.com/hubfs/IMPACT%20Live/2019/CTAs/IMPACT-Live-Blog-CTA-Video--Desktop-min.png\" /></a></p>\r\n\r\n<h2>How to Use Calls-to-Action for Lead Generation</h2>\r\n\r\n<p>You can place a CTA anywhere on your site, but it&rsquo;s important to do it right.</p>\r\n\r\n<p>You want your CTA to grab the attention of your visitors and really entice them to click.</p>\r\n\r\n<p>Show your visitors an offer that they can&rsquo;t refuse. Not only does it have to be visually appealing, but&nbsp;<a href=\"https://www.impactbnd.com/blog/13-simple-tricks-to-make-your-website-more-persuasive\" target=\"_blank\">the content has to persuasive</a>&nbsp;as well.&nbsp;</p>\r\n\r\n<p>In order to ensure your calls-to-action generate leads, you also need to remember&nbsp;<a href=\"https://www.impactbnd.com/blog/what-is-the-buyers-journey\">the buyer&#39;s journey.</a></p>\r\n\r\n<p>If the call-to-action you&rsquo;re adding to your site doesn&rsquo;t fulfill the need of your visitor at the specific point in their journey, it&rsquo;s not going to resonate with them, and they&rsquo;re not likely to move forward.</p>\r\n\r\n<p>Follow these&nbsp;<a href=\"https://www.impactbnd.com/blog/cta-best-practices\">best practices for creating strong calls-to-action</a>&nbsp;that will generate leads:</p>\r\n\r\n<ol>\r\n	<li>Make them Action-Oriented</li>\r\n	<li>Use Persuasive Text</li>\r\n	<li>Include Strong Visuals</li>\r\n	<li>Create a Sense of Urgency</li>\r\n	<li>Make them Easy to Find</li>\r\n</ol>\r\n\r\n<p>Then, use these 15 call-to-action examples to inspire your next design.&nbsp;</p>\r\n\r\n<h2>1.&nbsp;<a href=\"http://netflix.com/\">Netflix</a></h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples Netflix\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20Netflix.jpg?width=600&amp;name=Call-to-Action%20Examples%20Netflix.jpg\" style=\"width:600px\" title=\"Call-to-Action Examples Netflix\" /></p>\r\n\r\n<p>Netflix uses persuasive text to guide you to their free trial. They let you know how convenient their product is with the ability to watch anywhere as well as the ability to cancel if you&rsquo;re not satisfied.</p>\r\n\r\n<h2>&nbsp;2.&nbsp;<a href=\"http://wordstream.com/\">WordStream</a>&nbsp;</h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples Wordstream\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20Wordstream.jpg?width=600&amp;name=Call-to-Action%20Examples%20Wordstream.jpg\" style=\"width:600px\" title=\"Call-to-Action Examples Wordstream\" /></p>\r\n\r\n<p>WordStream uses contrasting colors with their CTA to make it stand out against the all-blue site. They also reiterate the fact that their product is free to use twice further enticing you to try it out.</p>\r\n\r\n<h2>3.&nbsp;<a href=\"http://hotjar.com/\">Hotja</a><a href=\"http://hotjar.com/\">r</a>&nbsp;</h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples Hotjar\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20Hotjar.jpg?width=600&amp;name=Call-to-Action%20Examples%20Hotjar.jpg\" style=\"width:600px\" title=\"Call-to-Action Examples Hotjar\" /></p>\r\n\r\n<p>Hotjar&rsquo;s homepage features two calls-to-action leading you to the same place. Both CTAs are easy to find with placement and color.</p>\r\n\r\n<h2>&nbsp;4.&nbsp;<a href=\"https://www.hubspot.com/products/sales/sales-tools?__hstc=191390709.c81124620f6db2f59e3a086745d0b203.1548771565625.1548771565625.1548771565625.1&amp;__hssc=191390709.1.1548771565626&amp;__hsfp=3210458627\">HubSpot Sales</a>&nbsp;</h2>\r\n\r\n<p><img alt=\"Call-to-Action Examples HubSpot\" src=\"https://www.impactbnd.com/hs-fs/hubfs/blog-image-uploads/Call-to-Action%20Examples%20HubSpot.jpg?width=600&amp;name=Call-to-Action%20Examples%20HubSpot.jpg\" style=\"width:600px\" title=\"Call-to-Action Examples HubSpot\" /></p>\r\n\r\n<p>HubSpot uses simple persuasive content alongside relatable images of fellow salespeople to get you to try their sales tools.</p>\r\n\r\n<p>Showing relevant images behind the CTA helps people visualize themselves using the product and in turn, make clicking through more appealing.</p>', 'notice-for-share-auction-through-close-bid-1-2', 'publish', 'page', 0, 'nojVLtGYFkI7CPW4kOLiRYrabJbuqkqZfNrehgvYBMQy8CGhf6GXBrlCcuFHAMU2ATdhUwcy6CAaxECAgn3rdXy0XMxyiph3HGKX', 1, 1, '2019-01-29 14:55:48', '2019-01-29 15:43:16', 'http://localhost/deltacms/notice-for-share-auction-through-close-bid-1-2', 0, NULL, NULL, 'pages/NOiYs2hBoAQ5tvFlLQWBofycTErQIdtjRhcu5s1GxTwN7.jpg', 'You want your CTA to grab the attention of your visitors and really entice them to click.', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 'About United Finance Ltd?Askldf/fdkjjjsdffvsad1111111111111', '<p>About United Finance Ltd?Askldf/fdkjjjsdffvsad</p>', 'about-united-finance-ltd-askldf-fdkjjjsdffvsad1111111111111', 'publish', 'page', 0, '9iuy920doU9OJiKFa4BLxQWXQCJLiEtKXOmYCWrvIPVDp33pfaYUuHsamtmT66nZvpRRHB8lxazghdfVlgkcnvkT8GZ902rZevoE', 1, NULL, '2019-01-30 14:52:16', '2019-01-30 14:52:16', 'http://localhost/deltacms/about-united-finance-ltd-askldf-fdkjjjsdffvsad1111111111111', 0, NULL, NULL, '', 'About United Finance Ltd?Askldf/fdkjjjsdffvsad', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 'News and Notices', NULL, 'downloads-item-news-and-notices', 'publish', 'downloads', 0, 'PfSYF3YXX2TftnphcLplPiR9uSO4tEADmaeZijfA90Ud6Oty9KPxJxhDYNPC9WZJizv1MCkOjC2DcjHvpokAG2V33iWJ3OfMbyBW', 1, NULL, '2019-02-07 18:19:30', '2019-02-07 18:19:30', 'http://localhost/deltacms/downloads/downloads-item-news-and-notices', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 0, 0, NULL),
(65, 'ONLINE BANKING', NULL, 'online-banking', 'published', 'popup', 0, 'u2sCw6Ttq0xs88G6vcAJqHEok2TDkoaFiSx4C9tmq0LnZS6T1A4aexnA7UgyDqwQbXi6WTwTES4w9Y8K8a3X5Msrsv9C6Mqb3iX1', 1, NULL, '2019-02-19 14:59:19', '2019-02-19 14:59:19', '', 0, NULL, NULL, 'popup/cJEbiQsN9GD44CetUf3QXOzeJIYowdvFkHvh0Ya9.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, 0, NULL),
(66, 'This is a test page', '<p>asdfsadfasdfdsf</p>', 'this-is-a-test-page', 'publish', 'page', 0, 'Yb45ing21ntiKJbYsqTEl0DEMNtfLk1waULVZtUd4Rer6MQdKhcLXdZmt9diS2Wn5OsNujxeXHdiJZR2bR9fegTdfG9q6s3aoycP', 1, NULL, '2019-03-15 12:29:11', '2019-03-15 12:29:11', 'http://localhost/deltacms/this-is-a-test-page', 0, NULL, NULL, '', 'asdfasdfasf', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `post_category`
--

CREATE TABLE `post_category` (
  `id` int(11) NOT NULL,
  `category_title` varchar(255) DEFAULT NULL,
  `category_subtitle` text,
  `category_slug` text NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_category`
--

INSERT INTO `post_category` (`id`, `category_title`, `category_subtitle`, `category_slug`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'News and notice', 'This is test subtitle', 'news-and-notice', 1, '2018-11-30 09:53:57', '2018-12-06 06:40:41');

-- --------------------------------------------------------

--
-- Table structure for table `post_meta`
--

CREATE TABLE `post_meta` (
  `post_meta_id` int(11) NOT NULL,
  `post_id` int(11) DEFAULT NULL,
  `meta_id` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(255) NOT NULL,
  `meta_title` text,
  `meta_description` text,
  `meta_button_title` varchar(255) DEFAULT NULL,
  `meta_button_link` text,
  `meta_file` varchar(255) DEFAULT NULL,
  `meta_button_target` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `post_meta`
--

INSERT INTO `post_meta` (`post_meta_id`, `post_id`, `meta_id`, `created_at`, `updated_at`, `slug`, `meta_title`, `meta_description`, `meta_button_title`, `meta_button_link`, `meta_file`, `meta_button_target`) VALUES
(1, 49, 1, '2019-01-29 14:51:59', '2019-01-29 14:51:59', 'category', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 50, 1, '2019-01-29 14:53:09', '2019-01-29 14:53:09', 'category', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 50, NULL, '2019-01-29 14:53:09', '2019-01-29 14:53:09', 'call-to-action', 'WordStream uses contrasting colors wi', 'th their CTA to make it stand out against the all-blue site. They also reiterate the fact that their product is free to use twice further enticing you to try it out.', 'learn more', 'learn more', NULL, '_blank'),
(4, 51, NULL, '2019-01-29 14:55:48', '2019-01-29 14:55:48', 'call-to-action', 'Read More', 'You want your CTA to grab the attention of your visitors and really entice them to click.\r\n\r\nShow your', 'Read More', 'https://www.impactbnd.com/blog/examples-of-calls-to-action-for-lead-generation', 'post_meta/OpuZTqTmSqsY46WbDU2Pp8qCz4gPFmH3QzmWWQyl.png', '_blank'),
(5, 38, NULL, '2019-01-29 15:44:20', '2019-01-29 15:54:26', 'call-to-action', 'TestEdited', 'test descEdited', 'Read MoreEdited', 'https://www.impactbnd.com/blog/examples-of-calls-to-action-for-lead-generationEdited', 'post_meta/1otaSvXriJlunLp0apwopYJ6jHBkce7rFgq9BXYY.jpeg', NULL),
(6, 37, NULL, '2019-01-30 14:51:49', '2019-01-30 14:51:49', 'call-to-action', NULL, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `request_form`
--

CREATE TABLE `request_form` (
  `form_id` int(11) NOT NULL,
  `ccode` varchar(255) DEFAULT NULL,
  `prvccode` varchar(255) DEFAULT NULL,
  `ctype` varchar(255) DEFAULT NULL,
  `cstatus` varchar(255) DEFAULT NULL,
  `cbranch` varchar(255) DEFAULT NULL,
  `cowner` varchar(255) DEFAULT NULL,
  `ccategory` varchar(255) DEFAULT NULL,
  `cminor` varchar(255) DEFAULT NULL,
  `cfname` varchar(255) DEFAULT NULL,
  `cmiddlename` varchar(255) DEFAULT NULL,
  `clastname` varchar(255) DEFAULT NULL,
  `cdatebirth` varchar(255) DEFAULT NULL,
  `cexpiryminor` varchar(255) DEFAULT NULL,
  `ccountry` varchar(255) DEFAULT NULL,
  `cobligator` varchar(255) DEFAULT NULL,
  `cacofficer` varchar(255) DEFAULT NULL,
  `cintro` varchar(255) DEFAULT NULL,
  `cresident` varchar(255) DEFAULT NULL,
  `caddress` varchar(255) DEFAULT NULL,
  `ccity` varchar(255) DEFAULT NULL,
  `chouseblock` varchar(255) DEFAULT NULL,
  `cvcmdc` varchar(255) DEFAULT NULL,
  `cvcmdcname` varchar(255) DEFAULT NULL,
  `ctelno` varchar(255) DEFAULT NULL,
  `cpobox` varchar(255) DEFAULT NULL,
  `cdistrict` varchar(255) DEFAULT NULL,
  `cwardno` varchar(255) DEFAULT NULL,
  `cstate` varchar(255) DEFAULT NULL,
  `peraddress` varchar(255) DEFAULT NULL,
  `percity` varchar(255) DEFAULT NULL,
  `pervcmdc` varchar(255) DEFAULT NULL,
  `pervcmdcname` varchar(255) DEFAULT NULL,
  `pertel` varchar(255) DEFAULT NULL,
  `perpobox` varchar(255) DEFAULT NULL,
  `perdistrict` varchar(255) DEFAULT NULL,
  `perward` varchar(255) DEFAULT NULL,
  `percountry` varchar(255) DEFAULT NULL,
  `perstate` varchar(255) DEFAULT NULL,
  `perfax` varchar(255) DEFAULT NULL,
  `perfax2` varchar(255) DEFAULT NULL,
  `permobile` varchar(255) DEFAULT NULL,
  `permobile2` varchar(255) DEFAULT NULL,
  `peremail` varchar(255) DEFAULT NULL,
  `cusid` varchar(255) DEFAULT NULL,
  `cusprevcitizen` varchar(255) DEFAULT NULL,
  `cusidno` varchar(255) DEFAULT NULL,
  `cusauthority` varchar(255) DEFAULT NULL,
  `cusissdate` varchar(255) DEFAULT NULL,
  `cusiddistrict` varchar(255) DEFAULT NULL,
  `cusmarital` varchar(255) DEFAULT NULL,
  `cusnationality` varchar(255) DEFAULT NULL,
  `cussalutation` varchar(255) DEFAULT NULL,
  `cusreligion` varchar(255) DEFAULT NULL,
  `cusgender` varchar(255) DEFAULT NULL,
  `emporgname` varchar(255) DEFAULT NULL,
  `empdesignation` varchar(255) DEFAULT NULL,
  `emporgaddress` varchar(255) DEFAULT NULL,
  `empsalary` varchar(255) DEFAULT NULL,
  `regaddress` varchar(255) DEFAULT NULL,
  `regcity` varchar(255) DEFAULT NULL,
  `regcountry` varchar(255) DEFAULT NULL,
  `regvcmdc` varchar(255) DEFAULT NULL,
  `regward` varchar(255) DEFAULT NULL,
  `regdistrict` varchar(255) DEFAULT NULL,
  `regstate` varchar(255) DEFAULT NULL,
  `grnffirst` varchar(255) DEFAULT NULL,
  `grnfmiddle` varchar(255) DEFAULT NULL,
  `grnflast` varchar(255) DEFAULT NULL,
  `grnfctznno` varchar(255) DEFAULT NULL,
  `grnfctznissdate` varchar(255) DEFAULT NULL,
  `grnfctznissdistrict` varchar(255) DEFAULT NULL,
  `grnfctzndistrict` varchar(255) DEFAULT NULL,
  `grmfirst` varchar(255) DEFAULT NULL,
  `grmmiddle` varchar(255) DEFAULT NULL,
  `grmlast` varchar(255) DEFAULT NULL,
  `grmctznno` varchar(255) DEFAULT NULL,
  `grmctznissdate` varchar(255) DEFAULT NULL,
  `grmctznissdistrict` varchar(255) DEFAULT NULL,
  `grmctzndistrict` varchar(255) DEFAULT NULL,
  `ftrfirst` text,
  `ftrmiddle` text,
  `ftrfast` text,
  `ftrctznno` text,
  `ftrctznissdate` text,
  `ftrctznissdistrict` text,
  `ftrctzndistrict` text,
  `mthrfirst` text,
  `mthrmiddle` text,
  `mthrlast` text,
  `mthrctznno` text,
  `mthrctznissdate` text,
  `mthrctznissdistrict` text,
  `mthrctzndistrict` text,
  `comid` varchar(100) DEFAULT NULL,
  `comauthority` varchar(100) DEFAULT NULL,
  `comidnum` varchar(100) DEFAULT NULL,
  `comissdistrict` varchar(100) DEFAULT NULL,
  `comactcode` varchar(100) DEFAULT NULL,
  `comissdate` varchar(100) DEFAULT NULL,
  `comexpiry` varchar(100) DEFAULT NULL,
  `othoccupation` text,
  `othsource` text,
  `othpurpose` text,
  `othturnover` text,
  `forassociate` text,
  `forresidential` text,
  `fordirector` text,
  `forcountry` text,
  `kycbusiness` text,
  `kycreviewed` text,
  `kycriskgrade` text,
  `kyctrans` text,
  `kycreason` text,
  `docstatus` text,
  `docdateupdate` text,
  `docremarks` text,
  `docpersonal` text,
  `perexposed` text,
  `perpunish` text,
  `perhighpositioned` text,
  `pernonface` text,
  `pervimpper` text,
  `insinfo` text,
  `insremark` text,
  `inslimit` text,
  `insgroup` text,
  `insborrower` text,
  `insstatement` text,
  `insmis` text,
  `inskycupdate` text,
  `extname` text,
  `extcitizen` text,
  `extriskgrade` text,
  `extalias` text,
  `extctzndistrict` text,
  `extorgname` text,
  `extdesignation` text,
  `extbirth` text,
  `extfather` text,
  `extgrandfather` text,
  `extspouse` text,
  `extctznissddate` text,
  `extsignatory` text,
  `extdirector` text,
  `extrole` text,
  `reshouse` text,
  `resstreet` text,
  `resward` text,
  `resvcmdc` text,
  `resdistrict` text,
  `rescity` text,
  `restel` text,
  `extperhouse` text,
  `extperarea` text,
  `extperward` text,
  `extpervcmdc` text,
  `extperdistrict` text,
  `extpercity` text,
  `extpertel` text,
  `extperstreet` text,
  `mailhouse` text,
  `mailarea` text,
  `mailward` text,
  `mailvcmdc` text,
  `maildistrict` text,
  `mailcity` text,
  `mailtel` text,
  `mailaddress` text,
  `mailfax` text,
  `mailstreet` text,
  `bustel` text,
  `busfax` text,
  `page` int(11) NOT NULL,
  `is_completed` int(11) NOT NULL DEFAULT '0',
  `status` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `request_form`
--

INSERT INTO `request_form` (`form_id`, `ccode`, `prvccode`, `ctype`, `cstatus`, `cbranch`, `cowner`, `ccategory`, `cminor`, `cfname`, `cmiddlename`, `clastname`, `cdatebirth`, `cexpiryminor`, `ccountry`, `cobligator`, `cacofficer`, `cintro`, `cresident`, `caddress`, `ccity`, `chouseblock`, `cvcmdc`, `cvcmdcname`, `ctelno`, `cpobox`, `cdistrict`, `cwardno`, `cstate`, `peraddress`, `percity`, `pervcmdc`, `pervcmdcname`, `pertel`, `perpobox`, `perdistrict`, `perward`, `percountry`, `perstate`, `perfax`, `perfax2`, `permobile`, `permobile2`, `peremail`, `cusid`, `cusprevcitizen`, `cusidno`, `cusauthority`, `cusissdate`, `cusiddistrict`, `cusmarital`, `cusnationality`, `cussalutation`, `cusreligion`, `cusgender`, `emporgname`, `empdesignation`, `emporgaddress`, `empsalary`, `regaddress`, `regcity`, `regcountry`, `regvcmdc`, `regward`, `regdistrict`, `regstate`, `grnffirst`, `grnfmiddle`, `grnflast`, `grnfctznno`, `grnfctznissdate`, `grnfctznissdistrict`, `grnfctzndistrict`, `grmfirst`, `grmmiddle`, `grmlast`, `grmctznno`, `grmctznissdate`, `grmctznissdistrict`, `grmctzndistrict`, `ftrfirst`, `ftrmiddle`, `ftrfast`, `ftrctznno`, `ftrctznissdate`, `ftrctznissdistrict`, `ftrctzndistrict`, `mthrfirst`, `mthrmiddle`, `mthrlast`, `mthrctznno`, `mthrctznissdate`, `mthrctznissdistrict`, `mthrctzndistrict`, `comid`, `comauthority`, `comidnum`, `comissdistrict`, `comactcode`, `comissdate`, `comexpiry`, `othoccupation`, `othsource`, `othpurpose`, `othturnover`, `forassociate`, `forresidential`, `fordirector`, `forcountry`, `kycbusiness`, `kycreviewed`, `kycriskgrade`, `kyctrans`, `kycreason`, `docstatus`, `docdateupdate`, `docremarks`, `docpersonal`, `perexposed`, `perpunish`, `perhighpositioned`, `pernonface`, `pervimpper`, `insinfo`, `insremark`, `inslimit`, `insgroup`, `insborrower`, `insstatement`, `insmis`, `inskycupdate`, `extname`, `extcitizen`, `extriskgrade`, `extalias`, `extctzndistrict`, `extorgname`, `extdesignation`, `extbirth`, `extfather`, `extgrandfather`, `extspouse`, `extctznissddate`, `extsignatory`, `extdirector`, `extrole`, `reshouse`, `resstreet`, `resward`, `resvcmdc`, `resdistrict`, `rescity`, `restel`, `extperhouse`, `extperarea`, `extperward`, `extpervcmdc`, `extperdistrict`, `extpercity`, `extpertel`, `extperstreet`, `mailhouse`, `mailarea`, `mailward`, `mailvcmdc`, `maildistrict`, `mailcity`, `mailtel`, `mailaddress`, `mailfax`, `mailstreet`, `bustel`, `busfax`, `page`, `is_completed`, `status`, `created_at`, `updated_at`) VALUES
(1, '1', '2', 'Afghanistan', '3', '4', 'Afghanistan', 'Afghanistan', NULL, '5', '6', '7', '8', '9', '10', '11', '12', '13', 'Afghanistan', '15', '16', '17', '18', 'Afghanistan', '22', '25', '1', '27', '31', '20', '21', '19', 'Afghanistan', '23', '24', '1', '26', '29', '30', '32', '33', '34', '35', '36', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, 0, 1, '2019-04-20 05:13:56', '2019-04-27 15:36:05'),
(2, '1', 'asdf', 'Afghanistan', 'asdf', 'asdf', 'Afghanistan', 'Afghanistan', NULL, 'fdsa', 'asdf', 'asdf', 'asdf', 'asdf', 'sdfas', 'asdf', 'asdf', 'asdfsadfasd', 'Afghanistan', 'asdf', 'asdf', 'dfasdf', 'asdfas', 'Afghanistan', 'asdfas', 'fadsf', '1', 'asdfa', 'fasdf', 'adsfads', 'fadsfdasf', 'asdf', 'Afghanistan', 'dfasdf', 'asfsad', '1', 'fdasfads', 'dfasdf', 'asdf', 'sdfsad', 'asdfa', 'asdf', 'asdfas', 'dfasdf', NULL, 'sdfs', 'asdf', 'fads', 'asdf', 'asdf', NULL, NULL, NULL, 'Afghanistan', 'Afghanistan', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', NULL, 'asdf', '1', 'asdf', 'asdfasdf', 'asdf', 'sadf', 'fasd', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'sdaf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', 'asfdas', 'fsadf', 'adsf', 'asdf', 'asdf', 'dsafdas', 'sdf', 'sfdasfad', 'asdf', 'asdf', 'asdf', NULL, 'asdfsadf', 'asdf', 'asdf', 'asdf', 'asdf', 'asdf', NULL, NULL, NULL, 'asdfsadfdsf', NULL, 'sdfasd', 'fdsafdsa', 'asdfsadf', 'asdf', 'asdf', NULL, 'asdf', 'asdf', NULL, 'fdsafasdf', 'asdfadsfasdf', NULL, NULL, NULL, NULL, NULL, NULL, 'asdf', 'fadsfsd', 'asdf', 'fdsa', 'asdf', NULL, NULL, NULL, 'sdfasdf', 'fsda', 'fasd', 'fdsa', 'asdf', 'sdafs', 'fdas', 'fadsf', 'sdaf', 'adsf', 'asdf', 'fdsa', NULL, NULL, 'asdf', 'f', 'sdf', 'sdf', 'sdf', 'dsf', 's', 'fsa', 'asdf', NULL, 'sa', 'af', 'asdf', 'sdaf', 'sdaf', 'asdasd', 'fasd', NULL, 's', 'df', 'sdf', 'sdf', 'sdf', 'sdf', 'sdf', 'dasdas', 'd', 'sdf', 1, 0, 0, '2019-04-23 15:28:08', '2019-04-23 17:10:30');

-- --------------------------------------------------------

--
-- Table structure for table `seo`
--

CREATE TABLE `seo` (
  `seo_id` int(11) NOT NULL,
  `page_id` int(11) NOT NULL,
  `seo_title` varchar(255) DEFAULT NULL,
  `seo_description` text,
  `seo_keywords` varchar(255) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `seo`
--

INSERT INTO `seo` (`seo_id`, `page_id`, `seo_title`, `seo_description`, `seo_keywords`, `created_at`, `updated_at`) VALUES
(17, 38, 'asdf', 'ASDFADSFASDFASDF', 'asdf', '2019-01-24 16:00:23', '2019-01-24 16:24:13'),
(18, 7, 'Anubhabi Jeeban Bachat Khata', 'asdfadsfadsfdasf', 'assdfasdf', '2019-01-24 16:08:51', '2019-01-24 16:22:35'),
(19, 8, 'Anubhabi Jeeban Bachat Khata', 'ahf;ionasd hfuasd', 'asdfasdf23423fasd', '2019-01-24 16:42:01', '2019-01-24 16:42:01'),
(20, 3, 'Anubhabi Jeeban Bachat Khata', 'asdfasdfasdfa', 'asdfsadf', '2019-01-24 16:53:40', '2019-01-24 16:53:40'),
(21, 49, 'Anubhabi Jeeban Bachat Khata', 'WordStream uses contrasting colors with their CTA to make it stand out against the all-blue site. They also reiterate the fact that their product is free to use twice further enticing you to try it out.', 'asdfasdf23423fasd', '2019-01-29 14:51:59', '2019-01-29 14:51:59'),
(22, 50, 'Anubhabi Jeeban Bachat Khata', 'WordStream uses contrasting colors with their CTA to make it stand out against the all-blue site. They also reiterate the fact that their product is free to use twice further enticing you to try it out.', 'asdfasdf23423fasd', '2019-01-29 14:53:09', '2019-01-29 14:53:09'),
(23, 51, 'asdf', 'You want your CTA to grab the attention of your visitors and really entice them to click.\r\n\r\nShow your', 'asdfasdf23423fasd', '2019-01-29 14:55:48', '2019-01-29 14:55:48'),
(24, 37, 'Anubhabi Jeeban Bachat Khata', 'About United Finance Ltd?Askldf/fdkjjjsdffvsad', 'About United Finance Ltd?Askldf/fdkjjjsdffvsad', '2019-01-30 14:51:49', '2019-01-30 14:51:49'),
(25, 52, 'About United Finance Ltd?Askldf/fdkjjjsdffvsad', 'About United Finance Ltd?Askldf/fdkjjjsdffvsadAbout United Finance Ltd?Askldf/fdkjjjsdffvsadAbout United Finance Ltd?Askldf/fdkjjjsdffvsadAbout United Finance Ltd?Askldf/fdkjjjsdffvsadAbout United Finance Ltd?Askldf/fdkjjjsdffvsadAbout United Finance Ltd?Askldf/fdkjjjsdffvsadAbout United Finance Ltd?Askldf/fdkjjjsdffvsadAbout United Finance Ltd?Askldf/fdkjjjsdffvsad', 'About United Finance Ltd?Askldf/fdkjjjsdffvsadsdadsadsadasdsa', '2019-01-30 14:52:16', '2019-01-30 14:52:16'),
(26, 66, NULL, NULL, NULL, '2019-03-15 12:29:11', '2019-03-15 12:29:11');

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE `settings` (
  `id` int(11) NOT NULL,
  `url` varchar(255) DEFAULT NULL,
  `slug` varchar(200) NOT NULL,
  `value` int(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `file` varchar(255) DEFAULT NULL,
  `text` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `url`, `slug`, `value`, `created_at`, `updated_at`, `file`, `text`) VALUES
(1, '/', 'site_logo', NULL, '2018-11-25 09:36:15', '2019-01-24 15:29:43', 'logo/RUgHpPntFS3k1zUu4fXzDsmfKaaWKxuXj8A7ZvNFWh5Lb.png', NULL),
(2, NULL, 'image_post_max_size', 9216, '2018-11-26 05:44:37', '2019-03-15 12:26:58', NULL, NULL),
(3, NULL, 'file_post_max_size', 8192, '2018-11-26 05:44:42', '2018-11-26 05:44:42', NULL, NULL),
(4, NULL, 'pre_nav_contact_info', NULL, '2018-12-05 16:18:39', '2018-12-05 16:18:39', NULL, '01-4241648, 01-4241645 | IJ plaza, Durbar Marga, Kathmandu, Nepal'),
(6, NULL, 'ac_email', NULL, '2019-04-27 15:21:43', '2019-04-27 15:21:43', NULL, 'sadfsadasdfadsf');

-- --------------------------------------------------------

--
-- Table structure for table `site_users`
--

CREATE TABLE `site_users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `middle_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `dob` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `gender` varchar(50) DEFAULT NULL,
  `marital_status` varchar(50) DEFAULT NULL,
  `blood_group` varchar(50) DEFAULT NULL,
  `citizenship_number` varchar(255) DEFAULT NULL,
  `citizenship_district` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `email_alt` varchar(255) DEFAULT NULL,
  `contact` varchar(255) DEFAULT NULL,
  `contact_office` varchar(255) DEFAULT NULL,
  `permanent_district` varchar(255) DEFAULT NULL,
  `permanent_address` varchar(255) DEFAULT NULL,
  `permanent_telephone` varchar(255) DEFAULT NULL,
  `current_district` varchar(255) DEFAULT NULL,
  `current_address` varchar(255) DEFAULT NULL,
  `current_telephone` varchar(255) DEFAULT NULL,
  `mobile` varchar(255) DEFAULT NULL,
  `current_organization` varchar(255) DEFAULT NULL,
  `current_designation` varchar(255) DEFAULT NULL,
  `current_salary` varchar(255) DEFAULT NULL,
  `experience` varchar(255) DEFAULT NULL,
  `expected_salary` varchar(255) DEFAULT NULL,
  `religion` varchar(255) DEFAULT NULL,
  `ethnicity` varchar(255) DEFAULT NULL,
  `user_bio` text,
  `notice_period` varchar(255) DEFAULT NULL,
  `prefer_working_areas` varchar(255) DEFAULT NULL,
  `jobcategory` varchar(255) DEFAULT NULL,
  `jobtype` varchar(255) DEFAULT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `member_name` varchar(255) DEFAULT NULL,
  `member_relation` varchar(255) DEFAULT NULL,
  `member_dob` varchar(255) DEFAULT NULL,
  `member_phone` varchar(255) DEFAULT NULL,
  `member_occupation` varchar(255) DEFAULT NULL,
  `member_remarks` text,
  `education_level` varchar(255) DEFAULT NULL,
  `degree` varchar(255) DEFAULT NULL,
  `major` varchar(255) DEFAULT NULL,
  `institution` varchar(255) DEFAULT NULL,
  `start_year` varchar(255) DEFAULT NULL,
  `passed_year` varchar(255) DEFAULT NULL,
  `is_running` int(11) NOT NULL DEFAULT '0',
  `cv` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `is_verified` int(11) NOT NULL DEFAULT '0',
  `last_login_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `site_users`
--

INSERT INTO `site_users` (`id`, `first_name`, `middle_name`, `last_name`, `dob`, `country`, `gender`, `marital_status`, `blood_group`, `citizenship_number`, `citizenship_district`, `email`, `email_alt`, `contact`, `contact_office`, `permanent_district`, `permanent_address`, `permanent_telephone`, `current_district`, `current_address`, `current_telephone`, `mobile`, `current_organization`, `current_designation`, `current_salary`, `experience`, `expected_salary`, `religion`, `ethnicity`, `user_bio`, `notice_period`, `prefer_working_areas`, `jobcategory`, `jobtype`, `avatar`, `member_name`, `member_relation`, `member_dob`, `member_phone`, `member_occupation`, `member_remarks`, `education_level`, `degree`, `major`, `institution`, `start_year`, `passed_year`, `is_running`, `cv`, `password`, `is_verified`, `last_login_at`, `created_at`, `updated_at`) VALUES
(1, 'Engineer', 'jkll', 'Editor', '10th July, 2018', 'Kazakhstan', 'Male', 'Single', NULL, '65465465', '1', 'bishal.khatri343@gmail.com', 'bishal.khatri@gmail.com', '1', '2', '1', '3', '4', '2', '5', '6', '7', '8', '9', '11', '10', '12', '13', '14', '15', '16', 'Inside Valley', 'Banking', 'Full Time', NULL, '17', '18', '19', '20', '21', '22', 'Bachelor', '23', '24', '25', '26', '27', 0, NULL, '$2y$10$1OlDQpuszwTx7HfA21189ucQWVJphLSTHdm4g9tiTYSQ8NsVLEjRG', 0, '2019-01-01 08:54:30', '2018-12-21 09:30:00', '2019-01-01 08:54:30'),
(5, 'Engineer', 'jkll', 'Editor', '10th July, 2018', 'Kazakhstan', 'Male', 'Single', NULL, '65465465', '1', 'bishal.khatri342@gmail.com', 'bishal.khatri@gmail.com', '1', '2', '1', '3', '4', '2', '5', '6', '7', '8', '9', '11', '10', '12', '13', '14', '15', '16', 'Inside Valley', 'Banking', 'Full Time', 'site_user/tTfTJY0grvWxtMf6mSdKkm9ZWeHkno1yU9mE92X6.jpeg', '17', '18', '19', '20', '21', '22', 'Bachelor', '23', '24', '25', '26', '27', 0, 'site_user/Z4Nx39moG52bEdkVCIFmPEGyVmZyzPGLnxo75G4u.pdf', '$2y$10$08XI1OAePq7.MACIR18rxOTg5obMTKM9p/qzYCPuHvaNQP8CZQHMu', 0, NULL, '2018-12-21 09:43:05', '2018-12-21 09:43:05'),
(6, 'Bishal', 'Jung', 'Chettri', '2050-1-20', 'Nepal', 'Male', 'Single', NULL, '989-9277-221/233', '26', 'bishal.khatri@gmail.com', 'bishal.game343@gmail.com', '9876543212', '01987678', '26', 'Ilam', '027656543', '35', 'Kathmandu', '9876543212', '9876543212', 'Technology sales Pvt.Ltd', 'software engineer', '30000', '3', '45000', 'Hindu', 'Hindu', NULL, '3', 'Inside Valley', 'Information Technology', 'Full Time', 'site_user/OQkt7s8Ugj2fSemR3m2TLY0Ccqamj275gWgqqcTy.jpeg', 'me', 'friend', '2050-2-21', '9876543212', 'work', NULL, 'Bachelor', 'Bsc CSIT', 'Software', 'New Summit College', '2012', '2017', 0, NULL, '$2y$10$eD.Ok23kVasNks36mAg/LuU422d5ylIjcEC.xqS6EM8hprFwCSKgu', 0, '2019-02-03 15:28:08', '2018-12-22 03:24:31', '2019-02-03 15:28:08');

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL,
  `is_active` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `link` varchar(200) DEFAULT NULL,
  `type_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `button_text` varchar(255) DEFAULT NULL,
  `target` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `is_active`, `title`, `description`, `link`, `type_id`, `created_by`, `created_at`, `updated_at`, `button_text`, `target`) VALUES
(11, 'sliders/pRz5XkxuudfiJHvZFsyKdxclWUwvxRA0YYPKwRz7.jpeg', 1, 'What is Lorem Ipsum?', 'What is Lorem Ipsum?What is Lorem Ipsum?What is Lorem Ipsum?', 'http://google.com', 1, 1, '2019-01-24 15:30:37', '2019-01-24 15:35:05', 'More on Mobile Banking', '_blank'),
(12, 'sliders/L36FJ95ipqTAW4vtyMNXAw3n0AUyNKB9mbDJFjqq.jpeg', 1, NULL, NULL, '', 1, 1, '2019-01-24 15:31:01', '2019-01-24 15:31:49', NULL, '_self'),
(13, 'sliders/LefRCjKGRlUYDAW4k5xKraTxowhFyQUia4WmgcPq.jpeg', 0, NULL, NULL, '', 1, 1, '2019-01-24 15:34:17', '2019-01-24 15:34:17', NULL, '_self'),
(14, 'sliders/78v5V7JSnQqkU88x6j8u1lt8OQe3c0b0JuqKlcPi.jpeg', 0, NULL, NULL, '', 1, 1, '2019-01-24 15:34:25', '2019-01-24 15:34:25', NULL, '_self'),
(15, 'sliders/j1ryIVX2lbvRiRq0rHbJqDm0tRibai6pFZDbWLdU.jpeg', 0, NULL, NULL, '', 1, 1, '2019-01-24 15:37:05', '2019-01-24 15:37:05', NULL, '_self'),
(16, 'sliders/E8zEZB31lttqFU5qOnDI9KHlzKE8FJNN8NzqoBR6.jpeg', 0, NULL, NULL, '', 1, 1, '2019-01-24 15:37:14', '2019-01-24 15:37:14', NULL, '_self');

-- --------------------------------------------------------

--
-- Table structure for table `slider_type`
--

CREATE TABLE `slider_type` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `status` int(11) DEFAULT '0',
  `slug` varchar(45) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `slider_type`
--

INSERT INTO `slider_type` (`id`, `name`, `status`, `slug`, `created_at`, `updated_at`) VALUES
(1, 'Uncategorized', 0, 'uncategorized', '2018-11-23 05:10:52', '2018-11-23 05:10:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contact` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_login_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `is_super` int(11) DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `contact`, `address`, `email`, `email_verified_at`, `password`, `last_login_at`, `created_by`, `is_super`, `remember_token`, `created_at`, `updated_at`, `image`) VALUES
(1, 'Bishal Jung', 'Chettri', '9856325412', 'Nepal', 'bishal.khatri343@gmail.com', NULL, '$2y$10$YrJ3CUdZ9HQ/DbQkvfO.Oe9PIiT8ZTul8Pj7v5XioTDXrfY/fngMS', '2019-04-27 14:19:40', NULL, 1, '25rSI9ZPjQTm3THknmdUG1gnQM01PfY4YyXc7rcS3qKSiwyE9uVTvKYvXirD', '2018-09-10 23:29:31', '2019-04-27 14:19:40', 'profile/cRaLJguuPPqWtnE10RXpbVj1pha8Pz1YmqbT9Jjp.jpeg'),
(9, 'Samir', 'Giri', '0', 'Kathmandu', 'sameergiri@hotmail.com', NULL, '$2y$10$dPlz1yfmxWoq1r2Jqkj1me9c0yFmQ9mWeqM/lTKuZ9VMNtsItXoC6', '2018-12-10 18:43:43', NULL, 0, 'VuPCnEMYxODQQoVFAWvS1O8QzSEhsw7YIbiPDGeXqpJ5ITrTQ5rn6gYH8kxy', '2018-11-25 16:51:12', '2018-12-10 18:43:43', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `weblinks`
--

CREATE TABLE `weblinks` (
  `weblink_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `slug` varchar(255) NOT NULL,
  `weblink_icon` varchar(255) DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weblinks`
--

INSERT INTO `weblinks` (`weblink_id`, `name`, `slug`, `weblink_icon`, `created_by`, `created_at`, `updated_at`) VALUES
(1, 'Features', 'features', NULL, 1, '2018-11-23 05:06:56', '2018-11-23 05:06:56'),
(3, 'Deposit', 'deposit', 'weblink/ll21lGFMFHjtLpfaDUCQbX0AtpiH3Rmf2dcQDdL0.png', 1, '2018-11-25 09:12:24', '2018-11-25 09:12:24'),
(4, 'Loan & Advances', 'loan-&-advances', 'weblink/ddrQQbnzpMyCakezhhFg6VCfmY1Rzj2JcMkJxUTk.png', 1, '2018-11-25 09:31:23', '2018-11-25 09:31:23');

-- --------------------------------------------------------

--
-- Table structure for table `weblink_post`
--

CREATE TABLE `weblink_post` (
  `weblink_post_id` int(11) NOT NULL,
  `weblink_id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `display_name` varchar(255) DEFAULT NULL,
  `description` text,
  `created_by` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `weblink_post`
--

INSERT INTO `weblink_post` (`weblink_post_id`, `weblink_id`, `post_id`, `icon`, `display_name`, `description`, `created_by`, `created_at`, `updated_at`) VALUES
(2, 1, 4, 'weblink/eoc6WnidDjE8StDMrI3uPejX1LXXzBaFNG8ZzbFd.png', 'Mobile Banking', NULL, 1, '2018-11-25 08:45:40', '2018-11-25 08:45:54'),
(3, 1, 3, 'weblink/x3udUm4fFRRIUklGyez3IPPy3sW8XIbt68a0fGTS.png', 'Safe Deposit Locker', NULL, 1, '2018-11-25 08:59:47', '2018-11-25 09:01:22'),
(4, 3, 5, NULL, NULL, NULL, 1, '2018-11-25 09:12:33', '2018-11-25 09:12:33'),
(5, 3, 6, NULL, NULL, NULL, 1, '2018-11-25 09:13:32', '2018-11-25 09:13:32'),
(6, 3, 7, NULL, NULL, NULL, 1, '2018-11-25 09:13:32', '2018-11-25 09:13:32'),
(7, 4, 3, NULL, NULL, NULL, 1, '2018-11-25 09:31:31', '2018-11-25 09:31:31'),
(8, 4, 4, NULL, NULL, NULL, 1, '2018-11-25 09:31:31', '2018-11-25 09:31:31'),
(9, 4, 5, NULL, NULL, NULL, 1, '2018-11-25 09:31:31', '2018-11-25 09:31:31'),
(10, 4, 6, NULL, NULL, NULL, 1, '2018-11-25 09:31:32', '2018-11-25 09:31:32'),
(11, 4, 7, NULL, NULL, NULL, 1, '2018-11-25 09:31:32', '2018-11-25 09:31:32'),
(17, 4, 6, NULL, NULL, NULL, 1, '2018-11-28 14:25:27', '2018-11-28 14:25:27'),
(19, 4, 28, NULL, NULL, NULL, 1, '2018-12-27 11:12:07', '2018-12-27 11:12:07'),
(20, 4, 29, NULL, NULL, NULL, 1, '2018-12-27 11:12:22', '2018-12-27 11:12:22'),
(21, 4, 30, NULL, NULL, NULL, 1, '2018-12-27 11:12:42', '2018-12-27 11:12:42'),
(22, 4, 31, NULL, NULL, NULL, 1, '2018-12-27 11:12:54', '2018-12-27 11:12:54'),
(23, 3, 32, NULL, NULL, NULL, 1, '2018-12-27 11:13:05', '2018-12-27 11:13:05'),
(24, 1, 33, NULL, NULL, NULL, 1, '2018-12-27 11:13:15', '2018-12-27 11:13:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `auth_permission_user`
--
ALTER TABLE `auth_permission_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `downloads_categories`
--
ALTER TABLE `downloads_categories`
  ADD PRIMARY KEY (`downloads_category_id`);

--
-- Indexes for table `downloads_items`
--
ALTER TABLE `downloads_items`
  ADD PRIMARY KEY (`download_items_id`);

--
-- Indexes for table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_images`
--
ALTER TABLE `gallery_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `job_application`
--
ALTER TABLE `job_application`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menus`
--
ALTER TABLE `menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu_post`
--
ALTER TABLE `menu_post`
  ADD PRIMARY KEY (`menu_post_id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_category`
--
ALTER TABLE `post_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `post_meta`
--
ALTER TABLE `post_meta`
  ADD PRIMARY KEY (`post_meta_id`);

--
-- Indexes for table `request_form`
--
ALTER TABLE `request_form`
  ADD PRIMARY KEY (`form_id`);

--
-- Indexes for table `seo`
--
ALTER TABLE `seo`
  ADD PRIMARY KEY (`seo_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_users`
--
ALTER TABLE `site_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `slider_type`
--
ALTER TABLE `slider_type`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `weblinks`
--
ALTER TABLE `weblinks`
  ADD PRIMARY KEY (`weblink_id`);

--
-- Indexes for table `weblink_post`
--
ALTER TABLE `weblink_post`
  ADD PRIMARY KEY (`weblink_post_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_permissions`
--
ALTER TABLE `auth_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `auth_permission_user`
--
ALTER TABLE `auth_permission_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=171;

--
-- AUTO_INCREMENT for table `downloads_categories`
--
ALTER TABLE `downloads_categories`
  MODIFY `downloads_category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `downloads_items`
--
ALTER TABLE `downloads_items`
  MODIFY `download_items_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `gallery_images`
--
ALTER TABLE `gallery_images`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `job_application`
--
ALTER TABLE `job_application`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `medias`
--
ALTER TABLE `medias`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menus`
--
ALTER TABLE `menus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `menu_post`
--
ALTER TABLE `menu_post`
  MODIFY `menu_post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=67;

--
-- AUTO_INCREMENT for table `post_category`
--
ALTER TABLE `post_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `post_meta`
--
ALTER TABLE `post_meta`
  MODIFY `post_meta_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `request_form`
--
ALTER TABLE `request_form`
  MODIFY `form_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `seo`
--
ALTER TABLE `seo`
  MODIFY `seo_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `settings`
--
ALTER TABLE `settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `site_users`
--
ALTER TABLE `site_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `slider_type`
--
ALTER TABLE `slider_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `weblinks`
--
ALTER TABLE `weblinks`
  MODIFY `weblink_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `weblink_post`
--
ALTER TABLE `weblink_post`
  MODIFY `weblink_post_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
