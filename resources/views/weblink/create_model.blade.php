{{--ADD FORM--}}
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<form action="{{ route('weblink.add') }}" method="post" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel1"><i class="fa fa-plus-circle" id="icon-terminate" ></i> Create new</h4> </div>
				<div class="modal-body">
					@csrf
					<div class="form-group @if ($errors->has('name')) has-error @endif">
						<label for="input-file-now-custom-3"><small>Icon</small></label>
						<input type="file" name="weblink_icon" id="input-file-now-custom-3" class="dropify" data-height="120" data-default-file="@if(!empty($value->icon)){{ asset(STATIC_DIR.'storage/'.$value->icon) }}@endif" />
						@if ($errors->has('weblink_icon'))
							<span class="text-danger" role="alert">
							<p>{{ $errors->first('weblink_icon') }}</p>
						</span>
						@endif
					</div>
					<div class="form-group @if ($errors->has('name')) has-error @endif">
						<label for="name" class="control-label"><small>Title</small></label>
						<input type="text" class="form-control" name="name" placeholder="Enter weblink name here" value="{{ old('name') }}">
						@if ($errors->has('name'))
							<span class="text-danger" role="alert">
							<p>{{ $errors->first('name') }}</p>
						</span>
						@endif
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success btn-outline btn-sm" id="confirm_yes"><i class="icon-check"></i> Save</button>
				</div>
			</div>
		</form>
	</div>
</div>