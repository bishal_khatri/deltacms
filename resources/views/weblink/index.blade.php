@extends('layouts.app')
@section('css')
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet">

@endsection
@section('page_title')
	Listing Weblink
@endsection
@section('right_button')
	<a href="#add" class="btn btn-info btn-outline pull-right" data-toggle="modal" ><i class="fa fa-plus"></i>&nbsp;Create new</a>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div class="row">
					<div class="col-lg-12 col-md-12 col-sm-12">
						@if($weblinks->count()>0)
							@foreach($weblinks as $value)
								<div class="steamline">
									<div class="sl-item">
										<a href="{{ route('profile.view',$value->user->id) }}" class="text-primary">
											<div class="sl-left bg-info">
												<i class="fa fa-image"></i>
											</div>
										</a>
										<div class="sl-right">
											<h4>
												<a href="{{ route('weblink.view',$value->weblink_id) }}" class="text-primary">{{ $value->name ?? '' }} [ {{ $value->weblink_post->count() }} items ]</a>
												<sup><a href="#addPost" class="btn btn-link btn-sm" data-id="{{ $value->weblink_id }}" data-toggle="modal">add page</a></sup>
												<sup><a href="#addLink" class="btn btn-link btn-sm" data-id="{{ $value->weblink_id }}" data-toggle="modal">add custom link</a></sup>
												<sup><a href="#deleteWeblink" data-toggle="modal" data-id="{{ $value->weblink_id }}" class="text-sm text-danger">Delete</a></sup>
											</h4>
											<div class="desc">
												<div>
													<a href="{{ route('profile.view',$value->user->id) }}">
														@if(!empty($value->user->first_name) AND !empty($value->user->last_name))
															<span class="text-primary">{{ $value->user->first_name }} {{ $value->user->last_name }}</span>
														@else
															{{ $value->user->email }}
														@endif
													</a>
													<span class="sl-date">{{ $value->created_at->diffForHumans() }}</span>
												</div>
											</div>
										</div>
									</div>
								</div>
							@endforeach
						@else
							<p>No items found &nbsp; <a href="#add" class="btn btn-link" data-toggle="modal" >Create new</a></p>
						@endif
					</div>
				</div>
			
			</div>
		</div>
	</div>
	
	@include('weblink.create_model')
	@include('weblink.add_post_modal')
	@include('weblink.add_link_modal')
	@include('weblink.delete_modal')
@endsection

@section('scripts')
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
	<script>
        $('.dropify').dropify();
        // ADD POST TO WEB LINK
        $('#addPost').on('show.bs.modal', function(e){
            var button = $(e.relatedTarget);
            var id = button.data('id');
            $('#weblink_id').val(id);
        });

        // ADD LINK TO WEB LINK
        $('#addLink').on('show.bs.modal', function(e){
            var button = $(e.relatedTarget);
            var id = button.data('id');
            $('#link_weblink_id').val(id);
        });

        // AJAX WEBLINK DELETE
        $('#deleteWeblink').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#delete_weblink_id").val(id);
        });

        $('#deleteWeblink').find('.modal-footer #confirm_yes').on('click', function () {
            var id = $("#delete_weblink_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('weblink.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    window.location.reload();
                }
            });
        });
	</script>
@endsection
