<div class="modal fade" id="addPost" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<form action="{{ route('weblink.item.addPost') }}" method="post" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel1"><i class="fa fa-plus-circle" id="icon-terminate" ></i> Add post</h4> </div>
				<div class="modal-body">
					@csrf
					<input type="hidden" name="weblink_id" value="" id="weblink_id">
					<input type="hidden" name="type" value="post">
					<div class="form-group">
						@if (isset($posts))
							@foreach ($posts as $val)
								<div class="checkbox checkbox-custom">
									<input id="checkbox11" type="checkbox" name="page_id[]" multiple="" value="{{ $val->id }}">
									<label for="checkbox11"> {{ $val->post_title }}</label>
								</div>
							@endforeach
						@endif
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success btn-outline btn-sm" id="confirm_yes"><i class="icon-check"></i> Save</button>
				</div>
			</div>
		</form>
	</div>
</div>