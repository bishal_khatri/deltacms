<div class="modal fade" id="addLink" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<form action="{{ route('weblink.item.addPost') }}" method="post" enctype="multipart/form-data">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel1"><i class="fa fa-plus-circle" id="icon-terminate" ></i> Add post</h4> </div>
				<div class="modal-body">
					@csrf
					<input type="hidden" name="weblink_id" value="" id="link_weblink_id">
					<input type="hidden" name="type" value="custom_link">
					<div class="form-group">
						<label for="checkbox11"> Name </label>
						<input type="text" name="link_name" class="form-control">
					</div>
					<div class="form-group">
						<label for="checkbox11"> Link </label>
						<input type="text" name="link" class="form-control" value="http://">
					</div>
                    <div class="form-group @if ($errors->has('target')) has-error @endif">
                        <label for="target" class="control-label">Target</label>
                        <select name="target" id="" class="form-control">
                            <option value="_self">Parent</option>
                            <option value="_blank">New Tab</option>
                        </select>
                        @if ($errors->has('target'))
                            <span class="text-danger" role="alert">
                                {{ $errors->first('target') }}
                            </span>
                        @endif
                    </div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success btn-outline btn-sm" id="confirm_yes"><i class="icon-check"></i> Save</button>
				</div>
			</div>
		</form>
	</div>
</div>
