@extends('layouts.app')
@section('css')
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet">
	<style>
		.collapsible {
			cursor: pointer;
			padding-right: 20px;
			border: none;
		}
		.content {
			padding-top: 20px;
			display: none;
			overflow: hidden;
		}
		.myadmin-dd-empty .dd-list button {
			width: auto;
			height: auto;
			font-size: 12px;
			margin-top: 12px;
		}
	</style>
@endsection
@section('page_title')
	Listing Weblinks For [ {{ $weblink->name }} ]
@endsection
@section('right_button')
	<div class="pull-right">
		<a href="#addPost" data-id="{{ $weblink->weblink_id }}" data-toggle="modal" class="btn btn-info btn-outline " role="button"><i class="fa fa-plus"></i> Add Page</a>
		<a href="#addLink" data-id="{{ $weblink->weblink_id }}" data-toggle="modal" class="btn btn-info btn-outline " role="button"><i class="fa fa-plus"></i> Add Link</a>
		<a href="{{ route('weblink.index') }}" class="btn btn-info btn-outline" role="button"><i class="fa fa-undo"></i> Back</a>
	</div>
@stop
@section('content')
	<div class="row">
		<div class="col-lg-12 col-md-12 col-sm-12">
			<ul class="list-group list-group-full">
				@if($weblinkPost->count()>0)
					@foreach($weblinkPost as $value)
						{{-- {{ dd($value) }} --}}
						<li class="list-group-item" style="margin-bottom: 3px;">
							<a class="link" href="{{ route('weblink.item.remove',$value->weblink_post_id) }}"><i class="mdi mdi-close text-danger pull-right"></i></a>
							<a class="collapsible pull-right"><i id="menu-edit-icon" class="fa fa-caret-down"></i></a>
							{{ $value->display_name ?? $value->post->post_title }}
							<div class="content">
								<form action="{{ route('weblink.item.update') }}" method="post" enctype="multipart/form-data">
									@csrf
									<input type="hidden" name ="weblink_post_id" value="{{ $value->weblink_post_id }}">
									@if(!empty($value->display_name))
										[<em><small class="form-italic">Original name: <a>{{ $value->post->post_title }}</a></small></em>]<br>
								@endif
								<!--ITEM DISPLAY NAME -->
									<div class="row">
										<div class="col-md-9">
											<label for=""><small>Display Name</small></label>
											<input type="text" class="form-control menu-edit" name="display_name" value="{{ $value->display_name ?? $value->post->post_title }}">
											<label for=""><small>Description</small></label>
											<textarea class="form-control" name="description" id="" cols="8" rows="3">{{ $value->description ?? '' }}</textarea>
										</div>
										<div class="col-md-3">
											<label for="icon"><small>Icon</small></label>
											<input type="file" name="icon" id="input-file-now-custom-3" class="dropify" data-height="120" data-default-file="@if(!empty($value->icon)){{ asset(STATIC_DIR.'storage/'.$value->icon) }}@endif" />
										</div>
									</div>
									<button type="submit" style="margin-top: 5px;" class="btn btn-success btn-outline btn-sm pull-right"><i class="icon-check"></i> Save</button>
								</form>
							</div>
						</li>
					@endforeach
				@else
					<p>No items found &nbsp; <a href="#addPost" data-id="{{ $weblink->weblink_id }}" data-toggle="modal" class="btn btn-link" >Add new</a></p>
				@endif
			</ul>
		
		</div>
	</div>
	
	@include('weblink.add_post_modal')
	@include('weblink.add_link_modal')
@endsection

@section('scripts')
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
	<script>
        $('#slimtest1').slimScroll({
            height: '150px'
        });
        $('.dropify').dropify();

        // ADD POST TO WEB LINK
        $('#addPost').on('show.bs.modal', function(e){
            var button = $(e.relatedTarget);
            var id = button.data('id');
            $('#weblink_id').val(id);
        });

        // ADD LINK TO WEB LINK
        $('#addLink').on('show.bs.modal', function(e){
            var button = $(e.relatedTarget);
            var id = button.data('id');
            $('#link_weblink_id').val(id);
        });
	</script>
	
	<script>
        var coll = document.getElementsByClassName("collapsible");
        var i;

        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var content = this.nextElementSibling;
                if (content.style.display === "block") {
                    content.style.display = "none";
                } else {
                    content.style.display = "block";
                }
            });
        }
	</script>
@endsection
