{{--DELETE IMAGE MODAL--}}
<div class="modal fade bs-modal-sm" id="deleteWeblink" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i>&nbsp;Delete
				</h4>
			</div>
			<div class="modal-body">Delete this Item? </div>
			<input type="hidden" id="delete_weblink_id">
			<div class="modal-footer">
				<button type="button" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
				</button>
				<button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
					No
				</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
</div>