@extends('layouts.app')
@section('css')
    <style>
        .switch {
            position: relative;
            display: inline-block;
            width: 60px;
            height: 34px;
        }
        
        .switch input {
            opacity: 0;
            width: 0;
            height: 0;
        }
        
        .slider {
            position: absolute;
            cursor: pointer;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
            background-color: #ccc;
            -webkit-transition: .4s;
            transition: .4s;
        }
        
        .slider:before {
            position: absolute;
            content: "";
            height: 26px;
            width: 26px;
            left: 4px;
            bottom: 4px;
            background-color: white;
            -webkit-transition: .4s;
            transition: .4s;
        }
        
        input:checked + .slider {
            background-color: #2196F3;
        }
        
        input:focus + .slider {
            box-shadow: 0 0 1px #2196F3;
        }
        
        input:checked + .slider:before {
            -webkit-transform: translateX(26px);
            -ms-transform: translateX(26px);
            transform: translateX(26px);
        }
        
        /* Rounded sliders */
        .slider.round {
            border-radius: 34px;
        }
        
        .slider.round:before {
            border-radius: 50%;
        }
    </style>
@endsection
@section('page_title')
    All pages
@endsection
@section('right_button')
    <a href="{{ route('pages.add') }}" class="btn btn-block btn-info btn-outline" data-toggle="modal" ><i class="fa fa-plus"></i>&nbsp;Create new page</a>
@stop
@section('content-title')
	<h2>
		All pages
	</h2>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <a href="{{ route('pages.index','trashed=true') }}" class="pull-right btn btn-link btn-sm"><span class="fa fa-trash text-danger"></span>&nbsp;Trash({{ $trash_count }})</a>
            <a href="{{ route('pages.index') }}" class="pull-right btn btn-link btn-sm">List pages</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table id="datatable" class="table table-borderless table-striped" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th style="width: 700px;">Page Title</th>
                            <th>Author</th>
	                        <th>Make Featured</th>
	                        <th>Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($pages->count()>0)
                            @foreach($pages as $val)
                                <tr class="title">
                                    <td>
                                        {{ $val->post_title }}
                                        @if($val->post_status == 'draft')
                                        <span style="font-style: italic;padding-left: 50px;" class="text-danger">(draft)</span>
                                        @elseif($val->post_status=='publish')
                                        <span class="text-success" style="font-style: italic;padding-left: 50px;">&nbsp;&nbsp;(Published)</span>
                                        @endif
                                        <div class="action">
                                            @if($val->is_trashed==1)
                                                <a class="btn btn-link btn-sm text-success" href="#confirmTrash" data-ids="{{ $val->id }}" data-msg="Do you want to restore this page ?" data-action="restore" data-toggle="modal">Restore</a>
                                                <a class="btn btn-link btn-sm text-danger" href="#confirmTrash" data-ids="{{ $val->id }}" data-msg="Do you want to delete this page ?" data-action="delete" data-toggle="modal">Delete</a>
                                            @else
                                                <a href="{{ route('front_page',$val->post_slug) }}" target="_blank" class="btn btn-link btn-sm">Preview</a>
                                                <span class="vl"></span>
                                                <a href="{{ route('pages.edit',$val->id) }}" class="btn btn-link btn-sm">Edit</a>
                                                <span class="vl"></span>
                                                <a class="btn btn-link btn-sm text-danger" href="#confirmTrash" data-ids="{{ $val->id }}" data-msg="Do you want to move this page to Trash ?" data-action="trash" data-toggle="modal">Trash</a>
											@endif
                                        </div>
                                    </td>
                                    <td><a href="{{ route('profile.view',$val->user->id) }}" calss="btn btn-link btn-sm">{{ $val->user->first_name }} {{ $val->user->last_name }}</a></td>
	                                <td>
		                                <label class="switch">
			                                <input class="feature_switch" type="checkbox" value="{{ $val->id }}" @if($val->is_featured==1) checked @endif>
			                                <span class="slider round"></span>
		                                </label>
	                                </td>
                                    <td>{{ $val->created_at->diffForHumans() }}</td>
	                                
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">No items found <a href="{{ route('pages.add') }}" class="btn btn-link btn-sm" data-toggle="modal" >Create new page</a></td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>

@include('pages.modal')
@endsection

@section('scripts')
	<script>
        $('#datatable').DataTable({
            dom: 'Bfrtip',
            columnDefs: [ { "orderable": false, "visible": true } ],
        });

        $('#add').find('.modal-footer #confirm_yes').on('click', function () {
            $("#confirmDelete").modal("hide");
            window.location.reload();

        });

        // MOVE TO TRASH
        $('#confirmTrash').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            // console.log(button.data('ids'));
            var ids = button.data('ids');
            var msg = button.data('msg');
            var action = button.data('action');
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#hidden_id").val(ids);
            $("#msg").html(msg);
            $("#action").val(action);
        });

        $('#confirmTrash').find('.modal-footer #confirm_yes').on('click', function () {
            //$(this).data('form').submit();
            var id = $("#hidden_id").val();
            var action = $("#action").val();
            // console.log(id);
            $.ajax({
                type: "POST",
                url: "{{ route('pages.recycle') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: {id: id , action: action},
                success: function (msg) {
                    window.location.reload();
                }
            });
        });
        // AJAX DELETE PERMISSION END
		
		$('.feature_switch').on('change', function(e){
		    var post_id = $(this).val();
            $.ajax({
                type: "POST",
                url: "{{ route('pages.make_featured') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + post_id,
                success: function (msg) {
                    window.location.reload();
                }
            });
		})
	</script>
@endsection
