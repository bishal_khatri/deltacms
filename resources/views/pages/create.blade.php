@extends('layouts.app')
@section('css')
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet">
@endsection
@section('page_title')
	Create new page
@endsection
@section('right_button')
	<a href="{{ route('pages.add') }}" class="btn btn-block btn-info btn-outline" data-toggle="modal" ><i class="fa fa-plus"></i>&nbsp;Create new</a>
@stop

@section('content')
	<form class="form-horizontal" method="post" action="{{ route('pages.add') }}" enctype="multipart/form-data">
		@csrf
		<input type="hidden" value="page" name="post_type">
		<div class="row">
			<div class="col-md-12">
				<div class="white-box">
					<div class="row pull-right">
						<div class="col-md-12">
							<button type="submit" name="draft" class="btn btn-success btn-outline btn-sm" value="draft">Draft</button>
							<button type="submit" name="publish" class="btn btn-info btn-outline btn-sm" value="publish">Publish</button>
						</div>
					</div>
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">
								<span class="visible-xs"><i class="ti-home"></i></span>
								<span class="hidden-xs"> Page Content</span>
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#cover" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">
								<span class="visible-xs"><i class="ti-user"></i></span>
								<span class="hidden-xs">Add Cover Image</span>
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#seo" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false">
								<span class="visible-xs"><i class="ti-email"></i></span>
								<span class="hidden-xs">SEO</span>
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#category" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false">
								<span class="visible-xs"><i class="ti-settings"></i></span>
								<span class="hidden-xs">Add Category</span>
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#gallery" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false">
								<span class="visible-xs"><i class="ti-settings"></i></span>
								<span class="hidden-xs">Add Gallery</span>
							</a>
						</li>
                        <li role="presentation" class="">
                            <a href="#call-to-action" aria-controls="call-to-action" role="tab" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="ti-link"></i></span>
                                <span class="hidden-xs">Add Call To Action</span>
                            </a>
                        </li>
						<li role="presentation" class="">
							<a href="#social" aria-controls="settings" role="tab" data-toggle="tab" aria-expanded="false">
								<span class="visible-xs"><i class="ti-settings"></i></span>
								<span class="hidden-xs">Share</span>
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="home">
							<div class="col-md-12">
								<div class="form-group">
									<input type="text" class="form-control" placeholder="Enter title here" name="post_title" value = "{{ old('post_title') }}" id="title">
									@if ($errors->has('post_title'))
										<span class="invalid-feedback text-danger" role="alert">
                                            {{ $errors->first('post_title') }}
                                        </span>
									@endif
									<br>
									<strong>Slug: </strong>
									<input type="text" class="form-control" name="post_slug" id="post_slug">
									<input type="hidden" class="form-control" name="post_url" id="post_url" value="{{ old('post_url') }}">
									@if ($errors->has('post_url'))
										<span class="invalid-feedback text-danger" role="alert">
                                            {{ $errors->first('post_url') }}
                                        </span>
									@endif
								</div>
								<div class="form-group">
									<textarea rows="8" class="form-control" placeholder="Enter sub-title here" name="post_subtitle"  id="post_subtitle">{{ old('post_subtitle') }}</textarea>
								</div>
								<div class="form-group">
									<textarea id="ckeditor" name="post_content">{{ old('post_content') }}</textarea>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div role="tabpanel" class="tab-pane" id="cover">
							<div class="col-md-12">
								<div class="form-group">
									<div class="row">
										<div class="col-md-12">
											<div class="text-center">
												<i class="fa fa-plus"></i>&nbsp;Insert Cover Image <br><br>
												<img class="text-center" id="holder" alt="" style="min-width: 200px;min-height: 200px;max-width: 300px;max-height: 300px;" alt="Cover Image"> <br><br>
												<a id="sitelogo" data-input="thumbnail" data-preview="holder" class="btn btn-block btn-primary btn-sm">
													<i class="fa fa-picture-o"></i> Choose
												</a>
											</div>
											<input id="thumbnail" class="form-control" type="hidden" value="" name="cover_image">
										</div>
									</div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div role="tabpanel" class="tab-pane" id="seo">
							<div class="col-md-12">
								<div class="form-group">
									<label for="title">SEO Title</label>
									<input type="text" name="seo_title" id="" class="form-control" placeholder="Enter SEO title" value="{{ old('seo_title') }}">
								</div>
								<div class="form-group">
									<label for="title">SEO Key Words</label>
									<input type="text" name="seo_keywords" id="" class="form-control" placeholder="Enter SEO key words" value="{{ old('seo_keywords') }}">
								</div>
								<div class="form-group">
									<label for="title">SEO Description</label>
									<textarea name="seo_description" class="form-control" id="" cols="30" rows="10" placeholder="Enter SEO description">{{ old('seo_description') }}</textarea>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div role="tabpanel" class="tab-pane" id="category">
							<div class="col-md-12">
								<table class="table table-responsive">
									<thead>
									<tr>
										<th></th>
										<th>Title</th>
										<th>Author</th>
									</tr>
									</thead>
									<tbody>
									@if(!empty($categories))
										@foreach($categories as $value)
											<tr>
												<td><input type="checkbox" name="category_id[]" value="{{ $value->id }}" id=""></td>
												<td><a href="" target="_blank">{{ $value->category_title ?? '' }}</a></td>
												<td>{{ $value->user->first_name ?? '' }} {{ $value->user->last_name ?? '' }}</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td colspan="3">No data available</td>
										</tr>
									@endif
									</tbody>
								</table>
							</div>
							<div class="clearfix"></div>
						</div>
						<div role="tabpanel" class="tab-pane" id="gallery">
							<div class="col-md-12">
								<table class="table table-responsive">
									<thead>
									<tr>
										<th></th>
										<th>Title</th>
										<th>Author</th>
									</tr>
									</thead>
									<tbody>
									@if(isset($galleries))
										@foreach($galleries as $value)
											<tr>
												<td><input type="checkbox" name="gallery_id[]" value="{{ $value->id }}" id=""></td>
												<td><a href="{{ route('gallery.view',$value->id) }}" target="_blank">{{ $value->gallery_name ?? '' }}</a></td>
												<td>{{ $value->user->first_name ?? '' }} {{ $value->user->last_name ?? '' }}</td>
											</tr>
										@endforeach
									@else
										<tr>
											<td colspan="3">No data available</td>
										</tr>
									@endif
									</tbody>
								</table>
							</div>
							<div class="clearfix"></div>
						</div>
                        <div role="tabpanel" class="tab-pane" id="call-to-action">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label for="title">Image</label>
                                                <input type="file" name="cta_image">
                                            </div>
                                            <div class="form-group @if ($errors->has('image')) has-error @endif">
                                                <label for="image" class="control-label">Slider Image:</label>
                                                <input type="file" id="input-file-now" name="image" class="dropify" accept="image/x-png,image/gif,image/jpeg" />
                                                @if ($errors->has('image'))
                                                    <span class="text-danger" role="alert">
										                {{ $errors->first('image') }}
									                </span>
                                                @endif
                                                <p style="font-style: italic;color: darkorange;">Appropriate file type: .jpg .jpeg .png</p>
                                            </div>
                                            <div class="form-group">
                                                <label for="title">Title</label>
                                                <input type="text" name="cta_title" id="" class="form-control" placeholder="Enter title" value="{{ old('cta_title') }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="title">Description</label>
                                                <textarea name="cta_description" class="form-control" id="" cols="30" rows="10" placeholder="Enter description">{{ old('cta_description') }}</textarea>
                                            </div>
                                            <p>Button</p>
                                            <div class="form-group">
                                                <label for="title">Button Text</label>
                                                <input type="text" name="cta_button_text" id="" class="form-control" placeholder="Enter button text" value="{{ old('cta_button_text') }}">
                                            </div>
                                            <div class="form-group">
                                                <label for="title">Button Link</label>
                                                <input type="text" name="cta_button_link" id="" class="form-control" placeholder="Enter button link" value="{{ old('cta_button_text') }}">
                                            </div>
                                            <div class="form-group">
                                                <input type="checkbox" name="cta_button_target" value="_blank">
                                                <label for="title">Open in new tab?</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
						<div role="tabpanel" class="tab-pane" id="social">
							<div class="col-md-12">
								<div class="form-group">
									<td>Share on social media</td>
									<br><br>
									<td>
										<input type="checkbox" name="facebook_share" value="1" id="" class="checkbox-inline" @if(old('facebook_share')==1) checked @endif>
										<img src="{{ asset(STATIC_DIR.'assets/icons/facebook.png') }}" alt="" width="50">
									</td>
									<br><br>
									<td>
										<input type="checkbox" name="twitter_share" value="1" id="" class="checkbox-inline" @if(old('twitter_share')==1) checked @endif>
										<img src="{{ asset(STATIC_DIR.'assets/icons/twitter.png') }}" alt="" width="47">
									</td>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
	
	@include('pages.modal')
@endsection

@section('scripts')
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/ckeditor/ckeditor.js') }}"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
	<script>
        $('.dropify').dropify();
        $('#title').on('focusout',function(){
            var title = $('#title').val();
            $.ajax({
                type: "POST",
                url: "{{ route('pages.create_url') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "title=" + title,
                success: function (msg) {
                    $('#post_url').val(msg.url);
                    $('#post_slug').val(msg.slug);
                }
            });
        });

        CKEDITOR.replace( 'ckeditor' ,{
            filebrowserImageBrowseUrl: "{{ url(STATIC_DIR.'laravel-filemanager?type=Images') }}",
            filebrowserImageUploadUrl: "{{ url(STATIC_DIR.'laravel-filemanager/upload?type=Images&_token=') }}",
            filebrowserBrowseUrl: "{{ url(STATIC_DIR.'laravel-filemanager?type=Files') }}",
            filebrowserUploadUrl: "{{ url(STATIC_DIR.'laravel-filemanager/upload?type=Files&_token=') }}"
        });
	</script>
	<script src="{{asset(STATIC_DIR.'vendor/laravel-filemanager/js/lfm.js')}}"></script>
	<script>
        $(function () {
            $('#sitelogo').filemanager('image',{prefix:"{{URL(STATIC_DIR.'laravel-filemanager')}}"});
        });
	</script>
@endsection
