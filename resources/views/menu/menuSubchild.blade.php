<ol class="dd-list">
	@foreach($child->children as $subchild)
		<li class="dd-item dd3-item" data-id="{!! $subchild->menu_post_id !!}">
			<div class="dd-handle dd3-handle"></div>
			<div class="dd3-content">
				<form action="{{ route('menu.edit_menu') }}" method="post">
					@csrf
					{!! $subchild->menu_display_name ?? $subchild->post->post_title !!}
					<span style="font-weight: lighter; padding-left: 20px;font-style: italic;">
																                        @if($subchild->post->post_type=='page')
							<span>page</span>
						@elseif($subchild->post->post_type=='custom_link')
							<span>Custom Link</span>
						@else
							<span></span>
						@endif
														                            </span>
					<a class="link" href="{{ route('menu.remove_item',$subchild->menu_post_id) }}">
						<i class="mdi mdi-close text-danger pull-right"></i>
					</a>
					<a class="collapsible pull-right">
						<i id="menu-edit-icon" class="fa fa-caret-down"></i>
					</a>
					<div class="content">
						@include('menu.editMenuFormSubchild')
					</div>
				</form>
			</div>
		</li>
	@endforeach
</ol>