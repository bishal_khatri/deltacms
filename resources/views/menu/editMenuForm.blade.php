<input type="hidden" name ="menu_post_id" value="{{ $value->menu_post_id }}">
[<em><small class="form-italic">Original name: <a>{{ $value->post_title }}</a></small></em>]<br>
<!--MENU DISPLAY NAME -->
<small>Display Name</small><br>
<input type="text" class="form-control menu-edit" name="menu_display_name" value="{{ $value->menu_display_name ?? $value->post_title }}">
<!--MENU ICON -->
<small>Icon</small>
<input type="text" name="menu_icon" class="form-control menu-edit" value="{{ $value->menu_icon ?? '' }}">
<!--MENU DESCRIPTION -->
<small>Description</small>
<textarea class="form-control" name="menu_description" id="" cols="8" rows="3">{{ $value->menu_description ?? '' }}</textarea>
<button type="submit" name="action" value="edit_menu" class="btn btn-success btn-outline btn-sm pull-right" value="edit-menu" ><i class="icon-check"></i> Save</button>