<ol class="dd-list">
	@foreach($value->children as $child)
		{{--{{dd($child)}}--}}
		<li class="dd-item dd3-item" data-id="{{ $child->menu_post_id }}">
			<div class="dd-handle dd3-handle"></div>
			<div class="dd3-content">
				<form action="{{ route('menu.edit_menu') }}" method="post">
					@csrf
					{{ $child->menu_display_name ?? $child->post->post_title }}
					<span style="font-weight: lighter; padding-left: 20px;font-style: italic;">
													                    @if($child->post->post_type=='page')
							<span>page</span>
						@elseif($child->post->post_type=='custom_link')
							<span>Custom Link</span>
						@else
							<span></span>
						@endif
											                        </span>
					<a class="link" href="{{ route('menu.remove_item',$child->menu_post_id) }}">
						<i class="mdi mdi-close text-danger pull-right"></i>
					</a>
					<a class="collapsible pull-right">
						<i id="menu-edit-icon" class="fa fa-caret-down"></i>
					</a>
					<div class="content">
						@include('menu.editMenuFormChild')
					</div>
				</form>
			</div>
			@if ($child->children->count())
				@include('menu.menuSubchild')
			@endif
		</li>
	@endforeach
</ol>