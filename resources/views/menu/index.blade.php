@extends('layouts.app')
@section('css')
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/nestable/nestable.css') }}" rel="stylesheet" type="text/css" />
	<style>
		.myadmin-dd-empty .dd-list button {
			width: auto;
			height: auto;
			font-size: 12px;
			margin-top: 12px;
		}
		.checkbox label::before {
			border-radius: 2px;
			border: 1px solid #c5c5c5;
		}
		div.disabled{
			pointer-events: none;
			opacity: 0.5;
			background: #CCC;
		}
		.collapsible {
			cursor: pointer;
			padding-right: 20px;
			border: none;
		}
		.content {
			padding-top: 20px;
			display: none;
			overflow: hidden;
		}
	</style>
@endsection
@section('page_title')
	List menu
@endsection
@section('right_button')
@stop

@section('content')
	<!--SELECT MENU START-->
	<form action="{{ route('menu.change') }}" method="post">
		<div class="row">
			<div class="col-md-12">
				<div class="white-box">
					<h3 class="box-title">Select a menu to edit</h3>
					@csrf
					<div class="row">
						@if(!empty($menus))
							<div class="col-md-6 {{ $errors->has('menu_id_change') ? 'has-error' : '' }}">
								<select name="menu_id_change" id="" class="form-control" >
									<option value="">--Select Menu--</option>
									@foreach($menus as $val)
										<option @if(isset($selected_menu)) @if($val->id==$selected_menu->id) selected @endif @endif value="{{ $val->id }}">{{ $val->title }}</option>
									@endforeach
								</select>
								@if ($errors->has('menu_id_change'))
									<span class="text-danger text-sm-left" role="alert">
                                		<small>{{ $errors->first('menu_id_change') }}</small>
                            		</span>
								@endif
							</div>
							<div class="col-md-6">
								<button type="submit" name="action" value="select_menu" class="btn btn-success btn-outline btn-sm">Select</button>
								&nbsp;&nbsp;or<a class="btn btn-link btn-sm" href="#add" data-toggle="modal">Create a new menu</a>
							</div>
						@else
							<div class="col-md-12">
								<a href="" class="btn btn-link btn-sm">Create a new menu</a>
							</div>
						@endif
					</div>
				</div>
			</div>
		</div>
	</form>
	<!--SELECT MENU END-->
	
	<div class="row @if(empty($selected_menu)) disabled @endif">
		<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12" >
			<!--ADD PAGES START-->
			<form action="{{ route('menu.change') }}" method="post">
				@csrf
				<input type="hidden" name="selected_menu_id" id="" value="{{ $selected_menu->id ?? '' }}">
				<div class="panel panel-default">
					<div class="panel-heading">Pages</div>
					<div class="panel-wrapper collapse in" >
						<div class="panel-body" style="padding-top: 2px;padding-bottom: 25px !important;height:200px;overflow-y: scroll;">
							@if(isset($pages))
								@foreach($pages as $val)
									<div class="checkbox checkbox-custom">
										<input id="checkbox11" type="checkbox" name="page_id[]" multiple="" value="{{ $val->id }}">
										<label for="checkbox11"> {{ $val->post_title }} </label>
									</div>
								@endforeach
							@else
								<p>No pages found. <a href="" class="btn btn-link btn-sm">Create a new page</a></p>
							@endif
						</div>
						<div class="panel-footer" style="padding-top: 5px;padding-bottom: 35px;">
							<button class="btn btn-success btn-outline pull-right btn-sm" name="action" value="add_pages">Add to menu</button>
						</div>
					</div>
				</div>
			</form>
			<!--ADD PAGES END-->
			
			<!--ADD LINK START-->
			<form action="{{ route('menu.change') }}" method="post">
				@csrf
				<input type="hidden" name="selected_menu_id" id="" value="{{ $selected_menu->id ?? '' }}">
				<div class="panel panel-default">
					<div class="panel-heading">Add custom link</div>
					<div class="panel-wrapper collapse in">
						<div class="panel-body" style="padding-bottom: 5px !important;">
							<div class="form-group">
								<label for="link_name">Name</label>
								<input id="link_name" type="text" class="form-control" name="link_name">
							</div>
							<div class="form-group">
								<label for="name">Link</label>
								<input id="link" type="text" class="form-control" name="custom_link" value="https://">
							</div>
							<button class="btn btn-success btn-outline btn-sm pull-right" name="action" value="add_link">Add to menu</button>
						</div>
						{{--<div class="panel-footer"> Add custom link </div>--}}
					</div>
				</div>
			</form>
			<!--ADD LINK END-->

			<!--ADD DOWNLOADS START-->
			<form action="{{ route('menu.change') }}" method="post">
				@csrf
				<input type="hidden" name="selected_menu_id" id="" value="{{ $selected_menu->id ?? '' }}">
				<div class="panel panel-default">
					<div class="panel-heading">Downloads</div>
					<div class="panel-wrapper collapse in" >
						<div class="panel-body" style="padding-top: 2px;padding-bottom: 25px !important;height:200px;overflow-y: scroll;">
							@if($downloads->count()>0)
								@foreach($downloads as $val)
									<div class="checkbox checkbox-custom">
										<input id="checkbox11" type="checkbox" name="downloads_id[]" multiple="" value="{{ $val->downloads_category_id }}">
										<label for="checkbox11"> {{ $val->title }} </label>
									</div>
								@endforeach
							@else
								<p>No downloads found. <a href="{{ route('downloads.index') }}" class="btn btn-link btn-sm">Create new</a></p>
							@endif
						</div>
						<div class="panel-footer" style="padding-top: 5px;padding-bottom: 35px;">
							<button class="btn pull-right btn-success btn-outline btn-sm" name="action" value="add_downloads">Add to menu</button>
						</div>
					</div>
				</div>
			</form>
			<!--ADD DOWNLOADS END-->
		</div>
		
		<!--MENU NESTABLE START-->
		<div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
			<div class="panel panel-default">
				<form action="{{ route('menu.change') }}" method="post">
					@csrf
					<input type="hidden" name="selected_menu_id" id="" value="{{ $selected_menu->id ?? '' }}">
					<div class="panel-heading">
						Menu structure
						@if (isset($selected_menu))
							@if ($selected_menu->is_active==1)
								<span class="badge badge-success">Active</span> <sup><a href="{{ route('menu.change_status',[$selected_menu->id,'deactivate']) }}" class="text-danger">Deactivate?</a></sup>
							@else
								<span class="badge badge-danger">Inactive</span> <sup><a href="{{ route('menu.change_status',[$selected_menu->id,'activate']) }}" class="text-success">Activate?</a></sup>
							@endif
						@endif
						<hr>
						<div class="row">
							<div class="col-md-6 {{ $errors->has('menu_title') ? 'has-error' : '' }}">
								<input type="text" name="menu_title" class="form-control" id="" value="{{ $selected_menu->title ?? '' }}">
								@if ($errors->has('menu_title'))
									<span class="text-danger text-sm-left" role="alert">
                                		<small>{{ $errors->first('menu_title') }}</small>
                            		</span>
								@endif
								<input type="hidden" name="selected_menu_id" id="" value="{{ $selected_menu->id ?? '' }}">
							</div>
							<div class="col-md-6">
								<button type="submit" name="action" value="save_menu_name" class="btn btn-success btn-outline btn-sm"><i class="icon-check"></i> Save</button>
							</div>
						</div>
					</div>
				</form>
				@include('menu.menu')
			</div>
		</div>
		<!--MENU NESTABLE END-->
	</div>
	
	<!--SET MENU DEPTH-->
	<input type="hidden" id="menu_type" value="{{ $selected_menu->menu_type ?? '' }}">
	<!--SET MENU DEPTH END-->
	@include('menu.model')
@endsection

@section('scripts')
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/nestable/jquery.nestable.js') }}"></script>
	<script type="text/javascript">
        $(document).ready(function() {
            // Nestable
            // var updateOutput = function(e) {
            //     var list = e.length ? e : $(e.target),
            //         output = list.data('output');
            //
            // };
            $('#nestable2').on('click', function(e) {
                var target = $(e.target),
                    action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });
            
            // SETTING MENU DEPTH
	        var menuType = $('#menu_type').val();
	        var depth = 0;
	        switch (menuType) {
                case 'nav_menu':
                    depth = 3;
                    break;
                case 'prenav_menu':
                    depth = 1;
                    break;
                case 'footer_menu':
                    depth = 1;
                    break;
                default:
                    depth = 3;
            }
            
            $('#nestable2').nestable({maxDepth:depth});
            
            
            $('#saveMenuOrder').on('click', function(){
                $.ajax({
                    type: "POST",
                    url: "{{ route('menu.saveOrder') }}",
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: {data:$('.dd').nestable('serialize')},
                    success: function (msg) {
                        console.log(msg);
                        // $("#confirmDelete").modal("hide");
                        window.location.reload();
                    }
                });
            });
        });

        // ADD MENU
        // $('#add').find('.modal-footer #confirm_yes').on('click', function (e) {
        // 	e.preventDefault();
        //     $("#confirmDelete").modal("hide");
        //     window.location.reload();
        //
        // });
        //
        // var clickData = {};
	</script>
	<script>
        var coll = document.getElementsByClassName("collapsible");
        var i;

        for (i = 0; i < coll.length; i++) {
            coll[i].addEventListener("click", function() {
                this.classList.toggle("active");
                var content = this.nextElementSibling;
                if (content.style.display === "block") {
                    content.style.display = "none";
                } else {
                    content.style.display = "block";
                }
            });
        }
	</script>
@endsection


