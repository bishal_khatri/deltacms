<div class="panel-wrapper collapse in" aria-expanded="true">
	<div class="panel-body">
		<h3 class="box-title">{{ $selected_menu->title ?? '' }}
			[&nbsp;<span class="text-blue">
                                @if(isset($selected_menu))
					@if($selected_menu->menu_type=='nav_menu')
						Navigation Menu
					@elseif($selected_menu->menu_type=='prenav_menu')
						Pre Navigation Menu
					@elseif($selected_menu->menu_type=='footer_menu')
						Footer Menu
					@endif
				@endif
                            </span>&nbsp;]
		</h3>
		<div class="dd myadmin-dd-empty" id="nestable2">
			<ol class="dd-list">
				@if(isset($selected_menu_item))
					@foreach($selected_menu_item as $value)
						<li class="dd-item dd3-item" data-id="{{ $value->menu_post_id }}">
							<div class="dd-handle dd3-handle"></div>
							<div class="dd3-content">
								<form action="{{ route('menu.edit_menu') }}" method="post">
									@csrf
									{{ $value->menu_display_name ?? $value->post->post_title }}
									<span style="font-weight: lighter; padding-left: 20px;font-style: italic;">
														@if($value->post->post_type=='page')
											<span>page</span>
										@elseif($value->post->post_type=='custom_link')
											<span>Custom Link</span>
										@else
											<span></span>
										@endif
								                    </span>
									<a class="link" href="{{ route('menu.remove_item',$value->menu_post_id) }}">
										<i class="mdi mdi-close text-danger pull-right"></i>
									</a>
									<a class="collapsible pull-right"><i id="menu-edit-icon" class="fa fa-caret-down"></i></a>
									<div class="content">
										@include('menu.editMenuForm')
									</div>
								</form>
							</div>
							{{--CHILD MENU START--}}
							@if ($value->children->count())
								@include('menu.menuChild')
							@endif
							{{--CHILD MENU END--}}
						</li>
					@endforeach
				@else
					<p>No items found. App pages</p>
				@endif
			</ol>
		</div>
	</div>
	<div class="panel-footer">
		MENU STRUCTURE
		<button id="saveMenuOrder" type="button" class="btn btn-success btn-outline btn-sm pull-right">Save Order</button>
	</div>
</div>