<div class="modal fade bs-modal-sm" id="add" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><i class="fa fa-plus-circle" id="icon-terminate" ></i> Create new menu
				</h4>
			</div>
			<form action="{{ route('menu.add') }}" method="post">
				@csrf
				<div class="modal-body">
					<div class="form-group row">
						<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Menu name') }}</label>
						<div class="col-md-6">
							<input id="name" type="text" class="form-control" name="title" value="{{ old('title') }}"  placeholder="Enter menu name" required autofocus>

							@if ($errors->has('title'))
								<span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('title') }}</strong>
                                    </span>
							@endif
							<p style="font-style: italic;color: darkorange;">Example: Main menu</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Menu type') }}</label>
						<div class="col-md-6">
							<select class="form-control" required="" name="menu_type">
                                <option value="">--Select menu type--</option>
                                <option value="prenav_menu">Pre Navigation</option>
                                <option value="nav_menu">Navigation</option>
                                <option value="footer_menu">Footer</option>
                            </select>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success btn-outline btn-sm confirm_yes" id="confirm_yes"><i class="icon-check"></i> Save
					</button>
					<button type="button" class="pull-left btn btn-default btn-sm btn-outline" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
						close
					</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
</div>
