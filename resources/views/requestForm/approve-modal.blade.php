<div class="modal fade bs-modal-sm" id="approve" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Approve Form
                </h4>
            </div>
            <div class="modal-body">
                <h5>Do you want to approve this form ? </h5>
                {{--<h4>[ Client code : <span id="ccode"></span> ]</h4>--}}
            </div>
            <form action="{{ route('form.approve') }}" method="post">
                @csrf
                <input type="hidden" name="form_id" id="form_id">
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
                    </button>
                    <button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
                        No
                    </button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
