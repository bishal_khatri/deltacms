@extends('layouts.app')
@section('css')
	<style>
		.form-w150{
			width: 250px;
		}
	</style>
@endsection
@section('page_title')
    Form
@endsection
@section('right_button')
    <a href="#editEmail" class="btn btn-info btn-outline pull-right" data-toggle="modal" data-title = "{{ $email->text ?? '' }}"><i class="fa fa-envelope fa-fw"></i>Edit Email</a>
@stop
@section('content-title')
    <h2>
        Form Listing
    </h2>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
	            <form class="form-horizontal" method="post" action="{{ route('form.update') }}" enctype="multipart/form-data">
		            @csrf
                    <input type="hidden" class="form-control" name="form_id" value="{{ $formData->form_id }}" id="">
		            <div class="row">
			            <div class="col-md-6">
				            <div class="form-group">
					            <label for="">Client Code</label>
					            <input type="text" name="ccode" value="{{ $formData->ccode ?? '' }}" class="form-w150 form-control" id="">
				            </div>
				            <div class="form-group">
					            <label for="">Branch</label>
					            <input type="text" name="cbranch" value="{{ $formData->cbranch ?? '' }}" class="form-w150 form-control" id="">
				            </div>
				            <div class="form-group">
					            <label for="">Is Approved</label>
					            <input type="checkbox" name="status" value="1">
				            </div>
				            <div class="form-group">
					            <label>Client Status</label>
					            <input type="text" name="cstatus" value="{{ $formData->cstatus ?? '' }}" class="form-w150 form-control" id="">
				            </div>
			            </div>
			            <div class="col-md-6">
				            <div class="form-group">
					            <label for="">Obligor</label>
					            <input type="text" name="cobligator" value="{{ $formData->cobligator ?? '' }}" class="form-w150 form-control" id="">
				            </div>
				            <div class="form-group">
					            <label for="">A/c Office</label>
					            <input type="text" name="cacofficer" value="{{ $formData->cacofficer ?? '' }}" class="form-w150 form-control" id="">
				            </div>
				
				            <div class="form-group">
					            <label for="">Introduced By</label>
					            <input type="text" name="cintro" value="{{ $formData->cintro ?? '' }}" class="form-w150 form-control" id="">
				            </div>
			            </div>
		            </div>
		
		            <hr>
		            
		            <div class="row">
			            <div class="col-md-6">
				            <div class="form-group">
					            <label for="">Insurance Information</label>
					            <input type="text" name="insinfo" value="{{ $formData->insinfo ?? '' }}" class="form-w150 form-control" id="">
				            </div>
				            <div class="form-group">
					            <label for="">Remarks</label>
					            <input type="text" name="insremark" value="{{ $formData->insremark ?? '' }}" class="form-w150 form-control" id="">
				            </div>
				
				            <div class="form-group">
					            <label for="">Client Limit</label>
					            <input type="text" name="inslimit" value="{{ $formData->inslimit ?? '' }}" class="form-w150 form-control" id="">
				            </div>
				
				            <div class="form-group">
					            <label for="">Borrower Code</label>
					            <input type="text" name="insborrower" value="{{ $formData->insborrower ?? '' }}" class="form-w150 form-control" id="">
				            </div>
				
				            <div class="form-group">
					            <label for="">Group</label>
					            <input type="text" name="insgroup" value="{{ $formData->insgroup ?? '' }}" class="form-w150 form-control" id="">
				            </div>
				            <div class="form-group">
					            <label for="">Mail Statement</label>
					            <input type="checkbox" name="insstatement">
				            </div>
				            <div class="form-group">
					            <label for="">Keep MIS?</label>
					            <input type="checkbox" name="insmis" id="">
				            </div>
				            <div class="form-group">
					            <label for="">KYC Update</label>
					            <input type="checkbox" name="inskycupdate">
				            </div>
			            </div>
			            <div class="col-md-6">
				           <div class="row">
					           <div class="col-md-6">
						           <h3>KYC</h3>
						           <div class="form-group">
							           <label for="">Business Unit</label>
							           <input type="text" name="kycbusiness" value="{{ $formData->kycbusiness ?? '' }}" class="form-w150 form-control" id="">
						           </div>
						
						           <div class="form-group">
							           <label for="">Reviewed Date AD</label>
							           <input type="text" name="kycreviewed" value="{{ $formData->kycreviewed ?? '' }}" class="form-w150 form-control" id="">
						           </div>
						
						           <div class="form-group">
							           <label for="">Deposit Risk Grade</label>
							           <select id="job" name="kycriskgrade">
								           <option value="">menu</option>
								           <option value="">menu</option>
							           </select>
						           </div>
						
						           <div class="form-group">
							           <label for="">Trans Reviewed Date AD</label>
							           <input type="text" name="kyctrans" value="{{ $formData->kyctrans ?? '' }}" class="form-w150 form-control" id="">
						           </div>
						
						           <div class="form-group">
							           <label for="">Reason</label>
							           <input type="text" name="kycreason" value="{{ $formData->kycreason ?? '' }}" class="form-w150 form-control" id="">
						           </div>
						
						           kyc doc update
						
						           <div class="form-group">
							           <label for="">Docs Update Status</label>
							           <select id="job" name="docstatus">
								           <option value="">menu</option>
								           <option value="">menu</option>
							           </select>
						           </div>
						
						           <div class="form-group">
							           <label for="">Date of Update BS</label>
							           <input type="text" name="docdateupdate" value="{{ $formData->docdateupdate ?? '' }}" class="form-w150 form-control" id="">
						           </div>
						
						           <div class="form-group">
							           <label for="">KYC Remarks</label>
							           <input type="text" name="docremarks" value="{{ $formData->docremarks ?? '' }}" class="form-w150 form-control" id="">
						           </div>
						
						           <div class="form-group">
							           <label for="">Keep Personal Category</label>
							           <input type="checkbox" name="">
						           </div>
					           </div>
					           <div class="col-md-6">
						           <h3>Personal Category</h3>
						           <div class="form-group">
							           <label for="">Politically Exposed Person (PEP)</label>
							           <input type="checkbox" name=perexposed">
						           </div>
						
						           <div class="form-group">
							           <label for="">Declaration for punishment any crime</label>
							           <input type="checkbox" name="perpunish">
						           </div>
						
						           <div class="form-group">
							           <label for="">High Positiomed Person (HPP)</label>
							           <input type="checkbox" name="perhighpositioned">
						           </div>
						
						           <div class="form-group">
							           <label for="">Non Face To Face Customer (NF3F)</label>
							           <input type="checkbox" name="pernonface">
						           </div>
						
						           <div class="form-group">
							           <label for="">Very Important Person (VIP)</label>
							           <input type="checkbox" name="pervimpper">
						           </div>
						
						           <div class="form-group">
							           <label for="">Tag 1</label>
							           <input type="text" name="tag1" value="{{ $formData->tag1 ?? '' }}" class="form-w150 form-control" id="">
						           </div>
						           <div class="form-group">
							           <label for="">Tag 2</label>
							           <input type="text" name="tag2" value="{{ $formData->tag2 ?? '' }}" class="form-w150 form-control" id="">
						           </div>
						           <div class="form-group">
							           <label for="">Tag 3</label>
							           <input type="text" name="tag3" value="{{ $formData->tag3 ?? '' }}" class="form-w150 form-control" id="">
						           </div>
					           </div>
				           </div>
				           
			            </div>
		            </div>
		            <div class="row">
			            <div class="col-md-12">
				            <button type="submit" class="btn btn-success">Update</button>
			            </div>
		            </div>
                </form>
            </div>
        </div>
    </div>

    @include('requestForm.edit-email-modal')
    @include('requestForm.approve-modal')
@endsection

@section('scripts')
    <script>
        $('#editEmail').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var title = button.data('title');
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#email").val(title);
        });

        $('#approve').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var title = button.data('title');
            // alert(title)
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#form_id").val(id);
            $("#ccode").html(title);
        });

        $('#datatable').DataTable({
            dom: 'Bfrtip',
            columnDefs: [ { "orderable": false, "visible": true } ],
        });

        // AJAX SLIDER DELETE
        $('#editForm').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#edit_id").val(id);
        });

        {{--$('#confirmDelete').find('.modal-footer #confirm_yes').on('click', function () {--}}
            {{--var id = $("#hidden_id").val();--}}
            {{--$.ajax({--}}
                {{--type: "POST",--}}
                {{--url: "{{ route('popup.delete') }}",--}}
                {{--headers: {--}}
                    {{--'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')--}}
                {{--},--}}
                {{--data: "id=" + id,--}}
                {{--success: function (msg) {--}}
                    {{--$("#confirmDelete").modal("hide");--}}
                    {{--window.location.reload();--}}
                {{--}--}}
            {{--});--}}
        {{--});--}}


    </script>
@endsection
