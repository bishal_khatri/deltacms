@extends('layouts.app')
@section('css')
@endsection
@section('page_title')
	Account Opening Form
@endsection
@section('right_button')
	<div class="pull-right">
		<a href="{{ route('form.index','approved') }}" class="btn btn-info btn-outline">Approved List</a>
		<a href="#editEmail" class="btn btn-info btn-outline" data-toggle="modal" data-title = "{{ $email->text ?? '' }}"><i class="fa fa-envelope fa-fw"></i>Edit Email</a>
	</div>
@stop
@section('content-title')
	<h2>
		Form Listing
	</h2>

@endsection

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div class="row">
					<div class="col-md-12">
						<div class="btn-group pull-right" style="padding-left: 20px;">
							<button class="btn dropdown-toggle" data-toggle="dropdown"> Export <i class="fa fa-angle-down"></i>
							</button>
							<ul class="dropdown-menu pull-right">
								<li> <a href="{{ route('form.export',['xls','all']) }}"> Export to XLS </a> </li>
								<li> <a href="{{ route('form.export',['xlsx','all']) }}"> Export to xlsx </a> </li>
								<li> <a href="{{ route('form.export',['csv','all']) }}"> Export to CSV </a> </li>
								<li> <a href="{{ route('form.export',['pdf','all']) }}"> Export to PDF </a> </li>
							</ul>
						</div>
					</div>
				</div>
				<br>
				<div class="table-responsive">
					<table id="datatable" class="table table-borderless table-striped" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th style="width: 70px">Code</th>
							<th style="">Full Name</th>
							<th>Client Category</th>
							<th>Date of Birth</th>
							<th>Status</th>
							<th>Citizenship</th>
							<th>PP</th>
							<th>Created</th>
                            <th style="width: 120px;">Export</th>
						</tr>
						</thead>
						<tbody>
						@if($forms->count()>0)
							@foreach($forms as $val)
								<tr class="title">
									<td>{{ $val->ccode }}</td>
									<td>
										{{ $val->cfname }} {{ $val->cmiddlename }} {{ $val->clastname }}
										<div class="action">
											<a href="{{ route('form.editForm',$val->form_id) }}" class="btn btn-link text-primary btn-sm">Edit</a>
											<span class="vl"></span>
											<a href="#approve" data-toggle="modal" data-id = "{{ $val->form_id }}" data-title = "{{ $val->ccode }}" class="btn btn-link text-success btn-sm">Approve</a>
											{{--<span class="vl"></span>--}}
											{{--<a class="btn default btn-outline text-danger" href="#confirmDelete" data-id="" data-toggle="modal">--}}
											{{--<i class="icon-trash"></i>--}}
											{{--</a>--}}
										</div>
									</td>
									<td>{{ $val->ccategory }}</td>
									<td>{{ $val->cdatebirth }}</td>
									<td>
										@if($val->status==1)
											<span class="label label-success">Approved</span>
										@else
											<span class="label label-primary">New</span>
										@endif
									</td>
                                    <td><a href="{{ asset(STATIC_DIR.'storage/'.$val->citizenship_image) }}" target="_blank"><img width="50" src="{{ asset(STATIC_DIR.'storage/'.$val->citizenship_image) }}" alt=""></a></td>
                                    <td><a href="{{ asset(STATIC_DIR.'storage/'.$val->pp_image) }}" target="_blank"><img width="50" src="{{ asset(STATIC_DIR.'storage/'.$val->pp_image) }}" alt=""></a></td>
									<td>{{ $val->created_at->diffForHumans() }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button class="btn dropdown-toggle" data-toggle="dropdown"> Export <i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li> <a href="{{ route('form.export',['xls',$val->form_id]) }}"> Export to XLS </a> </li>
                                                <li> <a href="{{ route('form.export',['xlsx',$val->form_id]) }}"> Export to xlsx </a> </li>
                                                <li> <a href="{{ route('form.export',['csv',$val->form_id]) }}"> Export to CSV </a> </li>
                                                <li> <a href="{{ route('form.export',['pdf',$val->form_id]) }}"> Export to PDF </a> </li>
                                            </ul>
                                        </div>
                                    </td>
								</tr>
							@endforeach
						@else
							<tr>
								<td colspan="3">No items found </td>
							</tr>
						@endif
					</table>
				</div>
			</div>
		</div>
	</div>

	@include('requestForm.edit-email-modal')
	@include('requestForm.approve-modal')
@endsection

@section('scripts')
	<script>
        $('#editEmail').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var title = button.data('title');
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#email").val(title);
        });

        $('#approve').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var title = button.data('title');
            // alert(title)
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#form_id").val(id);
            $("#ccode").html(title);
        });

        $('#datatable').DataTable({
            dom: 'Bfrtip',
            columnDefs: [ { "orderable": false, "visible": true } ],
        });

        // AJAX SLIDER DELETE
        $('#editForm').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#edit_id").val(id);
        });

		{{--$('#confirmDelete').find('.modal-footer #confirm_yes').on('click', function () {--}}
		{{--var id = $("#hidden_id").val();--}}
		{{--$.ajax({--}}
		{{--type: "POST",--}}
		{{--url: "{{ route('popup.delete') }}",--}}
		{{--headers: {--}}
		{{--'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')--}}
		{{--},--}}
		{{--data: "id=" + id,--}}
		{{--success: function (msg) {--}}
		{{--$("#confirmDelete").modal("hide");--}}
		{{--window.location.reload();--}}
		{{--}--}}
		{{--});--}}
		{{--});--}}


	</script>
@endsection
