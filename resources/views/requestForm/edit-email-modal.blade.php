<div class="modal fade bs-modal-sm" id="editEmail" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><i class="fa fa-plus-circle" id="icon-terminate" ></i> Email Content
                </h4>
            </div>
            <form action="{{ route('form.update_email') }}" method="post">
                @csrf
                <div class="modal-body">
                    <div class="form-group">
                        <textarea name="email" id="email" cols="30" rows="10" class="form-control"></textarea>
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                        <p style="font-style: italic;color: darkorange;">Note: This email is sent to user on approve.</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success btn-outline btn-sm confirm_yes" id="confirm_yes"><i class="icon-check"></i> update
                    </button>
                    <button type="button" class="pull-left btn btn-default btn-sm btn-outline" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
                        close
                    </button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
</div>
