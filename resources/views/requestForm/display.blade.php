@extends('front.layouts.main')
@section('front-css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'frontend/css/form.css') }}">
@stop
@section('page-title')
    Online Account Opening
@endsection
@section('content')
    <div id="content-inner">


        <h1>Online Account Opening</h1>


        <form action="page1.php" method="post">

            <fieldset>
                <legend><span class="number">1</span>General Information</legend>



                <table cellspacing="0">
                    <tr valign="top">
                        <td width="50%"><label for="name">Client Code:</label>
                            <input readonly type="text" id="name" name="ccode" value="{{ $formData->ccode ?? old('ccode') }}"></td>
                        <td><label for="name">Previous Client Code:</label>
                            <input readonly type="text" id="name" name="prvccode" value="{{ $formData->prvccode ?? old('prvccode') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Client Status:</label>
                            <input readonly type="text" id="name" name="cstatus" value="{{ $formData->cstatus ?? old('cstatus') }}"></td>
                        <td><label for="name">Branch:</label>
                            <input readonly type="text" id="name" name="cbranch" value="{{ $formData->cbranch ?? old('cbranch') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <label for="job">Type of Client:</label>
                            <input readonly type="text" id="name" name="ctype" value="{{ $formData->ctype ?? old('ctype') }}">
                        </td>
                        <td><label for="job">Ownership type:</label>
                        	<input readonly type="text" id="name" name="cowner" value="{{ $formData->cowner ?? old('cowner') }}">
                            </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Client Category:</label>
                        	<input readonly type="text" id="name" name="ccategory" value="{{ $formData->ccategory ?? old('ccategory') }}">
                            </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">First Name:</label>
                            <input readonly type="text" id="name" name="cfname" value="{{ $formData->cfname ?? old('cfname') }}"></td>
                        <td><label for="name">Middle Name:</label>
                            <input readonly type="text" id="name" name="cmiddlename" value="{{ $formData->cmiddlename ?? old('cmiddlename') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Last Name:</label>
                            <input readonly type="text" id="name" name="clastname" value="{{ $formData->clastname ?? old('clastname') }}"></td>
                        <td><label for="name">Date of Birth:</label>
                            <input readonly type="text" id="name" name="cdatebirth" value="{{ $formData->cdatebirth ?? old('cdatebirth') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Expiry in case of Minor:</label>
                            <input readonly type="text" id="name" name="cexpiryminor" value="{{ $formData->cexpiryminor ?? old('cexpiryminor') }}"></td>
                        <td><label for="name">Country:</label>
                            <input readonly type="text" id="name" name="ccountry" value="{{ $formData->ccountry ?? old('ccountry') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Obligor:</label>
                            <input readonly type="text" id="name" name="cobligator" value="{{ $formData->cobligator ?? old('cobligator') }}"></td>
                        <td><label for="name">A/c Officer:</label>
                            <input readonly type="text" id="name" name="cacofficer" value="{{ $formData->cacofficer ?? old('cacofficer') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Introduced By:</label>
                            <input readonly type="text" id="name" name="cintro" value="{{ $formData->cintro ?? old('cintro') }}"></td>
                        <td><label for="name">Resident:</label>
                            <input readonly type="text" id="name" name="cresident" value="{{ $formData->cresident ?? old('cresident') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job"><input readonly type="checkbox" id="" name="cminor" /> Is Minor:</label> </td>
                        <td><label for="job"><input readonly type="checkbox" id="" name="capprove" /> Is Approved:</label> </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Resident:</label>
                        	<input readonly type="text" id="name" name="cresident" value="{{ $formData->cresident ?? old('cresident') }}">
                            </td>
                        <td></td>
                    </tr>
                    <tr valign="top">
                        <td><strong>Contact Address</strong></td>
                        <td><strong>Permanent Contact</strong></td>
                    </tr>
                    <tr valign="top">
                        <td>

                            <table cellspacing="0">
                                <tr valign="top">
                                    <td><label for="name">Address</label><input readonly type="text" id="name" name="caddress" value="{{ $formData->caddress ?? old('caddress') }}"></td>
                                    <td><label for="name">City</label><input readonly type="text" id="name" name="ccity" value="{{ $formData->ccity ?? old('ccity') }}"></td>
                                </tr>
                                <tr valign="top">
                                    <td><label for="name">House/Block No</label><input readonly type="text" id="name" name="chouseblock" value="{{ $formData->chouseblock ?? old('chouseblock') }}"></td>
                                    <td><label for="name">VC/MDC</label><input readonly type="text" id="name" name="cvcmdc" value="{{ $formData->cvcmdc ?? old('cvcmdc') }}"></td>
                                </tr>
                                <tr valign="top">
                                    <td><label for="name">VC/MDC Name</label>
                                    	<input readonly type="text" id="name" name="cvcmdcname" value="{{ $formData->cvcmdcname ?? old('cvcmdcname') }}">
                                       
                                    </td>
                                    <td><label for="name">Tel No</label><input readonly type="text" id="name" name="ctelno" value="{{ $formData->ctelno ?? old('ctelno') }}"></td>
                                </tr>
                                <tr valign="top">
                                    <td><label for="name">PO Box No</label><input readonly type="text" id="name" name="cpobox" value="{{ $formData->cpobox ?? old('cpobox') }}"></td>
                                    <td><label for="name">Disrict:</label>
                                    	<input readonly type="text" id="name" name="cdistrict" value="{{ $formData->cdistrict ?? old('cdistrict') }}">
                                        </td>
                                </tr>
                                <tr valign="top">
                                    <td><label for="name">Ward no</label><input readonly type="text" id="name" name="cwardno" value="{{ $formData->cwardno ?? old('cwardno') }}"></td>
                                    <td><label for="name">Country</label><input readonly type="text" id="name" name="ccountry" value="{{ $formData->ccountry ?? old('ccountry') }}"></td>
                                </tr>
                                <tr valign="top">
                                    <td><label for="name">State</label><input readonly type="text" id="name" name="cstate" value="{{ $formData->cstate ?? old('cstate') }}"></td>
                                    <td></td>
                                </tr>
                            </table>

                        </td>
                        <td>

                            <table cellspacing="0">
                                <tr valign="top">
                                    <td><label for="name">Address</label><input readonly type="text" id="name" name="peraddress" value="{{ $formData->peraddress ?? old('peraddress') }}"></td>
                                    <td><label for="name">City</label><input readonly type="text" id="name" name="percity" value="{{ $formData->percity ?? old('percity') }}"></td>
                                </tr>
                                <tr valign="top">
                                    <td><label for="name">VC/MDC</label><input readonly type="text" id="name" name="pervcmdc" value="{{ $formData->pervcmdc ?? old('pervcmdc') }}"></td>
                                    <td><label for="name">VC/MDC Name</label>
                                    	<input readonly type="text" id="name" name="pervcmdcname" value="{{ $formData->pervcmdcname ?? old('pervcmdcname') }}">
                                       
                                    </td>
                                </tr>
                                <tr valign="top">
                                    <td><label for="name">Tel No</label><input readonly type="text" id="name" name="pertel" value="{{ $formData->pertel ?? old('pertel') }}"></td>
                                    <td><label for="name">PO Box No</label><input readonly type="text" id="name" name="perpobox" value="{{ $formData->perpobox ?? old('perpobox') }}"></td>
                                </tr>
                                <tr valign="top">
                                    <td><label for="name">Disrict:</label>
                                    	<input readonly type="text" id="name" name="perdistrict" value="{{ $formData->perdistrict ?? old('perdistrict') }}">
                                        </td>
                                    <td><label for="name">Ward no</label><input readonly type="text" id="name" name="perward" value="{{ $formData->perward ?? old('perward') }}"></td>
                                </tr>
                                <tr valign="top">
                                    <td><label for="name">Country</label><input readonly type="text" id="name" name="percountry" value="{{ $formData->percountry ?? old('percountry') }}"></td>
                                    <td><label for="name">State</label><input readonly type="text" id="name" name="perstate" value="{{ $formData->perstate ?? old('perstate') }}"></td>
                                </tr>
                                <tr valign="top">
                                    <td><label for="name">Fax</label><input readonly type="text" id="name" name="perfax" value="{{ $formData->perfax ?? old('perfax') }}"></td>
                                    <td><label for="name">Fax2</label><input readonly type="text" id="name" name="perfax2" value="{{ $formData->perfax2 ?? old('perfax2') }}"></td>
                                </tr>
                                <tr valign="top">
                                    <td><label for="name">Mobile</label><input readonly type="text" id="name" name="permobile" value="{{ $formData->permobile ?? old('permobile') }}"></td>
                                    <td><label for="name">Mobile 2</label><input readonly type="text" id="name" name="permobile2" value="{{ $formData->permobile2 ?? old('permobile2') }}"></td>
                                </tr>
                                <tr valign="top">
                                    <td><label for="name">Email</label><input readonly type="text" id="name" name="peremail" value="{{ $formData->peremail ?? old('peremail') }}"></td>
                                </tr>
                            </table>

                        </td>
                </table>

                </td>
                </tr>
                </table>


            </fieldset>


            <fieldset>
                <legend><span class="number">2</span>Customer Information</legend>



                <table cellspacing="0">
                    <tr valign="top">
                        <td colspan="2"><strong>Citizenship Detail</strong></td>
                    </tr>
                    <tr valign="top">
                        <td width="50%"><label for="name">Type of ID:</label>
                        	<input readonly type="text" id="name" name="cusid" value="{{ $formData->cusid ?? old('cusid') }}">
                            </td>
                        <td><label for="name">Prev Citizenship No:</label>
                            <input readonly type="text" id="name" name="cusprevcitizen" value="{{ $formData->cusprevcitizen ?? old('cusprevcitizen') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name"> Id No:</label>
                            <input readonly type="text" id="name" name="cusidno" value="{{ $formData->cusidno ?? old('cusidno') }}"></td>
                        <td><label for="name">Issued Authority:</label>
                            <input readonly type="text" id="name" name="cusauthority" value="{{ $formData->cusauthority ?? old('cusauthority') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Issued Date:</label>
                            <input readonly type="text" id="name" name="cusissdate" value="{{ $formData->cusissdate ?? old('cusissdate') }}"></td></td>
                        <td><label for="job">ID District:</label>
                            <input readonly type="text" id="name" name="cusiddistrict" value="{{ $formData->cusiddistrict ?? old('cusiddistrict') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Customer Info</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Marital Status:</label>
                        	<input readonly type="text" id="name" name="cusmarital" value="{{ $formData->cusmarital ?? old('cusmarital') }}">
                            </td>
                        <td><label for="job">Nationality:</label>
                        	<input readonly type="text" id="name" name="cusnationality" value="{{ $formData->cusnationality ?? old('cusnationality') }}">
                                </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Salutation:</label>
                        	<input readonly type="text" id="name" name="cussalutation" value="{{ $formData->cussalutation ?? old('cussalutation') }}">
                          
                        </td>
                        <td><label for="job">Religion:</label>
                        	<input readonly type="text" id="name" name="cusreligion" value="{{ $formData->cusreligion ?? old('cusreligion') }}">
                            </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Gender:</label>
                        	<input readonly type="text" id="name" name="cusgender" value="{{ $formData->cusgender ?? old('cusgender') }}">
                            </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Employer Information</strong></td>
                    </tr>
                    <tr valign="top">
                        <td width="50%"><label for="name">Org. Name:</label>
                            <input readonly type="text" id="name" name="emporgname" value="{{ $formData->emporgname ?? old('emporgname') }}"></td>
                        <td><label for="name">Designation:</label>
                            <input readonly type="text" id="name" name="empdesignation" value="{{ $formData->empdesignation ?? old('empdesignation') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td width="50%"><label for="name">Org. Address:</label>
                            <input readonly type="text" id="name" name="emporgaddress" value="{{ $formData->emporgaddress ?? old('emporgaddress') }}"></td>
                        <td><label for="name">Appox. Annual Income/Salary:</label>
                            <input readonly type="text" id="name" name="empsalary" value="{{ $formData->empsalary ?? old('empsalary') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Registered Office Address</strong></td>
                    </tr>
                    <tr valign="top">
                        <td width="50%">
                            <label for="name">Address:</label>
                            <input readonly type="text" id="name" name="regaddress" value="{{ $formData->regaddress ?? old('regaddress') }}">
                        </td>
                        <td>
                            <label for="name">City </label>
                            <input readonly type="text" id="name" name="regcity" value="{{ $formData->regcity ?? old('regcity') }}">
                        </td>
                    </tr>
                    <tr valign="top">
                        <td width="50%"><label for="name">Country:</label>
                            <input readonly type="text" id="name" name="regcountry" value="{{ $formData->regcountry ?? old('regcountry') }}">
                        </td>
                        <td><label for="name">VC/MDC:</label>
                        	<input readonly type="text" id="name" name="regvcmdc" value="{{ $formData->regvcmdc ?? old('regvcmdc') }}">
                            
                        </td>
                    </tr>
                    <tr valign="top">
                        <td width="50%"><label for="name">Ward No:</label>
                            <input readonly type="text" id="name" name="regward" value="{{ $formData->regward ?? old('regward') }}"></td>
                        <td>
                            <label for="name">District:</label>
                            <input readonly type="text" id="name" name="regdistrict" value="{{ $formData->regdistrict ?? old('regdistrict') }}">
                        </td>
                    </tr>
                    <tr valign="top">
                        <td width="50%"><label for="name">State:</label>
                            <input readonly type="text" id="name" name="regstate" value="{{ $formData->regstate ?? old('regstate') }}"></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>


            </fieldset>

            <fieldset>
                <legend><span class="number">3</span>Family Information</legend>
                <table cellspacing="0">
                    <tr>
                        <td>&nbsp;</td>
                        <td>First Name</td>
                        <td>Middle Name</td>
                        <td>Last Name</td>
                        <td>Citizenship No</td>
                        <td>Citizen Issued Date</td>
                        <td>Citizen Issued District</td>
                        <td>Citizen District Name</td>
                    </tr>
                    <tr>
                        <td>Grand Father</td>
                        <td><input readonly type="text" id="name" name="grnffirst" value="{{ $formData->grnffirst ?? old('grnffirst') }}"></td>
                        <td><input readonly type="text" id="name" name="grnfmiddle" value="{{ $formData->grnfmiddle ?? old('grnfmiddle') }}"></td>
                        <td><input readonly type="text" id="name" name="grnflast" value="{{ $formData->grnflast ?? old('grnflast') }}"></td>
                        <td><input readonly type="text" id="name" name="grnfctznno" value="{{ $formData->grnfctznno ?? old('grnfctznno') }}"></td>
                        <td><input readonly type="text" id="name" name="grnfctznissdate" value="{{ $formData->grnfctznissdate ?? old('grnfctznissdate') }}"></td>
                        <td><input readonly type="text" id="name" name="grnfctznissdistrict" value="{{ $formData->grnfctznissdistrict ?? old('grnfctznissdistrict') }}"></td>
                        <td><input readonly type="text" id="name" name="grnfctzndistrict" value="{{ $formData->grnfctzndistrict ?? old('grnfctzndistrict') }}"></td>
                    </tr>
                    <tr>
                        <td>Grand Mother</td>
                        <td><input readonly type="text" id="name" name="grmfirst" value="{{ $formData->grmfirst ?? old('grmfirst') }}"></td>
                        <td><input readonly type="text" id="name" name="grmmiddle" value="{{ $formData->grmmiddle ?? old('grmmiddle') }}"></td>
                        <td><input readonly type="text" id="name" name="grmlast" value="{{ $formData->grmlast ?? old('grmlast') }}"></td>
                        <td><input readonly type="text" id="name" name="grmctznno" value="{{ $formData->grmctznno ?? old('grmctznno') }}"></td>
                        <td><input readonly type="text" id="name" name="grmctznissdate" value="{{ $formData->grmctznissdate ?? old('grmctznissdate') }}"></td>
                        <td><input readonly type="text" id="name" name="grmctznissdistrict" value="{{ $formData->grmctznissdistrict ?? old('grmctznissdistrict') }}"></td>
                        <td><input readonly type="text" id="name" name="grmctzndistrict" value="{{ $formData->grmctzndistrict ?? old('grmctzndistrict') }}"></td>
                    </tr>
                    <tr>
                        <td>Father</td>
                        <td><input readonly type="text" id="name" name="ftrfirst" value="{{ $formData->ftrfirst ?? old('ftrfirst') }}"></td>
                        <td><input readonly type="text" id="name" name="ftrmiddle" value="{{ $formData->ftrmiddle ?? old('ftrmiddle') }}"></td>
                        <td><input readonly type="text" id="name" name="ftrfast" value="{{ $formData->ftrfast ?? old('ftrfast') }}"></td>
                        <td><input readonly type="text" id="name" name="ftrctznno" value="{{ $formData->ftrctznno ?? old('ftrctznno') }}"></td>
                        <td><input readonly type="text" id="name" name="ftrctznissdate" value="{{ $formData->ftrctznissdate ?? old('ftrctznissdate') }}"></td>
                        <td><input readonly type="text" id="name" name="ftrctznissdistrict" value="{{ $formData->ftrctznissdistrict ?? old('ftrctznissdistrict') }}"></td>
                        <td><input readonly type="text" id="name" name="ftrctzndistrict" value="{{ $formData->ftrctzndistrict ?? old('ftrctzndistrict') }}"></td>
                    </tr>
                    <tr>
                        <td>Mother</td>
                        <td><input readonly type="text" id="name" name="mthrfirst" value="{{ $formData->mthrfirst ?? old('mthrfirst') }}"></td>
                        <td><input readonly type="text" id="name" name="mthrmiddle" value="{{ $formData->mthrmiddle ?? old('mthrmiddle') }}"></td>
                        <td><input readonly type="text" id="name" name="mthrlast" value="{{ $formData->mthrlast ?? old('mthrlast') }}"></td>
                        <td><input readonly type="text" id="name" name="mthrctznno" value="{{ $formData->mthrctznno ?? old('mthrctznno') }}"></td>
                        <td><input readonly type="text" id="name" name="mthrctznissdate" value="{{ $formData->mthrctznissdate ?? old('mthrctznissdate') }}"></td>
                        <td><input readonly type="text" id="name" name="mthrctznissdistrict" value="{{ $formData->mthrctznissdistrict ?? old('mthrctznissdistrict') }}"></td>
                        <td><input readonly type="text" id="name" name="mthrctzndistrict" value="{{ $formData->mthrctzndistrict ?? old('mthrctzndistrict') }}"></td>
                    </tr>
                </table>




            </fieldset>

            <fieldset>
                <legend><span class="number">4</span>Info Comm/Pers</legend>
                <table cellspacing="0">
                    <tr valign="top">
                        <td colspan="2"><strong>PAN/VAT No</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">ID Type:</label>
                        	<input readonly type="text" id="name" name="comid" value="{{ $formData->comid ?? old('comid') }}">
                           </td>
                        <td><label for="name">Issued Authority:</label>
                            <input readonly type="text" id="name" name="comauthority" value="{{ $formData->comauthority ?? old('comauthority') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Id Number:</label>
                            <input readonly type="text" id="name" name="comidnum" value="{{ $formData->comidnum ?? old('comidnum') }}"></td>
                        <td><label for="name">Issued in District:</label>
                            <input readonly type="text" id="name" name="comissdistrict" value="{{ $formData->comissdistrict ?? old('comissdistrict') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Business Activity Code:</label>
                            <input readonly type="text" id="name" name="comactcode" value="{{ $formData->comactcode ?? old('comactcode') }}"></td>
                        <td><label for="name">Issued Date:</label>
                            <input readonly type="text" id="name" name="comissdate" value="{{ $formData->comissdate ?? old('comissdate') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Expiry Date:</label>
                            <input readonly type="text" id="name" name="comexpiry" value="{{ $formData->comexpiry ?? old('comexpiry') }}"></td>
                        <td></td>
                    </tr>
                </table>




            </fieldset>


            <fieldset>
                <legend><span class="number">5</span>Others</legend>
                <table cellspacing="0">
                    <tr valign="top">
                        <td><label for="name">Occupation:</label>
                        	<input readonly type="text" id="name" name="othoccupation" value="{{ $formData->othoccupation ?? old('othoccupation') }}">
                            </td>
                        <td><label for="name">Source of Fund:</label>
                        	<input readonly type="text" id="name" name="othsource" value="{{ $formData->othsource ?? old('othsource') }}">
                            </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Purpoose of A/C:</label>
                        	<input readonly type="text" id="name" name="othpurpose" value="{{ $formData->othpurpose ?? old('othpurpose') }}">
                           </td>
                        <td><label for="name">Annual Turnover:</label>
                            <input readonly type="text" id="name" name="othturnover" value="{{ $formData->othturnover ?? old('othturnover') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Foreign Associate</strong></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <label for="job"><input readonly type="checkbox" id="" name="forassociate" /> Is Foreign Associate:</label></td>
                        <td><label for="name">Residential Status:</label>
                            <input readonly type="text" id="name" name="forresidential" value="{{ $formData->forresidential ?? old('forresidential') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Name of Director (None Personal):</label>
                            <input readonly type="text" id="name" name="fordirector" value="{{ $formData->fordirector ?? old('fordirector') }}"></td>
                        <td><label for="name">Country:</label>
                            <input readonly type="text" id="name" name="forcountry" value="{{ $formData->forcountry ?? old('forcountry') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>KYC</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Business Unit:</label>
                            <input readonly type="text" id="name" name="kycbusiness" value="{{ $formData->kycbusiness ?? old('kycbusiness') }}"></td>
                        <td><label for="name">Reviewed Date:</label>
                            <input readonly type="text" id="name" name="kycreviewed" value="{{ $formData->kycreviewed ?? old('kycreviewed') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Deposit Risk Grade:</label>
                        	<input readonly type="text" id="name" name="kycriskgrade" value="{{ $formData->kycriskgrade ?? old('kycriskgrade') }}">
                            </td>
                        <td><label for="name">Trans Reviewed Date:</label>
                            <input readonly type="text" id="name" name="kyctrans" value="{{ $formData->kyctrans ?? old('kyctrans') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Reason:</label>
                            <input readonly type="text" id="name" name="kycreason" value="{{ $formData->kycreason ?? old('kycreason') }}"></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>KYC Doc Update</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">DOC Update Status:</label>
                        	<input readonly type="text" id="name" name="docstatus" value="{{ $formData->docstatus ?? old('docstatus') }}">
                            </td>
                        <td><label for="name">Date of Update:</label>
                            <input readonly type="text" id="name" name="docdateupdate" value="{{ $formData->docdateupdate ?? old('docdateupdate') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">KYC Remarks:</label>
                            <input readonly type="text" id="name" name="docremarks" value="{{ $formData->docremarks ?? old('docremarks') }}"></td>
                        <td><label for="name">Keep Personal Category:</label>
                            <label for="job"><input readonly type="checkbox" id="" name="perexposed" /> Politically Exposed Person(PEP):</label>
                            <label for="job"><input readonly type="checkbox" id="" name="perpunish" /> Declaration for punishment any crime:</label>
                            <label for="job"><input readonly type="checkbox" id="" name="perhighpositioned" /> High Positioned Person (HPP):</label>
                            <label for="job"><input readonly type="checkbox" id="" name="pernonface" /> Non Face to face Customer(NF2F):</label>
                            <label for="job"><input readonly type="checkbox" id="" name="pervimpper" /> Very Important Person (VIP):</label>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Insurance</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Insurance Information:</label>
                            <input readonly type="text" id="name" name="insinfo" value="{{ $formData->insinfo ?? old('insinfo') }}"></td>
                        <td><label for="name">Remarks:</label>
                            <input readonly type="text" id="name" name="insremark" value="{{ $formData->insremark ?? old('insremark') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Client Limit:</label>
                            <input readonly type="text" id="name" name="inslimit" value="{{ $formData->inslimit ?? old('inslimit') }}"></td>
                        <td><label for="name">Group:</label>
                            <input readonly type="text" id="name" name="insgroup" value="{{ $formData->insgroup ?? old('insgroup') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Borrower Code:</label>
                            <input readonly type="text" id="name" name="insborrower" value="{{ $formData->insborrower ?? old('insborrower') }}"></td>
                        <td><label for="job"><input readonly type="checkbox" id="" name="insstatement" /> Mail Statement</label></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job"><input readonly type="checkbox" id="" name="insmis" /> Keep MIS?</label></td>
                        <td><label for="job"><input readonly type="checkbox" id="" name="inskycupdate" /> KYC Update</label></td>
                    </tr>
                </table>




            </fieldset>


            <fieldset>
                <legend><span class="number">6</span>Extra Detail</legend>
                <table cellspacing="0">
                    <tr valign="top">
                        <td colspan="2"><strong>Client Extra Detail</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Name:</label>
                            <input readonly type="text" id="name" name="extname" value="{{ $formData->extname ?? old('extname') }}"></td>
                        <td><label for="name">Citizenship No:</label>
                            <input readonly type="text" id="name" name="extcitizen" value="{{ $formData->extcitizen ?? old('extcitizen') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Key Risk Grade:</label>
                            <input readonly type="text" id="name" name="extriskgrade" value="{{ $formData->extriskgrade ?? old('extriskgrade') }}"></td>
                        <td><label for="name">Alias:</label>
                            <input readonly type="text" id="name" name="extalias" value="{{ $formData->extalias ?? old('extalias') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Citizen District:</label>
                            <input readonly type="text" id="name" name="extctzndistrict" value="{{ $formData->extctzndistrict ?? old('extctzndistrict') }}"></td>
                        <td><label for="name">Org Name:</label>
                            <input readonly type="text" id="name" name="extorgname" value="{{ $formData->extorgname ?? old('extorgname') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Designation:</label>
                            <input readonly type="text" id="name" name="extdesignation" value="{{ $formData->extdesignation ?? old('extdesignation') }}"></td>
                        <td><label for="name">Date of Birth:</label>
                            <input readonly type="text" id="name" name="extbirth" value="{{ $formData->extbirth ?? old('extbirth') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Father Name:</label>
                            <input readonly type="text" id="name" name="extfather" value="{{ $formData->extfather ?? old('extfather') }}"></td>
                        <td><label for="name">Grand Father's Name:</label>
                            <input readonly type="text" id="name" name="extgrandfather" value="{{ $formData->extgrandfather ?? old('extgrandfather') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Spouse Name:</label>
                            <input readonly type="text" id="name" name="extspouse" value="{{ $formData->extspouse ?? old('extspouse') }}"></td>
                        <td><label for="name">Citizen Number Issue Date:</label>
                            <input readonly type="text" id="name" name="extctznissddate" value="{{ $formData->extctznissddate ?? old('extctznissddate') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <label for="job"><input readonly type="checkbox" id="" name="extsignatory" /> Is Signatory</label></td>
                        <td><label for="job"><input readonly type="checkbox" id="" name="extdirector" /> Is Director</label></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Role of Director:</label>
                            <input readonly type="text" id="name" name="extrole" value="{{ $formData->extrole ?? old('extrole') }}"></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Residential Address</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">House/Block no:</label>
                            <input readonly type="text" id="name" name="reshouse" value="{{ $formData->reshouse ?? old('reshouse') }}"></td>
                        <td><label for="name">Area/Street Name:</label>
                            <input readonly type="text" id="name" name="resstreet" value="{{ $formData->resstreet ?? old('resstreet') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Ward No:</label>
                            <input readonly type="text" id="name" name="resward" value="{{ $formData->resward ?? old('resward') }}"></td>
                        <td><label for="name">VC/MDC:</label>
                            <input readonly type="text" id="name" name="resvcmdc" value="{{ $formData->resvcmdc ?? old('resvcmdc') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">District:</label>
                            <input readonly type="text" id="name" name="resdistrict" value="{{ $formData->resdistrict ?? old('resdistrict') }}"></td>
                        <td><label for="name">City:</label>
                            <input readonly type="text" id="name" name="rescity" value="{{ $formData->rescity ?? old('rescity') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Tel No:</label>
                            <input readonly type="text" id="name" name="restel" value="{{ $formData->restel ?? old('restel') }}"></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Permanent Address</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">House/Block no:</label>
                            <input readonly type="text" id="name" name="extperhouse" value="{{ $formData->extperhouse ?? old('extperhouse') }}"></td>
                        <td><label for="name">Area/Street Name:</label>
                            <input readonly type="text" id="name" name="extperstreet" value="{{ $formData->extperstreet ?? old('extperstreet') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Ward No:</label>
                            <input readonly type="text" id="name" name="extperward" value="{{ $formData->extperward ?? old('extperward') }}"></td>
                        <td><label for="name">VC/MDC:</label>
                            <input readonly type="text" id="name" name="extpervcmdc" value="{{ $formData->extpervcmdc ?? old('extpervcmdc') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">District:</label>
                            <input readonly type="text" id="name" name="extperdistrict" value="{{ $formData->extperdistrict ?? old('extperdistrict') }}"></td>
                        <td><label for="name">City:</label>
                            <input readonly type="text" id="name" name="extpercity" value="{{ $formData->extpercity ?? old('extpercity') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Tel No:</label>
                            <input readonly type="text" id="name" name="extpertel" value="{{ $formData->extpertel ?? old('extpertel') }}"></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Permanent Address</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">House/Block no:</label>
                            <input readonly type="text" id="name" name="mailhouse" value="{{ $formData->mailhouse ?? old('mailhouse') }}"></td>
                        <td><label for="name">Area/Street Name:</label>
                            <input readonly type="text" id="name" name="mailstreet" value="{{ $formData->mailstreet ?? old('mailstreet') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Ward No:</label>
                            <input readonly type="text" id="name" name="mailward" value="{{ $formData->mailward ?? old('mailward') }}"></td>
                        <td><label for="name">VC/MDC:</label>
                            <input readonly type="text" id="name" name="mailvcmdc" value="{{ $formData->mailvcmdc ?? old('mailvcmdc') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">District:</label>
                            <input readonly type="text" id="name" name="maildistrict" value="{{ $formData->maildistrict ?? old('maildistrict') }}"></td>
                        <td><label for="name">City:</label>
                            <input readonly type="text" id="name" name="mailcity" value="{{ $formData->mailcity ?? old('mailcity') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Tel No:</label>
                            <input readonly type="text" id="name" name="mailtel" value="{{ $formData->mailtel ?? old('mailtel') }}"></td>
                        <td><label for="name">Address:</label>
                            <input readonly type="text" id="name" name="mailaddress" value="{{ $formData->mailaddress ?? old('mailaddress') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Fax No:</label>
                            <input readonly type="text" id="name" name="mailfax" value="{{ $formData->mailfax ?? old('mailfax') }}"></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Business Address</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Tel No:</label>
                            <input readonly type="text" id="name" name="bustel" value="{{ $formData->bustel ?? old('bustel') }}"></td>
                        <td><label for="name">Fax No:</label>
                            <input readonly type="text" id="name" name="busfax" value="{{ $formData->busfax ?? old('busfax') }}"></td>
                    </tr>
                </table>




            </fieldset>




        </form>
    </div>

@endsection
