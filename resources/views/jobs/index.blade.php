@extends('layouts.app')
@section('css')

@endsection
@section('page_title')
    Jobs Listing
@endsection
@section('right_button')
    <a href="{{ route('jobs.add') }}" data-toggle="modal" class="btn btn-outline btn-info pull-right" ><i class="fa fa-plus"></i>&nbsp;Create new job</a>
@stop
@section('content')
<<<<<<< HEAD
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Filter by category</label>
                <select name="category" id="category" class="form-control">
                    <option value="">---Choose---</option>
                    @if(isset($category))
                        @foreach($category as $value)
                            <option value="{{ $value }}" >{{ $value }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="">Filter by designation</label>
                <select name="" id="" class="form-control">
                    <option value="">---Choose---</option>
                    @if(isset($designation))
                        @foreach($designation as $value)
                            <option value="{{ $value }}">{{ $value }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
    </div>
=======
    {{--<div class="row">--}}
        {{--<div class="col-md-6">--}}
            {{--<div class="form-group">--}}
                {{--<label for="">Filter by category</label>--}}
                {{--<select name="category" id="category" class="form-control">--}}
                    {{--<option value="">---Choose---</option>--}}
                    {{--@if(isset($category))--}}
                        {{--@foreach($category as $value)--}}
                            {{--<option value="{{ $value }}" >{{ $value }}</option>--}}
                        {{--@endforeach--}}
                    {{--@endif--}}
                {{--</select>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<div class="col-md-6">--}}
            {{--<div class="form-group">--}}
                {{--<label for="">Filter by designation</label>--}}
                {{--<select name="" id="" class="form-control">--}}
                    {{--<option value="">---Choose---</option>--}}
                    {{--@if(isset($designation))--}}
                        {{--@foreach($designation as $value)--}}
                            {{--<option value="{{ $value }}">{{ $value }}</option>--}}
                        {{--@endforeach--}}
                    {{--@endif--}}
                {{--</select>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
>>>>>>> 851e66ed786a49b8a2293e62a98b9cf3402bbdd8
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table id="datatable" class="table table-borderless table-striped" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th style="width: 500px;">Job Title</th>
                            <th>Deadline</th>
                            <th>Applications</th>
                            <th>Category</th>
                            <th>Designation</th>
                            <th>Author</th>
                            <th>Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($jobs) AND $jobs->count()>0)
                            @foreach($jobs as $val)
                                <tr class="title">
                                    <td>
                                        {{ $val->title }} @if($val->is_published==1) <small class="label label-success">Published</small> @else <small class="label label-danger">Draft</small> @endif
                                        <div class="action">
                                            <a href="{{ route('jobs.application',$val->id) }}" class="btn btn-link btn-sm">View application list</a>
                                            <span class="vl"></span>
                                            <a href="{{ route('jobs.edit',$val->id) }}" class="btn btn-link btn-sm">Edit</a>
                                            <span class="vl"></span>
                                            <a class="btn btn-link btn-sm text-danger" href="#deleteJob" data-id="{{ $val->id }}" data-toggle="modal">Delete</a>
                                        </div>
                                    </td>
                                    <td>{{ $val->deadline->diffForHumans() }} @if($val->deadline->isPast()==true) <span class="badge badge-danger">Expired</span> @else <span class="badge badge-success">Open</span> @endif</td>
                                    <td>
                                        <a href="{{ route('jobs.application',$val->id) }}" class="btn btn-link btn-sm">
                                            {{ '[ '.$val->application->count().' ] Application(s) ' }}
                                        </a>
                                    </td>
                                    <td>{{ $val->category ?? '' }}</td>
                                    <td>{{ $val->designation ?? '' }}</td>
<<<<<<< HEAD
                                    <td><a href="{{ route('profile.view',$val->user->id) }}" calss="btn btn-link btn-sm">{{ $val->user->first_name }} {{ $val->user->last_name }}</a></td>
=======
                                    <td><a href="{{ route('profile.view',$val->user->id) }}" class="btn btn-link btn-sm">{{ $val->user->first_name }} {{ $val->user->last_name }}</a></td>
>>>>>>> 851e66ed786a49b8a2293e62a98b9cf3402bbdd8
                                    <td>{{ $val->created_at->diffForHumans() }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="5">No items found <a href="{{ route('jobs.add') }}" data-toggle="modal" class="btn btn-link btn-sm" >&nbsp;Create new job</a></td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('jobs.modal')
@endsection

@section('scripts')
    <script>
        $('#datatable').DataTable({
            dom: 'Bfrtip',
            columnDefs: [ { "orderable": false, "visible": true } ],
        });


        // MOVE TO TRASH
        $('#deleteJob').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#job_id").val(id);
        });

        $('#deleteJob').find('.modal-footer #confirm_yes').on('click', function () {
            var id = $("#job_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('jobs.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    // console.log(msg);
                    $("#deleteJob").modal("hide");
                    window.location.reload();
                }
            });
        });
        // AJAX DELETE PERMISSION END

        // FILTER
        $(document).ready(function(){
            $("#category").on('change', function(){
                var category = this.value;
                $.ajax({
                    type: "GET",
                    url: "",
                    headers: {
                        'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                    },
                    data: "key=" + category,
                    success: function (msg) {
                        window.location.reload();
                        category='';
                    }
                });
            });

        });
    </script>
@endsection
