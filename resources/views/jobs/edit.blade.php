@extends('layouts.app')
@section('css')
    <link href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet">
    <link href="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page_title')
    Edit job - {{ $job->title ?? '' }}
@endsection
@section('right_button')

@stop

@section('content')
    <form class="form-horizontal" method="post" action="{{ route('jobs.update') }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="job_id" value="{{ $job->id }}" id="">
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="row pull-right">
                        <div class="col-md-12">
                            <button type="submit" name="submit" class="btn btn-success btn-outline btn-sm" value="0">Save</button>
                            <button type="submit" name="submit" class="btn btn-info btn-outline btn-sm" value="1">Save & Publish</button>
                        </div>
                    </div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="ti-home"></i></span>
                                <span class="hidden-xs"> Job Description</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Enter title here" name="title" value = "{{ $job->title ?? '' }}" id="title">
                                    @if ($errors->has('title'))
                                        <span class="invalid-feedback text-danger" role="alert">
                                            {{ $errors->first('title') }}
                                        </span>
                                    @endif
                                </div>
                                <div class="form-group example">
                                    <div class="input-group">
                                        <label for="">Job Deadline</label>
                                        <input type="text" name="deadline" class="form-control" id="datepicker-autoclose" placeholder="job deadline" value="{{ $job->deadline ?? '' }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <textarea id="ckeditor" name="description">{{ $job->description ?? '' }}</textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@section('scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script>
        CKEDITOR.replace( 'ckeditor' ,{
            filebrowserImageBrowseUrl: "{{ url(STATIC_DIR.'laravel-filemanager?type=Images') }}",
            filebrowserImageUploadUrl: "{{ url(STATIC_DIR.'laravel-filemanager/upload?type=Images&_token=') }}",
            filebrowserBrowseUrl: "{{ url(STATIC_DIR.'laravel-filemanager?type=Files') }}",
            filebrowserUploadUrl: "{{ url(STATIC_DIR.'laravel-filemanager/upload?type=Files&_token=') }}"
        });
    </script>
    <script src="{{asset(STATIC_DIR.'vendor/laravel-filemanager/js/lfm.js')}}"></script>
    <script>
        $(function () {
            $('#sitelogo').filemanager('image',{prefix:"{{URL(STATIC_DIR.'laravel-filemanager')}}"});
        });

        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });
    </script>
@endsection
