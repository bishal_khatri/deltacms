@extends('layouts.app')
@section('css')

@endsection
@section('page_title')
	Application Listing
@endsection
@section('right_button')
	<a href="{{ route('jobs.index') }}" class="btn btn-outline btn-info pull-right" data-toggle="modal" ><i class="fa fa-undo"></i>&nbsp;Back</a>
@stop
@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
                <div class="row">
                    <div class="col-md-12">
                        <div class="btn-group pull-right" style="padding-left: 20px;">
                            <button class="btn dropdown-toggle" data-toggle="dropdown"> Export <i class="fa fa-angle-down"></i>
                            </button>
                            <ul class="dropdown-menu pull-right">
                                <li> <a href="{{ route('jobs.application.export_users',[$job_id,'xls']) }}"> Export to XLS </a> </li>
                                <li> <a href="{{ route('jobs.application.export_users',[$job_id,'xlsx']) }}"> Export to xlsx </a> </li>
                                <li> <a href="{{ route('jobs.application.export_users',[$job_id,'csv']) }}"> Export to CSV </a> </li>
                            </ul>
                        </div>
                    </div>
                </div>
				<br>
				<div class="row">
					<div class="col-md-12">
						<div class="table-responsive">
							<table id="datatable" class="table table-borderless table-striped" cellspacing="0" width="100%">
								<thead>
								<tr>
									<th>Applicant</th>
									<th>Contact</th>
									<th>Email</th>
									<th>Applied Date</th>
								</tr>
								</thead>
								<tbody>
								@if(isset($applicants) AND $applicants->count()>0)
									@foreach($applicants as $val)
										<tr class="title">
											<td>
												<a href="{{ route('view_user_profile',$val->site_user->id) }}" target="_blank">{{ $val->site_user->first_name }} {{ $val->site_user->last_name }}</a>
												<div class="action">
													<a href="{{ route('view_user_profile',$val->site_user->id) }}" class="btn btn-link btn-sm text-success" target="_blank">View Profile</a>
												</div>
											</td>
											<td>{{ $val->site_user->contact }}</td>
											<td>{{ $val->site_user->email }}</td>
											<td>{{ $val->created_at->format('M d, Y') }} <small style="font-style: italic;">{{ $val->created_at->diffForHumans() }}</small></td>
										</tr>
									@endforeach
								@else
									<tr>
										<td colspan="5">No items found.</td>
									</tr>
								@endif
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	@include('category.modal')
@endsection

@section('scripts')
	<script>
        $('#datatable').DataTable({
            dom: 'Bfrtip',
            columnDefs: [ { "orderable": false, "visible": true } ],
        });

        $('#add').find('.modal-footer #confirm_yes').on('click', function () {
            $("#confirmDelete").modal("hide");
            window.location.reload();

        });

        // MOVE TO TRASH
        $('#deleteCategory').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#category_id").val(id);
        });

        $('#deleteCategory').find('.modal-footer #confirm_yes').on('click', function () {
            var id = $("#category_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('category.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    // console.log(msg);
                    $("#confirmDelete").modal("hide");
                    window.location.reload();
                }
            });
        });
        // AJAX DELETE PERMISSION END
	</script>
@endsection
