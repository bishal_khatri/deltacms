@extends('layouts.app')
@section('css')
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/owl.carousel/owl.carousel.min.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/owl.carousel/owl.theme.default.css') }}" rel="stylesheet" type="text/css" />
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet">
	<style>
		.item {
			position:relative;
			padding-top:20px;
			display:inline-block;
		}
		.notify-badge{
			z-index: 99999 !important;
			position: absolute;
			right:-20px;
			top:10px;
			background:red;
			text-align: center;
			border-radius: 30px 30px 30px 30px;
			color:white;
			padding:5px 10px;
			font-size:20px;
		}
	</style>

@endsection
@section('page_title')
	List slider image
@endsection
@section('right_button')
	<a href="#add" class="btn btn-info btn-outline pull-right" data-toggle="modal" ><i class="fa fa-plus"></i>&nbsp;Add image</a>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div class="row">
					<div class="col-md-12">
						<div class="row el-element-overlay m-b-40">
							{{-- {{ dd($sliders) }} --}}
							@if($sliders->count()>0)
								@foreach($sliders as $value)
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 col-md-offset-3">
										<div class="white-box">
											<div class="el-card-item">
												<div class="el-card-avatar el-overlay-1 item">
													<img src="{{ asset(STATIC_DIR.'storage/'.$value->image) }}" style="padding-bottom:5px;"/>
													@if($value->is_active==1)
														<span class="badge badge-pill badge-success" >Active</span>
													@endif
													<div class="el-overlay">
														<ul class="el-info">
															<li>
																<a class="btn default btn-outline image-popup-vertical-fit" href="{{ asset(STATIC_DIR.'storage/'.$value->image) }}">
																	<i class="icon-magnifier"></i>
																</a>
															</li>
															@if($value->is_active==0)
																<li>
																	<a class="btn default btn-outline" href="{{route('slider.activate',[$value->id,'activate'])}}">Activate
																	</a>
																</li>
															@else
																<li>
																	<a class="btn default btn-outline text-success" href="{{route('slider.activate',[$value->id,'deactivate'])}}">Deactivate
																	</a>
																</li>
															@endif
															<li>
																<a class="btn default btn-outline text-danger" href="#confirmDelete" data-ids="{{ $value->id }}" data-toggle="modal">
																	<i class="icon-trash"></i>
																</a>
															</li>
														</ul>
													</div>
												</div>
												<div class="el-card-content">
													{{--<h3 class="box-title">{{ $value->title }}--}}
                                                        <sup><a href="{{ route('slider.edit',$value->id) }}" class="btn btn-link btn-sm text-info">Edit</a></sup>
                                                    {{--</h3>--}}
													{{--<small>{{ $value->description }}</small>--}}
													{{--@if (isset($value->link))--}}
														{{--<p>--}}
															{{--<a href="{{ url($value->link ?? '') }}" class="btn btn-link" target="{{ $value->target }}">--}}
																{{--<small class="text-gray-dark">{{ $value->button_text ?? $value->link }}</small>--}}
															{{--</a>--}}
														{{--</p>--}}
													{{--@endif--}}
													{{--<br/>--}}
												</div>
											</div>
										</div>
									</div>
								@endforeach
							@else
								<p>No items found <a href="#add" class="btn btn-link btn-sm" data-toggle="modal" >Add image</a></p>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	@include('slider.modal')
	@include('slider.create_modal')
@endsection

@section('scripts')
	<script>

        $('#add').find('.modal-footer #confirm_yes').on('click', function () {
            $("#confirmDelete").modal("hide");
            window.location.reload();
        });
		
		
		@if (count($errors) > 0)
        $('#add').modal('show');
		@endif


        // AJAX SLIDER DELETE
        $('#confirmDelete').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            // console.log(button.data('ids'));
            var ids = button.data('ids');
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#hidden_id").val(ids);
        });

        $('#confirmDelete').find('.modal-footer #confirm_yes').on('click', function () {
            //$(this).data('form').submit();
            var id = $("#hidden_id").val();
            // console.log(id);
            $.ajax({
                type: "POST",
                url: "{{ route('slider.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    // console.log(msg);
                    $("#confirmDelete").modal("hide");
                    window.location.reload();
                }
            });
        });
        // AJAX SLIDER DELETE END
	</script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/owl.carousel/owl.carousel.min.js') }}"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/owl.carousel/owl.custom.js') }}"></script>
	
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
	<script type="text/javascript">
        $('.dropify').dropify();
        // $(document).ready(function($) {
        //     // delegate calls to data-toggle="lightbox"
        //     $(document).delegate('*[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', 'click', function(event) {
        //         event.preventDefault();
        //         return $(this).ekkoLightbox({
        //             onShown: function() {
        //                 if (window.console) {
        //                     return console.log('Checking our the events huh?');
        //                 }
        //             },
        //             onNavigate: function(direction, itemIndex) {
        //                 if (window.console) {
        //                     return console.log('Navigating ' + direction + '. Current item: ' + itemIndex);
        //                 }
        //             }
        //         });
        //     });
        //     //Programatically call
        //     $('#open-image').click(function(e) {
        //         e.preventDefault();
        //         $(this).ekkoLightbox();
        //     });
        //     $('#open-youtube').click(function(e) {
        //         e.preventDefault();
        //         $(this).ekkoLightbox();
        //     });
        //     // navigateTo
        //     $(document).delegate('*[data-gallery="navigateTo"]', 'click', function(event) {
        //         event.preventDefault();
        //         var lb;
        //         return $(this).ekkoLightbox({
        //             onShown: function() {
        //                 lb = this;
        //                 $(lb.modal_content).on('click', '.modal-footer a', function(e) {
        //                     e.preventDefault();
        //                     lb.navigateTo(2);
        //                 });
        //             }
        //         });
        //     });
        // });
	</script>
@endsection
