{{--ADD FORM--}}
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<form action="{{ route('slider.add') }}" method="post" enctype="multipart/form-data">
			@csrf
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
					<h4 class="modal-title" id="exampleModalLabel1">
						<i class="fa fa-plus-circle" id="icon-terminate" ></i> Add Slider Image
					</h4>
				</div>
				<div class="modal-body">
					<div class="row pull-right">
						<div class="col-md-12">
							<button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Close</button>
							<button type="submit" class="btn btn-success btn-outline btn-sm" id="confirm_yes">Upload</button>
						</div>
					</div>
					<ul class="nav nav-tabs" role="tablist">
						<li role="presentation" class="active">
							<a href="#image" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">
								<span class="visible-xs"><i class="ti-home"></i></span>
								<span class="hidden-xs"> Image</span>
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#content" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">
								<span class="visible-xs"><i class="ti-home"></i></span>
								<span class="hidden-xs"> Content</span>
							</a>
						</li>
						<li role="presentation" class="">
							<a href="#link" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">
								<span class="visible-xs"><i class="ti-user"></i></span>
								<span class="hidden-xs">Link</span>
							</a>
						</li>
					</ul>
					<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="image">
							<div class="col-md-12">
								<div class="form-group @if ($errors->has('image')) has-error @endif">
									<label for="image" class="control-label">Slider Image:</label>
									<input type="file" id="input-file-now" name="image" class="dropify" accept="image/x-png,image/gif,image/jpeg" />
									@if ($errors->has('image'))
										<span class="text-danger" role="alert">
										<p>{{ $errors->first('image') }}</p>
									</span>
									@endif
									<p style="font-style: italic;color: darkorange;">Appropriate file type: .jpg .jpeg .png</p>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div role="tabpanel" class="tab-pane" id="content">
							<div class="col-md-12">
								<div class="form-group @if ($errors->has('title')) has-error @endif">
									<label for="title" class="control-label">Title:</label>
									<input type="text" class="form-control" name="title" placeholder="Enter title here" value="{{ old('title') }}">
									@if ($errors->has('title'))
										<span class="text-danger" role="alert">
										<p>{{ $errors->first('title') }}</p>
									</span>
									@endif
								</div>
								<div class="form-group">
									<label for="message-text" class="control-label">Description:</label>
									<textarea class="form-control" id="message-text1" name="description" placeholder="Enter description here" rows="8" cols="10">{{ old('description') }}</textarea>
									@if ($errors->has('description'))
										<span class="text-danger" role="alert">
										<p>{{ $errors->first('description') }}</p>
									</span>
									@endif
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
						<div role="tabpanel" class="tab-pane" id="link">
							<div class="col-md-12">
								<div class="form-group @if ($errors->has('button_text')) has-error @endif">
									<label for="button_text" class="control-label">Button Text</label>
									<input type="text" class="form-control" placeholder="Enter button text here" name="button_text" value="{{ old('button_text') ?? '' }}">
									@if ($errors->has('button_text'))
										<span class="text-danger" role="alert">
										    {{ $errors->first('button_text') }}
									    </span>
									@endif
								</div>
								<div class="form-group @if ($errors->has('link')) has-error @endif">
									<label for="link" class="control-label">Link:</label>
									<div class="row">
										<div class="col-md-3">
											<select name="protocol" class="form-control" id="">
												<option value="http://">http://</option>
												<option value="https://">https://</option>
											</select>
										</div>
										<div class="col-md-9">
											<input type="text" class="form-control" name="link" value="{{ old('link') }}" placeholder="Enter Link here">
											@if ($errors->has('link'))
												<span class="text-danger" role="alert">
												    {{ $errors->first('link') }}
											    </span>
											@endif
										</div>
									</div>
								</div>
								<div class="form-group @if ($errors->has('target')) has-error @endif">
									<label for="target" class="control-label">Target</label>
									<select name="target" id="" class="form-control">
										<option value="_self">Parent</option>
										<option value="_blank">New Tab</option>
									</select>
									@if ($errors->has('target'))
										<span class="text-danger" role="alert">
										    {{ $errors->first('target') }}
									    </span>
									@endif
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
				
				</div>
			</div>
		</form>
	</div>
</div>
