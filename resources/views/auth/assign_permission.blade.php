@extends('layouts.app')
@section('css')
	<link rel="stylesheet" href="{{ asset(STATIC_DIR.'plugins/bower_components/jquery-multiselect/css/multi-select.css') }}">
	<style>
		.ms-container{
			min-width: 1000px !important;
		}
		.ms-container .ms-list {
			height: 400px;
		}
	</style>
@endsection
@section('page_title')
	Assign permission
@endsection
@section('content-title')
	<h1>
		Assign permission
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Assign permission</a></li>
	</ol>
@endsection

@section('content')
    <div class="box">
        <!-- /.box-header -->
        <div class="box-body">
	        <div class="row">
		        <div class="col-md-12">
			        <h4>Assign permission to [<strong>{{ $user->first_name }} {{ $user->last_name }}</strong>]</h4>
			        <form action="{{ route('user.assign_permission_store') }}" method="post">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
				        <div class="form-group">
					        <select multiple="multiple" class="form-control" id="assign" name="selected_permission[]" >
						        <option disabled="" >Remaining Permissions</option>
						        <option disabled="" selected >Assigned Permissions</option>
						        @foreach($permissions as $prem)
							        <option value="{{ $prem->id }}" @if(in_array($prem->code_name,$active_user_permission)) selected @endif>{{ $prem->display_name }}</option>
						        @endforeach

					        </select>
				        </div>
				        <input type="submit" class="btn btn-success btn-outline btn-sm" name="assign" value="Assign">
				        <input type="submit" class="btn btn-info btn-outline btn-sm" name="assign_close" value="Assign & close">
			        </form>
		        </div>
	        </div>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="modal fade bs-modal-md" id="viewPermission" tabindex="-1" role="dialog" aria-hidden="true">
	    <div class="modal-dialog modal-md">
		    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				    <h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Permissions
				    </h4>
			    </div>
			    <div class="modal-body" style="padding-left: 20px;padding-right: 20px;">
				    <ul class="list-group" id="list">

				    </ul>
			    </div>
			    <input type="hidden" id="hidden_id">
			    <div class="modal-footer">
				    <button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
					    close
				    </button>
			    </div>
		    </div>
		    <!-- /.modal-content -->
	    </div>
    </div>
@endsection

@section('scripts')
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/jquery-multiselect/js/jquery.multi-select.js') }}"></script>
	<script>
        $('#assign').multiSelect();
	</script>
@endsection
