@extends('layouts.app')

@section('page_title')
	Register user
@endsection
@section('content-title')
	<h1>
		Register user
	</h1>
	<ol class="breadcrumb">
		<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
		<li><a href="#">Register user</a></li>
	</ol>
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <form method="POST" action="{{ route('user.update') }}">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ $id }}" id="">
                     <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                        <label>{{ __('First Name') }}</label>
                        <input type="text" class="form-control " value="{{ $user->first_name ?? ' ' }}" name="first_name" placeholder="Enter first name">
                        @if ($errors->has('first_name'))
                            <span class="invalid-feedback" role="alert">
                                <p class="text-danger">{{ $errors->first('first_name') }}</p>
                            </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                        <label>{{ __('Last Name') }}</label>
                        <input type="text" class="form-control " value="{{ $user->last_name ?? ' ' }}" name="last_name" placeholder="Enter last name">
                        @if ($errors->has('last_name'))
                            <span class="invalid-feedback" role="alert">
                                <p class="text-danger">{{ $errors->first('last_name') }}</p>
                            </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('contact') ? 'has-error' : '' }}">
                        <label>{{ __('Contact') }}</label>
                        <input type="text" class="form-control " value="{{ $user->contact ?? ' ' }}" name="contact" placeholder="Enter contact number">
                        @if ($errors->has('contact'))
                            <span class="invalid-feedback" role="alert">
                                <p class="text-danger">{{ $errors->first('contact') }}</p>
                            </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                        <label>{{ __('Address') }}</label>
                        <input type="text" class="form-control " value="{{ $user->address ?? ' ' }}" name="address" placeholder="Enter address">
                        @if ($errors->has('address'))
                            <span class="invalid-feedback" role="alert">
                                <p class="text-danger">{{ $errors->first('address') }}</p>
                            </span>
                        @endif
                    </div>

                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label>{{ __('E-Mail email') }}</label>
                        <input type="email" class="form-control " value="{{ $user->email ?? ' ' }}" name="email" placeholder="Enter email address">
                        @if ($errors->has('email'))
                            <span class="invalid-feedback" role="alert">
                                <p class="text-danger">{{ $errors->first('email') }}</p>
                            </span>
                        @endif
                    </div>                            
                    <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">Update</button>
                    <a href="{{ route('user.index') }}" class="btn btn-inverse waves-effect waves-light">Cancel</a>
                </form>
            </div>
        </div>
    </div>

@endsection
