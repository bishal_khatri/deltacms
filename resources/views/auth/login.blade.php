<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>DeltaCMS | Log in</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/bootstrap/dist/css/bootstrap.min.css') }}">
    {{-- <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets-old/bower_components/font-awesome/css/font-awesome.min.css') }}"> --}}
    {{-- <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets-old/bower_components/Ionicons/css/ionicons.min.css') }}"> --}}
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/login/css/AdminLTE.min.css') }}">
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'assets/login/css/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        .invalid-feedback{color: red;}
    </style>
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        {{--<a href=""><b>Delta</b>CMS</a>--}}
        <img src="{{ asset(STATIC_DIR.'assets/icons/logo_name.jpg') }}" alt="">
    </div>
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-group has-feedback">
                <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" placeholder="Email" name="email" value="{{ old('email') }}" required autofocus>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                @if ($errors->has('email'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('email') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" placeholder="Password" name="password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                @if ($errors->has('password'))
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('password') }}</strong>
                    </span>
                @endif
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox"> Remember Me
                        </label>
                    </div>
                </div>
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div>
            </div>
        </form>
        <a href="#">I forgot my password</a><br>
    </div>
</div>
<script src="{{ asset(STATIC_DIR.'plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<script src="{{ asset(STATIC_DIR.'assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset(STATIC_DIR.'assets/login/css/iCheck/icheck.min.js') }}"></script>
<script>
    $(function (){$('input').iCheck({checkboxClass:'icheckbox_square-blue',radioClass:'iradio_square-blue',increaseArea:'20%'});});</script>
</body>
</html>
