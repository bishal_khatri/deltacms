@extends('layouts.app')

@section('page_title')
    Profile
@endsection
@section('content-title')
    <h1>
        Profile
    </h1>

@endsection
@section('css')
    <link href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet">
@endsection

@section('content')
    <!-- .row -->
    <div class="row">
        <div class="col-md-4 col-xs-12">
            <div class="white-box">
                <div class="user-bg">
                    <div class="overlay-box">
                        <div class="user-content">
                            <a href="javascript:void(0)">
                                @if(!empty($user->image))
                                <img src="{{ asset('public/storage/'.$user->image) }}" class="thumb-lg img-circle" alt="img">
                                @else
                                    <img src="{{ asset('public/assets/icons/default-user.png') }}" class="thumb-lg img-circle" alt="img">
                                @endif
                            </a>
                            <h4 class="text-white">{{ $user->first_name }} {{ $user->last_name }}</h4>
                            <h5 class="text-white">{{ $user->email }}</h5>
                        </div>
                    </div>
                </div>
                @if(Auth::user()->id==$user->id)
                <div class="user-btm-box">
                    <h4>Change Password</h4>
                    <form action="{{ route('profile.update','password') }}" method="post" class="form-horizontal form-material">
                        <input type="hidden" name="user_id" value="{{ $user->id }}" id="">
                        @csrf
                        <div class="form-group">
                            <label class="col-md-12">Old Password</label>
                            <div class="col-md-12">
                                <input type="password" placeholder="Enter old password" name="old_password" class="form-control form-control-line">
                                @if ($errors->has('old_password'))
                                    <span class="text-danger" role="alert">
                                        {{ $errors->first('old_password') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">New Password</label>
                            <div class="col-md-12">
                                <input type="password" placeholder="Enter New Password" name="password" class="form-control form-control-line">
                                @if ($errors->has('password'))
                                    <span class="text-danger" role="alert">
                                        {{ $errors->first('password') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Re-type Password</label>
                            <div class="col-md-12">
                                <input type="password" placeholder="Re Enter New Password" name="password_confirmation" class="form-control form-control-line">
                                @if ($errors->has('password_confirmation'))
                                    <span class="text-danger" role="alert">
                                        {{ $errors->first('password_confirmation') }}
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <button class="btn btn-success">Change</button>
                            </div>
                        </div>
                    </form>
                </div>
                    @endif
            </div>
        </div>
        <div class="col-md-8 col-xs-12">
            <div class="white-box">
                <ul class="nav nav-tabs tabs customtab">
                   @if(Auth::user()->id==$user->id)
                        <li class="tab">
                            <a href="#settings" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Settings</span> </a>
                        </li>
                   @else
                        <li class="tab">
                            <a href="#profile" data-toggle="tab" aria-expanded="false"> <span class="visible-xs"><i class="fa fa-cog"></i></span> <span class="hidden-xs">Profile</span> </a>
                        </li>
                   @endif
                </ul>
                <div class="tab-content ">
                    @if(Auth::user()->id==$user->id)
                        <div class="tab-pane active" id="settings">
                            <form class="form-horizontal form-material" action="{{ route('profile.update','profile') }}" method="post" enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="user_id" value="{{ $user->id }}" id="">
                                <div class="form-group @if ($errors->has('image')) has-error @endif">
                                    <label for="image" class="control-label">Slider Image:</label>
                                    <input type="file" name="image" id="input-file-now-custom-3" class="dropify" data-height="200" data-default-file="@if(!empty($user->image)){{ asset('public/storage/'.$user->image) }}@endif"/>
                                    @if ($errors->has('image'))
                                        <span class="text-danger" role="alert">
										<p>{{ $errors->first('image') }}</p>
									</span>
                                    @endif
                                    <p style="font-style: italic;color: darkorange;">Appropriate file type: .jpg .jpeg .png</p>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">First Name</label>
                                    <div class="col-md-12">
                                        <input type="text" name="first_name" placeholder="{{ $user->first_name ?? '' }}" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Last Name</label>
                                    <div class="col-md-12">
                                        <input type="text" name="last_name" placeholder="{{ $user->last_name ?? '' }}" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <input type="email" name="email" placeholder="{{ $user->email ?? '' }}" class="form-control form-control-line" name="example-email" id="example-email"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Phone No</label>
                                    <div class="col-md-12">
                                        <input type="text" name="contact" placeholder="{{ $user->contact ?? '' }}" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Address</label>
                                    <div class="col-md-12">
                                        <input type="text" name="address" placeholder="{{ $user->address ?? '' }}" class="form-control form-control-line"> </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button class="btn btn-success">Update Profile</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    @else
                        <div class="tab-pane active" id="profile">
                            <form class="form-horizontal form-material">
                                <div class="form-group">
                                    <label for="image" class="control-label">Profile Image:</label><br>
                                    <img src="{{ asset($user->image ?? 'public/assets/icons/default-user.png') }}" alt="" width="150">
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">First Name</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="{{ $user->first_name ?? '' }}" class="form-control form-control-line" readonly> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Last Name</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="{{ $user->last_name ?? '' }}" class="form-control form-control-line" readonly> </div>
                                </div>
                                <div class="form-group">
                                    <label for="example-email" class="col-md-12">Email</label>
                                    <div class="col-md-12">
                                        <input type="email" placeholder="{{ $user->email ?? '' }}" class="form-control form-control-line" name="example-email" id="example-email" readonly> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Phone No</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="{{ $user->contact ?? '' }}" class="form-control form-control-line" readonly> </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Address</label>
                                    <div class="col-md-12">
                                        <input type="text" placeholder="{{ $user->address ?? '' }}" class="form-control form-control-line" readonly> </div>
                                </div>
                            </form>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <script type="text/javascript">
        $('.dropify').dropify();
    </script>
@endsection
