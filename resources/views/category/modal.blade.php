{{--Category MODAL--}}
<div class="modal fade" id="createCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<form action="{{ route('category.store') }}" id="categoryForm" method="post" enctype="multipart/form-data">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel1">
					<i class="fa fa-plus-circle" id="icon-terminate" ></i> 
					Create new category
				</h4> 
			</div>
			<div class="modal-body">
				@csrf
				<div class="form-group @if ($errors->has('title')) has-error @endif">
					<label for="title" class="control-label">Title:</label>
					<input type="text" class="form-control" name="title" placeholder="Enter category name here" value="{{ old('title') }}">
					@if ($errors->has('title'))
						<span class="text-danger" role="alert">
							<p>{{ $errors->first('title') }}</p>
						</span>
					@endif
				</div>
				<div class="form-group @if ($errors->has('sub_title')) has-error @endif">
					<label for="sub_title" class="control-label">Sub-Title:</label>
					<textarea class="form-control" name="sub_title" placeholder="Enter subtitle" cols="30" rows="10">{{ old('sub_title') }}</textarea>
					@if ($errors->has('sub_title'))
						<span class="text-danger" role="alert">
							<p>{{ $errors->first('sub_title') }}</p>
						</span>
					@endif
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-outline btn-sm" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success btn-outline btn-sm" id="confirm_yes"><i class="icon-check"></i> Save</button>
			</div>
		</div>
		</form>
	</div>
</div>

<!--EDIT CATEGORY MODAL-->
<div class="modal fade" id="editCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<form action="{{ route('category.update') }}" id="categoryForm" method="post" enctype="multipart/form-data">
			<input type="hidden" name="category_id" id="e_category_id">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel1">
						<i class="fa fa-plus-circle" id="icon-terminate" ></i>
						Update new category
					</h4>
				</div>
				<div class="modal-body">
					@csrf
					<div class="form-group @if ($errors->has('title')) has-error @endif">
						<label for="title" class="control-label">Title:</label>
						<input type="text" class="form-control" name="title" placeholder="Enter category name here" id="e_category_title">
						@if ($errors->has('title'))
							<span class="text-danger" role="alert">
							<p>{{ $errors->first('title') }}</p>
						</span>
						@endif
					</div>
					<div class="form-group @if ($errors->has('sub_title')) has-error @endif">
						<label for="sub_title" class="control-label">Sub-Title:</label>
						<textarea class="form-control" name="sub_title" placeholder="Enter subtitle" cols="30" rows="10" id="e_category_subtitle"></textarea>
						@if ($errors->has('sub_title'))
							<span class="text-danger" role="alert">
							<p>{{ $errors->first('sub_title') }}</p>
						</span>
						@endif
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default btn-outline btn-sm" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-success btn-outline btn-sm" id="confirm_yes"><i class="icon-check"></i> Save</button>
				</div>
			</div>
		</form>
	</div>
</div>

<!-- DELETE ITEM MODAL -->
<div class="modal fade bs-modal-sm" id="deleteItem" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Delete Permanently
				</h4>
			</div>
			<div class="modal-body"> Do you want to delete this page ?</div>
			<input type="hidden" id="item_id">
			<div class="modal-footer">
				<button type="button" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
				</button>
				<button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
					No
				</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
</div>


<!-- DELETE Category MODAL -->
<div class="modal fade bs-modal-sm" id="deleteCategory" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Delete Permanently
				</h4>
			</div>
			<div class="modal-body"> Do you want to delete this page ?</div>
			<input type="hidden" id="category_id">
			<div class="modal-footer">
				<button type="button" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
				</button>
				<button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
					No
				</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
</div>
