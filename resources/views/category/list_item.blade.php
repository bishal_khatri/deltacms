@extends('layouts.app')
@section('css')

@endsection
@section('page_title')
   Category Item Listing ( {{ $category->category_title }} )
@endsection
@section('right_button')
<div class="pull-right">
    <a href="{{ route('category.add_item',$category_id) }}" class="btn btn-info btn-outline" data-toggle="modal" ><i class="fa fa-plus"></i> Add page</a>
    <a href="{{ route('category.index') }}" class="btn btn-info btn-outline" data-toggle="modal" ><i class="fa fa-undo"></i>&nbsp;Back</a>
</div>
@stop
@section('content')
 <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table id="datatable" class="table table-borderless table-striped" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th style="width: 500px;">Title</th>
                            <th>Cover Image</th>
                            <th>Author</th>
                            <th>Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($items->count()>0)
                            @foreach($items as $val)
                                <tr class="title">
                                    <td>{{ $loop->iteration }}</td>
                                    <td>
                                        <a href="{{ $val->post_url }}" target="_blank" class="btn btn-link btn-sm">
                                            {{ str_limit($val->post_title,300) }}
                                        </a>
                                        <div class="action">
                                            <a href="{{ $val->post_url }}" target="_blank" class="btn btn-link btn-sm">Preview</a>
                                            <span class="vl"></span>
                                            <a href="{{ route('category.edit_item',$val->id) }}" class="btn btn-link btn-sm">Edit</a>
                                            <span class="vl"></span>
                                            <a class="btn btn-link btn-sm text-danger" href="#deleteItem" data-id="{{ $val->id }}" data-action="trash" data-toggle="modal">Delete</a>
                                        </div>
                                    </td>
                                    <td>
                                        @if(!empty($val->cover_icon))
                                            <img class="img-responsive" alt="user" width="150" src="{{ asset(STATIC_DIR.$val->cover_icon) }}">
                                        @endif
                                    </td>
                                    <td><a href="{{ route('profile.view',$val->user->id) }}" calss="btn btn-link btn-sm">{{ $val->user->first_name }} {{ $val->user->last_name }}</a></td>
                                    <td>{{ $val->created_at->format('M d Y') }} (<span style="font-style: italic;font-size: 12px;">{{ $val->created_at->diffForHumans() }}</span>)</td>
                                </tr>
                         @endforeach
                        @else
                            <tr>
                                <td colspan="5">
                                    <p>No items found <a href="{{ route('category.add_item',$category_id) }}" class="btn btn-link btn-sm" data-toggle="modal" >Add pages to category</a></p>
                                </td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
    @include('category.modal')
@endsection

@section('scripts')
	<script>
        $('#datatable').DataTable({
            dom: 'Bfrtip',
           columnDefs: [ { "orderable": false, "visible": true } ],
        });

        // DELETE ITEM
        $('#deleteItem').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#item_id").val(id);
        });

        $('#deleteItem').find('.modal-footer #confirm_yes').on('click', function () {
            var id = $("#item_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('category.delete_item') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    // console.log(msg);
                    $("#confirmDelete").modal("hide");
                    window.location.reload();
                }
            });
        });
        // DELETE ITEM END
	</script>
@endsection
