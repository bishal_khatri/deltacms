@extends('layouts.app')
@section('css')

@endsection
@section('page_title')
    Category Listing
@endsection
@section('right_button')
    <a href="#createCategory" data-toggle="modal" class="btn btn-outline btn-info pull-right" ><i class="fa fa-plus"></i>&nbsp;Create new category</a>
@stop
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table id="datatable" class="table table-borderless table-striped" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th style="width: 500px;">Category Title</th>
                            <th>Items</th>
                            <th>Author</th>
                            <th>Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($categories->count()>0)
                            @foreach($categories as $val)
                                <tr class="title">
                                    <td>
                                        {{ $val->category_title }}
                                        <div class="action">
                                            <a href="{{ route('category.list_item',$val->id) }}" class="btn btn-link btn-sm">View list</a>
                                            <span class="vl"></span>
                                            <a class="btn btn-link btn-sm" href="#editCategory" data-id="{{ $val->id }}" data-title="{{ $val->category_title }}" data-subtitle="{{ $val->category_subtitle }}" data-toggle="modal">Edit</a>
                                            <span class="vl"></span>
                                            <a class="btn btn-link btn-sm text-danger" href="#deleteCategory" data-id="{{ $val->id }}" data-toggle="modal">Delete</a>
                                        </div>
                                    </td>
	                                <td>
		                                <a href="{{ route('category.list_item',$val->id) }}" class="btn btn-link btn-sm">
			                                {{ '[ '.count($val->posts).' ] Item(s) ' }}
		                                </a>
	                                </td>
                                    <td><a href="{{ route('profile.view',$val->user->id) }}" calss="btn btn-link btn-sm">{{ $val->user->first_name }} {{ $val->user->last_name }}</a></td>
                                    <td>{{ $val->created_at->diffForHumans() }}</td>
                                </tr>
                        @endforeach
                        @else
                            <tr>
                                <td colspan="4">No items found <a href="#createCategory" data-toggle="modal" class="btn btn-link btn-sm" >&nbsp;Create new category</a></td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>
    
    @include('category.modal')
@endsection

@section('scripts')
    <script>
        // $('#datatable').DataTable({
        //     dom: 'Bfrtip',
        //     columnDefs: [ { "orderable": false, "visible": true } ],
        // });

        $('#add').find('.modal-footer #confirm_yes').on('click', function () {
            $("#confirmDelete").modal("hide");
            window.location.reload();

        });

        // AJAX DELETE CATEGORY
        $('#deleteCategory').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#category_id").val(id);
        });

        $('#deleteCategory').find('.modal-footer #confirm_yes').on('click', function () {
            var id = $("#category_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('category.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    // console.log(msg);
                    $("#confirmDelete").modal("hide");
                    window.location.reload();
                }
            });
        });
        // AJAX DELETE CATEGORY END

        $('#editCategory').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var title = button.data('title');
            var subtitle = button.data('subtitle');
            // var form = $(e.relatedTarget).closest('form');
            // $(this).find('.modal-footer #confirm').data('form', form);
            $("#e_category_id").val(id);
            $("#e_category_title").val(title);
            $("#e_category_subtitle").val(subtitle);
        });
    </script>
@endsection
