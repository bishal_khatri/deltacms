@extends('layouts.app')
@section('css')

@endsection
@section('page_title')
    Create category item
@endsection
@section('right_button')
    <a href="{{ route('category.list_item',$category_id) }}" class="btn btn-outline btn-info pull-right" data-toggle="modal" ><i class="fa fa-undo"></i>&nbsp;Back</a>
@stop

@section('content')
    <form class="form-horizontal" method="post" action="{{ route('category.store_item') }}" enctype="multipart/form-data">
        @csrf
        <input type="hidden" value="category_item" name="post_type">
        <input type="hidden" value="{{ $category_id }}" name="category_id">
        <div class="row">
            <div class="col-md-12">
                <div class="white-box">
                    <div class="row pull-right">
                        <div class="col-md-12">
                            <button type="submit" name="status" class="btn btn-success btn-outline btn-sm" value="draft">Draft</button>
                            <button type="submit" name="status" class="btn btn-info btn-outline btn-sm" value="publish">Publish</button>
                        </div>
                    </div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#home" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="ti-home"></i></span>
                                <span class="hidden-xs"> Category Page Content</span>
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#cover" aria-controls="profile" role="tab" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="ti-user"></i></span>
                                <span class="hidden-xs">Cover Image</span>
                            </a>
                        </li>
                        <li role="presentation" class="">
                            <a href="#seo" aria-controls="messages" role="tab" data-toggle="tab" aria-expanded="false">
                                <span class="visible-xs"><i class="ti-email"></i></span>
                                <span class="hidden-xs">SEO</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Enter title here" name="post_title"  id="title"><br>
                                    <strong>link: </strong>
                                    <input type="hidden" class="form-control" name="post_slug" id="post_slug">
                                    <input type="text" class="form-control" name="post_url" id="post_url" readonly="">
                                </div>
                                <div class="form-group">
                                    <textarea rows="8" class="form-control" placeholder="Enter sub-title here" name="post_subtitle" id="post_subtitle"></textarea>
                                </div>
                                <div class="form-group">
                                    <textarea id="ckeditor" name="post_content"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="cover">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="text-center">
                                                <i class="fa fa-plus"></i>&nbsp;Insert Cover Image <br><br>
                                                <img class="text-center" id="holder" alt="" style="min-width: 200px;min-height: 200px;max-width: 300px;max-height: 300px;" alt="Cover Image"> <br><br>
                                                <a id="sitelogo" data-input="thumbnail" data-preview="holder" class="btn btn-block btn-primary btn-sm">
                                                    <i class="fa fa-picture-o"></i> Choose
                                                </a>
                                            </div>
                                            <input id="thumbnail" class="form-control" type="hidden" name="cover_icon">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="seo">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="title">SEO Title</label>
                                    <input type="text" name="seo_title" id="" class="form-control" placeholder="Enter SEO title" value="{{ old('seo_title') }}">
                                </div>
                                <div class="form-group">
                                    <label for="title">SEO Key Words</label>
                                    <input type="text" name="seo_keywords" id="" class="form-control" placeholder="Enter SEO key words" value="{{ old('seo_keywords') }}">
                                </div>
                                <div class="form-group">
                                    <label for="title">SEO Description</label>
                                    <textarea name="seo_description" class="form-control" id="" cols="30" rows="10" placeholder="Enter SEO description">{{ old('seo_description') }}</textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    @include('pages.modal')
@endsection

@section('scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/ckeditor/ckeditor.js') }}"></script>
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <script src="{{asset(STATIC_DIR.'vendor/laravel-filemanager/js/lfm.js')}}"></script>
    
    <script>
        $('.dropify').dropify();
        $('#title').on('focusout',function(){
            var title = $('#title').val();
            $.ajax({
                type: "POST",
                url: "{{ route('pages.create_url') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "title=" + title,
                success: function (msg) {
                    $('#post_url').val(msg.url);
                    $('#post_slug').val(msg.slug);
                }
            });
        });

        CKEDITOR.replace( 'ckeditor' ,{
            filebrowserImageBrowseUrl: "{{ url(STATIC_DIR.'laravel-filemanager?type=Images') }}",
            filebrowserImageUploadUrl: "{{ url(STATIC_DIR.'laravel-filemanager/upload?type=Images&_token=') }}",
            filebrowserBrowseUrl: "{{ url(STATIC_DIR.'laravel-filemanager?type=Files') }}",
            filebrowserUploadUrl: "{{ url(STATIC_DIR.'laravel-filemanager/upload?type=Files&_token=') }}"
        });

        $(function () {
            $('#sitelogo').filemanager('image',{prefix:"{{URL(STATIC_DIR.'laravel-filemanager')}}"});
        });
    </script>
    
    
    <script>
        $(document).ready(function() {
            $('#text-icon').on('click', function() {
                $('#text').show();
                $('#image').hide();
            });
            $('#image-icon').on('click', function() {
                $('#text').hide();
                $('#image').show();
            });
        });

        // $(document).ready(function (e) {
        //     $('#categoryForm').on('submit',(function(e) {
        //         e.preventDefault();
        //         var formData = new FormData(this);
        //         $.ajax({
        //             type:'POST',
        //             headers: {
        //             'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') },
        //             url: $(this).attr('action'),
        //             data:formData,
        //             cache:false,
        //             contentType: false,
        //             processData: false,
        //             success:function(data){
        //                 window.location.reload();
        //                 // $('#category').fadeOut( function(){
        //                 //     $('#category').fadeIn();
        //                 //   });
        //             },
        //             error: function(data){
        //                 console.log("error");
        //                 console.log(data);
        //             }
        //         });
        //     }));
        // });
    </script>
@endsection
