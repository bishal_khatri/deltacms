@extends('layouts.app')
@section('css')
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/ion-rangeslider/css/ion.rangeSlider.css') }}" rel="stylesheet">
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/ion-rangeslider/css/ion.rangeSlider.skinModern.css') }}" rel="stylesheet">
@endsection
@section('page_title')
	Setting
@endsection
@section('right_button')
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="row">
				<div class="col-md-6">
					<div class="white-box">
						<form action="{{ route('setting.store_max_image_size') }}" method="post">
							@csrf
							<div class="row">
								<div class="col-md-12">
									<h3 class="box-title">Maximum Image upload size [MB]</h3>
									<div id="range_01"></div>
									<input type="hidden" name="image_size" id="image_size">
								</div>
								<div class="row pull-right">
									<div class="col-md-12">
										<button type="submit" class="btn btn-success btn-outline btn-sm"><i class="icon-check"></i>  Save</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="col-md-6">
					<div class="white-box">
						<form action="{{ route('setting.store_max_file_size') }}" method="post">
							@csrf
							<div class="row">
								<div class="col-md-12">
									<h3 class="box-title">Maximum file upload size [MB]</h3>
									<div id="file"></div>
									<input type="hidden" name="file_size" id="file_size">
								</div>
								<div class="row pull-right">
									<div class="col-md-12 ">
										<button type="submit" class="btn btn-success btn-outline btn-sm"><i class="icon-check"></i> Save</button>
									</div>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<div class="white-box">
						<h4 class="text-center">SITE CUSTOMIZATION</h4>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="white-box">
						<form method="POST" action="{{ route('setting.store_logo') }}">
                    		@csrf
		                     <div class="form-group {{ $errors->has('site_logo') ? 'has-error' : '' }}">
		                        <label>Logo</label><br>
		                        <div class="text-center">
		                        	<img class="text-center" id="holder" alt="" style="min-width: 150px;min-height: 150px;max-width: 300px;max-height: 300px;" alt="Cover Image" @if(isset($site_logo->file)) src="{{ asset(STATIC_DIR.'storage/'.$site_logo->file) }}" @endif> <br><br>
		                        </div>
                                 <a id="sitelogo" data-input="thumbnail" data-preview="holder" class="btn btn-block btn-info btn-outline btn-sm">
                                    <i class="fa fa-picture-o"></i> Choose
                                </a>
                                <input id="thumbnail" class="form-control" type="hidden" value="" name="site_logo">
		                    </div>   
		                     <div class="form-group {{ $errors->has('site_logo_url') ? 'has-error' : '' }}">
		                        <label>URL</label>
		                        <input type="text" class="form-control" value="{{ $site_logo->url ?? '#' }}" name="site_logo_url" placeholder="Enter URL">
		                        @if ($errors->has('site_logo_url'))
		                            <span class="invalid-feedback" role="alert">
		                                <p class="text-danger">{{ $errors->first('site_logo_url') }}</p>
		                            </span>
		                        @endif
		                    </div>                                      
		                    <button type="submit" class="btn btn-success btn-outline btn-sm"><i class="icon-check"></i> Save</button>
		                </form>
					</div>
				</div>
				<div class="col-md-6">
					<div class="white-box">
						<form method="POST" action="{{ route('setting.contact_info') }}">
                    		@csrf
		                     <div class="form-group {{ $errors->has('contact_info') ? 'has-error' : '' }}">
		                        <label>Top Header</label>
		                        <textarea name="contact_info" class="form-control" id="" cols="30" rows="10">{{ $contact_info->text ?? '' }}</textarea>
		                        @if ($errors->has('contact_info'))
		                            <span class="invalid-feedback" role="alert">
		                                <p class="text-danger">{{ $errors->first('contact_info') }}</p>
		                            </span>
		                        @endif
		                    </div>                                      
		                    <button type="submit" class="btn btn-success btn-outline btn-sm"><i class="icon-check"></i> Save</button>
		                </form>
					</div>
				</div>
			</div>
		</div>
	</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/ion-rangeslider/js/ion-rangeSlider/ion.rangeSlider.min.js') }}"></script>
	<script>
        // IMAGE SIZE
        $("#range_01").ionRangeSlider({
            min: 2,
            max: 100,
            from: {{ $image_post_max_size }}
        });
        $('#range_01').on('change',function(){
            var value = $(this).val();
            $('#image_size').val(value);
        });

        // FILE SIZE
        $("#file_size").ionRangeSlider({
            min: 2,
            max: 100,
            from: {{ $file_post_max_size }}
        });
        $('#file_size').on('change',function(){
            var value = $(this).val();
            $('#image_size').val(value);
        });
	</script>
	<script src="{{asset(STATIC_DIR.'vendor/laravel-filemanager/js/lfm.js')}}"></script>
<script>
    $(function () {
        $('#sitelogo').filemanager('image',{prefix:"{{URL(STATIC_DIR.'laravel-filemanager')}}"});
    });
</script>
@endsection
