{{--ADD FORM--}}
<div class="modal fade" id="update" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
    <div class="modal-dialog" role="document">
        <form action="{{ route('popup.update') }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" id="popup_id">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="exampleModalLabel1">
                        <i class="fa fa-plus-circle" id="icon-terminate" ></i> Edit Popup Image
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="row pull-right">
                        <div class="col-md-12">
                            <button type="button" class="btn btn-default btn-outline" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-success btn-outline btn-sm" id="confirm_yes">Update</button>
                        </div>
                    </div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active">
                            <a href="#image" aria-controls="home" role="tab" data-toggle="tab" aria-expanded="true">
                                <span class="visible-xs"><i class="ti-home"></i></span>
                                <span class="hidden-xs"> Image</span>
                            </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane active" id="image">
                            <div class="col-md-12">
                                <div class="form-group @if ($errors->has('title')) has-error @endif">
                                    <label for="title" class="control-label">Title:</label>
                                    <input type="text" name="title" id="title" class="form-control" placeholder="Enter title here" />
                                    @if ($errors->has('title'))
                                        <span class="text-title" role="alert">
										{{ $errors->first('title') }}
									</span>
                                    @endif
                                </div>
                                <div class="form-group @if ($errors->has('image')) has-error @endif">
                                    <label for="image" class="control-label">Image:</label>
                                    <input type="file" id="input-file-now" name="image" class="dropify" accept="image/x-png,image/gif,image/jpeg" />
                                    @if ($errors->has('image'))
                                        <span class="text-danger" role="alert">
										{{ $errors->first('image') }}
									</span>
                                    @endif
                                    <p style="font-style: italic;color: darkorange;">Appropriate file type: .jpg .jpeg .png</p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
