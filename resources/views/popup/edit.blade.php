@extends('layouts.app')
@section('css')
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet">
@endsection
@section('page_title')
	Edit slider
@endsection
@section('right_button')
	<a href="{{ route('slider.index') }}" class="btn btn-info btn-outline pull-right" data-toggle="modal" ><i class="fa fa-undo"></i>&nbsp;Back</a>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<form action="{{ route('slider.update') }}" method="post" enctype="multipart/form-data">
					@csrf
					<input type="hidden" name="slider_id" value="{{ $slider->id }}" id="">
					<div class="form-group @if ($errors->has('image')) has-error @endif">
						<label for="image" class="control-label">Slider Image:</label>
						<input type="file" name="icon" id="input-file-now-custom-3" class="dropify" data-height="200" data-default-file="@if(!empty($slider->image)){{ asset(STATIC_DIR.'storage/'.$slider->image) }}@endif" />
						@if ($errors->has('image'))
							<span class="text-danger" role="alert">
								{{ $errors->first('image') }}
							</span>
						@endif
					</div>
					<div class="form-group @if ($errors->has('title')) has-error @endif">
						<label for="title" class="control-label">Title:</label>
						<input type="text" class="form-control" name="title" placeholder="Enter title here" value="{{ $slider->title ?? '' }}">
						@if ($errors->has('title'))
							<span class="text-danger" role="alert">
								{{ $errors->first('title') }}
							</span>
						@endif
					</div>
					<div class="form-group">
						<label for="message-text" class="control-label">Description:</label>
						<textarea class="form-control" id="message-text1" name="description" placeholder="Enter description here" rows="8" cols="10">{{ $slider->description ?? '' }}</textarea>
						@if ($errors->has('description'))
							<span class="text-danger" role="alert">
								{{ $errors->first('description') }}
							</span>
						@endif
					</div>
					<div class="form-group @if ($errors->has('button_text')) has-error @endif">
						<label for="button_text" class="control-label">Button Text</label>
						<input type="text" class="form-control" name="button_text" value="{{ $slider->button_text ?? '' }}">
						@if ($errors->has('button_text'))
							<span class="text-danger" role="alert">
								{{ $errors->first('button_text') }}
							</span>
						@endif
					</div>
					<div class="form-group @if ($errors->has('link')) has-error @endif">
						<label for="link" class="control-label">Link:</label>
						<input type="text" class="form-control" name="link" value="{{ $slider->link ?? '' }}">
						@if ($errors->has('link'))
							<span class="text-danger" role="alert">
								{{ $errors->first('link') }}
							</span>
						@endif
					</div>
					<div class="form-group @if ($errors->has('target')) has-error @endif">
						<label for="target" class="control-label">Target</label>
						<select name="target" id="" class="form-control">
							<option value="_self" @if($slider->target=='_self') selected @endif>Parent</option>
							<option value="_blank" @if($slider->target=='_blank') selected @endif>New Tab</option>
						</select>
						@if ($errors->has('target'))
							<span class="text-danger" role="alert">
								{{ $errors->first('target') }}
							</span>
						@endif
					</div>
					<button class="btn btn-success btn-outline btn-sm">Update</button>
				</form>
			</div>
		</div>
	</div>
@endsection

@section('scripts')
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
	<script type="text/javascript">
        $('.dropify').dropify();
	</script>
@endsection
