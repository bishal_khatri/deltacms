@extends('layouts.app')
@section('css')

@endsection
@section('page_title')
    Popups
@endsection
@section('right_button')
    <a href="#add" class="btn btn-info btn-outline pull-right" data-toggle="modal" ><i class="fa fa-plus"></i>&nbsp;Add image</a>
@stop
@section('content-title')
    <h2>
        Popups Listing
    </h2>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table id="datatable" class="table table-borderless table-striped" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th style="width: 700px;">Page Title</th>
                            <th>Author</th>
                            <th>Created</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if($popups->count()>0)
                            @foreach($popups as $val)
                                <tr class="title">
                                    <td>
                                        {{ $val->post_title }}
                                        <div class="action">
                                            <a href="{{ asset(STATIC_DIR.'storage/'.$val->cover_image) }}" target="_blank" class="btn btn-link btn-sm">Preview</a>
                                            <span class="vl"></span>
                                            <a href="#update" data-toggle="modal" data-id = "{{ $val->id }}" data-title = "{{ $val->post_title }}" class="btn btn-link btn-sm">Edit</a>
                                            <span class="vl"></span>
                                            <a class="btn default btn-outline text-danger" href="#confirmDelete" data-id="{{ $val->id }}" data-toggle="modal">
                                                <i class="icon-trash"></i>
                                            </a>
                                        </div>
                                    </td>
                                    <td><a href="{{ route('profile.view',$val->user->id) }}" calss="btn btn-link btn-sm">{{ $val->user->first_name }} {{ $val->user->last_name }}</a></td>
                                    <td>{{ $val->created_at->diffForHumans() }}</td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="3">No items found <a href="#add" class="btn btn-link btn-sm" data-toggle="modal" >Add new image</a></td>
                            </tr>
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>

    @include('popup.modal')
    @include('popup.create_modal')
    @include('popup.update_modal')
@endsection

@section('scripts')
    <script>
        $('#update').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var title = button.data('title');
            // alert(id)
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#popup_id").val(id);
            $("#title").val(title);
        });
        $('#add').find('.modal-footer #confirm_yes').on('click', function () {
            window.location.reload();
        });

        $('#datatable').DataTable({
            dom: 'Bfrtip',
            columnDefs: [ { "orderable": false, "visible": true } ],
        });

        // AJAX SLIDER DELETE
        $('#confirmDelete').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#hidden_id").val(id);
        });

        $('#confirmDelete').find('.modal-footer #confirm_yes').on('click', function () {
            var id = $("#hidden_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('popup.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    $("#confirmDelete").modal("hide");
                    window.location.reload();
                }
            });
        });


    </script>
@endsection
