{{-- DELETE MODAL --}}
<div class="modal fade bs-modal-sm" id="deleteDownloads" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Delete Permanently
				</h4>
			</div>
			<div class="modal-body"> Do you want to delete permanently?</div>
			<input type="hidden" id="downloads_id">
			<div class="modal-footer">
				<button type="button" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
				</button>
				<button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
					No
				</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
</div>

{{--ADD FORM--}}
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<form action="{{ route('downloads.add') }}" method="post" enctype="multipart/form-data">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel1"><i class="fa fa-plus-circle" id="icon-terminate" ></i> Add new file</h4> </div>
			<div class="modal-body">
				@csrf
				<div class="form-group @if ($errors->has('file')) has-error @endif">
					<label for="image" class="control-label"> File:</label>
					<input type="file" class="form-control-file" name="file" value="{{ old('file') }}">
					@if ($errors->has('file'))
						<span class="text-danger" role="alert">
							{{ $errors->first('file') }}
						</span>
					@endif
					<p style="font-style: italic;color: darkorange;">Appropriate file size: 2MB</p>
				</div>
				<div class="form-group @if ($errors->has('title')) has-error @endif">
					<label for="gallery_name" class="control-label">Title:</label>
					<input type="text" class="form-control" name="title" placeholder="Enter title here" value="{{ old('title') }}">
					@if ($errors->has('title'))
						<span class="text-danger" role="alert">
							{{ $errors->first('title') }}
						</span>
					@endif
				</div>
				<div class="form-group">
					<label for="message-text" class="control-label">Description:</label>
					<textarea class="form-control" rows="5" id="message-text1" name="body" placeholder="Enter description here">{{ old('body') }}</textarea>
					@if ($errors->has('body'))
						<span class="text-danger" role="alert">
							{{ $errors->first('body') }}
						</span>
					@endif
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-primary" id="confirm_yes">Upload</button>
			</div>
		</div>
		</form>
	</div>
</div>