@extends('layouts.app')
@section('css')
@endsection
@section('page_title')
	Listing Downloads Item
@endsection
@section('right_button')
<div class="pull-right">
	<a href="#addItem" class="btn btn-info btn-outline" data-toggle="modal" data-group="{{ $category->has_group }}" data-id="{{ $category->downloads_category_id }}"><i class="fa fa-plus"></i> Add file</a>
	<a href="{{ route('downloads.index') }}" class="btn btn-info btn-outline" data-toggle="modal" ><i class="fa fa-undo"></i>&nbsp;Back</a>
</div>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div class="media">
					<div class="media-body">
						<h4 class="media-heading">{{ $category->title }}
							<sup class="text-uppercase">
								<a href="#editCategory" class="text-sm text-danger" data-toggle="modal" data-id="{{ $category->downloads_category_id }}" data-title="{{ $category->title }}" data-description="{{ $category->description }}">edit</a>
							</sup>
						</h4>
						{{ $category->description }}
						@if ($items->count()>0 AND $has_group==0)
							@foreach($items as $value)
								<div class="media">
									<div class="media-left">
										<a href="javascript:void(0)">
											<a href="{{ route('downloads.download',$value->download_items_id) }}"><i class="mdi mdi-download fa-fw"></i></a>
										</a>
									</div>
									<div class="media-body">
										<h4 class="media-heading">{{ $value->title }}
											<div class="pull-right">
												<a href="#deleteItem" class="text-sm text-danger" data-toggle="modal" data-id="{{ $value->download_items_id }}">
													<i class="fa fa-trash"></i>
												</a>
											</div>
										</h4>
										{{ $value->body }}
									</div>
								</div>
							@endforeach
                        @elseif($items->count()>0 AND $has_group==1)
                            @foreach($items as $key=>$value)
                                <br>
                                <div class="row ">
                                    <div class="col-md-12">
                                        <h4>{{ $key }}</h4>
                                    </div>
                                </div>
                            @foreach($value as $item)
                                <div class="media">
                                    <div class="media-left">
                                        <a href="javascript:void(0)">
                                            <a href="{{ route('downloads.download',$item->download_items_id) }}"><i class="mdi mdi-download fa-fw"></i></a>
                                        </a>
                                    </div>
                                    <div class="media-body">
                                        <h4 class="media-heading">{{ $item->title }}
                                            <div class="pull-right">
                                                <a href="#deleteItem" class="text-sm text-danger" data-toggle="modal" data-id="{{ $item->download_items_id }}">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </div>
                                        </h4>
                                        {{ $item->body }}
                                    </div>
                                </div>
                                @endforeach
                            @endforeach
						@else
						<p>No items found <a href="#addItem"  data-toggle="modal" data-group="{{ $category->has_group }}" data-id="{{ $category->downloads_category_id }}"> Add new file</a></p>

						@endif
					</div>
				</div>
			</div>
		</div>
		
		@include('downloads.create_item')
		@include('downloads.edit_category')
		@include('downloads.delete_item')
		@endsection
		
		@section('scripts')
			<script>
                $('#addItem').on('show.bs.modal', function (e) {
                    var button = $(e.relatedTarget);
                    var id = button.data('id');
                    var group = button.data('group');
                    var form = $(e.relatedTarget).closest('form');
                    $(this).find('.modal-footer #confirm').data('form', form);
                    $(".hidden_id").val(id);
                    if (group==1){
                        $('#group').show()
                    }
                });

                // EDIT CATEGORY
                $('#editCategory').on('show.bs.modal', function (e) {
                    var button = $(e.relatedTarget);
                    var id = button.data('id');
                    var title = button.data('title');
                    var description = button.data('description');
                    var form = $(e.relatedTarget).closest('form');
                    $(this).find('.modal-footer #confirm').data('form', form);
                    $("#id").val(id);
                    $("#title").val(title);
                    $("#description").val(description);
                });

                // DELETE ITEM
                $('#deleteItem').on('show.bs.modal', function (e) {
                    var button = $(e.relatedTarget);
                    var ids = button.data('id');
                    var form = $(e.relatedTarget).closest('form');
                    $(this).find('.modal-footer #confirm').data('form', form);
                    $("#hidden_id").val(ids);
                });

                $('#deleteItem').find('.modal-footer #confirm_yes').on('click', function () {
                    var id = $("#hidden_id").val();
                    $.ajax({
                        type: "POST",
                        url: "{{ route('downloads.item.delete') }}",
                        headers: {
                            'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: "id=" + id,
                        success: function (msg) {
                            // console.log(msg);
                            $("#deleteItem").modal("hide");
                            window.location.reload();
                        }
                    });
                });
			</script>
@endsection
