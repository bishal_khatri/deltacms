<div class="modal fade" id="editCategory" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<form action="{{ route('downloads.edit') }}" method="post" enctype="multipart/form-data">
			<input type="hidden" id="id" name="id">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel1"><i class="fa fa-plus-circle" id="icon-terminate" ></i> Add new file</h4> </div>
				<div class="modal-body">
					@csrf
					<div class="form-group @if ($errors->has('title')) has-error @endif">
						<label for="gallery_name" class="control-label">Title:</label>
						<input type="text" class="form-control" name="title" id="title" placeholder="Enter title here" value="{{ old('title') }}">
						@if ($errors->has('title'))
							<span class="text-danger" role="alert">
							{{ $errors->first('title') }}
						</span>
						@endif
					</div>
					<div class="form-group">
						<label for="message-text" class="control-label">Description:</label>
						<textarea class="form-control" rows="5" id="description" name="description" placeholder="Enter description here">{{ old('description') }}</textarea>
						@if ($errors->has('description'))
							<span class="text-danger" role="alert">
							{{ $errors->first('description') }}
						</span>
						@endif
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary" id="confirm_yes">Update</button>
				</div>
			</div>
		</form>
	</div>
</div>