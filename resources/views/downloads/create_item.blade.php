<div class="modal fade" id="addItem" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<form action="{{ route('downloads.item.add') }}" method="post" enctype="multipart/form-data">
			@csrf
			<input type="hidden" class="hidden_id" name="category_id" id="">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" id="exampleModalLabel1"><i class="fa fa-plus-circle" id="icon-terminate" ></i> Add new file</h4> </div>
				<div class="modal-body">
					<div class="form-group @if ($errors->has('file')) has-error @endif">
						<label for="image" class="control-label"> File:</label>
						<input type="file" class="form-control-file" name="file" value="{{ old('file') }}">
						@if ($errors->has('file'))
							<span class="text-danger" role="alert">
							{{ $errors->first('file') }}
						</span>
						@endif
						<p style="font-style: italic;color: darkorange;">Appropriate file size: 2MB</p>
					</div>
					<div class="form-group @if ($errors->has('title')) has-error @endif">
						<label for="gallery_name" class="control-label">Title:</label>
						<input type="text" class="form-control" name="title" placeholder="Enter title here" value="{{ old('title') }}">
						@if ($errors->has('title'))
							<span class="text-danger" role="alert">
							{{ $errors->first('title') }}
						</span>
						@endif
					</div>
                    <div class="form-group" id="group" style="display: none;">
                        <label for="group_slug" class="control-label">Group Slug:</label>
                        <input type="text" class="form-control" name="group_slug" placeholder="Enter group slug here" value="{{ old('group_slug') }}">
                        @if ($errors->has('group_slug'))
                            <span class="text-danger" role="alert">
							{{ $errors->first('group_slug') }}
						</span>
                        @endif
                    </div>
					<div class="form-group">
						<label for="message-text" class="control-label">Description:</label>
						<textarea class="form-control" rows="5" id="message-text1" name="body" placeholder="Enter description here">{{ old('body') }}</textarea>
						@if ($errors->has('body'))
							<span class="text-danger" role="alert">
							{{ $errors->first('body') }}
						</span>
						@endif
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					<button type="submit" class="btn btn-primary" id="confirm_yes">Upload</button>
				</div>
			</div>
		</form>
	</div>
</div>
