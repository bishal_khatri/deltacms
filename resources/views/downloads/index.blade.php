@extends('layouts.app')
@section('css')
@endsection
@section('page_title')
	Listing Downloads
@endsection
@section('right_button')
	<a href="#addCategory" class="btn btn-info btn-outline pull-right" data-toggle="modal" ><i class="fa fa-plus"></i>&nbsp;Create New</a>
@stop

@section('content')
	<div class="row">
		<div class="col-md-12">
			<div class="white-box">
				<div class="table-responsive">
					<table id="datatable" class="table table-borderless table-striped" cellspacing="0" width="100%">
						<thead>
						<tr>
							<th>#</th>
							<th style="width: 150px;">Title</th>
							<th style="width: 200px;">Description</th>
							<th style="width: 200px">File</th>
							<th>Author</th>
							<th>Created</th>
						</tr>
						</thead>
						<tbody>
						@if($downloads->count()>0)
							@foreach($downloads as $val)
								<tr class="title">
									<td>{{ $loop->iteration }}</td>
									<td>
										{{ str_limit($val->title,15) }}
                                        @if($val->has_group==1)
                                            <span class="label label-success">Group</span>
                                        @endif
										<div class="action">
											<a href="{{ route('downloads.item.view_item',$val->downloads_category_id) }}" class="btn btn-link btn-sm">view</a>&nbsp;
											<span class="vl"></span>
											<a class="btn btn-link btn-sm text-danger"
											   href="#deleteDownloads" data-id="{{ $val->downloads_category_id }}" data-toggle="modal">Delete</a>
										</div>
									</td>
									<td>{{ str_limit($val->description,25) }}</td>
									<td>
										<a href="{{ route('downloads.item.view_item',$val->downloads_category_id) }}" class="btn btn-link btn-sm">
											{{ '[ '.count($val->downloadItem).' ] Files ' }}
										</a>
										<a href="#addItem" data-toggle="modal" data-id="{{ $val->downloads_category_id }}" data-group="{{ $val->has_group }}">add</a>&nbsp;
									</td>
									<td><a href="{{ route('profile.view',$val->user->id) }}" calss="btn btn-link btn-sm">{{ $val->user->first_name }} {{ $val->user->last_name }}</a></td>
									<td>{{ $val->created_at->diffForHumans() }}</td>
								</tr>
						@endforeach
						@else
						<tr>
							<td colspan="6">
								<p>No items found <a href="#addCategory" class="btn btn-link btn-sm" data-toggle="modal" >&nbsp;Create new</a></p>
							</td>
						</tr>
						@endif
					</table>
				</div>
			</div>
		</div>
	</div>
	
	@include('downloads.create_item')
	@include('downloads.modal')
	@include('downloads.create_category')
@endsection

@section('scripts')
	<script>
        $('#addItem').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var group = button.data('group');
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $(".hidden_id").val(id);
            if (group==1){
                $('#group').show()
            }
        });

        // DELETE DOWNLOADS
        $('#deleteDownloads').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#downloads_id").val(id);
        });

        $('#deleteDownloads').find('.modal-footer #confirm_yes').on('click', function () {
            var id = $("#downloads_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('downloads.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    // console.log(msg);
                    $("#deleteItem").modal("hide");
                    window.location.reload();
                }
            });
        });
	</script>
@endsection
