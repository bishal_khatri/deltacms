@extends('front.layouts.main')
@section('page-title')
Home
@endsection
@section('content')
@if(!empty($sliders))
<div id="animation">
	<div class="callbacks_container">
		<ul class="rslides" id="slider4">
			@foreach($sliders as $value)
			<li>
				<img src="{{ asset(STATIC_DIR.'storage/'.$value->image) }}" alt="">
				@if(!empty($value->title) OR !empty($value->description))
				<p class="caption">
					<strong>{{ $value->title }}</strong>
				<br/>{{ $value->description ?? '' }}
				<br/><br/>
					<a href="{{ $value->link ?? '#' }}" target="{{ $value->target ?? '' }}">
						{{ $value->button_text ?? $value->link }}
					</a>
				</p>
				@endif
			</li>
			@endforeach
		</ul>
	</div>
	<!-- Animation End -->
</div>
@endif
<!-- About -->
@if (isset($page))
	<div id="about-ufl">
		<h1>About <strong>United Finance Ltd</strong></h1>
		<span>{{ $page->post_subtitle }}</span><br /><br />
		{{--<img src="{{ asset(STATIC_DIR.'frontend/images/img-about.png') }}" width="434" height="263" alt="" />--}}
		{!! str_limit($page->post_content,685) !!}
		<br><br><br>
		<a href="{{ route('front_page',$page->post_slug) }}">read more</a>
	
	</div>
@endif

<!-- About End -->
<div id="feature-product">
	<!-- Feature Product -->
	<div id="feature-top">
		<ul>
			@if ($features->count()>0)
				@foreach ($features as $value)
					<li><a href="{{ route('front_page',$value->post->post_slug) }}"><span><img src="{{ asset(STATIC_DIR.'storage/'.$value->icon) }}" width="49" height="64" alt="" /></span>{{ $value->display_name ?? $value->post_title }}</a></li>
				@endforeach
			@endif
		</ul>
	</div>
	<div id="more-products">
		
		<h2><strong>Explore</strong> More Products...</h2>
		@if (isset($features_mega1))
		<span>
		<p><img src="{{ asset(STATIC_DIR.'storage/'.$features_mega1->weblink_icon) }}" width="125" height="106" alt="" /></p>
		<p class="title-product">{{ $features_mega1->name }}</p>
			<ul>
				@if ($features_mega1->weblink_post->count()>0)
					@foreach ($features_mega1->weblink_post as $value)
						<li><a href="{{ $value->post->post_url }}">{{ $value->display_name ?? $value->post->post_title }}</a></li>
					@endforeach
				@endif
			</ul>
		</span>
		@endif
		
		
		<span>
			@if (isset($features_mega2))
				<p><img src="{{ asset(STATIC_DIR.'storage/'.$features_mega2->weblink_icon) }}" width="144" height="81" alt="" /></p>
				<p class="title-product">{{ $features_mega2->name }}</p>
				<ul>
					@if ($features_mega2->weblink_post->count()>0)
						@foreach ($features_mega2->weblink_post as $value)
							<li><a href="{{ $value->post->post_url }}">{{ $value->display_name ?? $value->post->post_title }}</a></li>
						@endforeach
					@endif
			</ul>
		</span>
		@endif
	</div>
	<!-- Feature Product End -->
</div>
@if (isset($remitance_partner))
<div id="remitance-partner">
	<!-- Remitance Partner -->
	<h3>Remitance Partner</h3>
	<ul>
		@foreach ($remitance_partner->weblink_post as $value)
			<li>
				<a href="{{ $value->post->post_url  }}" target="{{ $value->post->url_target ?? '' }}"><img src="{{  asset(STATIC_DIR.'storage/'.$value->icon) }}" width="141" height="68" alt=""/></a>
			</li>
		@endforeach
	</ul>
	<!-- Remitance Partner End -->
</div>
@endif

@if (isset($news))
	<div id="news-notice">
		<!-- News/Notice -->
		<h4>News/notices</h4>
		<ul><?php $count = 0; ?>
			@foreach ($news->posts as $value)
				<li><a href="{{ route('front_page',$value->post_slug) }}">{{ $value->post_title }}</a><br /><strong>{{ $value->created_at->format('M j Y') }}</strong><br />&nbsp;</li>
                @if($count == 2)
					<a href="" class="pull-right">Read more</a>
				<?php break; ?>
				@endif
                <?php $count++; ?>
			@endforeach
		</ul>
		<!-- News/Notice End -->
	</div>
@endif


@endsection
