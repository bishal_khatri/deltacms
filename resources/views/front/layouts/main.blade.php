<?php
$data = \App\Helpers\Helper::menu();
isset($data['nav_menus']) ? $nav_menus = $data['nav_menus'] : '';
isset($data['footer_menus'])?$footer_menus = $data['footer_menus']:'';
isset($data['prenav_menus'])?$prenav_menus = $data['prenav_menus']:'';
isset($data['pre_nav_contact_info'])?$pre_nav_contact_info = $data['pre_nav_contact_info']:'';
isset($data['sliders'])?$sliders = $data['sliders']:'';
isset($data['site_logo'])?$site_logo = $data['site_logo']:'';
?>
{{--{{dd($popups)}}--}}
    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
@include('front.layouts._partials.head')
<body>
@include('front.layouts._partials.nav')

@yield('content')
@include('front.layouts._partials.footer')
@include('front.layouts._partials.scripts')
</body>
</html>

