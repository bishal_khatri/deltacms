<head>
    <title>  @yield('page-title')| United Finance Limited </title>
    <meta name="generator" content="editplus" />
    <meta name="author" content="{{ $page->seo->seo_title ?? '' }}" />
    <meta name="keywords" content="{{ $page->seo->seo_keywords ?? '' }}" />
    <meta name="description" content="{{ $page->seo->seo_description ?? '' }}" />
    <link rel="icon" href="{{ asset(STATIC_DIR.'frontend/images/favicon.png') }}" type="image/x-icon"/>
    <link href="{{ asset(STATIC_DIR.'frontend/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset(STATIC_DIR.'frontend/css/responsiveslides.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset(STATIC_DIR.'frontend/css/demo.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset(STATIC_DIR.'frontend/css/ace-responsive-menu.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset(STATIC_DIR.'frontend/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css"/>


    <link href="{{ asset(STATIC_DIR.'frontend/css/media-queries.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset(STATIC_DIR.'frontend/css/overlaypopup.css') }}"/>
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'frontend/css/dcalendar.picker.css') }}">

    <style>
        .alert-success{
            color: green;
        }
        .alert-error{
            color: red;
        }
        #quick-links
        {
            position: fixed;
            width: 15%;
            top: 35%;
            right: 0;
            z-index: 99999;
        }


        #quick-links ul li
        {
            list-style: none;
            position: relative;
            right: -70%;
            -webkit-transition: all 300ms ease-in-out;
            -moz-transition: all 300ms ease-in-out;
            -ms-transition: all 300ms ease-in-out;
            -o-transition: all 300ms ease-in-out;
            transition: all 300ms ease-in-out;
            margin-bottom: 3px;
        }


        #quick-links ul li:hover
        {
            list-style: none;
            position: relative;
            right: 0;
        }


        #quick-links ul li a
        {
            color: #fff;
            padding: 8% 10% 8% 10%;
            width: 90%;
            display: block;
            background: #d91966;
            -webkit-border-radius: 10px 0 0 10px;
            border-radius: 10px 0 0 10px;
            border: 1px solid #fff;
            border-right: none;
        }


        #quick-links ul li a:hover
        {
            color: #fff;
            padding: 8% 10% 8% 10%;
            width: 90%;
            display: block;
            background: #003399;
            -webkit-border-radius: 10px 0 0 10px;
            border-radius: 10px 0 0 10px;
            border: 1px solid #fff;
            border-right: none;
        }


        #quick-links ul li img
        {
            float: left;
            margin-right: 20px;
            width: auto !important;
            height: auto !important;
            margin-left: 0;
            margin-right: 20px !important;
        }
    </style>
    @yield('front-css')
</head>
