@if(Session::has('message'))
    <div class="alert @if(Session::get('message')['type']=='success') alert-success @else alert-error @endif">
        <p><span style="font-size: 23px;font-weight: bold;">@if(Session::get('message')['type']=='success') Success @else Error @endif ! </span> {!! Session::get('message')['message'] !!}</p>
    </div>
@endif
