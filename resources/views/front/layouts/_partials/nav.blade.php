<div id="banner-holder">
    <div id="top-banner">
        <!-- Top Banner -->
        <div id="top-left">{{ $pre_nav_contact_info->text ?? '' }}</div>
        <div id="top-right">
            @if(isset($prenav_menus))
                <span class="top-links">
				@foreach($prenav_menus as $value)
                        <a href="@if($value->post_type=='custom_link') {{ url($value->post_url) }} @else {{ route('front_page',$value->post_slug) }} @endif" target="{{ $value->url_target ?? '' }}">{{ $value->menu_display_name ?? $value->post_title }}</a>&nbsp;&nbsp;
                    @endforeach
			</span>
            @endif
            <span class="search">
				<form action="search/" id="cse-search-box">
				<div>
				<input type="hidden" name="cx" value="010780530562257797199:lhkgsrhnxug" />
				<input type="hidden" name="cof" value="FORID:11" />
				<input type="hidden" name="ie" value="UTF-8" />
				<input type="text" name="q" id="q"  class="search-input" placeholder="Enter your Keywords..." />
				</div>
				</form>
			</span>
        </div>
        <!-- Top Banner End -->
    </div>

    <div id="banner"><!-- Banner -->
        <div id="logo">
            <a href="{{ $site_logo->url ?? '#' }}">
                @if(isset($site_logo->file))
                    <img src="{{ asset(STATIC_DIR.'storage/'.$site_logo->file) }}" width="284" height="89" alt="" />
                @endif
            </a>
        </div>
        <div id="navigation"  id="myHeader">
            <!-- Ace Responsive Menu -->
            <nav>
                <!-- Menu Toggle btn-->
                <div class="menu-toggle">
                    <h3>Menu</h3>
                    <button type="button" id="menu-btn">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <!-- Responsive Menu Structure-->
                <!--Note: declare the Menu style in the data-menu-style="horizontal" (options: horizontal, vertical, accordion) -->
                <ul id="respMenu" class="ace-responsive-menu" data-menu-style="horizontal">
                    @if(isset($nav_menus))
                        @foreach($nav_menus as $value)
                            <li>
                                <a href="@if($value->post->post_type=='custom_link') {{ url($value->post->post_url) }} @else {{ route('front_page',$value->post->post_slug) }} @endif">
                                    {{  $value->menu_display_name ?? $value->post->post_title }}
                                </a>
                                @if($value->children->count())
                                    <ul>
                                        @foreach($value->children as $child)
                                            <li>
                                                <a href="@if($child->post->post_slug=='custom_link') {{ url($child->post->post_url) }} @else {{ route('front_page',$child->post->post_slug) }} @endif">
                                                    {{ $child->menu_display_name ?? $child->post->post_title }}
                                                </a>
                                                @if ($child->children->count())
                                                    <ul>
                                                        @foreach($child->children as $subchild)
                                                            <li>
                                                                <a href="@if($subchild->post->post_type=='custom_link') {{ url($subchild->post->post_url) }} @else {{ route('front_page',$subchild->post->post_slug) }} @endif">
                                                                    {{ $subchild->menu_display_name ?? $subchild->post->post_title }}
                                                                </a>
                                                            </li>
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        @endforeach
                                    </ul>
                                @endif
                            </li>
                        @endforeach
                    @endif
                </ul>
            </nav>
            <!-- End of Responsive Menu -->
        </div>
        <!-- Banner End -->
    </div>
</div>
