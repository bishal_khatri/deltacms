<script src="{{ asset(STATIC_DIR.'frontend/js/jquery-1.7.1.min.js') }}" language="JavaScript" type="text/javascript"></script>
<script src="{{ asset(STATIC_DIR.'frontend/js/responsiveslides.min.js') }}" type="text/javascript"></script>
@yield('front-scripts')
<script>

    $(function () {

        // Slideshow 4
        $("#slider4").responsiveSlides({
            auto: true,
            pager: false,
            nav: false,
            speed: 400,
            namespace: "callbacks",
            before: function () {
                $('.events').append("<li>before event fired.</li>");
            },
            after: function () {
                $('.events').append("<li>after event fired.</li>");
            }
        });

    });
</script>



<script src="{{ asset(STATIC_DIR.'frontend/js/ace-responsive-menu.js') }}"></script>
<!--Plugin Initialization-->
<script type="text/javascript">
    $(document).ready(function () {
        $("#respMenu").aceResponsiveMenu({
            resizeWidth: '768', // Set the same in Media query
            animationSpeed: 'fast', //slow, medium, fast
            accoridonExpAll: false //Expands all the accordion menu on click
        });
    });
</script>
<script type="text/javascript" src="{{ asset(STATIC_DIR.'frontend/js/overlay.js') }}"></script>
<script src="{{ asset(STATIC_DIR.'frontend/js/dcalendar.picker.js') }}"></script>
<script>
    $('.datepicker').dcalendarpicker({format: 'yyyy-mm-dd'});
    // $('#calendar-demo').dcalendar(); //creates the calendar
</script>

