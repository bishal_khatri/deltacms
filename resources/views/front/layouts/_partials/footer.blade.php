<div id="footer">
    <!-- Footer -->
    @if (isset($footer_menus))
        <div id="footer-links">
            @foreach ($footer_menus as $value)
                <span>
					<ul>
						@foreach ($value as $menu)
                            <li><a href="@if($menu->post->post_type=='custom_link') {{ url($menu->post->post_url) }} @else {{ route('front_page',$menu->post->post_slug) }} @endif">{{ $menu->menu_display_name ?? $menu->post->post_title}}</a></li>
                        @endforeach
					</ul>
			</span>
            @endforeach
        </div>
    @endif

    <div id="contact-footer">
        <img src="{{ asset(STATIC_DIR.'frontend/images/map.jpg') }}" width="121" height="121" alt="" />
        United Finance Ltd.<br />
        IJ plaza,Durbar Marga,Kathmandu,Nepal<br />
        P.O.Box : 12311<br />
        Phone:01-4241648, 01-4241645<br />
        Email: <a href="#">info@ufl.com.np</a>
    </div>
    <!-- Footer End -->
</div>

<div id="copyright-footer">
    <!-- Copyright Footer -->
    <span>Copyright &#169 2018.  United Finance Limited. All rights reserved.</span>
    <span class="social-footer">Follow us on:
		<ul>
			<li><a href="#"><img src="{{ asset(STATIC_DIR.'frontend/images/facebook.png') }}" width="28" height="26" alt="" /></a></li>
			<li><a href="#"><img src="{{ asset(STATIC_DIR.'frontend/images/twitter.png') }}" width="27" height="26" alt="" /></a></li>
			<li><a href="#"><img src="{{ asset(STATIC_DIR.'frontend/images/icon-youtube.png') }}" width="28" height="26" alt="" /></a></li>
		</ul>
	</span>
    <!-- Copyright Footer End -->
</div>
<div id="quick-links">
    <ul>
        <li><a href="#"><img src="{{ asset('public/frontend/images/quickicon-grievance.png') }}" width="30" height="25" alt="" /> Grievance</a></li>
        <li><a href="#"><img src="{{ asset('public/frontend/images/quickicon-interest-rate.png') }}" width="30" height="25" alt="" /> Interest Rate</a></li>
        <li><a href="#"><img src="{{ asset('public/frontend/images/quickicon-emi-calculator.png') }}" width="27" height="27" alt="" />EMI Calculator</a></li>
        <li><a href="#"><img src="{{ asset('public/frontend/images/quickicon-ac-opening.png') }}" width="29" height="29" alt="" />Audit Report</a></li>
    </ul>
</div>

@if(Request::is('/'))
    <?php $popup = \App\Helpers\Helper::get_popup(); ?>
    @if($popup->count()>0)
        <div class="overlay-bg" style="display: block; height: 2400px;"></div>
        @foreach ($popup as $value)
            <div class="overlay-content popup{{$loop->iteration}}" @if($loop->first) style="display: block;" @endif>
                <p><img src="{{ asset(STATIC_DIR.'storage/'.$value->cover_image) }}" width="640" height="327" alt="" /></p>
                @if($loop->last)
                    <button class="close-btn" id="" data-showpopup="">X</button>
                @else
                    <button class="close-btn" id="{{$loop->iteration+1}}" data-showpopup="{{$loop->iteration+1}}">X</button>
                @endif
            </div>
        @endforeach
    @endif
@endif






