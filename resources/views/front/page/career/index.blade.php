@extends('front.layouts.main')
@section('front-css')

@endsection
@section('page-title')
    Jobs Listing
@endsection
@section('content')
    <div id="content-inner">
        @if(Auth::guard('front_web')->check())
            <div style="float: right; margin-bottom: 30px;"><a href="{{ route('user_profile') }}">User Profile</a> | <a href="{{ route('change_password') }}">Change Password</a> |
	            <form action="{{ route('logout') }}" method="post">@csrf <button type="submit" class="">Log out</button></form></div>
        @else
            <div style="float: right; margin-bottom: 30px;"><a href="{{ route('user_login') }}">Login</a></div>
        @endif
        <h1>Career</h1>
        <table width="100%" cellspacing="0" cellpadding="10" border="0">
            <thead>
            <tr>
                <th><string>Job Title</string></th>
                <th><string>Posted Date</string></th>
                <th style="width: 800px;"><string>Description</string></th>
                <th>&nbsp;</th>
            </tr>
            </thead>
            <tbody>
            @if(isset($jobs))
                @foreach($jobs as $value)
                    <tr>
                        <td>{{ $value->title }}</td>
                        <td>{{ $value->created_at->format('F d, Y') }}</td>
                        <td>{!! $value->description !!}</td>
                        <td>
	                        @if (in_array($value->id,$applied_job))
		                        <label class="label label-success">Applied</label>
							@else
		                        <a href="{{ route('career.apply',$value->id) }}">Apply Now</a>
	                        @endif
                        </td>
                    </tr>
                @endforeach
            @endif
            </tbody>
        </table>
    </div>

@endsection
