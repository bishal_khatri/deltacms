@extends('front.layouts.main')
@section('page-title')
    {{ $page->post_title }}
@endsection
@section('content')
    <div id="content-inner">
        <h1>{{ $page->post_title }}</h1>

        {!! $page->post_subtitle !!}
        <br/><br/>

        {!! $page->post_content !!}
        
        @if ($page->facebook_share==1 OR $page->twitter_share==1)
			Share On Social Media
		    @if ($page->twitter_share==1)
			    <iframe src="https://www.facebook.com/plugins/share_button.php?href={{ Request::url() }}&layout=button&size=large&mobile_iframe=true&appId=103720390242945&width=72&height=28" width="72" height="28" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
		    @endif
		    @if ($page->facebook_share==1)
			    <a class="twitter-share-button" href="https://twitter.com/share?url="{{ Request::url() }} data-size="large">
				    <img src="{{ asset(STATIC_DIR.'assets/icons/Twitter-Social-Share-Button.png') }}" alt="" style="width: 150px">
			    </a>
		    @endif
        @endif
    </div>
@endsection
