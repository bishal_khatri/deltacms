@extends('front.layouts.main')
@section('page-title')
{{ $page->post_title }}
@endsection
@section('content')
<h1>{{ $page->post_title }}</h1>
@if(isset($page->cover_image))
	<img src="{{ asset(STATIC_DIR.'storage/'.$page->cover_image) }}" width="1366" height="780" alt="" />
@endif
{!! $page->post_content !!}

@endsection