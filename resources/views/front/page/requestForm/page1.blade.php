@extends('front.layouts.main')
@section('front-css')
	<link rel="stylesheet" href="{{ asset(STATIC_DIR.'frontend/css/form.css') }}">
@stop
@section('page-title')
    Online Account Opening
@endsection
@section('content')
	<div id="content-inner">
        <h1>Online Account Opening</h1>
		<form action="{{ route('ac_form.store') }}" method="post">
			@csrf
			<input type="hidden" name="form_id" value="{{ $formData->form_id ?? '' }}" id="">
			<input type="hidden" name="page" value="1" id="">
			<fieldset>
				<legend><span class="number">1</span>General Information</legend>
				<table cellspacing="0">
					<tr valign="top">
						<td><label for="name">Branch:</label>
							<input type="text" id="name" name="cbranch" value="{{ $formData->cbranch ?? old('cbranch') }}"></td>
                        <td></td>

                    </tr>
					<tr valign="top">
						<td><label for="job">Type of Client:</label>
							<select id="job" name="ctype" required>
								<option @if(isset($formData->ctype) AND $formData->ctype=='Afghanistan') selected @endif value="Afghanistan">Individual</option>
								<option @if(isset($formData->ctype) AND $formData->ctype=='Bahrain') selected @endif value="Bahrain">Business</option>
							</select></td>
						<td><label for="job">Ownership type:</label>
							<select id="job" name="cowner">
								<option value="Afghanistan">Individual</option>
								<option value="Bahrain">Business</option>
							</select></td>
					</tr>
					<tr valign="top">
						<td><label for="job">Client Category:</label>
							<select id="job" name="ccategory">
								<option value="Afghanistan">Individual</option>
								<option value="Bahrain">Business</option>
							</select></td>
					</tr>
					<tr valign="top">
						<td><label for="name">First Name:</label>
							<input type="text" id="name" name="cfname" value="{{ $formData->cfname ?? old('cfname') }}" required></td>
						<td><label for="name">Middle Name:</label>
							<input type="text" id="name" name="cmiddlename" value="{{ $formData->cmiddlename ?? old('cmiddlename') }}"></td>
					</tr>
					<tr valign="top">
						<td><label for="name">Last Name:</label>
							<input type="text" id="name" name="clastname" value="{{ $formData->clastname ?? old('clastname') }}" required></td>
						<td><label for="name">Date of Birth:</label>
                            <input class="form-control datepicker" id="" type="text" placeholder="Select Date" name="cdatebirth" value="{{ $formData->cdatebirth ?? old('cdatebirth') }}" required>
                        </td>
					</tr>
					<tr valign="top">
						<td><label for="name">Expiry in case of Minor:</label>
							<input type="text" id="name" name="cexpiryminor" value="{{ $formData->cexpiryminor ?? old('cexpiryminor') }}"></td>
						<td><label for="name">Country:</label>
							<input type="text" id="name" name="ccountry" value="{{ $formData->ccountry ?? old('ccountry') }}"></td>
					</tr>
					<tr valign="top">
						<td></td>
						<td><label for="name">A/c Officer:</label>
							<input type="text" id="name" name="cacofficer" value="{{ $formData->cacofficer ?? old('cacofficer') }}"></td>
					</tr>
					<tr valign="top">
						<td><label for="name">Introduced By:</label>
							<input type="text" id="name" name="cintro" value="{{ $formData->cintro ?? old('cintro') }}"></td>
						<td><label for="name">Resident:</label>
							<input type="text" id="name" name="cresident" value="{{ $formData->cresident ?? old('cresident') }}"></td>
					</tr>
					<tr valign="top">
						<td><label for="job"><input type="checkbox" id="" name="cminor" /> Is Minor:</label> </td>
						<td></td>
					</tr>
					<tr valign="top">
						<td><label for="job">Resident:</label>
							<select id="job" name="cresident">
								<option value="Afghanistan">Nepali</option>
								<option value="Bahrain">English</option>
							</select></td>
						<td></td>
					</tr>
					<tr valign="top">
						<td><strong>Contact Address</strong></td>
						<td><strong>Permanent Contact</strong></td>
					</tr>
					<tr valign="top">
						<td>

							<table cellspacing="0">
								<tr valign="top">
									<td><label for="name">Address</label><input type="text" id="name" name="caddress" value="{{ $formData->caddress ?? old('caddress') }}" required></td>
									<td><label for="name">City</label><input type="text" id="name" name="ccity" value="{{ $formData->ccity ?? old('ccity') }}"></td>
								</tr>
								<tr valign="top">
									<td><label for="name">House/Block No</label><input type="text" id="name" name="chouseblock" value="{{ $formData->chouseblock ?? old('chouseblock') }}"></td>
									<td><label for="name">VC/MDC</label><input type="text" id="name" name="cvcmdc" value="{{ $formData->cvcmdc ?? old('cvcmdc') }}"></td>
								</tr>
								<tr valign="top">
									<td><label for="name">VC/MDC Name</label>
										<select id="job" name="cvcmdcname">
											<option value="Afghanistan">VC</option>
											<option value="Bahrain">MDC</option>
										</select>
									</td>
									<td><label for="name">Tel No</label><input type="text" id="name" name="ctelno" value="{{ $formData->ctelno ?? old('ctelno') }}"></td>
								</tr>
								<tr valign="top">
									<td><label for="name">PO Box No</label><input type="text" id="name" name="cpobox" value="{{ $formData->cpobox ?? old('cpobox') }}"></td>
									<td><label for="name">Disrict:</label>
										<select id="IssueDistrict" name="cdistrict"><option value="1">Achham</option>
											<option value="2">Arghakhanchi</option>
											<option value="3">Baglung</option>
											<option value="4">Baitadi</option>
											<option value="5">Bajhang</option>
											<option value="6">Bajura</option>
											<option value="7">Banke</option>
											<option value="8">Bara</option>
											<option value="9">Bardiya</option>
											<option value="10">Bhaktapur</option>
											<option value="11">Bhojpur</option>
											<option value="12">Chitwan</option>
											<option value="13">Dadeldhura</option>
											<option value="14">Dailekh</option>
											<option value="15">Dang</option>
											<option value="16">Darchula</option>
											<option value="17">Dhading</option>
											<option value="18">Dhankuta</option>
											<option value="19">Dhanusa</option>
											<option value="20">Dolakha</option>
											<option value="21">Dolpa</option>
											<option value="22">Doti</option>
											<option value="23">Gorkha</option>
											<option value="24">Gulmi</option>
											<option value="25">Humla</option>
											<option value="26">Ilam</option>
											<option value="27">Jajarkot</option>
											<option value="28">Jhapa</option>
											<option value="29">Jumla</option>
											<option value="30">Kailali</option>
											<option value="31">Kalikot</option>
											<option value="32">Kanchanpur</option>
											<option value="33">Kapilbastu</option>
											<option value="34">Kaski</option>
											<option value="35">Kathmandu</option>
											<option value="36">Kavrepalanchok</option>
											<option value="37">Khotang</option>
											<option value="38">Lalitpur</option>
											<option value="39">Lamjung</option>
											<option value="40">Mahottari</option>
											<option value="41">Makwanpur</option>
											<option value="42">Manang</option>
											<option value="43">Morang</option>
											<option value="44">Mugu</option>
											<option value="45">Mustang</option>
											<option value="46">Myagdi</option>
											<option value="47">Nawalparasi</option>
											<option value="48">Nuwakot</option>
											<option value="49">Okhaldhunga</option>
											<option value="50">Palpa</option>
											<option value="51">Panchthar</option>
											<option value="52">Parbat</option>
											<option value="53">Parsa</option>
											<option value="54">Pyuthan</option>
											<option value="55">Ramechhap</option>
											<option value="56">Rasuwa</option>
											<option value="57">Rautahat</option>
											<option value="58">Rolpa</option>
											<option value="59">Rukum</option>
											<option value="60">Rupandehi</option>
											<option value="61">Salyan</option>
											<option value="62">Sankhuwasabha</option>
											<option value="63">Saptari</option>
											<option value="64">Sarlahi</option>
											<option value="65">Sindhuli</option>
											<option value="66">Sindhupalchok</option>
											<option value="67">Siraha</option>
											<option value="68">Solukhumbu</option>
											<option value="69">Sunsari</option>
											<option value="70">Surkhet</option>
											<option value="71">Syangja</option>
											<option value="72">Tanahu</option>
											<option value="73">Taplejung</option>
											<option value="74">Terhathum</option>
											<option value="75">Udayapur</option>
										</select></td>
								</tr>
								<tr valign="top">
									<td><label for="name">Ward no</label><input type="text" id="name" name="cwardno" value="{{ $formData->cwardno ?? old('cwardno') }}"></td>
									<td><label for="name">Country</label><input type="text" id="name" name="ccountry" value="{{ $formData->ccountry ?? old('ccountry') }}"></td>
								</tr>
								<tr valign="top">
									<td><label for="name">State</label><input type="text" id="name" name="cstate" value="{{ $formData->cstate ?? old('cstate') }}"></td>
									<td></td>
								</tr>
							</table>

						</td>
						<td>

							<table cellspacing="0">
								<tr valign="top">
									<td><label for="name">Address</label><input type="text" id="name" name="peraddress" value="{{ $formData->peraddress ?? old('peraddress') }}"></td>
									<td><label for="name">City</label><input type="text" id="name" name="percity" value="{{ $formData->percity ?? old('percity') }}"></td>
								</tr>
								<tr valign="top">
									<td><label for="name">VC/MDC</label><input type="text" id="name" name="pervcmdc" value="{{ $formData->pervcmdc ?? old('pervcmdc') }}"></td>
									<td><label for="name">VC/MDC Name</label>
										<select id="job" name="pervcmdcname">
											<option value="Afghanistan">VC</option>
											<option value="Bahrain">MDC</option>
										</select>
									</td>
								</tr>
								<tr valign="top">
									<td><label for="name">Tel No</label><input type="text" id="name" name="pertel" value="{{ $formData->pertel ?? old('pertel') }}"></td>
									<td><label for="name">PO Box No</label><input type="text" id="name" name="perpobox" value="{{ $formData->perpobox ?? old('perpobox') }}"></td>
								</tr>
								<tr valign="top">
									<td><label for="name">Disrict:</label>
										<select id="IssueDistrict" name="perdistrict"><option value="1">Achham</option>
											<option value="2">Arghakhanchi</option>
											<option value="3">Baglung</option>
											<option value="4">Baitadi</option>
											<option value="5">Bajhang</option>
											<option value="6">Bajura</option>
											<option value="7">Banke</option>
											<option value="8">Bara</option>
											<option value="9">Bardiya</option>
											<option value="10">Bhaktapur</option>
											<option value="11">Bhojpur</option>
											<option value="12">Chitwan</option>
											<option value="13">Dadeldhura</option>
											<option value="14">Dailekh</option>
											<option value="15">Dang</option>
											<option value="16">Darchula</option>
											<option value="17">Dhading</option>
											<option value="18">Dhankuta</option>
											<option value="19">Dhanusa</option>
											<option value="20">Dolakha</option>
											<option value="21">Dolpa</option>
											<option value="22">Doti</option>
											<option value="23">Gorkha</option>
											<option value="24">Gulmi</option>
											<option value="25">Humla</option>
											<option value="26">Ilam</option>
											<option value="27">Jajarkot</option>
											<option value="28">Jhapa</option>
											<option value="29">Jumla</option>
											<option value="30">Kailali</option>
											<option value="31">Kalikot</option>
											<option value="32">Kanchanpur</option>
											<option value="33">Kapilbastu</option>
											<option value="34">Kaski</option>
											<option value="35">Kathmandu</option>
											<option value="36">Kavrepalanchok</option>
											<option value="37">Khotang</option>
											<option value="38">Lalitpur</option>
											<option value="39">Lamjung</option>
											<option value="40">Mahottari</option>
											<option value="41">Makwanpur</option>
											<option value="42">Manang</option>
											<option value="43">Morang</option>
											<option value="44">Mugu</option>
											<option value="45">Mustang</option>
											<option value="46">Myagdi</option>
											<option value="47">Nawalparasi</option>
											<option value="48">Nuwakot</option>
											<option value="49">Okhaldhunga</option>
											<option value="50">Palpa</option>
											<option value="51">Panchthar</option>
											<option value="52">Parbat</option>
											<option value="53">Parsa</option>
											<option value="54">Pyuthan</option>
											<option value="55">Ramechhap</option>
											<option value="56">Rasuwa</option>
											<option value="57">Rautahat</option>
											<option value="58">Rolpa</option>
											<option value="59">Rukum</option>
											<option value="60">Rupandehi</option>
											<option value="61">Salyan</option>
											<option value="62">Sankhuwasabha</option>
											<option value="63">Saptari</option>
											<option value="64">Sarlahi</option>
											<option value="65">Sindhuli</option>
											<option value="66">Sindhupalchok</option>
											<option value="67">Siraha</option>
											<option value="68">Solukhumbu</option>
											<option value="69">Sunsari</option>
											<option value="70">Surkhet</option>
											<option value="71">Syangja</option>
											<option value="72">Tanahu</option>
											<option value="73">Taplejung</option>
											<option value="74">Terhathum</option>
											<option value="75">Udayapur</option>
										</select></td>
									<td><label for="name">Ward no</label><input type="text" id="name" name="perward" value="{{ $formData->perward ?? old('perward') }}"></td>
								</tr>
								<tr valign="top">
									<td><label for="name">Country</label><input type="text" id="name" name="percountry" value="{{ $formData->percountry ?? old('percountry') }}"></td>
									<td><label for="name">State</label><input type="text" id="name" name="perstate" value="{{ $formData->perstate ?? old('perstate') }}"></td>
								</tr>
								<tr valign="top">
									<td><label for="name">Fax</label><input type="text" id="name" name="perfax" value="{{ $formData->perfax ?? old('perfax') }}"></td>
									<td><label for="name">Fax2</label><input type="text" id="name" name="perfax2" value="{{ $formData->perfax2 ?? old('perfax2') }}"></td>
								</tr>
								<tr valign="top">
									<td><label for="name">Mobile</label><input type="text" id="name" name="permobile" value="{{ $formData->permobile ?? old('permobile') }}"></td>
									<td><label for="name">Mobile 2</label><input type="text" id="name" name="permobile2" value="{{ $formData->permobile2 ?? old('permobile2') }}"></td>
								</tr>
								<tr valign="top">
									<td><label for="name">Email</label><input type="text" id="name" name="peremail" value="{{ $formData->peremail ?? old('peremail') }}"></td>
								</tr>
							</table>

						</td>
					</tr>
				</table>


			</fieldset>


			<fieldset class="onlineapp">
				{{--<button type="submit">Go to Previous Step</button>--}}
				<button type="submit">Go to Next Step</button>
			</fieldset>

		</form>
	</div>

@endsection
@section('front-scripts')

@endsection
