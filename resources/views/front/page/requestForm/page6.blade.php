@extends('front.layouts.main')
@section('front-css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'frontend/css/form.css') }}">
@stop
@section('page-title')
    Online Account Opening
@endsection
@section('content')
    <div id="content-inner">
        <h1>Online Account Opening</h1>
        <form action="{{ route('ac_form.store') }}" method="post">
            @csrf
            <input type="hidden" name="form_id" value="{{ $formData->form_id ?? '' }}" id="">
            <input type="hidden" name="page" value="6" id="">
            <fieldset>
                <legend><span class="number">6</span>Extra Detail</legend>
                <table cellspacing="0">
                    <tr valign="top">
                        <td colspan="2"><strong>Client Extra Detail</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Name:</label>
                            <input type="text" id="name" name="extname" value="{{ $formData->extname ?? old('extname') }}"></td>
                        <td><label for="name">Citizenship No:</label>
                            <input type="text" id="name" name="extcitizen" value="{{ $formData->extcitizen ?? old('extcitizen') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Key Risk Grade:</label>
                            <input type="text" id="name" name="extriskgrade" value="{{ $formData->extriskgrade ?? old('extriskgrade') }}"></td>
                        <td><label for="name">Alias:</label>
                            <input type="text" id="name" name="extalias" value="{{ $formData->extalias ?? old('extalias') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Citizen District:</label>
                            <input type="text" id="name" name="extctzndistrict" value="{{ $formData->extctzndistrict ?? old('extctzndistrict') }}"></td>
                        <td><label for="name">Org Name:</label>
                            <input type="text" id="name" name="extorgname" value="{{ $formData->extorgname ?? old('extorgname') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Designation:</label>
                            <input type="text" id="name" name="extdesignation" value="{{ $formData->extdesignation ?? old('extdesignation') }}"></td>
                        <td><label for="name">Date of Birth:</label>
                            <input class="datepicker" type="text" id="name" name="extbirth" value="{{ $formData->extbirth ?? old('extbirth') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Father Name:</label>
                            <input type="text" id="name" name="extfather" value="{{ $formData->extfather ?? old('extfather') }}"></td>
                        <td><label for="name">Grand Father's Name:</label>
                            <input type="text" id="name" name="extgrandfather" value="{{ $formData->extgrandfather ?? old('extgrandfather') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Spouse Name:</label>
                            <input type="text" id="name" name="extspouse" value="{{ $formData->extspouse ?? old('extspouse') }}"></td>
                        <td><label for="name">Citizen Number Issue Date:</label>
                            <input class="datepicker" type="text" id="name" name="extctznissddate" value="{{ $formData->extctznissddate ?? old('extctznissddate') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <label for="job"><input type="checkbox" id="" name="extsignatory" /> Is Signatory</label></td>
                        <td><label for="job"><input type="checkbox" id="" name="extdirector" /> Is Director</label></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Role of Director:</label>
                            <input type="text" id="name" name="extrole" value="{{ $formData->extrole ?? old('extrole') }}"></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Residential Address</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">House/Block no:</label>
                            <input type="text" id="name" name="reshouse" value="{{ $formData->reshouse ?? old('reshouse') }}"></td>
                        <td><label for="name">Area/Street Name:</label>
                            <input type="text" id="name" name="resstreet" value="{{ $formData->resstreet ?? old('resstreet') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Ward No:</label>
                            <input type="text" id="name" name="resward" value="{{ $formData->resward ?? old('resward') }}"></td>
                        <td><label for="name">VC/MDC:</label>
                            <input type="text" id="name" name="resvcmdc" value="{{ $formData->resvcmdc ?? old('resvcmdc') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">District:</label>
                            <input type="text" id="name" name="resdistrict" value="{{ $formData->resdistrict ?? old('resdistrict') }}"></td>
                        <td><label for="name">City:</label>
                            <input type="text" id="name" name="rescity" value="{{ $formData->rescity ?? old('rescity') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Tel No:</label>
                            <input type="text" id="name" name="restel" value="{{ $formData->restel ?? old('restel') }}"></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Permanent Address</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">House/Block no:</label>
                            <input type="text" id="name" name="extperhouse" value="{{ $formData->extperhouse ?? old('extperhouse') }}"></td>
                        <td><label for="name">Area/Street Name:</label>
                            <input type="text" id="name" name="extperstreet" value="{{ $formData->extperstreet ?? old('extperstreet') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Ward No:</label>
                            <input type="text" id="name" name="extperward" value="{{ $formData->extperward ?? old('extperward') }}"></td>
                        <td><label for="name">VC/MDC:</label>
                            <input type="text" id="name" name="extpervcmdc" value="{{ $formData->extpervcmdc ?? old('extpervcmdc') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">District:</label>
                            <input type="text" id="name" name="extperdistrict" value="{{ $formData->extperdistrict ?? old('extperdistrict') }}"></td>
                        <td><label for="name">City:</label>
                            <input type="text" id="name" name="extpercity" value="{{ $formData->extpercity ?? old('extpercity') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Tel No:</label>
                            <input type="text" id="name" name="extpertel" value="{{ $formData->extpertel ?? old('extpertel') }}"></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Permanent Address</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">House/Block no:</label>
                            <input type="text" id="name" name="mailhouse" value="{{ $formData->mailhouse ?? old('mailhouse') }}"></td>
                        <td><label for="name">Area/Street Name:</label>
                            <input type="text" id="name" name="mailstreet" value="{{ $formData->mailstreet ?? old('mailstreet') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Ward No:</label>
                            <input type="text" id="name" name="mailward" value="{{ $formData->mailward ?? old('mailward') }}"></td>
                        <td><label for="name">VC/MDC:</label>
                            <input type="text" id="name" name="mailvcmdc" value="{{ $formData->mailvcmdc ?? old('mailvcmdc') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">District:</label>
                            <input type="text" id="name" name="maildistrict" value="{{ $formData->maildistrict ?? old('maildistrict') }}"></td>
                        <td><label for="name">City:</label>
                            <input type="text" id="name" name="mailcity" value="{{ $formData->mailcity ?? old('mailcity') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Tel No:</label>
                            <input type="text" id="name" name="mailtel" value="{{ $formData->mailtel ?? old('mailtel') }}"></td>
                        <td><label for="name">Address:</label>
                            <input type="text" id="name" name="mailaddress" value="{{ $formData->mailaddress ?? old('mailaddress') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Fax No:</label>
                            <input type="text" id="name" name="mailfax" value="{{ $formData->mailfax ?? old('mailfax') }}"></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Business Address</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Tel No:</label>
                            <input type="text" id="name" name="bustel" value="{{ $formData->bustel ?? old('bustel') }}"></td>
                        <td><label for="name">Fax No:</label>
                            <input type="text" id="name" name="busfax" value="{{ $formData->busfax ?? old('busfax') }}"></td>
                    </tr>
                </table>
            </fieldset>
            <fieldset class="onlineapp"><a href="{{ route('ac_form.page5',$formData->form_id) }}" class="back-onlap">Go to Previous Step</a><button type="submit">Submit Now</button></fieldset>
        </form>
    </div>
@endsection
