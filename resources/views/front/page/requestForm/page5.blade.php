@extends('front.layouts.main')
@section('front-css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'frontend/css/form.css') }}">
@stop
@section('page-title')
    Online Account Opening
@endsection
@section('content')
    <div id="content-inner">
        <h1>Online Account Opening</h1>
        <form action="{{ route('ac_form.store') }}" method="post">
            @csrf
            <input type="hidden" name="form_id" value="{{ $formData->form_id ?? '' }}" id="">
            <input type="hidden" name="page" value="5" id="">
            <fieldset>
                <legend><span class="number">5</span>Others</legend>
                <table cellspacing="0">
                    <tr valign="top">
                        <td><label for="name">Occupation:</label>
                            <select id="job" name="othoccupation">
                                <option value="">Salary Person</option>
                                <option value="">Service</option>
                                <option value="">Student</option>
                                <option value="">Doctor</option>
                                <option value="">Engineer</option>
                                <option value="">Professor</option>
                                <option value="">Others, Please specify</option>
                            </select></td>
                        <td><label for="name">Source of Fund:</label>
                            <select id="job" name="othsource">

                                <option value="">Saving</option>
                                <option value="">Salary</option>
                                <option value="">Inheritance/Gift</option>
                                <option value="">Disposal of Assets</option>
                                <option value="">Return on Investments</option>
                                <option value="">Others, Please specify</option>
                            </select></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Purpoose of A/C:</label>
                            <select id="job" name="othpurpose">

                                <option value="">Saving</option>
                                <option value="">Investment</option>
                                <option value="">Loan Repayment</option>
                                <option value="">Payroll</option>
                                <option value="">Remittance</option>
                                <option value="">Transactional</option>
                                <option value="">Others, Please specify</option>
                            </select></td>
                        <td><label for="name">Annual Turnover:</label>
                            <input type="text" id="name" name="othturnover" value="{{ $formData->othturnover ?? old('othturnover') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Foreign Associate</strong></td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <label for="job"><input type="checkbox" id="" name="forassociate" /> Is Foreign Associate:</label></td>
                        <td><label for="name">Residential Status:</label>
                            <input type="text" id="name" name="forresidential" value="{{ $formData->forresidential ?? old('forresidential') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Name of Director (None Personal):</label>
                            <input type="text" id="name" name="fordirector" value="{{ $formData->fordirector ?? old('fordirector') }}"></td>
                        <td><label for="name">Country:</label>
                            <input type="text" id="name" name="forcountry" value="{{ $formData->forcountry ?? old('forcountry') }}"></td>
                    </tr>
                    {{--<tr valign="top">--}}
                        {{--<td colspan="2"><strong>KYC</strong></td>--}}
                    {{--</tr>--}}
                    {{--<tr valign="top">--}}
                        {{--<td><label for="name">Business Unit:</label>--}}
                            {{--<input type="text" id="name" name="kycbusiness" value="{{ $formData->kycbusiness ?? old('kycbusiness') }}"></td>--}}
                        {{--<td><label for="name">Reviewed Date:</label>--}}
                            {{--<input class="datepicker" type="text" id="name" name="kycreviewed" value="{{ $formData->kycreviewed ?? old('kycreviewed') }}"></td>--}}
                    {{--</tr>--}}
                    {{--<tr valign="top">--}}
                        {{--<td><label for="name">Deposit Risk Grade:</label>--}}
                            {{--<select id="job" name="kycriskgrade">--}}
                                {{--<option value="">menu</option>--}}
                                {{--<option value="">menu</option>--}}
                            {{--</select></td>--}}
                        {{--<td><label for="name">Trans Reviewed Date:</label>--}}
                            {{--<input class="datepicker" type="text" id="name" name="kyctrans" value="{{ $formData->kyctrans ?? old('kyctrans') }}"></td>--}}
                    {{--</tr>--}}
                    {{--<tr valign="top">--}}
                        {{--<td><label for="name">Reason:</label>--}}
                            {{--<input type="text" id="name" name="kycreason" value="{{ $formData->kycreason ?? old('kycreason') }}"></td>--}}
                        {{--<td>&nbsp;</td>--}}
                    {{--</tr>--}}
                    {{--<tr valign="top">--}}
                        {{--<td colspan="2"><strong>KYC Doc Update</strong></td>--}}
                    {{--</tr>--}}
                    {{--<tr valign="top">--}}
                        {{--<td><label for="name">DOC Update Status:</label>--}}
                            {{--<select id="job" name="docstatus">--}}
                                {{--<option value="menu">menu</option>--}}
                                {{--<option value="menu">menu</option>--}}
                            {{--</select></td>--}}
                        {{--<td><label for="name">Date of Update:</label>--}}
                            {{--<input class="datepicker" type="text" id="name" name="docdateupdate" value="{{ $formData->docdateupdate ?? old('docdateupdate') }}"></td>--}}
                    {{--</tr>--}}
                    {{--<tr valign="top">--}}
                        {{--<td><label for="name">KYC Remarks:</label>--}}
                            {{--<input type="text" id="name" name="docremarks" value="{{ $formData->docremarks ?? old('docremarks') }}"></td>--}}
                        {{--<td><label for="name">Keep Personal Category:</label>--}}
                            {{--<label for="job"><input type="checkbox" id="" name="perexposed" /> Politically Exposed Person(PEP):</label>--}}
                            {{--<label for="job"><input type="checkbox" id="" name="perpunish" /> Declaration for punishment any crime:</label>--}}
                            {{--<label for="job"><input type="checkbox" id="" name="perhighpositioned" /> High Positioned Person (HPP):</label>--}}
                            {{--<label for="job"><input type="checkbox" id="" name="pernonface" /> Non Face to face Customer(NF2F):</label>--}}
                            {{--<label for="job"><input type="checkbox" id="" name="pervimpper" /> Very Important Person (VIP):</label>--}}
                        {{--</td>--}}
                    {{--</tr>--}}
                    <tr valign="top">
                        <td colspan="2"><strong>Insurance</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Insurance Information:</label>
                            <input type="text" id="name" name="insinfo" value="{{ $formData->insinfo ?? old('insinfo') }}"></td>
                        <td><label for="name">Remarks:</label>
                            <input type="text" id="name" name="insremark" value="{{ $formData->insremark ?? old('insremark') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Client Limit:</label>
                            <input type="text" id="name" name="inslimit" value="{{ $formData->inslimit ?? old('inslimit') }}"></td>
                        <td><label for="name">Group:</label>
                            <input type="text" id="name" name="insgroup" value="{{ $formData->insgroup ?? old('insgroup') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Borrower Code:</label>
                            <input type="text" id="name" name="insborrower" value="{{ $formData->insborrower ?? old('insborrower') }}"></td>
                        <td>
                            {{--<label for="job"><input type="checkbox" id="" name="insstatement" /> Mail Statement</label>--}}
                        </td>
                    </tr>
                    {{--<tr valign="top">--}}
                        {{--<td><label for="job"><input type="checkbox" id="" name="insmis" /> Keep MIS?</label></td>--}}
                        {{--<td><label for="job"><input type="checkbox" id="" name="inskycupdate" /> KYC Update</label></td>--}}
                    {{--</tr>--}}
                </table>
            </fieldset>
            <fieldset class="onlineapp"><a href="{{ route('ac_form.page4',$formData->form_id) }}" class="back-onlap">Go to Previous Step</a><button type="submit">Go to Next Step</button></fieldset>
        </form>
    </div>
@endsection
