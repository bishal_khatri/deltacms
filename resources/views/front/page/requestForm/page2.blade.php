@extends('front.layouts.main')
@section('front-css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'frontend/css/form.css') }}">
@stop
@section('page-title')
    Online Account Opening
@endsection
@section('content')
    <div id="content-inner">

        <h1>Online Account Opening</h1>
        <form action="{{ route('ac_form.store') }}" method="post" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="form_id" value="{{ $formData->form_id ?? '' }}" id="">
            <input type="hidden" name="page" value="2" id="">
            <fieldset>
                <legend><span class="number">2</span>Customer Information</legend>
                <table cellspacing="0">
                    <tr valign="top">
                        <td colspan="2"><strong>Citizenship Detail</strong></td>
                    </tr>
                    <tr valign="top">
                        <td width="50%"><label for="name">Type of ID:</label>
                            <select id="job" name="cusid">
                                <option value="">Citizenship</option>
                                <option value="">Birth Certificate</option>
                                <option value="">Voter ID</option>
                                <option value="">Driving License</option>
                                <option value="">Passport</option>
                                <option value="">Others</option>
                            </select></td>
                        <td><label for="name">Prev Citizenship No:</label>
                            <input type="text" id="name" name="cusprevcitizen" value="{{ $formData->cusprevcitizen ?? old('cusprevcitizen') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name"> Id No:</label>
                            <input type="text" id="name" name="cusidno" value="{{ $formData->cusidno ?? old('cusidno') }}"></td>
                        <td><label for="name">Issued Authority:</label>
                            <input type="text" id="name" name="cusauthority" value="{{ $formData->cusauthority ?? old('cusauthority') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Issued Date:</label>
                            <input class="datepicker" type="text" id="name" name="cusissdate" value="{{ $formData->cusissdate ?? old('cusissdate') }}"></td></td>
                        <td><label for="job">ID District:</label>
                            <input type="text" id="name" name="cusiddistrict" value="{{ $formData->cusiddistrict ?? old('cusiddistrict') }}"></td>
                    </tr>
                    <tr>
                        <td>Upload Your Citizenship:<br><input type="file" name="citizenship_image" id="avatar">
                        </td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Customer Info</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Marital Status:</label>
                            <select id="job" name="cusmarital">
                                <option value="">Single</option>
                                <option value="">Married</option>
                                <option value="">Widowed</option>
                                <option value="">Divorced</option>
                            </select></td>
                        <td><label for="job">Nationality:</label>

                            <select id="job" name="cusnationality">
                                <option value="">Nepali</option>
                                <option value="">Indian</option>
                                <option value="">NRN</option>
                                <option value="">Others</option>
                            </select></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Salutation:</label>
                            <select id="job" name="cussalutation">
                                <option value="">Miss</option>
                                <option value="">Mr.</option>
                                <option value="">Mrs.</option>
                            </select>
                        </td>
                        <td><label for="job">Religion:</label>
                            <select id="job" name="cusreligion">
                                <option value="Afghanistan">Hindu</option>
                                <option value="Bahrain">Muslim</option>
                                <option value="Bahrain">Christian</option>
                                <option value="Bahrain">Other</option>
                            </select></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Gender:</label>
                            <select id="job" name="cusgender">
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Other">Other</option>
                            </select></td>
                        <td>Upload PP:<br><input type="file" name="pp_image" id="avatar">
                        </td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Employer Information</strong></td>
                    </tr>
                    <tr valign="top">
                        <td width="50%"><label for="name">Org. Name:</label>
                            <input type="text" id="name" name="emporgname" value="{{ $formData->emporgname ?? old('emporgname') }}"></td>
                        <td><label for="name">Designation:</label>
                            <input type="text" id="name" name="empdesignation" value="{{ $formData->empdesignation ?? old('empdesignation') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td width="50%"><label for="name">Org. Address:</label>
                            <input type="text" id="name" name="emporgaddress" value="{{ $formData->emporgaddress ?? old('emporgaddress') }}"></td>
                        <td><label for="name">Appox. Annual Income/Salary:</label>
                            <input type="text" id="name" name="empsalary" value="{{ $formData->empsalary ?? old('empsalary') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td colspan="2"><strong>Registered Office Address</strong></td>
                    </tr>
                    <tr valign="top">
                        <td width="50%">
                            <label for="name">Address:</label>
                            <input type="text" id="name" name="regaddress" value="{{ $formData->regaddress ?? old('regaddress') }}">
                        </td>
                        <td>
                            <label for="name">City </label>
                            <input type="text" id="name" name="regcity" value="{{ $formData->regcity ?? old('regcity') }}">
                        </td>
                    </tr>
                    <tr valign="top">
                        <td width="50%"><label for="name">Country:</label>
                            <input type="text" id="name" name="regcountry" value="{{ $formData->regcountry ?? old('regcountry') }}">
                        </td>
                        <td><label for="name">VC/MDC:</label>
                            <select id="job" name="regvcmdc">
                                <option value="">Village Municipality</option>
                                <option value="">Municipality</option>
                                <option value="">Sub Metropolitan</option>
                                <option value="">Metropolitan</option>
                            </select>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td width="50%"><label for="name">Ward No:</label>
                            <input type="text" id="name" name="regward" value="{{ $formData->regward ?? old('regward') }}"></td>
                        <td>
                            <label for="name">District:</label>
                            <select id="IssueDistrict" name="regdistrict"><option value="1">Achham</option>
                                <option value="2">Arghakhanchi</option>
                                <option value="3">Baglung</option>
                                <option value="4">Baitadi</option>
                                <option value="5">Bajhang</option>
                                <option value="6">Bajura</option>
                                <option value="7">Banke</option>
                                <option value="8">Bara</option>
                                <option value="9">Bardiya</option>
                                <option value="10">Bhaktapur</option>
                                <option value="11">Bhojpur</option>
                                <option value="12">Chitwan</option>
                                <option value="13">Dadeldhura</option>
                                <option value="14">Dailekh</option>
                                <option value="15">Dang</option>
                                <option value="16">Darchula</option>
                                <option value="17">Dhading</option>
                                <option value="18">Dhankuta</option>
                                <option value="19">Dhanusa</option>
                                <option value="20">Dolakha</option>
                                <option value="21">Dolpa</option>
                                <option value="22">Doti</option>
                                <option value="23">Gorkha</option>
                                <option value="24">Gulmi</option>
                                <option value="25">Humla</option>
                                <option value="26">Ilam</option>
                                <option value="27">Jajarkot</option>
                                <option value="28">Jhapa</option>
                                <option value="29">Jumla</option>
                                <option value="30">Kailali</option>
                                <option value="31">Kalikot</option>
                                <option value="32">Kanchanpur</option>
                                <option value="33">Kapilbastu</option>
                                <option value="34">Kaski</option>
                                <option value="35">Kathmandu</option>
                                <option value="36">Kavrepalanchok</option>
                                <option value="37">Khotang</option>
                                <option value="38">Lalitpur</option>
                                <option value="39">Lamjung</option>
                                <option value="40">Mahottari</option>
                                <option value="41">Makwanpur</option>
                                <option value="42">Manang</option>
                                <option value="43">Morang</option>
                                <option value="44">Mugu</option>
                                <option value="45">Mustang</option>
                                <option value="46">Myagdi</option>
                                <option value="47">Nawalparasi</option>
                                <option value="48">Nuwakot</option>
                                <option value="49">Okhaldhunga</option>
                                <option value="50">Palpa</option>
                                <option value="51">Panchthar</option>
                                <option value="52">Parbat</option>
                                <option value="53">Parsa</option>
                                <option value="54">Pyuthan</option>
                                <option value="55">Ramechhap</option>
                                <option value="56">Rasuwa</option>
                                <option value="57">Rautahat</option>
                                <option value="58">Rolpa</option>
                                <option value="59">Rukum</option>
                                <option value="60">Rupandehi</option>
                                <option value="61">Salyan</option>
                                <option value="62">Sankhuwasabha</option>
                                <option value="63">Saptari</option>
                                <option value="64">Sarlahi</option>
                                <option value="65">Sindhuli</option>
                                <option value="66">Sindhupalchok</option>
                                <option value="67">Siraha</option>
                                <option value="68">Solukhumbu</option>
                                <option value="69">Sunsari</option>
                                <option value="70">Surkhet</option>
                                <option value="71">Syangja</option>
                                <option value="72">Tanahu</option>
                                <option value="73">Taplejung</option>
                                <option value="74">Terhathum</option>
                                <option value="75">Udayapur</option>
                            </select>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td width="50%"><label for="name">State:</label>
                            <input type="text" id="name" name="regstate" value="{{ $formData->regstate ?? old('regstate') }}"></td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </fieldset>
            <fieldset class="onlineapp"><a href="{{ route('ac_form.page1',$formData->form_id) }}" class="back-onlap">Go to Previous Step</a><button type="submit">Go to Next Step</button></fieldset>
        </form>
    </div>
@endsection
