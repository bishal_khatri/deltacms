@extends('front.layouts.main')
@section('front-css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'frontend/css/form.css') }}">
@stop
@section('page-title')
    Online Account Opening
@endsection
@section('content')
    <div id="content-inner">
        <h1>Online Account Opening</h1>
        <form action="{{ route('ac_form.store') }}" method="post">
            @csrf
            <input type="hidden" name="form_id" value="{{ $formData->form_id ?? '' }}" id="">
            <input type="hidden" name="page" value="3" id="">
            <fieldset>
                <legend><span class="number">3</span>Family Information</legend>
                <table cellspacing="0">
                    <tr>
                        <td>&nbsp;</td>
                        <td>First Name</td>
                        <td>Middle Name</td>
                        <td>Last Name</td>
                        <td>Citizenship No</td>
                        <td>Citizen Issued Date</td>
                        <td>Citizen Issued District</td>
                        <td>Citizen District Name</td>
                    </tr>
                    <tr>
                        <td>Grand Father</td>
                        <td><input type="text" id="name" name="grnffirst" value="{{ $formData->grnffirst ?? old('grnffirst') }}"></td>
                        <td><input type="text" id="name" name="grnfmiddle" value="{{ $formData->grnfmiddle ?? old('grnfmiddle') }}"></td>
                        <td><input type="text" id="name" name="grnflast" value="{{ $formData->grnflast ?? old('grnflast') }}"></td>
                        <td><input type="text" id="name" name="grnfctznno" value="{{ $formData->grnfctznno ?? old('grnfctznno') }}"></td>
                        <td><input type="text" id="name" name="grnfctznissdate" value="{{ $formData->grnfctznissdate ?? old('grnfctznissdate') }}"></td>
                        <td><input type="text" id="name" name="grnfctznissdistrict" value="{{ $formData->grnfctznissdistrict ?? old('grnfctznissdistrict') }}"></td>
                        <td><input type="text" id="name" name="grnfctzndistrict" value="{{ $formData->grnfctzndistrict ?? old('grnfctzndistrict') }}"></td>
                    </tr>
                    <tr>
                        <td>Grand Mother</td>
                        <td><input type="text" id="name" name="grmfirst" value="{{ $formData->grmfirst ?? old('grmfirst') }}"></td>
                        <td><input type="text" id="name" name="grmmiddle" value="{{ $formData->grmmiddle ?? old('grmmiddle') }}"></td>
                        <td><input type="text" id="name" name="grmlast" value="{{ $formData->grmlast ?? old('grmlast') }}"></td>
                        <td><input type="text" id="name" name="grmctznno" value="{{ $formData->grmctznno ?? old('grmctznno') }}"></td>
                        <td><input type="text" id="name" name="grmctznissdate" value="{{ $formData->grmctznissdate ?? old('grmctznissdate') }}"></td>
                        <td><input type="text" id="name" name="grmctznissdistrict" value="{{ $formData->grmctznissdistrict ?? old('grmctznissdistrict') }}"></td>
                        <td><input type="text" id="name" name="grmctzndistrict" value="{{ $formData->grmctzndistrict ?? old('grmctzndistrict') }}"></td>
                    </tr>
                    <tr>
                        <td>Father</td>
                        <td><input type="text" id="name" name="ftrfirst" value="{{ $formData->ftrfirst ?? old('ftrfirst') }}"></td>
                        <td><input type="text" id="name" name="ftrmiddle" value="{{ $formData->ftrmiddle ?? old('ftrmiddle') }}"></td>
                        <td><input type="text" id="name" name="ftrfast" value="{{ $formData->ftrfast ?? old('ftrfast') }}"></td>
                        <td><input type="text" id="name" name="ftrctznno" value="{{ $formData->ftrctznno ?? old('ftrctznno') }}"></td>
                        <td><input type="text" id="name" name="ftrctznissdate" value="{{ $formData->ftrctznissdate ?? old('ftrctznissdate') }}"></td>
                        <td><input type="text" id="name" name="ftrctznissdistrict" value="{{ $formData->ftrctznissdistrict ?? old('ftrctznissdistrict') }}"></td>
                        <td><input type="text" id="name" name="ftrctzndistrict" value="{{ $formData->ftrctzndistrict ?? old('ftrctzndistrict') }}"></td>
                    </tr>
                    <tr>
                        <td>Mother</td>
                        <td><input type="text" id="name" name="mthrfirst" value="{{ $formData->mthrfirst ?? old('mthrfirst') }}"></td>
                        <td><input type="text" id="name" name="mthrmiddle" value="{{ $formData->mthrmiddle ?? old('mthrmiddle') }}"></td>
                        <td><input type="text" id="name" name="mthrlast" value="{{ $formData->mthrlast ?? old('mthrlast') }}"></td>
                        <td><input type="text" id="name" name="mthrctznno" value="{{ $formData->mthrctznno ?? old('mthrctznno') }}"></td>
                        <td><input type="text" id="name" name="mthrctznissdate" value="{{ $formData->mthrctznissdate ?? old('mthrctznissdate') }}"></td>
                        <td><input type="text" id="name" name="mthrctznissdistrict" value="{{ $formData->mthrctznissdistrict ?? old('mthrctznissdistrict') }}"></td>
                        <td><input type="text" id="name" name="mthrctzndistrict" value="{{ $formData->mthrctzndistrict ?? old('mthrctzndistrict') }}"></td>
                    </tr>
                </table>
            </fieldset>
            <fieldset class="onlineapp"><a href="{{ route('ac_form.page2',$formData->form_id) }}" class="back-onlap">Go to Previous Step</a><button type="submit">Go to Next Step</button></fieldset>
        </form>
    </div>
@endsection
