@extends('front.layouts.main')
@section('front-css')
    <link rel="stylesheet" href="{{ asset(STATIC_DIR.'frontend/css/form.css') }}">
@stop
@section('page-title')
    Online Account Opening
@endsection
@section('content')
    <div id="content-inner">
        <h1>Online Account Opening</h1>
        <form action="{{ route('ac_form.store') }}" method="post">
            @csrf
            <input type="hidden" name="form_id" value="{{ $formData->form_id ?? '' }}" id="">
            <input type="hidden" name="page" value="4" id="">
            <fieldset>
                <legend><span class="number">4</span>Info Comm/Pers</legend>
                <table cellspacing="0">
                    <tr valign="top">
                        <td colspan="2"><strong>PAN/VAT No</strong></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">ID Type:</label>
                            <select id="job" name="comid">

                                <option value="">PAN</option>
                                <option value="">VAT</option>
                            </select></td>
                        <td><label for="name">Issued Authority:</label>
                            <input type="text" id="name" name="comauthority" value="{{ $formData->comauthority ?? old('comauthority') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Id Number:</label>
                            <input type="text" id="name" name="comidnum" value="{{ $formData->comidnum ?? old('comidnum') }}"></td>
                        <td><label for="name">Issued in District:</label>
                            <input type="text" id="name" name="comissdistrict" value="{{ $formData->comissdistrict ?? old('comissdistrict') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Business Activity Code:</label>
                            <input type="text" id="name" name="comactcode" value="{{ $formData->comactcode ?? old('comactcode') }}"></td>
                        <td><label for="name">Issued Date:</label>
                            <input class="datepicker" type="text" id="name" name="comissdate" value="{{ $formData->comissdate ?? old('comissdate') }}"></td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Expiry Date:</label>
                            <input class="datepicker" type="text" id="name" name="comexpiry" value="{{ $formData->comexpiry ?? old('comexpiry') }}"></td>
                        <td></td>
                    </tr>
                </table>
            </fieldset>
            <fieldset class="onlineapp"><a href="{{ route('ac_form.page3',$formData->form_id) }}" class="back-onlap">Go to Previous Step</a><button type="submit">Go to Next Step</button></fieldset>
        </form>
    </div>
@endsection
