@extends('front.layouts.main')
@section('page-title')
    {{ $downloads->title }}
@endsection
@section('content')
    <div id="content-inner">
        <h1>{{ $downloads->title ?? '' }}</h1>
        <table width="100%" cellspacing="0" border="0" class="table">
            <tbody>
            @if($downloadsItem->count()>0 AND $has_group==0)
                @foreach($downloadsItem as $value)
                    <tr>
                        <td>{{ $value->title }}</td>
                        <td><a href="{{ route('downloads.download',$value->download_items_id) }}" download="AGM minute 2073"> Download </a></td>
                    </tr>
                @endforeach
            @elseif($downloadsItem->count()>0 AND $has_group==1)
                @foreach($downloadsItem as $key=>$value)
                    <tr>
                        <td colspan="3"><strong>{{ $key }}</strong></td>
                    </tr>
                    @foreach($value as $item)
                        <tr>
                            <td>{{ $item->title }}</td>
                            <td><a href="http://www.ufl.com.np/wp-content/uploads/2019/01/Anusuchi-14-of-Poush-2075.pdf" target="_blank" rel="noopener">View</a></td>
                        </tr>
                    @endforeach
                @endforeach
            @else
                <tr>
                    <td colspan="2">No items available</td>
                </tr>
            @endif
            </tbody>
        </table>
    </div>

@endsection
