@extends('front.layouts.main')
@section('front-css')
    <link href="{{ asset(STATIC_DIR.'frontend/css/form.css') }}" rel="stylesheet" type="text/css" />
    {{--<link href="{{ asset(STATIC_DIR.'assets/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">--}}
    <link href="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.css') }}" rel="stylesheet" type="text/css" />
    <style>
        .required{
            color: red;
        }
    </style>
@endsection
@section('page-title')
    Register
@endsection
@section('content')
    <div id="content-inner">
        <h1>User Registration</h1>
        
        
        <form action="{{ route('user_register') }}" method="post" enctype="multipart/form-data">
            @csrf
            <fieldset>
                <legend><span class="number">1</span>General Information</legend>
                
                <table cellspacing="0">
                    <tr valign="top">
                        <td width="50%"><label for="first_name">First Name: <span class="required">*</span></label>
                            <input type="text" id="first_name" name="first_name" value="{{ old('first_name') }}">
	                        @if ($errors->has('first_name'))
		                        <span class="invalid-feedback" role="alert">
                                    <span style="color: red;font-style: italic;">{{ $errors->first('first_name') }}</span>
                                </span>
	                        @endif
                        </td>
                        <td><label for="middle_name">Middle Name:</label>
                            <input type="text" id="middle_name" name="middle_name" value="{{ old('middle_name') }}">
                        </td>
                    </tr>
                    <tr valign="top">
                        <td>
                            <label for="last_name">Last Name: <span class="required">*</span></label>
                            <input type="text" id="last_name" name="last_name" value="{{ old('last_name') }}">
	                        @if ($errors->has('last_name'))
		                        <span class="invalid-feedback" role="alert">
                                    <span style="color: red;font-style: italic;">{{ $errors->first('last_name') }}</span>
                                </span>
	                        @endif
                        </td>
                        <td>
                            <label for="dob">Date of Birth: <span class="required">*</span></label>
                            <input type="text" name="dob" class="form-control datepicker-autoclose" autocomplete="off" value="{{ old('dob') }}">
	                        @if ($errors->has('dob'))
		                        <span class="invalid-feedback" role="alert">
                                    <span style="color: red;font-style: italic;">{{ $errors->first('dob') }}</span>
                                </span>
	                        @endif
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Nationality:</label>
                            <select id="job" name="country">
                                <option value="Afghanistan">Afghanistan</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="Bhutan">Bhutan</option>
                                <option value="Brunei">Brunei</option>
                                <option value="Burma">Burma</option>
                                <option value="Cambodia">Cambodia</option>
                                <option value="China(PRC)">China (PRC)</option>
                                <option value="Hong Kong">Hong Kong</option>
                                <option value="India">India</option>
                                <option value="Indonesia">Indonesia</option>
                                <option value="Iran">Iran</option>
                                <option value="Iraq">Iraq</option>
                                <option value="Israel">Israel</option>
                                <option value="Japan">Japan</option>
                                <option value="Jordan">Jordan</option>
                                <option value="Kazakhstan">Kazakhstan</option>
                                <option value="North Korea">North Korea</option>
                                <option value="South Korea">South Korea</option>
                                <option value="Kuwait">Kuwait</option>
                                <option value="Kyrgyzstan">Kyrgyzstan</option>
                                <option value="Laos">Laos</option>
                                <option value="Lebanon">Lebanon</option>
                                <option value="Philippines">Philippines</option>
                                <option value="Malaysia">Malaysia</option>
                                <option value="Maldives">Maldives</option>
                                <option value="Mongolia">Mongolia</option>
                                <option value="Nepal">Nepal</option>
                                <option value="Oman">Oman</option>
                                <option value="Pakistan">Pakistan</option>
                                <option value="Qatar">Qatar</option>
                                <option value="Saudi Arabia">Saudi Arabia</option>
                                <option value="Singapore">Singapore</option>
                                <option value="Sri Lanka">Sri Lanka</option>
                                <option value=" Syria"> Syria</option>
                                <option value="Taiwan">Taiwan</option>
                                <option value="Tajikistan">Tajikistan</option>
                                <option value="Thailand">Thailand</option>
                                <option value="Timor-Leste">Timor-Leste</option>
                                <option value="Turkmenistan">Turkmenistan</option>
                                <option value="United Arab Emirates	">United Arab Emirates	</option>
                                <option value="Uzbekistan">Uzbekistan</option>
                                <option value="Vietnam">Vietnam</option>
                                <option value="Yemen">Yemen</option>
                            </select>
                        </td>
                        <td><label for="gender">Gender: <span class="required">*</span></label>
                            <select id="gender" name="gender"><option value="">-----------------Select Gender------------</option>
                                <option value="Male">Male</option>
                                <option value="Female">Female</option>
                                <option value="Other">Other</option>
                                <option value="-">---</option>
                            </select><br>
	                        @if ($errors->has('gender'))
		                        <span class="invalid-feedback" role="alert">
                                    <span style="color: red;font-style: italic;">{{ $errors->first('gender') }}</span>
                                </span>
	                        @endif
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Marital Status:</label>
                            <select id="marital_status" name="marital_status"><option value="">-----------------Select MaritalStatus------------</option>
                                <option value="Married">Married</option>
                                <option value="Single">Single</option>
                                <option value="Other">Other</option>
                            </select>
                        </td>
                        <td><label for="blood_group">Blood Group:</label>
                            <select id="blood_group" name="blood_group"><option value="">-----------------Select Gender------------</option>
                                <option value="O+">O+</option>
                                <option value="O-">O-</option>
                                <option value="A+">A+</option>
                                <option value="A-">A-</option>
                                <option value="B+">B+</option>
                                <option value="B-">B-</option>
                                <option value="AB+">AB+</option>
                                <option value="AB-">AB-</option>
                            </select>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="citizenship_number">Citizenship Number:</label>
                            <input type="text" id="citizenship_number" name="citizenship_number"></td>
                        <td><label for="name">Citizenship Issued Disrict:</label>
                            <select id="citizenship_district" name="citizenship_district"><option value="1">Achham</option>
                                <option value="2">Arghakhanchi</option>
                                <option value="3">Baglung</option>
                                <option value="4">Baitadi</option>
                                <option value="5">Bajhang</option>
                                <option value="6">Bajura</option>
                                <option value="7">Banke</option>
                                <option value="8">Bara</option>
                                <option value="9">Bardiya</option>
                                <option value="10">Bhaktapur</option>
                                <option value="11">Bhojpur</option>
                                <option value="12">Chitwan</option>
                                <option value="13">Dadeldhura</option>
                                <option value="14">Dailekh</option>
                                <option value="15">Dang</option>
                                <option value="16">Darchula</option>
                                <option value="17">Dhading</option>
                                <option value="18">Dhankuta</option>
                                <option value="19">Dhanusa</option>
                                <option value="20">Dolakha</option>
                                <option value="21">Dolpa</option>
                                <option value="22">Doti</option>
                                <option value="23">Gorkha</option>
                                <option value="24">Gulmi</option>
                                <option value="25">Humla</option>
                                <option value="26">Ilam</option>
                                <option value="27">Jajarkot</option>
                                <option value="28">Jhapa</option>
                                <option value="29">Jumla</option>
                                <option value="30">Kailali</option>
                                <option value="31">Kalikot</option>
                                <option value="32">Kanchanpur</option>
                                <option value="33">Kapilbastu</option>
                                <option value="34">Kaski</option>
                                <option value="35">Kathmandu</option>
                                <option value="36">Kavrepalanchok</option>
                                <option value="37">Khotang</option>
                                <option value="38">Lalitpur</option>
                                <option value="39">Lamjung</option>
                                <option value="40">Mahottari</option>
                                <option value="41">Makwanpur</option>
                                <option value="42">Manang</option>
                                <option value="43">Morang</option>
                                <option value="44">Mugu</option>
                                <option value="45">Mustang</option>
                                <option value="46">Myagdi</option>
                                <option value="47">Nawalparasi</option>
                                <option value="48">Nuwakot</option>
                                <option value="49">Okhaldhunga</option>
                                <option value="50">Palpa</option>
                                <option value="51">Panchthar</option>
                                <option value="52">Parbat</option>
                                <option value="53">Parsa</option>
                                <option value="54">Pyuthan</option>
                                <option value="55">Ramechhap</option>
                                <option value="56">Rasuwa</option>
                                <option value="57">Rautahat</option>
                                <option value="58">Rolpa</option>
                                <option value="59">Rukum</option>
                                <option value="60">Rupandehi</option>
                                <option value="61">Salyan</option>
                                <option value="62">Sankhuwasabha</option>
                                <option value="63">Saptari</option>
                                <option value="64">Sarlahi</option>
                                <option value="65">Sindhuli</option>
                                <option value="66">Sindhupalchok</option>
                                <option value="67">Siraha</option>
                                <option value="68">Solukhumbu</option>
                                <option value="69">Sunsari</option>
                                <option value="70">Surkhet</option>
                                <option value="71">Syangja</option>
                                <option value="72">Tanahu</option>
                                <option value="73">Taplejung</option>
                                <option value="74">Terhathum</option>
                                <option value="75">Udayapur</option>
                            </select>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="email">Email Address: <span class="required">*</span></label>
                            <input type="text" id="email" name="email" value="{{ old('email') }}">
	                        @if ($errors->has('email'))
		                        <span class="invalid-feedback" role="alert">
                                    <span style="color: red;font-style: italic;">{{ $errors->first('email') }}</span>
                                </span>
	                        @endif
                        </td>
                        <td><label for="email_alt">Alternative Email Address:</label>
                            <input type="text" id="email_alt" name="email_alt" value="{{ old('email_alt') }}">
	                        @if ($errors->has('email_alt'))
		                        <span class="invalid-feedback" role="alert">
                                    <span style="color: red;font-style: italic;">{{ $errors->first('email_alt') }}</span>
                                </span>
	                        @endif
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="password">Password: <span class="required">*</span></label>
                            <input type="password" id="password" name="password">
	                        @if ($errors->has('password'))
		                        <span class="invalid-feedback" role="alert">
                                    <span style="color: red;font-style: italic;">{{ $errors->first('password') }}</span>
                                </span>
	                        @endif
                        </td>
                        <td><label for="email_alt">Confirm Password: <span class="required">*</span></label>
                            <input type="password" id="password_confirmation" name="password_confirmation">
                        </td>
                    </tr>
                </table>
            </fieldset>
            
            
            <fieldset>
                <legend><span class="number">2</span>Contact Information</legend>
                
                <table cellspacing="0">
                    <tr valign="top">
                        <td><label for="contact">Emergency Contact:</label>
                            <input type="text" id="contact" name="contact" value="{{ old('contact') }}">
                        </td>
                        <td><label for="contact_office">Office Telephone Number:</label>
                            <input type="text" id="contact_office" name="contact_office" value="{{ old('contact_office') }}">
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="permanent_district">Permanent District:</label>
                            <select id="permanent_district" name="permanent_district"><option value="1">Achham</option>
                                <option value="2">Arghakhanchi</option>
                                <option value="3">Baglung</option>
                                <option value="4">Baitadi</option>
                                <option value="5">Bajhang</option>
                                <option value="6">Bajura</option>
                                <option value="7">Banke</option>
                                <option value="8">Bara</option>
                                <option value="9">Bardiya</option>
                                <option value="10">Bhaktapur</option>
                                <option value="11">Bhojpur</option>
                                <option value="12">Chitwan</option>
                                <option value="13">Dadeldhura</option>
                                <option value="14">Dailekh</option>
                                <option value="15">Dang</option>
                                <option value="16">Darchula</option>
                                <option value="17">Dhading</option>
                                <option value="18">Dhankuta</option>
                                <option value="19">Dhanusa</option>
                                <option value="20">Dolakha</option>
                                <option value="21">Dolpa</option>
                                <option value="22">Doti</option>
                                <option value="23">Gorkha</option>
                                <option value="24">Gulmi</option>
                                <option value="25">Humla</option>
                                <option value="26">Ilam</option>
                                <option value="27">Jajarkot</option>
                                <option value="28">Jhapa</option>
                                <option value="29">Jumla</option>
                                <option value="30">Kailali</option>
                                <option value="31">Kalikot</option>
                                <option value="32">Kanchanpur</option>
                                <option value="33">Kapilbastu</option>
                                <option value="34">Kaski</option>
                                <option value="35">Kathmandu</option>
                                <option value="36">Kavrepalanchok</option>
                                <option value="37">Khotang</option>
                                <option value="38">Lalitpur</option>
                                <option value="39">Lamjung</option>
                                <option value="40">Mahottari</option>
                                <option value="41">Makwanpur</option>
                                <option value="42">Manang</option>
                                <option value="43">Morang</option>
                                <option value="44">Mugu</option>
                                <option value="45">Mustang</option>
                                <option value="46">Myagdi</option>
                                <option value="47">Nawalparasi</option>
                                <option value="48">Nuwakot</option>
                                <option value="49">Okhaldhunga</option>
                                <option value="50">Palpa</option>
                                <option value="51">Panchthar</option>
                                <option value="52">Parbat</option>
                                <option value="53">Parsa</option>
                                <option value="54">Pyuthan</option>
                                <option value="55">Ramechhap</option>
                                <option value="56">Rasuwa</option>
                                <option value="57">Rautahat</option>
                                <option value="58">Rolpa</option>
                                <option value="59">Rukum</option>
                                <option value="60">Rupandehi</option>
                                <option value="61">Salyan</option>
                                <option value="62">Sankhuwasabha</option>
                                <option value="63">Saptari</option>
                                <option value="64">Sarlahi</option>
                                <option value="65">Sindhuli</option>
                                <option value="66">Sindhupalchok</option>
                                <option value="67">Siraha</option>
                                <option value="68">Solukhumbu</option>
                                <option value="69">Sunsari</option>
                                <option value="70">Surkhet</option>
                                <option value="71">Syangja</option>
                                <option value="72">Tanahu</option>
                                <option value="73">Taplejung</option>
                                <option value="74">Terhathum</option>
                                <option value="75">Udayapur</option>
                            </select></td>
                        <td rowspan="2"><label for="permanent_address">Permanent Address:</label>
                            <textarea id="permanent_address" name="permanent_address">{{ old('permanent_address') }}</textarea>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="permanent_telephone">Permanent Telephone:</label>
                            <input type="text" id="permanent_telephone" name="permanent_telephone" value="{{ old('permanent_telephone') }}">
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="current_district">Current District:</label>
                            <select id="current_district" name="current_district"><option value="1">Achham</option>
                                <option value="2">Arghakhanchi</option>
                                <option value="3">Baglung</option>
                                <option value="4">Baitadi</option>
                                <option value="5">Bajhang</option>
                                <option value="6">Bajura</option>
                                <option value="7">Banke</option>
                                <option value="8">Bara</option>
                                <option value="9">Bardiya</option>
                                <option value="10">Bhaktapur</option>
                                <option value="11">Bhojpur</option>
                                <option value="12">Chitwan</option>
                                <option value="13">Dadeldhura</option>
                                <option value="14">Dailekh</option>
                                <option value="15">Dang</option>
                                <option value="16">Darchula</option>
                                <option value="17">Dhading</option>
                                <option value="18">Dhankuta</option>
                                <option value="19">Dhanusa</option>
                                <option value="20">Dolakha</option>
                                <option value="21">Dolpa</option>
                                <option value="22">Doti</option>
                                <option value="23">Gorkha</option>
                                <option value="24">Gulmi</option>
                                <option value="25">Humla</option>
                                <option value="26">Ilam</option>
                                <option value="27">Jajarkot</option>
                                <option value="28">Jhapa</option>
                                <option value="29">Jumla</option>
                                <option value="30">Kailali</option>
                                <option value="31">Kalikot</option>
                                <option value="32">Kanchanpur</option>
                                <option value="33">Kapilbastu</option>
                                <option value="34">Kaski</option>
                                <option value="35">Kathmandu</option>
                                <option value="36">Kavrepalanchok</option>
                                <option value="37">Khotang</option>
                                <option value="38">Lalitpur</option>
                                <option value="39">Lamjung</option>
                                <option value="40">Mahottari</option>
                                <option value="41">Makwanpur</option>
                                <option value="42">Manang</option>
                                <option value="43">Morang</option>
                                <option value="44">Mugu</option>
                                <option value="45">Mustang</option>
                                <option value="46">Myagdi</option>
                                <option value="47">Nawalparasi</option>
                                <option value="48">Nuwakot</option>
                                <option value="49">Okhaldhunga</option>
                                <option value="50">Palpa</option>
                                <option value="51">Panchthar</option>
                                <option value="52">Parbat</option>
                                <option value="53">Parsa</option>
                                <option value="54">Pyuthan</option>
                                <option value="55">Ramechhap</option>
                                <option value="56">Rasuwa</option>
                                <option value="57">Rautahat</option>
                                <option value="58">Rolpa</option>
                                <option value="59">Rukum</option>
                                <option value="60">Rupandehi</option>
                                <option value="61">Salyan</option>
                                <option value="62">Sankhuwasabha</option>
                                <option value="63">Saptari</option>
                                <option value="64">Sarlahi</option>
                                <option value="65">Sindhuli</option>
                                <option value="66">Sindhupalchok</option>
                                <option value="67">Siraha</option>
                                <option value="68">Solukhumbu</option>
                                <option value="69">Sunsari</option>
                                <option value="70">Surkhet</option>
                                <option value="71">Syangja</option>
                                <option value="72">Tanahu</option>
                                <option value="73">Taplejung</option>
                                <option value="74">Terhathum</option>
                                <option value="75">Udayapur</option>
                            </select></td>
                        <td rowspan="2"><label for="current_address">Current Address:</label>
                            <textarea id="current_address" name="current_address">{{ old('current_address') }}</textarea>
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="current_telephone">Current Telephone:</label>
                            <input type="text" id="current_telephone" name="current_telephone" value="{{ old('current_telephone') }}">
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="mobile">Mobile:</label>
                            <input type="text" id="mobile" name="mobile" value="{{ old('mobile') }}">
                        </td>
                    </tr>
                </table>
            
            </fieldset>
            
            <fieldset>
                <legend><span class="number">3</span>Experience</legend>
                
                <table cellspacing="0"><tr valign="top">
                    
                    <tr>
                        <td width="50%"><label for="current_organization">Current Organization:</label>
                            <input type="text" id="current_organization" name="current_organization" value="{{ old('current_organization') }}">
                        </td>
                        <td><label for="current_designation">Current Designation:</label>
                            <input type="text" id="current_designation" name="current_designation" value="{{ old('current_designation') }}">
                        </td>
                    </tr>
                    <tr>
                        <td><label for="experience">Relevant Work Experience (Total Year) :</label>
                            <input type="text" id="experience" name="experience" value="{{ old('experience') }}">
                        </td>
                        <td><label for="current_salary">Current Salary:</label>
                            <input type="text" id="current_salary" name="current_salary" value="{{ old('current_salary') }}">
                        </td>
                    </tr>
                    <tr>
                        <td><label for="expected_salary">Expected Salary:</label>
                            <input type="text" id="expected_salary" name="expected_salary" value="{{ old('expected_salary') }}">
                        </td>
                        <td><label for="religion">Religion:</label>
                            <input type="text" id="religion" name="religion" value="{{ old('religion') }}">
                        </td>
                    </tr>
                    <tr>
                        <td><label for="ethnicity">Ethnicity:</label>
                            <input type="text" id="ethnicity" name="ethnicity"></td>
                        <td rowspan="2"><label for="user_bio">Any relatives working in this Organization? (If yes, write: Name, Designation, Location):</label>
                            <textarea id="user_bio" name="user_bio">{{ old('user_bio') }}</textarea>
                        </td>
                    </tr>
                    <tr>
                        <td><label for="notice_period">Notice Period Days :</label>
                            <input type="text" id="notice_period" name="notice_period" value="{{ old('notice_period') }}">
                        </td>
                    </tr>
                </table>
            
            </fieldset>
            
            <fieldset>
                <legend><span class="number">4</span>Prefer Working Areas</legend>
                
                <table cellspacing="0"><tr valign="top">
                    <tr>
                        <td width="50%"><label for="prefer_working_areas">Prefer Working Areas :</label>
                            <select name="prefer_working_areas" id="prefer_working_areas">
                                <option value="">--- Select Location ---</option>
                                <option value="Inside Valley" id="6">Inside Valley</option>
                                <option value="Outside Valley" id="7">Outside Valley</option>
                                <option value="Corporate Office" id="8">Corporate Office</option>
                            </select>
                        </td>
                        <td><label for="name">Job Category:</label>
                            <select name="jobcategory" id="jobcategory">
                                <option value="">--- Select Category ---</option>
                                <option value="Banking" id="6">Banking</option>
                                <option value="Financial" id="7">Financial</option>
                                <option value="Information Technology" id="7">Information Technology</option>
                            </select></td>
                    </tr>
                    <tr>
                        <td><label for="name">Job Types:</label>
                            <select name="jobtype" id="jobtype">
                                <option value="">--- Select Job Types ---</option>
                                <option value="Full Time" id="6">Full Time</option>
                                <option value="Part Time" id="7">Part Time</option>
                                <option value="Consultancy" id="7">Consultancy</option>
                            </select></td>
                    </tr>
                </table>
            
            </fieldset>
            
            <fieldset>
                <legend><span class="number">5</span>Profile Photo</legend>
                
                <table cellspacing="0"><tr valign="top">
                    <tr>
                        <td>Upload Your Photo:<br /><input type="file" name="avatar" id="avatar">
                        </td>
                    </tr>
                </table>
            
            </fieldset>
            
            
            <fieldset>
                <legend><span class="number">6</span>Family Details</legend>
                
                <table cellspacing="0"><tr valign="top">
                    <tr>
                        <td><label for="name">Member Name:</label>
                            <input type="text" id="name" name="member_name"></td>
                        <td><label for="name">Relation:</label>
                            <input type="text" id="name" name="member_relation"></td>
                        <td><label for="name">Date Of Birth:</label>
                            <input type="text" id="name" name="member_dob"></td>
                    </tr>
                    <tr>
                        <td><label for="name">Phone:</label>
                            <input type="text" id="name" name="member_phone"></td>
                        <td><label for="name">Occupation:</label>
                            <input type="text" id="name" name="member_occupation"></td>
                        <td><label for="name">Remarks:</label>
                            <input type="text" id="name" name="member_remarks"></td>
                    </tr>
                </table>
            
            </fieldset>
            
            <fieldset>
                <legend><span class="number">7</span>Academic Qualifications</legend>
                
                <table cellspacing="0"><tr valign="top">
                    <tr>
                        <td><label for="name">Qualification Level:</label>
                            <select class="skill-level" data-val="true" data-val-number="The field QualificationLevelID must be a number." id="CandidateEducationDetail_0__QualificationLevelID" name="education_level"><option value="">-- Select qualification level --</option>
                                <option value="0">-</option>
                                <option value="Bachelor">Bachelor</option>
                                <option value="SLC">SLC</option>
                                <option value="CA">CA</option>
                                <option value="Diploma">Diploma</option>
                                <option value="CA inter">CA inter</option>
                                <option value="School Level">School Level</option>
                                <option value="Intermediate">Intermediate</option>
                                <option value="ACCA">ACCA</option>
                                <option value="Masters">Masters</option>
                                <option value="EMBA">EMBA</option>
                            </select></td>
                        <td><label for="name">Degree Name:</label>
                            <input type="text" id="name" name="degree"></td>
                        <td><label for="name">Major Subjects:</label>
                            <input type="text" id="name" name="major"></td>
                    </tr>
                    <tr>
                        <td><label for="name">Institution:</label>
                            <input type="text" id="name" name="institution"></td>
                        <td><label for="name">Start Year (AD):</label>
                            <input type="text" id="name" name="start_year"></td>
                        <td><label for="name">Passed Year (AD):</label>
                            <input type="text" id="name" name="passed_year"></td>
                    </tr>
                    <tr>
                        <td>
                            <input type="checkbox" id="design" value="1" name="is_running"><label class="light" for="design">Running</label></td>
                    </tr>
                </table>
            
            </fieldset>
            
            
            <fieldset>
                <legend><span class="number">8</span>CV</legend>
                
                <table cellspacing="0"><tr valign="top">
                    <tr>
                        <td>Upload Your CV<br /><input type="file" name="cv" id="cv">
                        </td>
                    </tr>
                </table>
            
            </fieldset>
            
            
            <fieldset><button type="submit">Create Profile</button></fieldset>
        
        </form>
    
    </div>
@endsection
@section('front-scripts')
    <script src="{{ asset(STATIC_DIR.'plugins/bower_components/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
    <script>
        jQuery('.datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true,
            format: 'yyyy-mm-dd'
        });
    </script>
@endsection
