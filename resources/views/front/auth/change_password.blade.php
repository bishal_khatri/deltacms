@extends('front.layouts.main')
@section('front-css')
    <link href="{{ asset(STATIC_DIR.'frontend/css/form.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page-title')
    Change Password
@endsection
@section('content')
    <div id="content-inner">
        <h1>Change Password</h1>
        <div align="center">
            @if($errors->count()>0)
                @foreach($errors as $value)
                    <p style="color:red;">{{ $value }}</p>
                @endforeach
            @endif
            <form action="{{ route('change_password') }}" method="post">
                @csrf
                <table style="width: 40%;" cellspacing="0"><tr valign="top">
                    <tr valign="top">
                        <td width="50%"><label for="name">Current Password:</label>
                            <input type="password" id="name" name="current_password">
                            @if($errors->has('current_password'))
                                <small style="color: red;">{{ $errors->first('current_password') }}</small>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td><label for="name">New Password:</label>
                            <input type="password" id="name" name="password">
                            @if($errors->has('password'))
                                <small style="color: red;">{{ $errors->first('password') }}</small>
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td><label for="name">Confirm New Password:</label>
                            <input type="password" id="name" name="password_confirmation">
                        </td>
                    </tr>
                    <tr>
                        <td><button type="submit">Change Password</button></td>
                    </tr>
                </table>
            </form>
        </div>
    </div>
@endsection
