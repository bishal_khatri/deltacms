@extends('front.layouts.main')
@section('front-css')
    <link href="{{ asset(STATIC_DIR.'frontend/css/form.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page-title')
    Login
@endsection
@section('content')
    <div id="content-inner">
        <h1>User Login</h1>
        <div align="center">
            <form action="{{ route('user_login') }}" method="post">
                @csrf
                <table style="width: 40%;" cellspacing="0"><tr valign="top">
                    <tr valign="top">
                        @if ($errors->has('message'))
                            <span class="invalid-feedback" role="alert">
                                <span style="color: red;font-style: italic;">{{ $errors->first('message') }}</span>
                            </span>
                        @endif
                        <td width="50%">
                            <label for="name">Your Email:</label>
                            <input type="text" id="name" name="email">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <label for="name">Password:</label>
                            <input type="password" id="name" name="password">
                        </td>
                    </tr>
                    <tr>
                        <td><button type="submit">Login</button></td>
                    </tr>
                </table>
            </form><br />
            <a href="#">Forgot Password</a> | <a href="{{ route('user_register') }}">Create Profile</a>
        </div>
    </div>

@endsection
