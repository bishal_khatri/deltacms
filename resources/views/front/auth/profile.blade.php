@extends('front.layouts.main')
@section('front-css')
    <link href="{{ asset(STATIC_DIR.'frontend/css/form.css') }}" rel="stylesheet" type="text/css" />
@endsection
@section('page-title')
    Profile
@endsection
@section('content')
    <div id="content-inner">
        @include('front.layouts._partials.alert')
        <h1>User Profile</h1>
        @if(Auth::guard('front_web')->check() AND Auth::guard('front_web')->user()->id==$user->id)
        <div style="float: right;"><a href="#">Edit Profile</a></div>
        @endif
        <form action="index.html" method="post">

            <fieldset>
                <legend><span class="number">1</span>General Information</legend>

                <table cellspacing="0" width="100%">
                    <tr valign="top">
                        <td width="50%"><label for="name">First Name:</label>
                            {{ $user->first_name ?? '' }}</td>
                        <td><label for="name">Middle Name:</label>
                            {{ $user->middle_name ?? '' }}
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Last Name:</label>
                            {{ $user->last_name ?? '' }}</td>
                        <td><label for="name">Date of Birth:</label>
                            {{ $user->dob ?? '' }}</td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Nationality:</label>
                            {{ $user->country ?? '' }}
                        </td>
                        <td><label for="job">Gender:</label>
                            {{ $user->gender ?? '' }}
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="job">Marital Status:</label>
                            {{ $user->marital_status ?? '' }}
                        </td>
                        <td><label for="job">Blood Group:</label>
                            {{ $user->blood_group ?? '' }}
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Citizenship Number:</label>
                            {{ $user->citizenship_number ?? '' }}</td>
                        <td><label for="name">Citizenship Issued Disrict:</label>
                            {{ $user->citizenship_district ?? '' }}
                        </td>
                    </tr>
                    <tr valign="top">
                        <td><label for="mail">Email Address:</label>
                            {{ $user->email ?? '' }}</td>
                        <td><label for="mail">Alternative Email Address:</label>
                            {{ $user->email_alt ?? '' }}</td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Emergency Contact:</label>
                            {{ $user->contact ?? '' }}</td>
                        <td><label for="name">Office Telephone Number:</label>
                            {{ $user->contact_office ?? '' }}</td>
                    </tr>
                </table>
            </fieldset>


            <fieldset>
                <legend><span class="number">2</span>Contact Information</legend>

                <table cellspacing="0">
                    <tr valign="top">
                        <td width="50%"><label for="name">Emergency Contact:</label>
                            {{ $user->contact_office ?? '' }}</td>
                        <td><label for="name">Office Telephone Number:</label>
                            {{ $user->contact_office ?? '' }}</td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Permanent District:</label>
                            {{ $user->permanent_district ?? '' }}</td>
                        <td rowspan="2"><label for="name">Permanent Address:</label>
                            {{ $user->permanent_address ?? '' }}</td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Permanent Telephone:</label>
                            {{ $user->permanent_telephone ?? '' }}</td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Current District:</label>
                            {{ $user->current_district ?? '' }}</td>
                        <td rowspan="2"><label for="name">Current Address:</label>
                            {{ $user->current_address ?? '' }}</td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Current Telephone:</label>
                            {{ $user->current_telephone ?? '' }}</td>
                    </tr>
                    <tr valign="top">
                        <td><label for="name">Mobile:</label>
                            {{ $user->mobile ?? '' }}</td>
                    </tr>
                </table>

            </fieldset>


            <fieldset>
                <legend><span class="number">3</span>Experience</legend>

                <table cellspacing="0"><tr valign="top">

                    <tr>
                        <td width="50%"><label for="name">Current Organization:</label>
                            {{ $user->current_organization ?? '' }}</td>
                        <td><label for="name">Current Designation:</label>
                            {{ $user->current_designation ?? '' }}</td>
                    </tr>
                    <tr>
                        <td><label for="name">Relevant Work Experience (Total Year) :</label>
                            {{ $user->experience ?? '' }}</td>
                        <td><label for="name">Current Salary:</label>
                            {{ $user->current_salary ?? '' }}</td>
                    </tr>
                    <tr>
                        <td><label for="name">Expected Salary:</label>
                            {{ $user->expected_salary ?? '' }}</td>
                        <td><label for="name">Religion:</label>
                            {{ $user->religion ?? '' }}</td>
                    </tr>
                    <tr>
                        <td><label for="name">Ethnicity:</label>
                            {{ $user->ethnicity ?? '' }}</td>
                        <td rowspan="2"><label for="name">Any relatives working in this Organization? (If yes, write: Name, Designation, Location):</label>
                            {{ $user->user_bio ?? '' }}</td>
                    </tr>
                    <tr>
                        <td><label for="name">Notice Period Days :</label>
                            {{ $user->notice_period ?? '' }}</td>
                    </tr>
                </table>

            </fieldset>


            <fieldset>
                <legend><span class="number">4</span>Prefer Working Areas</legend>

                <table cellspacing="0"><tr valign="top">
                    <tr>
                        <td width="50%"><label for="name">Prefer Working Areas :</label>
                            {{ $user->prefer_working_areas ?? '' }}
                        </td>
                        <td><label for="name">Job Category:</label>
                            {{ $user->jobcategory ?? '' }}</td>
                    </tr>
                    <tr>
                        <td><label for="name">Job Types:</label>
                            {{ $user->jobtype ?? '' }}</td>
                    </tr>
                </table>

            </fieldset>


            <fieldset>
                <legend><span class="number">5</span>Profile Photo</legend>

                <img src="{{ asset(STATIC_DIR.'storage/'.$user->avatar) }}" width="100" alt="" style="width: auto; float: left;" />

            </fieldset>


            <fieldset>
                <legend><span class="number">6</span>Family Details</legend>

                <table cellspacing="0"><tr valign="top">
                    <tr>
                        <td><label for="name">Member Name:</label>
                            {{ $user->member_name ?? '' }}</td>
                        <td><label for="name">Relation:</label>
                            {{ $user->member_relation ?? '' }}</td>
                        <td><label for="name">Date Of Birth:</label>
                            {{ $user->member_dob ?? '' }}</td>
                    </tr>
                    <tr>
                        <td><label for="name">Phone:</label>
                            {{ $user->member_phone ?? '' }}</td>
                        <td><label for="name">Occupation:</label>
                            {{ $user->member_phone ?? '' }}</td>
                        <td><label for="name">Remarks:</label>
                            {{ $user->member_remarks ?? '' }}</td>
                    </tr>
                </table>

            </fieldset>


            <fieldset>
                <legend><span class="number">7</span>Academic Qualifications</legend>

                <table cellspacing="0"><tr valign="top">
                    <tr>
                        <td><label for="name">Qualification Level:</label>
                            {{ $user->education_level ?? '' }}</td>
                        <td><label for="name">Degree Name:</label>
                            {{ $user->degree ?? '' }}</td>
                        <td><label for="name">Major Subjects:</label>
                            {{ $user->major ?? '' }}</td>
                    </tr>
                    <tr>
                        <td><label for="name">Institution:</label>
                            {{ $user->institution ?? '' }}</td>
                        <td><label for="name">Start Year (AD):</label>
                            {{ $user->start_year ?? '' }}</td>
                        <td><label for="name">Passed Year (AD):</label>
                            {{ $user->passed_year ?? '' }}</td>
                    </tr>
                    <tr>
                        <td>
                            <!-- <input type="checkbox" id="design" value="interest_design" name="user_interest"><label class="light" for="design">Running</label> --></td>
                    </tr>
                </table>

            </fieldset>


            <fieldset>
                <legend><span class="number">8</span>CV</legend>
                @if(!empty($user->cv))
                <a href="{{ asset(STATIC_DIR.'storage/'.$user->cv) }}" target="_blank">Download CV</a>
                @endif
            </fieldset>




        </form>

    </div>

@endsection
