@extends('layouts.app')
@section('css')
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet">

@endsection
@section('page_title')
	Listing Gallery
@endsection
@section('right_button')
    <a href="#add" class="btn btn-info btn-outline pull-right" data-toggle="modal" ><i class="fa fa-plus"></i>&nbsp;Create new gallery</a>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="white-box">

			{{-- <div class="row">
				<div class="col-md-12">
					<a href="" class="btn btn-link pull-right">
						<i class="fa fa-trash text-danger">&nbsp;Trash (<span id="getTrashSize"></span>) </i>
					</a>
				</div>
			</div> --}}
			<div class="row">
				<div class="col-lg-12 col-md-12 col-sm-12">
					@if($gallery->count()>0)
					@foreach($gallery as $value)
                    <div class="steamline">
                        <div class="sl-item">
                            <a href="{{ route('profile.view',$value->user->id) }}" class="text-primary">
                                <div class="sl-left bg-info">
                                    <i class="fa fa-image"></i>
                                </div>
                            </a>
                            <div class="sl-right">
                                    <h4>
	                                    <a href="{{ route('gallery.view',$value->id) }}" class="text-primary">{{ $value->gallery_name }} [ {{ count($value->images) }} images ]</a>
	                                    <sup><a href="#deleteGallery" data-toggle="modal" data-ids="{{ $value->id }}" class="text-sm text-danger">Delete</a></sup>
                                    </h4>
                                <div class="desc">
                                	<div>
	                                	<a href="{{ route('profile.view',$value->user->id) }}">
		                                	@if(!empty($value->user->first_name) AND !empty($value->user->last_name))
		                                		<span class="text-primary">{{ $value->user->first_name }} {{ $value->user->last_name }}</span>
		                                	@else
		                                		{{ $value->user->email }}
		                                	@endif
	                                	</a> 
	                                	<span class="sl-date">{{ $value->created_at->diffForHumans() }}</span>
                                	</div>
                                </div>
                                <div class="row inline-photos">
                                	@if ($value->image->count()>0)
		                                @foreach($value->image as $val)
			                                <div class="col-xs-3">
				                                <img class="img-responsive" alt="user" src="{{ asset(STATIC_DIR.'storage/'.$val->image_name) }}">
			                                </div>
		                                @endforeach
									@else
										<p>No Images found</p>
                                	@endif
                                    <div class="col-xs-3" style="margin-top: 30px;">
                                    	<div class="row">
	                                    	<div class="col-md-12">
	                                    		<a href="#addImage" class="btn btn-link btn-sm text-primary" data-toggle="modal" data-ids="{{ $value->id }}">Add image</a>
	                                    	</div>
                                    	</div>
                                    	<div class="row">
                                  			<div class="col-md-12">
                                  				<a href="{{ route('gallery.view',$value->id) }}" class="btn btn-link btn-sm text-primary">View all</a>
                                  			</div>
                                    	</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
					@else
						<p>No items found <a href="#add" class="btn btn-link btn-sm" data-toggle="modal" >&nbsp;Create new</a></p>
                    @endif
	            </div>
			</div>

		</div>
	</div>
</div>
	
@include('gallery.modal')
@endsection

@section('scripts')
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
	<script>
		 // ADD IMAGE
        $('#addImage').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('ids');
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#gallery_id").val(id);
        });

        $(document).ready(function (e) {
		    $('#imageUploadForm').on('submit',(function(e) {
		        e.preventDefault();
		        var formData = new FormData(this);
		        $.ajax({
		            type:'POST',
		            headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content') },
		            url: $(this).attr('action'),
		            data:formData,
		            cache:false,
		            contentType: false,
		            processData: false,
		            success:function(data){
		                window.location.reload();
		               //  $('#gallery').fadeOut( function(){
			              //   $('#gallery').fadeIn();
			              // });
		            },
		            error: function(data){
		                console.log("error");
		                console.log(data);
		            }
		        });
		    }));
		});

		 // AJAX GALLERY DELETE
        $('#deleteGallery').on('show.bs.modal', function (e) {
            //e.preventDefault();
            var button = $(e.relatedTarget);
            // console.log(button.data('ids'));
            var ids = button.data('ids');
            // Pass form reference to modal for submission on yes/ok
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#hidden_id").val(ids);
        });

        $('#deleteGallery').find('.modal-footer #confirm_yes').on('click', function () {
            //$(this).data('form').submit();
            var id = $("#hidden_id").val();
            // console.log(id);
            $.ajax({
                type: "POST",
                url: "{{ route('gallery.delete_gallery') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    // console.log(msg);
                    $("#confirmDelete").modal("hide");
                    window.location.reload();
                }
            });
        });
		
	</script>
@endsection
