{{--DELETE GALLERY MODAL--}}
<div class="modal fade bs-modal-sm" id="deleteGallery" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Delete Permanently
				</h4>
			</div>
			<div class="modal-body"> Do you want to delete this gallery?</div>
			<input type="hidden" id="hidden_id">
			<div class="modal-footer">
				<button type="button" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
				</button>
				<button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
					No
				</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
</div>

{{--ADD FORM--}}
<div class="modal fade" id="add" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<form action="{{ route('gallery.add') }}" method="post" enctype="multipart/form-data">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel1"><i class="fa fa-plus-circle" id="icon-terminate" ></i> Create new gallery</h4> </div>
			<div class="modal-body">
				@csrf
				<div class="form-group @if ($errors->has('gallery_name')) has-error @endif">
					<label for="gallery_name" class="control-label">Title:</label>
					<input type="text" class="form-control" name="gallery_name" placeholder="Enter gallery name here" value="{{ old('gallery_name') }}">
					@if ($errors->has('gallery_name'))
						<span class="text-danger" role="alert">
							<p>{{ $errors->first('gallery_name') }}</p>
						</span>
					@endif
				</div>
				<div class="form-group">
					<label for="message-text" class="control-label">Description:</label>
					<textarea class="form-control" rows="5" id="message-text1" name="description" placeholder="Enter description here">{{ old('description') }}</textarea>
					@if ($errors->has('description'))
						<span class="text-danger" role="alert">
							<p>{{ $errors->first('description') }}</p>
						</span>
					@endif
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-outline btn-sm" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success btn-outline btn-sm" id="confirm_yes"><i class="icon-check"></i> Save</button>
			</div>
		</div>
		</form>
	</div>
</div>

{{--ADD IMAGE FORM--}}
<div class="modal fade" id="addImage" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<form action="{{ route('gallery.image.add') }}" method="post" enctype="multipart/form-data">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel1"><i class="fa fa-plus-circle" id="icon-terminate" ></i> Add Image</h4> </div>
			<div class="modal-body">
				@csrf
				<input type="hidden" name="gallery_id" id="gallery_id">
				<div class="form-group @if ($errors->has('image')) has-error @endif">
					<label for="image" class="control-label"> Image:</label>
					<input type="file" class="form-control-file" name="image[]" multiple accept=".jpg,.png,.bmp,.jpeg">
					@if ($errors->has('image'))
						<span class="text-danger" role="alert">
							<p>{{ $errors->first('image') }}</p>
						</span>
					@endif
					<p style="font-style: italic;color: darkorange;">Appropriate file type: .jpg .jpeg .png</p>
				</div>				
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-outline btn-sm" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success btn-outline btn-sm" id="confirm_yes">Upload</button>
			</div>
		</div>
		</form>
	</div>
</div>

{{--EDIT GALLERY FORM--}}
<div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel1">
	<div class="modal-dialog" role="document">
		<form action="{{ route('gallery.edit') }}" method="post" enctype="multipart/form-data">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="exampleModalLabel1"><i class="fa fa-plus-circle" id="icon-terminate" ></i> Edit Gallery</h4> </div>
			<div class="modal-body">
				@csrf
				<input type="hidden" name="gallery_id" id="id">
				<div class="form-group @if ($errors->has('title')) has-error @endif">
					<label for="title" class="control-label">Title:</label>
					<input type="text" id="gallery_name" class="form-control" name="gallery_name" placeholder="Enter title here" value="{{ old('title') }}">
					@if ($errors->has('title'))
						<span class="text-danger" role="alert">
							<p>{{ $errors->first('title') }}</p>
						</span>
					@endif
				</div>
				<div class="form-group">
					<label for="message-text" class="control-label">Description:</label>
					<textarea class="form-control" id="description" rows="5" id="message-text1" name="description" placeholder="Enter description here">{{ old('description') }}</textarea>
					@if ($errors->has('description'))
						<span class="text-danger" role="alert">
							<p>{{ $errors->first('description') }}</p>
						</span>
					@endif
				</div>	
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default btn-outline btn-sm" data-dismiss="modal">Close</button>
				<button type="submit" class="btn btn-success btn-outline btn-sm" id="confirm_yes">Update</button>
			</div>
		</div>
		</form>
	</div>
</div>



{{--DELETE IMAGE MODAL--}}
<div class="modal fade bs-modal-sm" id="confirmDeleteImage" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i>&nbsp;Delete
				</h4>
			</div>
			<div class="modal-body">Delete this Image? </div>
			<input type="hidden" id="hidden_image_id">
			<div class="modal-footer">
				<button type="button" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
				</button>
				<button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
					No
				</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
</div>