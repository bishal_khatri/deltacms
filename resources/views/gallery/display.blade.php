@extends('layouts.app')
@section('css')
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/Magnific-Popup-master/dist/magnific-popup.css') }}" rel="stylesheet">
	<link href="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/css/dropify.min.css') }}" rel="stylesheet">

@endsection
@section('page_title')
	Listing Gallery Images
@endsection
@section('right_button')
<a href="{{ route('gallery.index') }}" class="btn btn-info btn-outline pull-right"><i class="fa fa-undo"></i> Back</a>
@stop

@section('content')
<div class="row">
	<div class="col-md-12">
		<div class="white-box">
			{{-- <div class="row">
				<div class="col-md-12">
					<a href="" class="btn btn-link pull-right">
						<i class="fa fa-trash text-danger">&nbsp;Trash (<span id="getTrashSize"></span>) </i>
					</a>
				</div>
			</div> --}}
			<div class="row">
                    <div class="col-md-12">
                        {{-- <div class="row">
                            <div class="col-md-12">
                                <button class="btn btn-warning" id="col-3"><i class="fa fa-th"></i></button>
                                <button class="btn btn-warning" id="col-2"><i class="fa fa-th-large"></i></button>
                                <button class="btn btn-warning" id="col-4"><i class="fa fa-square"></i></button>
                            </div>
                        </div> --}}
                        <div class="row el-element-overlay m-b-40" id="gallery">
                            {{-- {{ dd($gallery->id) }} --}}
                            @if(!empty($gallery))
                            <div class="row">
                                <div class="col-lg-12 col-sm-12 col-xs-12 m-t-40">
                                    <h3 class="box-title">
                                        {{ $gallery->gallery_name }} 
                                        <sup>
                                            <a href="#edit" class="text-sm text-danger" data-toggle="modal" data-id="{{ $gallery->id }}" data-gallery_name="{{ $gallery->gallery_name }}" data-description="{{ $gallery->description }}">edit</a>
                                        </sup>&nbsp;&nbsp;
                                        <sup><a href="#addImage" data-toggle="modal" data-ids="{{ $gallery->id }}" class="text-sm text-primary">add image</a></sup>
                                    </h3>
                                    <blockquote>
                                        <p> {{ $gallery->description }} </p>
                                        <small>
                                            <a href="{{ route('profile.view',$gallery->user->id) }}">
                                                @if(!empty($gallery->user->first_name) AND !empty($gallery->user->last_name))
                                                {{ $gallery->user->first_name }} {{ $gallery->user->last_name }}
                                                @else
                                                {{ $gallery->user->email }}
                                                @endif
                                            </a> 
                                            <cite title="Source Title">{{ $gallery->created_at->diffForHumans() }}</cite>
                                        </small> 
                                    </blockquote>
                                </div>
                            </div>
                                @if ($gallery->images->count()>0)
			                        @foreach($gallery->images as $value)
				                        <div class="remove col-lg-3 col-md-4 col-sm-6 col-xs-12">
					                        <div class="white-box">
						                        <div class="el-card-item">
							                        <div class="el-card-avatar el-overlay-1"> <img src="{{ asset(STATIC_DIR.'storage/'.$value->image_name) }}" />
								                        <div class="el-overlay">
									                        <ul class="el-info">
										                        <li>
											                        <a class="btn default btn-outline image-popup-vertical-fit" href="{{ asset(STATIC_DIR.'storage/'.$value->image_name) }}">
												                        <i class="icon-magnifier"></i>
											                        </a>
										                        </li>
										                        <li>
											                        <a class="btn default btn-outline text-danger" href="#confirmDeleteImage" data-id="{{ $value->id }}" data-toggle="modal">
												                        <i class="icon-trash"></i>
											                        </a>
										                        </li>
									                        </ul>
								                        </div>
							                        </div>
						                        </div>
					                        </div>
				                        </div>
			                        @endforeach
								@else
									<p>No images found</p>
                                @endif
                            @endif
                        </div>
                    </div>
                </div>

		</div>
	</div>
</div>
	
@include('gallery.modal')
@endsection

@section('scripts')
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup.min.js') }}"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/Magnific-Popup-master/dist/jquery.magnific-popup-init.js') }}"></script>
	<script src="{{ asset(STATIC_DIR.'plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
	<script>
		 // ADD IMAGE
        $('#addImage').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('ids');
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#gallery_id").val(id);
        });

         // EDIT GALLERY
        $('#edit').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var id = button.data('id');
            var gallery_name = button.data('gallery_name');
            var description = button.data('description');
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#id").val(id);
            $("#gallery_name").val(gallery_name);
            $("#description").val(description);
        });
        
        // DELETE IMAGE
         $('#confirmDeleteImage').on('show.bs.modal', function (e) {
             //e.preventDefault();
             var button = $(e.relatedTarget);
             var id = button.data('id');
             // Pass form reference to modal for submission on yes/ok
             var form = $(e.relatedTarget).closest('form');
             $(this).find('.modal-footer #confirm').data('form', form);
             $("#hidden_image_id").val(id);
         });

         $('#confirmDeleteImage').find('.modal-footer #confirm_yes').on('click', function () {
             //$(this).data('form').submit();
             var id = $("#hidden_image_id").val();
             // console.log(id);
             $.ajax({
                 type: "POST",
                 url: "{{ route('gallery.image.delete') }}",
                 headers: {
                     'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                 },
                 data: "id=" + id,
                 success: function (msg) {
                     // console.log(msg);
                     $("#confirmDelete").modal("hide");
                     window.location.reload();
                 }
             });
         });
	</script>
@endsection
