<div class="navbar-default sidebar" role="navigation">
	<div class="sidebar-nav slimscrollsidebar">
		<div class="sidebar-head">
			<h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
		<ul class="nav" id="side-menu">
			<li class="user-pro">
				<a href="#" class="waves-effect"><img src="{{ asset(STATIC_DIR.DEFAULT_USER) }}" alt="user-img" class="img-circle">
					<span class="hide-menu"> {{ Auth::user()->first_name ?? ''  }} {{ Auth::user()->last_name ?? ''  }}</span>
				</a>

			</li>
			<li> <a href="{{ route('home') }}" class="waves-effect @if(Request::is('admin')) active @endif">
					<i class="mdi mdi-av-timer fa-fw"></i>
					<span class="hide-menu">Dashboard</span>
				</a>
			</li>
			<li> <a href="{{ route('permission.index') }}" class="@if(Request::is(ROOT_DIR.'permission/*')) active @endif">
					<i class="mdi mdi-lock fa-fw" data-icon="v"></i>
					<span class="hide-menu"> Permissions</span>
				</a>
			</li>
			<li> <a href="{{ route('setting.index') }}" class="@if(Request::is(ROOT_DIR.'setting/*')) active @endif">
					<i class="mdi mdi-settings fa-fw" data-icon="v"></i>
					<span class="hide-menu"> Settings</span>
				</a>
			</li>
			<li class="devider"></li>
			<li class="text-center" style="color: white; font-weight: bold;">MAIN NAVIGATION</li>
			<li class="devider"></li>
			@canany(['user-add','user-list'])
				<li>
					<a href="#" class="waves-effect @if(Request::is(ROOT_DIR.'user/*')) active @endif">
						<i class="mdi mdi-account-multiple fa-fw"></i>
						<span class="hide-menu">Users<span class="fa arrow open"></span></span>
					</a>
					<ul class="nav nav-second-level @if(Request::is(ROOT_DIR.'user/*')) collapse in @endif">
						<li>
							<a class="@if(Request::is(ROOT_DIR.'user/register')) active @endif" href="{{ route('user.register') }}">
								<i class="mdi mdi-account-multiple-plus fa-fw"></i> <span class="hide-menu">Add</span>
							</a>
						</li>
						<li>
							<a class="@if(Request::is(ROOT_DIR.'user/list')) active @endif" href="{{ route('user.index') }}">
								<i class="mdi mdi-format-list-bulleted fa-fw"></i> <span class="hide-menu">List</span>
							</a>
						</li>
					</ul>
				</li>
			@endcanany
			@can('slider-list')
				<li> <a href="{{ route('slider.index') }}" class="@if(Request::is(ROOT_DIR.'slider/*')) active @endif">
						<i class="mdi mdi-airplay fa-fw" data-icon="v"></i>
						<span class="hide-menu"> Slider</span>
					</a>
				</li>
			@endcan
            @can('popup-list')
                <li> <a href="{{ route('popup.index') }}" class="@if(Request::is(ROOT_DIR.'popup/*')) active @endif">
                        <i class="mdi mdi-airplay fa-fw" data-icon="v"></i>
                        <span class="hide-menu"> Popup</span>
                    </a>
                </li>
            @endcan
			@can('post-list')
				<li>
					<a href="{{ route('weblink.index') }}" class="@if(Request::is(ROOT_DIR.'weblink/*')) active @endif">
						<i class="mdi mdi-link fa-fw" data-icon="v"></i>
						<span class="hide-menu"> Web Links</span>
					</a>
				</li>
			@endcan
			@can('pages-list')
				<li>
					<a href="#" class="waves-effect @if(Request::is(ROOT_DIR.'pages/*')) active @endif">
						<i class="mdi mdi-image-filter-none fa-fw"></i>
						<span class="hide-menu">Pages<span class="fa arrow open"></span></span>
					</a>
					<ul class="nav nav-second-level @if(Request::is(ROOT_DIR.'pages/*')) collapse in @endif">
						<li>
							<a class="@if(Request::is(ROOT_DIR.'pages/add')) active @endif" href="{{ route('pages.add') }}">
								<i class="mdi mdi-plus fa-fw"></i> <span class="hide-menu">Create new page</span>
							</a>
						</li>
						<li>
							<a class="@if(Request::is(ROOT_DIR.'pages/list')) active @endif" href="{{ route('pages.index') }}">
								<i class="mdi mdi-format-list-bulleted fa-fw"></i> <span class="hide-menu">All pages</span>
							</a>
						</li>
					</ul>
			@endcan
			@can('menu-list')
				<li> <a href="{{ route('menu.index') }}" class="@if(Request::is(ROOT_DIR.'menu/*')) active @endif">
						<i class="mdi mdi-apps fa-fw" data-icon="v"></i>
						<span class="hide-menu"> Menu</span>
					</a>
				</li>
			@endcan
			@can('menu-list')
				<li>
					<a data-src="{{ URL(STATIC_DIR.'laravel-filemanager?type=Images') }}" href="javascript:;" data-fancybox data-type="iframe" class="@if(Request::is(ROOT_DIR.'media/*')) active @endif">
						<i class="mdi mdi-camera-iris fa-fw" data-icon="v"></i>
						<span class="hide-menu"> Media</span>
					</a>
				</li>
			@endcan
			@can('menu-list')
				<li> <a href="{{ route('gallery.index') }}" class="@if(Request::is(ROOT_DIR.'gallery/*')) active @endif">
						<i class="mdi mdi-folder-multiple-image fa-fw" data-icon="v"></i>
						<span class="hide-menu"> Gallery</span>
					</a>
				</li>
			@endcan
			@can('menu-list')
				<li> <a href="{{ route('category.index') }}" class="@if(Request::is(ROOT_DIR.'category/*')) active @endif">
						<i class="mdi mdi-group fa-fw" data-icon="v"></i>
						<span class="hide-menu"> Category</span>
					</a>
				</li>
			@endcan
			@can('menu-list')
				<li> <a href="{{ route('downloads.index') }}" class="@if(Request::is(ROOT_DIR.'downloads/*')) active @endif">
						<i class="mdi mdi-download fa-fw" data-icon="v"></i>
						<span class="hide-menu"> Downloads</span>
					</a>
				</li>
			@endcan
			@can('jobs-list')
				<li>
					<a href="#" class="waves-effect @if(Request::is(ROOT_DIR.'jobs/*')) active @endif">
						<i class="mdi mdi-briefcase fa-fw"></i>
						<span class="hide-menu">Job Portal<span class="fa arrow open"></span></span>
					</a>
					<ul class="nav nav-second-level @if(Request::is(ROOT_DIR.'jobs/*')) collapse in @endif">
						<li>
							<a class="@if(Request::is(ROOT_DIR.'jobs/add')) active @endif" href="{{ route('jobs.add') }}">
								<i class="mdi mdi-plus fa-fw"></i> <span class="hide-menu">Create new job</span>
							</a>
						</li>
						<li>
							<a class="@if(Request::is(ROOT_DIR.'jobs/list')) active @endif" href="{{ route('jobs.index') }}">
								<i class="mdi mdi-format-list-bulleted fa-fw"></i> <span class="hide-menu">All jobs</span>
							</a>
						</li>
						<li>
							<a class="@if(Request::is(ROOT_DIR.'jobs/user/*')) active @endif" href="{{ route('jobs.user.index') }}">
								<i class="mdi mdi-account-multiple fa-fw"></i> <span class="hide-menu">Registered Users</span>
							</a>
						</li>
					</ul>
				</li>
			@endcan
            @can('form-list')
                <li> <a href="{{ route('form.index') }}" class="@if(Request::is(ROOT_DIR.'form/*')) active @endif">
                        <i class="mdi mdi-file fa-fw" data-icon="v"></i>
                        <span class="hide-menu"> Request Form</span>
                    </a>
                </li>
            @endcan
			<li style="padding-bottom: 100px;"></li>
		</ul>
	</div>
</div>
