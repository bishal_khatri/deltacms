<!-- /#wrapper -->
<!-- jQuery -->
<script src="{{ asset(STATIC_DIR.'plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset(STATIC_DIR.'assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>

<script src="{{ asset(STATIC_DIR.'plugins/bower_components/datatables/jquery.dataTables.min.js') }}"></script>
<!-- Sidebar menu plugin JavaScript -->
<script src="{{ asset(STATIC_DIR.'plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
<!--Slimscroll JavaScript For custom scroll-->
<script src="{{ asset(STATIC_DIR.'assets/js/jquery.slimscroll.js') }}"></script>
<!--Wave Effects -->
<script src="{{ asset(STATIC_DIR.'assets/js/waves.js') }}"></script>
<script src="{{ asset(STATIC_DIR.'fancybox/dist/jquery.fancybox.min.js') }}"></script>
<script src="{{ asset(STATIC_DIR.'plugins/bower_components/toast-master/js/jquery.toast.js') }}"></script>
<!-- Custom Theme JavaScript -->
@yield('scripts')
<script src="{{ asset(STATIC_DIR.'assets/js/custom.js') }}"></script>
<script src="{{ asset(STATIC_DIR.'plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
<script>
    $('[data-fancybox]').fancybox({toolbar:false,smallBtn:true,iframe:{preload:false}});
    @if(Session::has('flash_message'))
    $.toast({heading:'{!!Session::get('flash_message')['heading']!!}',text:'{!! Session::get('flash_message')['message']!!}',position:'top-right',loaderBg: '#ff6849',icon:'{!! Session::get('flash_message')['type'] !!}',hideAfter:3000,stack:6});
    @endif
</script>