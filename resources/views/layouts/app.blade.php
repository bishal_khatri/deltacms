<!DOCTYPE html>
<html lang="en">
@include('layouts._partials.head')

<body class="fix-header">

<div class="preloader">
	<svg class="circular" viewBox="25 25 50 50">
		<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
	</svg>
</div>

<div id="wrapper">

	@include('layouts._partials.nav')

	@include('layouts._partials.sidebar')

	<div id="page-wrapper">
		<div class="container-fluid">
			<div class="row bg-title">
				<div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
					<h4 class="page-title">@yield('page_title')</h4>
				</div>
				<div class="col-lg-8 col-sm-8 col-md-8 col-xs-12 pull-right">
					<div class="col-md-6 pull-right">
                        @yield('right_button')
                    </div>
				</div>
			</div>
			{{-- <div class="preloader">
				<svg class="circular" viewBox="25 25 50 50">
					<circle class="path" cx="50" cy="50" r="20" fill="none" stroke-width="2" stroke-miterlimit="10" />
				</svg>
			</div> --}}
			@yield('content')
		</div>
		<footer class="footer text-center"> <?=date('Y') ?> &copy; <b>Delta</b>CMS brought to you by <a href="http://www.deltacreation.com.np/" target="_blank">Delta Creation</a> </footer>
	</div>

</div>
@include('layouts._partials.scripts')
</body>

</html>
