@extends('layouts.app')
@section('css')
@endsection
@section('page_title')
	List permissions
@endsection
@section('right_button')
    <a href="#add" class="btn btn-info btn-outline pull-right" data-toggle="modal" ><i class="fa fa-plus"></i>&nbsp;Add</a>
@stop
@section('content-title')
	<h2>
		List permissions
	</h2>

@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="white-box">
                <div class="table-responsive">
                    <table id="datatable" class="table table-borderless table-striped" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Display Name</th>
                            <th>Code Name</th>
                            <th style="width: 90px;text-align: center;">Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(isset($permissions))
                            @foreach($permissions as $val)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $val->display_name }}</td>
                                    <td>{{ $val->code_name }}</td>
                                    <td style="text-align: center">
                                        <a href=""><i class="fa fa-pencil"></i></a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a href="#confirmDelete" data-ids="{{ $val->id }}" data-toggle="modal" data-rel="delete" data-user="{{ $val->display_name }}">
                                            <i class="fa fa-trash text-danger"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
            </div>
        </div>
    </div>

@include('permission.model')
@endsection

@section('scripts')
	<script>
        $('#datatable').DataTable({
            dom: 'Bfrtip',
            columnDefs: [ { "targets": [3], "searchable": false, "orderable": false, "visible": true } ],
        });
        $('#add').find('.modal-footer #confirm_yes').on('click', function () {
            $("#confirmDelete").modal("hide");
            window.location.reload();
        });

        // AJAX PERMISSION USER
        $('#confirmDelete').on('show.bs.modal', function (e) {
            var button = $(e.relatedTarget);
            var ids = button.data('ids');
            var user = button.data('user');
            var form = $(e.relatedTarget).closest('form');
            $(this).find('.modal-footer #confirm').data('form', form);
            $("#hidden_id").val(ids);
            $(".hidden_title").html(' "' + user + '" ');
        });

        $('#confirmDelete').find('.modal-footer #confirm_yes').on('click', function () {
            var id = $("#hidden_id").val();
            $.ajax({
                type: "POST",
                url: "{{ route('permission.delete') }}",
                headers: {
                    'X-CSRF-Token': $('meta[name="csrf-token"]').attr('content')
                },
                data: "id=" + id,
                success: function (msg) {
                    $("#confirmDelete").modal("hide");
                    window.location.reload();
                }
            });
        });
        // AJAX DELETE PERMISSION END
	</script>
@endsection
