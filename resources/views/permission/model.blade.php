<div class="modal fade bs-modal-sm" id="add" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-md">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><i class="fa fa-plus-circle" id="icon-terminate" ></i> Add permission
				</h4>
			</div>
			<form action="{{ route('permission.add') }}" method="post">
				@csrf
				<div class="modal-body">
					<div class="form-group row">
						<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Display Name') }}</label>
						<div class="col-md-6">
							<input id="name" type="text" class="form-control" name="display_name" value="{{ old('display_name') }}"  placeholder="Permission display name" required autofocus>

							@if ($errors->has('display_name'))
								<span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('display_name') }}</strong>
                                    </span>
							@endif
							<p style="font-style: italic;color: darkorange;">Example: Can add user</p>
						</div>
					</div>
					<div class="form-group row">
						<label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Code Name') }}</label>
						<div class="col-md-6">
							<input id="name" type="text" class="form-control" name="code_name" value="{{ old('code_name') }}" placeholder="Permission code name" required autofocus>

							@if ($errors->has('code_name'))
								<span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('code_name') }}</strong>
                                    </span>
							@endif
							<p style="font-style: italic;color: darkorange;">Example: user-add</p>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-success btn-outline btn-sm confirm_yes" id="confirm_yes"><i class="icon-check"></i> Save
					</button>
					<button type="button" class="pull-left btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
						close
					</button>
				</div>
			</form>
		</div>
		<!-- /.modal-content -->
	</div>
</div>


{{--DELETE MODAL--}}
<div class="modal fade bs-modal-sm" id="confirmDelete" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
				<h4 class="modal-title"><i class="fa fa-info-circle" id="icon-terminate" ></i> Delete Permanently
				</h4>
			</div>
			<div class="modal-body"> Do you want to delete <span class='hidden_title'>" "</span>?</div>
			<input type="hidden" id="hidden_id">
			<div class="modal-footer">
				<button type="button" class="btn btn-danger green confirm_yes" id="confirm_yes"><i class="icon-check"></i> Yes
				</button>
				<button type="button" class="btn btn-defult" data-dismiss="modal"><i class="icon-close" id="icon-terminate"></i>
					No
				</button>
			</div>
		</div>
		<!-- /.modal-content -->
	</div>
</div>
