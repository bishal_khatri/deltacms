<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DownloadItem extends Model
{
    protected $table = 'downloads_items';
    protected $primaryKey = 'download_items_id';

    public function downloadCategory()
    {
        return $this->belongsTo('App\DownloadItem','category_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','created_by');
    }
}
