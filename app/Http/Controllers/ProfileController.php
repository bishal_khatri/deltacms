<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    
    public function view($id)
    {
    	$data['user'] = User::findOrFail($id);
    	return view('auth.profile',$data);
    }

    public function update(Request $request, $type)
    {
        if ($type=='profile'){
//        dd($request->all());
            $this->validate($request,[
               'user_id'=>'required',
            ]);

            $user = User::findOrFail($request->user_id);
            if ($request->hasFile('image')) {
                $path = $request->file('image')->store('profile','public');
                $user->image = $path;
            }

            !empty($request->first_name) ? $user->first_name = $request->first_name : '';
            !empty($request->last_name) ? $user->last_name = $request->last_name : '';
            !empty($request->conatct) ? $user->conatct = $request->conatct : '';
            !empty($request->address) ? $user->address = $request->address : '';
            !empty($request->email) ? $user->email = $request->email : '';
            $user->save();

        }
        elseif($type=='password'){
            $this->validate($request,[
                'user_id'=>'required',
                'old_password'=>'required',
                'password'=>'required|confirmed|min:6'
            ]);

            $user = User::findOrFail($request->user_id);
            if(Hash::check($request->old_password, $user->password)){
                $user->password = Hash::make($request->password);
                $user->save();
            }
            else{
                $flashMessage = [
                    'heading'=>'error',
                    'type'=>'error',
                    'message'=>'Old password did not match.'
                ];
                \Session::flash('flash_message', $flashMessage);
                return redirect()->back();
            }
        }
        else{
            abort(404);
        }
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Profile updated successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();
    }
}
