<?php

namespace App\Http\Controllers;

use App\Post;
use App\Seo;
use App\PostMeta;
use App\Category;
use App\Gallery;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;

class PostController extends Controller
{
    /**
     * Post Controller Start
     */

    public function get_post_index()
    {   
        $data['posts'] = Post::where('post_type','post')->where('is_trashed',0)->get();
        return view('post.index', $data);
    }

    public function get_post_create(Request $request)
    {
        return view('post.create');
    }

//    public function post_store(Request $request)
//    {
//        dd($request->all());
//        $this->validate($request,[
//            'post_title'=>'required|max:255',
//
//        ]);
//        $post_slug = $request->post_slug;
//        if (empty($post_slug)) {
//            $post_slug = $request->post_title;
//            $post_slug = strtolower($post_slug);
//            $post_slug = str_replace(' ','-',$post_slug);
//        }
//        if (!empty($request->post_url)) {
//            $post_url = $request->post_url;
//        }else{
//            $post_url = url($post_slug);
//        }
//        if (isset($request->publish)){
//            $post_status = $request->publish;
//        }else{
//            $post_status = $request->draft;
//        }
//
//        if (!empty($request->cover_image)) {
//
//            $cover_image = $request->cover_image;
//            $info = pathinfo(public_path().$cover_image);
//            $pages_storage = storage_path('app/public/pages/');
//            $image_name = str_random(45).'.'.$info['extension'];
//            $new_file = $pages_storage.$image_name;
//            // save to database name
//            $cover_image_name_full = 'pages/'.$image_name;
//            \File::copy(public_path().$cover_image,$new_file);
//        }
//        else{
//            $cover_image_name_full = '';
//        }
//
//        $post = new Post();
//        $post->post_title = $request->post_title;
//        $post->post_subtitle = $request->post_subtitle;
//        $post->post_content = $request->post_content;
//        $post->post_slug = $post_slug;
//        $post->post_type = $request->post_type;
//        $post->post_status = $post_status;
//        $post->post_url = $post_url;
//        $post->unique_key = str_random(100);
//        $post->created_by = Auth::user()->id;
//        $post->tags = $request->tags;
//        $post->cover_image = $cover_image_name_full;
//        $post->save();
//
//        //store seo
//        $seo = new Seo();
//        $seo->page_id = $post->id;
//        $seo->seo_title = $request->seo_title;
//        $seo->seo_description = $request->seo_description;
//        $seo->seo_keywords = $request->seo_keywords;
//        $seo->save();
//
//        $flashMessage = [
//            'heading'=>'success',
//            'type'=>'success',
//            'message'=>'Page created successfully.'
//        ];
//        \Session::flash('flash_message', $flashMessage);
//        return redirect()->route('post.index');
//    }

    /**
     * Pages Controller Start
     */
    public function index()
    {
        $data['trash_count'] = Post::where('post_type','page')->where('is_trashed',1)->count();
        $action = Input::get('trashed');    
        if (isset($action) && $action=='true') {
            $data['pages'] = Post::where('is_trashed',1)->get();
        }
        else{
            $data['pages'] = Post::where('post_type','page')->where('is_trashed',0)->get();
        }
        return view('pages.index', $data);
    }

    public function create(Request $request)
    {
        $data['categories'] = Category::all();
        $data['galleries'] = Gallery::all();
        return view('pages.create',$data);
    }

    public function store(Request $request)
    {
//        dd($request->all());
        $this->validate($request,[
            'post_title'=>'required|max:255',
            'post_url' => 'required|unique:posts'
        ]);
        $post_slug = $request->post_slug;
        if (empty($post_slug)) {
            $replaceable = array(' ', '/', '?');
            $post_slug = $request->post_title;
            $post_slug = strtolower($post_slug);
            $post_slug = str_replace($replaceable,'-',$post_slug);
        }
        if (!empty($request->post_url)) {
            $post_url = $request->post_url;
        }else{
            $post_url = url($post_slug);
        }
        if (isset($request->publish)){
            $post_status = $request->publish;
        }else{
            $post_status = $request->draft;
        }

        if (!empty($request->cover_image)) {

            $cover_image = $request->cover_image;
            $info = pathinfo(public_path().$cover_image);
            $pages_storage = storage_path('app/public/pages/');
            if (!File::exists($pages_storage)){
                File::makeDirectory($pages_storage, 0777, true, true);
            }
            $image_name = str_random(45).'.'.$info['extension'];
            $new_file = $pages_storage.$image_name;
            // save to database name
            $cover_image_name_full = 'pages/'.$image_name;
            \File::copy(public_path().$cover_image,$new_file);
        }
        else{
            $cover_image_name_full = '';
        }

        $post = new Post();
        $post->post_title = $request->post_title;
        $post->post_subtitle = $request->post_subtitle;
        $post->post_content = $request->post_content;
        $post->post_slug = $post_slug;
        $post->post_type = $request->post_type;
        $post->post_status = $post_status;
        $post->post_url = $post_url;
        $post->unique_key = str_random(100);
        $post->created_by = Auth::user()->id;
        $post->tags = $request->tags;
        $post->cover_image = $cover_image_name_full;
        $post->facebook_share = $request->facebook_share;
        $post->twitter_share = $request->twitter_share;
        $post->save();

        //store seo
        $seo = new Seo();
        $seo->page_id = $post->id;
        $seo->seo_title = $request->seo_title;
        $seo->seo_description = $request->seo_description;
        $seo->seo_keywords = $request->seo_keywords;
        $seo->save();

        // store category
        if (!empty($request->category_id)) {
            foreach ($request->category_id as $key => $value) {
                $meta = new PostMeta();
                $meta->post_id = $post->id;
                $meta->meta_id = $value;
                $meta->slug = 'category';
                $meta->save();
            }
        }

        // store gallery
        if (!empty($request->gallery_id)) {
            foreach ($request->gallery_id as $key => $value) {
                $meta = new PostMeta();
                $meta->post_id = $post->id;
                $meta->meta_id = $value;
                $meta->slug = 'gallery';
                $meta->save();
            }
        }

        if (!empty($request->cta_title) OR !empty($request->cta_link)){
            $cta = new PostMeta();
            if ($request->hasFile('cta_image')) {
                $cta_image= $request->file('cta_image')->store('post_meta','public');
                $cta->meta_file = $cta_image;
            }
            $cta->post_id = $post->id;
            $cta->meta_title = $request->cta_title;
            $cta->meta_description = $request->cta_description;
            $cta->meta_button_title = $request->cta_button_text;
            $cta->meta_button_link = $request->cta_button_link;
            $cta->meta_button_target = $request->cta_button_target;
            $cta->slug = 'call-to-action';
            $cta->save();
        }

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Page created successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->route('pages.index');
    }

    public function edit($id)
    {
        $data['post'] = Post::where('id',$id)->with('seo')->first();
        $data['call_to_action'] = PostMeta::where('post_id',$id)->where('slug','call-to-action')->first();
//        dd($data);
        if (empty($data['post'])){
            abort(404);
        }
        return view('pages.edit', $data);
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'post_title'=>'required'
        ]);
        
        if (isset($request->publish)){
            $post_status = $request->publish;
        }else{
            $post_status = $request->draft;
        }
        // post status
        if (isset($request->publish)){
            $post_status = $request->publish;
        }else{
            $post_status = $request->draft;
        }

        $post = Post::find($request->post_id);
        $post->post_title = $request->post_title;
        $post->post_content = $request->post_content;
        $post->post_status = $post_status;
        $post->post_status = $post_status;
        $post->facebook_share = $request->facebook_share;
        $post->twitter_share = $request->twitter_share;
        $post->updated_by = Auth::user()->id;

        $post->save();

        //store seo
        $seo = Seo::firstOrNew(['page_id'=>$request->post_id]);
        $seo->seo_title = $request->seo_title;
        $seo->seo_keywords = $request->seo_keywords;
        $seo->seo_description = $request->seo_description;
        $seo->save();

        $cta = PostMeta::firstOrCreate(['post_id'=>$request->post_id,'slug'=>'call-to-action']);
        if ($request->hasFile('cta_image')) {
            $cta_image= $request->file('cta_image')->store('post_meta','public');
            $cta->meta_file = $cta_image;
        }
//        $cta->post_id = $post->id;
        $cta->meta_title = $request->cta_title;
        $cta->meta_description = $request->cta_description;
        $cta->meta_button_title = $request->cta_button_text;
        $cta->meta_button_link = $request->cta_button_link;
        $cta->meta_button_target = $request->cta_button_target;
//        $cta->slug = 'call-to-action';
        $cta->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Page updated successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->route('pages.index');   
    }

    public function create_url(Request $request)
    {
        $post_slug = $request->title;
        $post_slug = strtolower($post_slug);
        $replaceable = array(' ', '/', '?');
        $post_slug = str_replace($replaceable,'-',$post_slug);
        $i=1;
        while (Post::where('post_slug',$post_slug)->count() !=0 ) {
            $post_slug = $post_slug .'-'.$i++;
        }
        $data['url'] = url($post_slug);

        $data['slug'] = $post_slug;
        
        return $data;
    }

    public function recycle(Request $request)
    {
        $post = Post::find($request->id);
        if ($request->action=='trash'){
            $post->is_trashed = 1;
            $post->post_status = 'trash';
            $post->save();
        }
        elseif($request->action=='restore'){
            $post->is_trashed=0;
            $post->post_status='draft';
            $post->save();
        }
        elseif ($request->action=='delete'){
            $post->delete();
        }
        return 'success';
    }

    public function make_featured(Request $request)
    {
        $post = Post::find($request->id);
        if (is_null($post->is_featured)){
            $status = 1;
        }elseif($post->is_featured==1){
            $status = 0;
        }elseif($post->is_featured==0){
            $status = 1;
        }else{
            $status = 0;
        }

        $post->is_featured = $status;
        $post->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Page updated successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

        return 'success';
    }


    //popup controller
    public function popup_index()
    {
        $data['popups'] = Post::where('post_type','popup')->get();
        return view('popup.index',$data);
    }

    public function popup_store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required',
            'image'=>'required'
        ]);
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('popup','public');
        }
        else{
            $path = '';
        }

        $post_slug = strtolower($request->title);
        $replaceable = array(' ', '/', '?');
        $post_slug = str_replace($replaceable,'-',$post_slug);

        $post = new Post();
        $post->post_title = $request->title;
        $post->post_type = 'popup';
        $post->post_slug = $post_slug;
        $post->cover_image = $path;
        $post->post_status = 'published';
        $post->unique_key = str_random(100);
        $post->created_by = Auth::user()->id;
        $post->post_url = '';
        $post->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Popup uploaded successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

        return redirect()->back();
    }

    public function popup_update(Request $request)
    {
//        dd($request->all());
        $post = Post::findOrFail($request->id);
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('popup','public');
            $post->cover_image = $path;
        }

        $post->post_title = $request->title;
        $post->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Popup updated successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

        return redirect()->back();
    }

    public function popup_delete(Request $request)
    {
        $post = Post::find($request->id);
        \Storage::delete('public/'.$post->cover_image);
        $post->delete();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Popup image deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return 's';
    }
}
