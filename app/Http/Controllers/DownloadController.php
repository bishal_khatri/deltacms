<?php

namespace App\Http\Controllers;

use App\DownloadItem;
use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\DownloadCategory;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

class DownloadController extends Controller
{
    public function index()
    {
    	$data['downloads'] = DownloadCategory::with('downloadItem')->get();
//    	dd($data);
    	return view('downloads.index',$data);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title'=>'required'
        ]);
        $slug = $request->title;
        $slug = strtolower($slug);
        $slug = 'downloads-item-'.str_replace(' ','-',$slug);
        $i=1;
        while (DownloadCategory::where('slug',$slug)->count() !=0 ) {
            $slug = $slug .'-'.$i++;
        }
        $downloads = new DownloadCategory();
        $downloads->title = $request->title;
        $downloads->description = $request->description;
        $downloads->slug = $slug;
        $downloads->has_group = $request->has_group;
        $downloads->created_by = Auth::user()->id;
        $downloads->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Category created successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();
    }

    public function edit(Request $request)
    {
        $download = DownloadCategory::find($request->id);
        $download->title = $request->title;
        $download->description = $request->description;
        $download->save();
        return redirect()->back();
    }

    public function view_item($id)
    {
        $data['category'] = DownloadCategory::findOrFail($id);
        if ($data['category']->has_group==1){
            $data['items'] = DownloadItem::where('category_id',$id)->get()->groupBy('group_slug');
            $data['has_group'] = 1;
        }else{
            $data['items'] = DownloadItem::where('category_id',$id)->get();
            $data['has_group'] = 0;
        }
//        dd($data['items']);
        return view('downloads.list_item',$data);
    }

    public function store_item(Request $request)
    {
        $this->validate($request,[
            'title' => 'required',
            'file' => 'required'
        ]);
        $path = $request->file('file')->store('file_downloads','public');
//        dd($path);
        $size = $request->file('file')->getClientSize();
        $size = Helper::bytesToHuman($size);
        $downloads = new DownloadItem();
        $downloads->category_id = $request->category_id;
        $downloads->title = $request->title;
        $downloads->body = $request->body;
        $downloads->file = $path;
        $downloads->size = $size;
        $downloads->url = url($path);
        $downloads->unique_key = str_random(75);
        $downloads->created_by = Auth::user()->id;
        $downloads->is_trashed = 0;
        if (isset($request->group_slug)){
            $downloads->group_slug = $request->group_slug;
        }
        $downloads->is_trashed = 0;
        $downloads->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'File uploaded successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();
    }

    public function download($id)
    {
        $item = DownloadItem::find($id);
        $path = public_path().'/storage/'.$item->file;
        $file = str_replace('file_downloads/','',$item->file);
        $headers = array(
            'Content-Type: application/pdf',
        );
        if (file_exists($path)) {
            return \Response::download($path,$file,$headers);
        }else{
            $flashMessage = [
            'heading'=>'Error',
            'type'=>'error',
            'message'=>'File has been removed or deleted.'
        ];
        \Session::flash('flash_message', $flashMessage);
            return redirect()->back();
        }
    }

    public function delete_item(Request $request)
    {
        $item = DownloadItem::find($request->id);
        \Storage::delete('public/'.$item->file);
        $item->delete();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'File deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return 's';
    }

    public function delete(Request $request)
    {
        $download = DownloadCategory::find($request->id);
        if (!empty($download)) {
            $item = DownloadItem::where('category_id',$request->id)->get();
            foreach ($item as $value) {
               \Storage::delete('public/'.$value->file);
               $value->delete();
            }
        }
        $download->delete();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'File deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return 's';
    }
}
