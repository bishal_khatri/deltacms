<?php

namespace App\Http\Controllers;

use App\Post;
use App\Weblink;
use App\WeblinkPost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Auth;

class WeblinkController extends Controller
{
    public function index()
    {
        $data['posts'] = Post::where('post_type','page')->get();
        $data['weblinks'] = Weblink::with('user','weblink_post')->get();
        // dd($data);
        return view('weblink.index', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'name' => 'required'
        ]);

        if ($request->hasFile('weblink_icon')) {
            $path = $request->file('weblink_icon')->store('weblink','public');
        }
        else{
            $path = '';
        }

        $slug = $request->name;
        $slug = strtolower($slug);
        $slug = str_replace(' ','-',$slug);

        $weblink = new Weblink();
        $weblink->name = $request->name;
        $weblink->created_by = Auth::user()->id;
        $weblink->slug = $slug;
        $weblink->weblink_icon = $path;
        $weblink->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Weblink created successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

        return redirect()->back();
    }

    public function display($weblink_id)
    {
        $data['weblink'] = Weblink::find($weblink_id);
        $data['weblinkPost'] = WeblinkPost::where('weblink_id',$weblink_id)->with('post','weblink')->get();
        $data['weblink_id'] = $weblink_id;
        $data['posts'] = Post::where('post_type','page')->get();
        return view('weblink.display', $data);
    }

    public function addPost(Request $request)
    {
//        dd($request->all());
        if (isset($request->type) AND $request->type=='custom_link'){
            $post_slug = $request->link_name;
            $post_slug = strtolower($post_slug);
            $post_slug = str_replace(' ','-',$post_slug);

            $post = new Post();
            $post->post_title = $request->link_name;
            $post->post_url = $request->link;
            $post->post_slug = $post_slug;
            $post->post_status = 'publish';
            $post->post_type = 'custom_link';
            $post->url_target = $request->target;
            $post->post_parent = 0;
            $post->unique_key = str_random(100);
            $post->created_by = Auth::user()->id;
            $post->save();

            $item = new WeblinkPost();
            $item->weblink_id = $request->weblink_id;
            $item->post_id = $post->id;
            $item->created_by = Auth::user()->id;
            $item->save();
        }
        else {
            foreach ($request->page_id as $post_id) {
                $item = new WeblinkPost();
                $item->weblink_id = $request->weblink_id;
                $item->post_id = $post_id;
                $item->created_by = Auth::user()->id;
                $item->save();
            }
        }
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Post added successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

        return redirect()->back();
    }

    public function updateItem(Request $request)
    {
        $wp = WeblinkPost::find($request->weblink_post_id);
        if ($request->hasFile('icon')) {
            $path = $request->file('icon')->store('weblink','public');
            $wp->icon = $path;
        }

        $wp->display_name = $request->display_name;
        $wp->description = $request->description;
        $wp->save();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Item updated successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

        return redirect()->back();
    }

    public function removeItem($id)
    {
        $post = WeblinkPost::find($id);
        $post->delete();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Post removed successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $weblink = Weblink::find($request->id);
        $weblinkPost = WeblinkPost::where('weblink_id',$weblink->weblink_id)->get();
        foreach ($weblinkPost as $value){
            \Storage::delete('public/'.$value->icon);
            $value->delete();
        }
        \Storage::delete('public/'.$value->weblink_icon);
        $weblink->delete();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Weblink and items deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return 's';
    }
}
