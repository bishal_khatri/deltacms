<?php

namespace App\Http\Controllers\frontend;

use App\Category;
use App\Mail\AccountNotification;
use Mail;
use App\Seo;
use App\Weblink;
use App\WeblinkPost;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Post;
use App\Menu;
use App\MenuPost;
use App\Slider;
use App\DownloadCategory;
use App\DownloadItem;
use App\Setting;
use App\Helpers\Helper;

class HomeController extends Controller
{
    public function index($slug=null)
    {
        $data['sliders'] = Slider::where('is_active',1)->get();

    	if (is_null($slug)) {
//            $data['posts'] = Post::where('post_type','post')->where('is_trashed',0)->get();
            $data['page'] = Post::where('post_slug','introduction')->with('seo')->firstOrFail();
            $data['features'] = WeblinkPost::join('weblinks as w','weblink_post.weblink_id','=','w.weblink_id')
                ->join('posts as p','weblink_post.post_id','=','p.id')
                ->where('w.slug','features')
                ->get();
            $data['features_mega1'] = Weblink::with('weblink_post','weblink_post.post')->where('slug','deposit')->first();
            $data['features_mega2'] = Weblink::with('weblink_post','weblink_post.post')->where('slug','loan-&-advances')->first();
            $data['remitance_partner'] = Weblink::with('weblink_post','weblink_post.post')->where('slug','remitance-partner')->first();
            $data['news'] = Category::with('posts')->where('category_slug','news-and-notices')->first();
    		return view('front.index',$data);
    	}
    	$data['page'] = Post::where('post_slug',$slug)->with('seo')->firstOrFail();

    	if ($data['page']->post_type=='downloads'){
    	    return self::downloads($data['page']->post_slug);
        }
        if (empty($data['page'])) {
            abort(404);
        }
    	return view('front.page.index',$data);
    }

    public function downloads($slug='')
    {
        $downloads = DownloadCategory::where('slug',$slug)->firstOrFail();
        $data['downloads'] = $downloads;
        if ($downloads->count()<0) {
            abort(404);
        }
        else{
            if ($downloads->has_group==1){
                $data['downloadsItem'] = DownloadItem::where('category_id',$downloads->downloads_category_id)->get()->groupBy('group_slug');
                $data['has_group'] = 1;
            }
            else{
                $data['downloadsItem'] = DownloadItem::where('category_id',$downloads->downloads_category_id)->get();
                $data['has_group'] = 0;
            }
        }
        return view('front.page.downloads.downloads',$data);
    }

    public function category($slug='')
    {
        $data['category'] = Category::where('category_slug',$slug)->with('posts')->firstOrFail();
        if ($data['category']->count()<0) {
            abort(404);
        }
        return view('front.page.category',$data);
    }

    public function mailTest()
    {
        $message = Setting::where('slug','ac_email')->firstOrFail();
        $a = Mail::to('bishal.game343@gmail.com')->send(new AccountNotification($message->text));
        dd($a);

    }
}
