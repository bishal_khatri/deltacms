<?php

namespace App\Http\Controllers\frontend;

use App\Job;
use App\JobApplication;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class CareerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:front_web')->except('index');
    }

    public function index()
    {
        if (Auth::guard('front_web')->check()){
            $data['applied_job'] = JobApplication::where('site_user_id',Auth::guard('front_web')->user()->id)->pluck('job_id')->toArray();
        }else{
            $data['applied_job'] = [];
        }
        $today = date('Y-m-d');
        $data['jobs'] = Job::where('is_published',1)->whereDate('deadline', '>=', $today)->get();
        return view('front.page.career.index',$data);
    }

    public function apply($job_id)
    {
        $job = new JobApplication();
        $job->site_user_id = Auth::user()->id;
        $job->job_id = $job_id;
        $job->save();
        return redirect()->back();
    }
}
