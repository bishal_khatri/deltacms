<?php

namespace App\Http\Controllers\Frontend\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;

class LoginController extends Controller
{
    public function __construct()
    {
        $this->middleware('guest:front_web');
    }

    public function ShowLoginForm()
    {
        return view('front.auth.login');
    }

    public function login(Request $request)
    {
        $this->validate($request,[
           'email' => 'required',
           'password' => 'required'
        ]);
        if (Auth::guard('front_web')->attempt(['email' => $request->email, 'password' => $request->password], $request->remember)){
            $message = [
                'type'=>'success',
                'message'=>'Logged In Successfully'
            ];
            \Session::flash('message', $message);
            return redirect()->route('user_profile');
        }
        return redirect()->back()->withInput($request->only('email','remember'))->withErrors(['message'=>'Login details not found. Please check your email or password.']);
    }
}
