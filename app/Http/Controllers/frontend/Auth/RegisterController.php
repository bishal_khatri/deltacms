<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\SiteUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Auth;

class RegisterController extends Controller
{
    public function ShowRegisterForm()
    {
        return view('front.auth.register');
    }

    public function register(Request $request)
    {
        $this->validate($request,[
            'first_name' => 'required|max:255',
            'last_name' => 'required|max:255',
            'dob' => 'required',
            'gender' => 'required',
            'email' => 'required|email|unique:site_users',
            'email_alt' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);
        $user = new SiteUser();
        $user->first_name = $request->first_name;
        $user->middle_name = $request->middle_name;
        $user->last_name = $request->last_name;
        $user->dob = $request->dob;
        $user->country = $request->country;
        $user->gender = $request->gender;
        $user->marital_status = $request->marital_status;
        $user->citizenship_number = $request->citizenship_number;
        $user->citizenship_district = $request->citizenship_district;
        $user->email = $request->email;
        $user->email_alt = $request->email_alt;
        $user->contact = $request->contact;
        $user->contact_office = $request->contact_office;
        $user->permanent_district = $request->permanent_district;
        $user->permanent_address = $request->permanent_address;
        $user->permanent_telephone = $request->permanent_telephone;
        $user->current_district = $request->current_district;
        $user->current_address = $request->current_address;
        $user->current_telephone = $request->current_telephone;
        $user->mobile = $request->mobile;
        $user->current_organization = $request->current_organization;
        $user->current_designation = $request->current_designation;
        $user->experience = $request->experience;
        $user->current_salary = $request->current_salary;
        $user->expected_salary = $request->expected_salary;
        $user->religion = $request->religion;
        $user->ethnicity = $request->ethnicity;
        $user->user_bio = $request->user_bio;
        $user->notice_period = $request->notice_period;
        $user->prefer_working_areas = $request->prefer_working_areas;
        $user->jobcategory = $request->jobcategory;
        $user->jobtype = $request->jobtype;
        $user->member_name = $request->member_name;
        $user->member_relation = $request->member_relation;
        $user->member_dob = $request->member_dob;
        $user->member_phone = $request->member_phone;
        $user->member_occupation = $request->member_occupation;
        $user->member_remarks = $request->member_remarks;
        $user->education_level = $request->education_level;
        $user->degree = $request->degree;
        $user->major = $request->major;
        $user->institution = $request->institution;
        $user->start_year = $request->start_year;
        $user->passed_year = $request->passed_year;
        $user->is_verified = 0;
        $user->password = Hash::make($request->password);

        if ($request->hasFile('avatar')) {
            $user->avatar = $request->file('avatar')->store('site_user','public');
        }
        if ($request->hasFile('cv')) {
            $user->cv = $request->file('cv')->store('site_user','public');
        }
        $user->save();

        if ($user){
            Auth::loginUsingId($user->id);
            return redirect()->route('career.career');
        }else{
            return redirect()->back();
        }
    }
}