<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\SiteUser;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:front_web')->except('view_profile');
    }

    public function index()
    {
        $data['user'] = Auth::guard('front_web')->user();
        return view('front.auth.profile',$data);
    }

    public function view_profile($user_id)
    {
        $data['user'] = SiteUser::find($user_id);
        return view('front.auth.profile',$data);
    }

    public function ShowChangePasswordForm()
    {
        return view('front.auth.change_password');
    }

    public function ChangePassword(Request $request)
    {
        $this->validate($request,[
            'password'=>'required|min:6|confirmed'
        ]);
        $current_password = Auth::guard('front_web')->user()->password;
//        dd($request->current_password);
        if (Hash::check($request->current_password,$current_password)){
            $user = SiteUser::find(Auth::guard('front_web')->user()->id);
            $user->password = Hash::make($request->password);
            $user->save();
            return redirect()->route('user_profile');
        }else{
            return redirect()->back()->withErrors(['current_password' => 'Password did not match']);
        }
    }
}
