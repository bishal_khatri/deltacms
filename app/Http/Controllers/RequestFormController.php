<?php

namespace App\Http\Controllers;

use App\Exports\RequestFormExport;
use App\Mail\AccountNotification;
use App\RequestForm;
use App\Setting;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use Mail;

class RequestFormController extends Controller
{
    // backend
    public function index($status='')
    {
        $email = Setting::where('slug','ac_email')->first();
        if (is_null($email)){
            $email = new Setting();
            $email->slug = 'ac_email';
            $email->text = '';
            $email->save();
        }
        $data['email'] = $email;
        if (isset($status) AND $status=='approved'){
            $data['forms'] = RequestForm::where('status',1)->where('is_completed',1)->get();
        }else{
            $data['forms'] = RequestForm::where('status',0)->where('is_completed',1)->get();
        }
        return view('requestForm.index', $data);
    }

    public function update_email(Request $request)
    {
        $email = Setting::firstOrNew(['slug'=>'ac_email']);
        $email->slug = 'ac_email';
        $email->text = $request->email;
        $email->save();
        $data['email'] = $email;
        return redirect()->route('form.index');
    }

    public function approveForm(Request $request)
    {
        $form = RequestForm::findOrFail($request->form_id);
        $form->status = 1;
        $form->save();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Form approved successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();
    }

    // Views
    public function page1($id = null)
    {
        if (!is_null($id)){
            $data['formData'] = RequestForm::findOrFail($id);
            return view('front.page.requestForm.page1', $data);
        }
        return view('front.page.requestForm.page1');
    }

    public function page2($id)
    {
        $data['formData'] = RequestForm::findOrFail($id);
        return view('front.page.requestForm.page2', $data);
    }

    public function page3($id)
    {
        $data['formData'] = RequestForm::findOrFail($id);
        return view('front.page.requestForm.page3', $data);
    }

    public function page4($id)
    {
        $data['formData'] = RequestForm::findOrFail($id);
        return view('front.page.requestForm.page4', $data);
    }

    public function page5($id)
    {
        $data['formData'] = RequestForm::findOrFail($id);
        return view('front.page.requestForm.page5', $data);
    }

    public function page6($id)
    {
        $data['formData'] = RequestForm::findOrFail($id);
        return view('front.page.requestForm.page6', $data);
    }


    public function store(Request $request)
    {
//        dd($request->all());
        // Storing General Information
        if ($request->page == 1){
            $data['formData'] = self::post_page1($request);
            return redirect(route('ac_form.page2',$data));
        }
        elseif($request->page == 2){
            $data['formData'] = self::post_page2($request);
            return redirect(route('ac_form.page3',$data));
        }
        elseif($request->page == 3){
            $data['formData'] = self::post_page3($request);
            return redirect(route('ac_form.page4',$data));
        }
        elseif($request->page == 4){
            $data['formData'] = self::post_page4($request);
            return redirect(route('ac_form.page5',$data));
        }
        elseif($request->page == 5){
            $data['formData'] = self::post_page5($request);
            return redirect(route('ac_form.page6',$data));
        }
        elseif($request->page == 6){
            $data['formData'] = self::post_page6($request);
            $flashMessage = [
                'heading'=>'success',
                'type'=>'success',
                'message'=>'Form submitted successfully.'
            ];
            \Session::flash('flash_message', $flashMessage);
            return redirect(route('ac_form.page1'));
        }


    }

    private function post_page1($request)
    {
        if (isset($request->form_id) AND !is_null($request->form_id) AND !empty($request->form_id)){
            $form = RequestForm::find($request->form_id);
        }else{
            $form = new RequestForm();
        }
        $form->ctype = $request->ctype;
        $form->cbranch = $request->cbranch;
        $form->cowner = $request->cowner;
        $form->ccategory = $request->ccategory;
        $form->cminor = $request->cminor;
        $form->cfname = $request->cfname;
        $form->cmiddlename = $request->cmiddlename;
        $form->clastname = $request->clastname;
        $form->cdatebirth = $request->cdatebirth;
        $form->cexpiryminor = $request->cexpiryminor;
        $form->ccountry = $request->ccountry;
        $form->cacofficer = $request->cacofficer;
        $form->cintro = $request->cintro;
        $form->cresident = $request->cresident;

        $form->caddress = $request->caddress;
        $form->ccity = $request->ccity;
        $form->chouseblock = $request->chouseblock;
        $form->cvcmdc = $request->cvcmdc;
        $form->cvcmdcname = $request->cvcmdcname;
        $form->ctelno = $request->ctelno;
        $form->cpobox = $request->cpobox;
        $form->cdistrict = $request->cdistrict;
        $form->cwardno = $request->cwardno;
//        $form->ccountry = $request->ccountry;
        $form->cstate = $request->cstate;

        $form->peraddress = $request->peraddress;
        $form->percity = $request->percity;
        $form->pervcmdc = $request->pervcmdc;
        $form->pervcmdcname = $request->pervcmdcname;
        $form->pertel = $request->pertel;
        $form->perpobox = $request->perpobox;
        $form->perdistrict = $request->perdistrict;
        $form->perward = $request->perward;
        $form->percountry = $request->percountry;
        $form->perstate = $request->perstate;
        $form->perfax = $request->perfax;
        $form->perfax2 = $request->perfax2;
        $form->permobile = $request->permobile;
        $form->permobile2 = $request->permobile2;
        $form->peremail = $request->peremail;

        $form->page = 1;
        $form->is_completed = 0;
        $form->status = 0;
        $form->save();

        return $form;
    }

    public function post_page2($request)
    {
        $form = RequestForm::find($request->form_id);

        if ($request->hasFile('citizenship_image')) {
            $form->citizenship_image = $request->file('citizenship_image')->store('ac_form','public');
        }
        if ($request->hasFile('pp_image')) {
            $form->pp_image = $request->file('pp_image')->store('ac_form','public');
        }
        $form->cusid = $request->cusid;
        $form->cusprevcitizen = $request->cusprevcitizen;
        $form->cusidno = $request->cusidno;
        $form->cusauthority = $request->cusauthority;
        $form->cusissdate = $request->cusissdate;
        $form->cusiddistrict = $request->cusiddistrict;
        $form->cusmarital = $request->cusmarital;
        $form->cusnationality = $request->cusnationality;
        $form->cussalutation = $request->cussalutation;
        $form->cusreligion = $request->cusreligion;
        $form->cusgender = $request->cusgender;

        $form->emporgname = $request->emporgname;
        $form->empdesignation = $request->empdesignation;
        $form->emporgaddress = $request->emporgaddress;
        $form->empsalary = $request->empsalary;
        $form->regaddress = $request->regaddress;
        $form->regcity = $request->regcity;
        $form->regcountry = $request->regcountry;
        $form->regvcmdc = $request->regvcmdc;
        $form->regward = $request->regward;
        $form->regdistrict = $request->regdistrict;
        $form->regstate = $request->regstate;



        $form->page = 2;
        $form->is_completed = 0;
        $form->save();

        return $form;
    }

    public function post_page3($request)
    {
        $form = RequestForm::find($request->form_id);

        $form->grnffirst = $request->grnffirst;
        $form->grnfmiddle = $request->grnfmiddle;
        $form->grnflast = $request->grnflast;
        $form->grnfctznno = $request->grnfctznno;
        $form->grnfctznissdate = $request->grnfctznissdate;
        $form->grnfctznissdistrict = $request->grnfctznissdistrict;
        $form->grnfctzndistrict = $request->grnfctzndistrict;
        $form->grmfirst = $request->grmfirst;
        $form->grmmiddle = $request->grmmiddle;
        $form->grmlast = $request->grmlast;
        $form->grmctznno = $request->grmctznno;
        $form->grmctznissdate = $request->grmctznissdate;
        $form->grmctznissdistrict = $request->grmctznissdistrict;
        $form->grmctzndistrict = $request->grmctzndistrict;

        $form->ftrfirst = $request->ftrfirst;
        $form->ftrmiddle = $request->ftrmiddle;
        $form->ftrfast = $request->ftrfast;
        $form->ftrctznno = $request->ftrctznno;
        $form->ftrctznissdate = $request->ftrctznissdate;
        $form->ftrctznissdistrict = $request->ftrctznissdistrict;
        $form->ftrctzndistrict = $request->ftrctzndistrict;

        $form->mthrfirst = $request->mthrfirst;
        $form->mthrmiddle = $request->mthrmiddle;
        $form->mthrlast = $request->mthrlast;
        $form->mthrctznno = $request->mthrctznno;
        $form->mthrctznissdate = $request->mthrctznissdate;
        $form->mthrctznissdistrict = $request->mthrctznissdistrict;
        $form->mthrctzndistrict = $request->mthrctzndistrict;

        $form->page = 3;
        $form->is_completed = 0;
        $form->save();

        return $form;
    }

    public function post_page4($request)
    {
        $form = RequestForm::find($request->form_id);

        $form->comid = $request->comid;
        $form->comauthority = $request->comauthority;
        $form->comidnum = $request->comidnum;
        $form->comissdistrict = $request->comissdistrict;
        $form->comactcode = $request->comactcode;
        $form->comissdate = $request->comissdate;
        $form->comexpiry = $request->comexpiry;

        $form->page = 4;
        $form->is_completed = 0;
        $form->save();

        return $form;
    }

    public function post_page5($request)
    {
        $form = RequestForm::find($request->form_id);

        $form->othoccupation = $request->othoccupation;
        $form->othsource = $request->othsource;
        $form->othpurpose = $request->othpurpose;
        $form->othturnover = $request->othturnover;

        $form->forassociate = $request->forassociate;
        $form->forresidential = $request->forresidential;
        $form->fordirector = $request->fordirector;
        $form->forcountry = $request->forcountry;
//
//        $form->kycbusiness = $request->kycbusiness;
//        $form->kycreviewed = $request->kycreviewed;
//        $form->kycriskgrade = $request->kycriskgrade;
//        $form->kyctrans = $request->kyctrans;
//        $form->kycreason = $request->kycreason;
//
//        $form->docstatus = $request->docstatus;
//        $form->docdateupdate = $request->docdateupdate;
//        $form->docremarks = $request->docremarks;
//        $form->docpersonal = $request->docpersonal;

        $form->perexposed = $request->perexposed;
        $form->perpunish = $request->perpunish;
        $form->perhighpositioned = $request->perhighpositioned;
        $form->pernonface = $request->pernonface;
        $form->pervimpper = $request->pervimpper;

        $form->insinfo = $request->insinfo;
        $form->insremark = $request->insremark;
        $form->inslimit = $request->inslimit;
        $form->insgroup = $request->insgroup;
        $form->insborrower = $request->insborrower;
//        $form->insstatement = $request->insstatement;
        $form->insmis = $request->insmis;
        $form->inskycupdate = $request->inskycupdate;

        $form->page = 5;
        $form->is_completed = 0;
        $form->save();

        return $form;
    }

    public function post_page6($request)
    {
        $form = RequestForm::find($request->form_id);

        $form->extname = $request->extname;
        $form->extcitizen = $request->extcitizen;
        $form->extriskgrade = $request->extriskgrade;
        $form->extalias = $request->extalias;
        $form->extctzndistrict = $request->extctzndistrict;
        $form->extorgname = $request->extorgname;
        $form->extdesignation = $request->extdesignation;
        $form->extbirth = $request->extbirth;
        $form->extfather = $request->extfather;
        $form->extgrandfather = $request->extgrandfather;
        $form->extspouse = $request->extspouse;
        $form->extctznissddate = $request->extctznissddate;
        $form->extsignatory = $request->extsignatory;
        $form->extdirector = $request->extdirector;
        $form->extrole = $request->extrole;

        $form->reshouse = $request->reshouse;
        $form->resstreet = $request->resstreet;
        $form->resward = $request->resward;
        $form->resvcmdc = $request->resvcmdc;
        $form->resdistrict = $request->resdistrict;
        $form->rescity = $request->rescity;
        $form->restel = $request->restel;

        $form->extperhouse = $request->extperhouse;
        $form->extperarea = $request->extperarea;
        $form->extperward = $request->extperward;
        $form->extpervcmdc = $request->extpervcmdc;
        $form->extperdistrict = $request->extperdistrict;
        $form->extpercity = $request->extpercity;
        $form->extpertel = $request->extpertel;
        $form->extperstreet = $request->extperstreet;

        $form->mailhouse = $request->mailhouse;
        $form->mailarea = $request->mailarea;
        $form->mailward = $request->mailward;
        $form->mailvcmdc = $request->mailvcmdc;
        $form->maildistrict = $request->maildistrict;
        $form->mailcity = $request->mailcity;
        $form->mailtel = $request->mailtel;
        $form->mailaddress = $request->mailaddress;
        $form->mailfax = $request->mailfax;
        $form->mailstreet = $request->mailstreet;

        $form->bustel = $request->bustel;
        $form->busfax = $request->busfax;

        $form->page = 6;
        $form->is_completed = 1;
        $form->save();

        return $form;
    }


    public function editForm($id)
    {
        $data['formData'] = RequestForm::findOrFail($id);
        return view('requestForm.display', $data);
    }
    public function display()
    {

        return view('requestForm.display');
    }

    public function update(Request $request)
    {
        $form = RequestForm::findOrFail($request->form_id);
        $form->ccode = $request->ccode;
        $form->cbranch = $request->cbranch;
        $form->cstatus = $request->cstatus;
        $form->cobligator = $request->cobligator;
        $form->cacofficer = $request->cacofficer;
        $form->cintro = $request->cintro;
        $form->insinfo = $request->insinfo;
        $form->insremark = $request->insremark;
        $form->inslimit = $request->inslimit;
        $form->insborrower = $request->insborrower;
        $form->insgroup = $request->insgroup;
        $form->insstatement = $request->insstatement;
        $form->insmis = $request->insmis;
        $form->inskycupdate = $request->inskycupdate;
        $form->kycbusiness = $request->kycbusiness;
        $form->kycreviewed = $request->kycreviewed;
        $form->kycriskgrade = $request->kycriskgrade;
        $form->kyctrans = $request->kyctrans;
        $form->kycreason = $request->kycreason;
        $form->docstatus = $request->docstatus;
        $form->docdateupdate = $request->docdateupdate;
        $form->docremarks = $request->docremarks;
        $form->perexposed = $request->perexposed;
        $form->perpunish = $request->perpunish;
        $form->perhighpositioned = $request->perhighpositioned;
        $form->pernonface = $request->pernonface;
        $form->pervimpper = $request->pervimpper;
        $form->status = $request->status;

        $form->save();


        $message = Setting::where('slug','ac_email')->first();
        Mail::to('bishal.game343@gmail.com')->send(new AccountNotification($message->text));

        return redirect()->back();
    }

    public function export($type,$id)
    {
        if ($id == 'all'){
            $name = 'account_opening_form_list_all.';
        }else{
            $form = RequestForm::find($id);
            $name = 'account_opening_form_list_all'.$form->ccode.'.';
        }
        return Excel::download(new RequestFormExport($id), $name.$type);
    }

}
