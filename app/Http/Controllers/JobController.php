<?php

namespace App\Http\Controllers;

use App\Exports\ApplicantsExport;
use App\Exports\SiteUsersExport;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Facades\Excel;
use App\Job;
use App\JobApplication;
use App\SiteUser;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class JobController extends Controller
{
    public function index()
    {
        $data['jobs'] = Job::with('user','application')->get();
        $data['category'] = Job::whereNotNull('category')->pluck('category');
        $data['designation'] = Job::whereNotNull('designation')->pluck('designation');
//        dd($data);
        return view('jobs.index',$data);
    }

    public function create()
    {
        return view('jobs.create');
    }

    public function store(Request $request)
    {
        $this->validate($request,[
           'title' => 'required|max:255'
        ]);

        $job = new Job();
        $job->title = $request->title;
        $job->deadline = $request->deadline;
        $job->description = $request->description;
        $job->designation = $request->designation;
        $job->category = $request->category;
        $job->is_published = $request->submit;
        $job->created_by = Auth::user()->id;
        $job->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Job created successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

        return redirect()->route('jobs.index');
    }

    public function edit($job_id)
    {
        $data['job'] = Job::find($job_id);
        return view('jobs.edit',$data);
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:255'
        ]);

        $job = Job::find($request->job_id);
        $job->title = $request->title;
        $job->deadline = $request->deadline;
        $job->description = $request->description;
        $job->is_published = $request->submit;
        $job->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Job updated successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

        return redirect()->route('jobs.index');
    }

    public function delete(Request $request)
    {
        $job = Job::find($request->id);
        JobApplication::where('job_id',$request->id)->each(function ($jobapp, $key) {
            $jobapp->delete();
        });
        $job->delete();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Job deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

        return response()->json('success');
    }

    public function view_application($job_id)
    {
        $data['applicants'] = JobApplication::where('job_id',$job_id)->with('site_user')->get();
        $data['job_id'] = $job_id;
        return view('jobs.application',$data);
    }

    public function get_users()
    {
        $data['users'] = SiteUser::all();
        return view('jobs.user',$data);
    }

    public function export_users($type)
    {
        return Excel::download(new SiteUsersExport, 'users.'.$type);
    }

    public function export_users_application($job_id,$type)
    {
        return Excel::download(new ApplicantsExport($job_id), 'applicants.'.$type);
    }
}
