<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Setting;
use Illuminate\Http\Request;
use File;

class SettingController extends Controller
{
    public function index()
    {
        $data['image_post_max_size'] = Helper::image_post_max_size() / 1024;
        $data['file_post_max_size'] = Helper::file_post_max_size() / 1024;
        $data['site_logo'] = Helper::site_logo();
        $data['contact_info'] = Setting::where('slug','pre_nav_contact_info')->first();
        // dd($data);
        return view('setting.index',$data);
    }

    public function store_max_image_size(Request $request)
    {
        $value = $request->image_size * 1024;
        $setting = Setting::firstOrCreate(['slug'=>'image_post_max_size']);
        $setting->value = $value;
        $setting->save();
        return redirect()->back();
    }

    public function store_max_file_size(Request $request)
    {
        $value = $request->file_size * 1024;
        $setting = Setting::firstOrCreate(['slug'=>'file_post_max_size']);
        $setting->value = $value;
        $setting->save();
        return redirect()->back();
    }

    public function store_contact_info(Request $request)
    {
        $setting = Setting::firstOrCreate(['slug'=>'pre_nav_contact_info']);
        $setting->text = $request->contact_info;
        $setting->save();
        return redirect()->back();
    }

    public function store_logo(Request $request)
    {
        // dd($request->all());
        if (!empty($request->site_logo)) {
            $site_logo = $request->site_logo;
            $info = pathinfo(public_path().$site_logo);
            $logo_storage = storage_path('app/public/logo/');
            File::isDirectory($logo_storage) or File::makeDirectory($logo_storage, 0777, true, true);
            $image_name = str_random(45).'.'.$info['extension'];
            $new_file = $logo_storage.$image_name;
            // save to database name
            $site_logo_name_full = 'logo/'.$image_name;
            \File::copy(public_path().$site_logo,$new_file);
        }
        else{
            $site_logo_name_full = '';
        }

        $logo = Setting::firstOrCreate(['slug'=>'site_logo']);
        $logo->file = $site_logo_name_full;
        $logo->url = $request->site_logo_url;
        $logo->save();
        return redirect()->back();
    }
}
