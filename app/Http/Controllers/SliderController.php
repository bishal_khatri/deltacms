<?php

namespace App\Http\Controllers;

use App\Slider;
use App\SliderType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;


class SliderController extends Controller
{
    public function index()
    {
        $data['sliders'] = Slider::all();
        return view('slider.index', $data);
    }

    public function create(Request $request)
    {
        $this->validate($request,[
            'title'=>'max:255',
            'link' => 'max:255',
            'description' => 'max:255',
            'button_text' => 'max:100',
            'image' => 'required|mimes:jpeg,bmp,png'
        ]);
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('sliders','public');
        }
        else{
            $path = '';
        }
        if (!isset($request->type_id)) {
            $slider_type = SliderType::firstOrCreate(['slug' => 'uncategorized']);
            $slider_type->name = 'Uncategorized';
            $slider_type->status = 0;
            $slider_type->save();
        }
        $link = isset($request->link)?$request->protocol.$request->link:'';
        $slider = new Slider();
        $slider->image = $path;
        $slider->is_active = 0;
        $slider->created_by = Auth::user()->id;
        $slider->title = $request->title;
        $slider->link = $link;
        $slider->description = $request->description;
        $slider->type_id = $slider_type->id;
        $slider->button_text = $request->button_text;
        $slider->target = $request->target;
        $slider->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Slider image uploaded successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();
    }

    public function edit($slider_id)
    {
        $data['slider'] = Slider::find($slider_id);
        return view('slider.edit',$data);
    }

    public function update(Request $request)
    {
        $this->validate($request,[
            'slider_id' => 'required',
            'title'=>'max:255',
            'link' => 'max:255',
            'description' => 'max:255',
            'button_text' => 'max:100',
            'image' => 'mimes:jpeg,bmp,png'
        ]);

        $slider = Slider::find($request->slider_id);

        if ($request->hasFile('image')){
            $old_image = $slider->image;
            $new_path = $request->file('image')->store('sliders','public');
            $slider->image = $new_path;
            \Storage::delete('public/'.$old_image);
        }
        $slider->title = $request->title;
        $slider->description = $request->description;
        $slider->button_text = $request->button_text;
        $slider->link = $request->link;
        $slider->target = $request->target;
        $slider->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Slider updated successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->route('slider.index');
    }

    public function set_active($id,$action)
    {
        $slider = Slider::find($id);
        if ($action=='activate'){
            $slider->is_active = 1;
        }else{
            $slider->is_active = 0;
        }$slider->save();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Slider image status changed successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();
    }

    public function delete(Request $request)
    {
        $slider = Slider::find($request->id);
        \Storage::delete('public/'.$slider->image);
        $slider->delete();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Slider image deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return 's';
    }

}
