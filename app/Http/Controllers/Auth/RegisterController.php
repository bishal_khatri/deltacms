<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
//    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'contact' => $data['contact'],
            'address' => $data['address'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'is_super' => 0,
        ]);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

//        $this->guard()->login($user);

//        return $this->registered($request, $user)
//            ?: redirect($this->redirectPath());
        return redirect()->back();
    }

    public function index()
    {
        $data['users'] = User::where('is_super',0)->get();
        return view('auth.index', $data);
    }

    public function showEditForm($id)
    {
        $data['user'] = User::find($id);
        $data['id'] = $id;
        return view('auth.edit',$data);
    }

    public function update(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'email' => 'required'
        ]);
        $user = User::find($request->user_id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->address = $request->address;
        $user->contact = $request->contact;
        $user->email = $request->email;
        $user->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'User detials updated successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

        return redirect()->route('user.index');

    }

    public function delete(Request $request)
    {
        $user = User::find($request->id);
        $user->delete();
    }

}
