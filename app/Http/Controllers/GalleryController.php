<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use Illuminate\Http\Request;
use App\Gallery;
use Auth;
use App\GalleryImage;

class GalleryController extends Controller
{
    private $image_post_max_size;
    public function __construct()
    {
        $this->image_post_max_size = Helper::image_post_max_size();
    }

    public function index(Request $request)
    {
    	$data['gallery'] = self::getImage();
        return view('gallery.index', $data);
    }

    private function getImage()
    {
        return Gallery::where('is_trashed',0)
                            ->with('image')
                            ->with('images')
                            ->with('user')
                            ->orderBy('created_at', 'desc')
                            ->get()
                            ->each(function($image) {
                                    $image->load('image');
                                });
    }

    public function create(Request $request)
    {
    	$this->validate($request,[
    		'gallery_name' => 'required|max:255',
            'description' => 'max:500'
    	]);
    	$gallery = new Gallery();
    	$gallery->gallery_name = $request->gallery_name;
    	$gallery->description = $request->description;
    	$gallery->is_trashed = 0;
    	$gallery->created_by = Auth::user()->id;
    	$gallery->created_at = date('Y-m-d H:i:s');
    	$gallery->save();

    	$flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Gallery created successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->route('gallery.index');
    }

    public function add_image(Request $request)
    {
//        dd($request->all());
//        dd($this->image_post_max_size);
        $this->validate($request,[
            'image' => 'required',
            'image.*' => 'mimes:jpeg,bmp,png|max:'.$this->image_post_max_size,
        ]);
        foreach($request->file('image') as $img) {
            $path = $img->store('gallery', 'public');
            $gallery_image = new GalleryImage();
            $gallery_image->image_name = $path;
            $gallery_image->gallery_id = $request->gallery_id;
            $gallery_image->is_trashed = 0;
            $gallery_image->created_by = Auth::user()->id;
            $gallery_image->created_at = date('Y-m-d H:i:s');
            $gallery_image->save();
        }

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Image added successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();
    }

    public function display($id)
    {
        $data['gallery'] = Gallery::where('id',$id)->with('user')->with('images')->first();
        return view('gallery.display',$data);
    }

    public function edit(Request $request)
    {
        // dd($request->all());
        $this->validate($request,[
            'gallery_name' => 'required|max:255'
        ]);
        $gallery = Gallery::find($request->gallery_id);
        $gallery->gallery_name = $request->gallery_name;
        $gallery->description = $request->description;
        $gallery->save();

        return redirect()->back();
    }

    public function delete_gallery(Request $request)
    {
        $gallery = Gallery::find($request->id);
        $galleryImage = GalleryImage::where('gallery_id',$gallery->id)->get();
        foreach ($galleryImage as $value){
            \Storage::delete('public/'.$value->image_name);
            $value->delete();
        }
        $gallery->delete();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Gallery and images deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return 's';
    }

    public function delete_image(Request $request)
    {
        $image = GalleryImage::find($request->id);
        \Storage::delete('public/'.$image->image_name);
        $image->delete();
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Image deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return 's';
    }
}
