<?php

namespace App\Http\Controllers;

use App\Helpers\Helper;
use App\Seo;
use Illuminate\Http\Request;
use App\Category;
use App\Post;
use Auth;
use Illuminate\Support\Facades\Input;

class CategoryController extends Controller
{
    
    public function index()
    {
    	$data['categories'] = Category::with('posts')->get();
    	return view('category.index', $data);
    }

    public function store(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:255'
        ]);
        $category_slug = Helper::createPostSlug($request->title);

        $category = new Category();
        $category->category_title = $request->title;
        $category->category_subtitle = $request->sub_title;
        $category->category_slug = $category_slug;
        $category->created_by = Auth::user()->id;
        $category->save();

        return redirect()->back();
    }

    public function update_category(Request $request)
    {
        $this->validate($request,[
            'title' => 'required|max:255'
        ]);
        $category_slug = Helper::createPostSlug($request->title);

        $category = Category::find($request->category_id);
        $category->category_title = $request->title;
        $category->category_subtitle = $request->sub_title;
        $category->category_slug = $category_slug;
        $category->save();

        return redirect()->back();
    }

    public function list_item($id)
    {
        $data['items'] = Post::where('post_type','category_item')->where('category_id',$id)->get();
        $data['category_id'] = $id;
        $data['category'] = Category::find($id);
        return view('category.list_item',$data);
    }

    public function add_item($id)
    {
        $data['category_id'] = $id;
        return view('category.create',$data);
    }

    public function store_item(Request $request)
    {
//         dd($request->all());
        $this->validate($request,[
            'post_title'=>'required|max:255'
        ]);
        $post_slug = $request->post_slug;
        if (empty($post_slug)) {
            $post_slug = Helper::createPostSlug($request->post_title);
        }
        if (!empty($request->post_url)) {
            $post_url = $request->post_url;
        }else{
            $post_url = url($post_slug);
        }

        if (Input::has('cover_image')) {
           $path = $request->file('cover_image')->store('pages','public');
        }
        else{
            $path = '';
        }

        // style
        if (isset($request->cover_icon)) {
            $cover_icon = $request->cover_icon;
        }else{
            $cover_icon = '';
        }
        // button
        if (isset($request->btn_type)) {
            $btn_type = $request->btn_type;
        }else{
            $btn_type = '';
        }
        if (isset($request->outline)) {
            $btn_outline = $request->outline;
        }else{
            $btn_outline = '';
        }
        if (isset($request->rounded)) {
           $btn_rounded = $request->rounded;
        }else{
            $btn_rounded = '';
        }

        $btn = $btn_type .' '. $btn_outline .' '. $btn_rounded;


        $post = new Post();
        $post->post_title = $request->post_title;
        $post->post_subtitle = $request->post_subtitle;
        $post->post_content = $request->post_content;
        $post->post_slug = $post_slug;
        $post->post_type = $request->post_type;
        $post->post_status = $request->status;
        $post->post_url = $post_url;
        $post->category_id = $request->category_id;
        $post->unique_key = str_random(100);
        $post->created_by = Auth::user()->id;
        $post->tags = $request->tags;
        $post->cover_image = $path;
        $post->cover_icon = $cover_icon;
        $post->button_type = $btn;
        $post->button_text = $request->button_text;
        $post->save();

        //store seo
        $seo = new Seo();
        $seo->page_id = $post->id;
        $seo->seo_title = $request->seo_title;
        $seo->seo_description = $request->seo_description;
        $seo->seo_keywords = $request->seo_keywords;
        $seo->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Page created successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->route('category.list_item',$request->category_id);
    }

    public function edit_item($id)
    {
        $data['category_id'] = $id;
        $data['item'] = Post::where('id',$id)->with('seo')->first();
        return view('category.edit_item',$data);
    }

    public function update_item(Request $request)
    {
//        dd($request->all());
        $this->validate($request,[
            'post_title'=>'required|max:255',
            'post_id'=>'required|max:255'
        ]);
        $post_slug = $request->post_slug;
        if (empty($post_slug)) {
            $post_slug = Helper::createPostSlug($request->post_title);
        }
        $post_url = !empty($request->post_url) ? $request->post_url : url($post_slug);

        $post = Post::findOrFail($request->post_id);
        $post->post_title = $request->post_title;
        $post->post_subtitle = $request->post_subtitle;
        $post->post_content = $request->post_content;
        $post->post_slug = $post_slug;
        $post->post_status = $request->status;
        $post->post_url = $post_url;
        if (isset($request->cover_icon)) {
            $cover_icon = $request->cover_icon;
            $post->cover_icon = $cover_icon;
        }
        $post->save();

        //store seo
        $seo = Seo::firstOrNew(['page_id'=>$request->post_id]);
        $seo->seo_title = $request->seo_title;
        $seo->seo_keywords = $request->seo_keywords;
        $seo->seo_description = $request->seo_description;
        $seo->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Page created successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->route('category.list_item',$post->category_id);
    }

    public function delete_item(Request $request)
    {
        $category_item = Post::find($request->id);
        $category_item->delete();
         $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Item deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();
    }

    public function delete_category(Request $request)
    {
        $category = Category::find($request->id);

        $post = Post::where('category_id',$category->id)->delete();
        $category->delete();
         $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Item deleted successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();
    }
}
