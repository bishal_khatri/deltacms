<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use DB;
use App\Post;
use App\MenuPost;
use App\DownloadCategory;
use Illuminate\Support\Facades\Auth;

class MenuController extends Controller
{
    public function index()
    {
        $data['pages'] = Post::where('post_type','page')->get();
    	$data['downloads'] = DownloadCategory::all();
    	$data['menus'] = Menu::all();
    	$data['selected_menu'] = Menu::where('is_selected',1)->first();
        if (!empty($data['selected_menu'])) {
            $data['selected_menu_item'] = MenuPost::where('menu_id',$data['selected_menu']->id)
                                                    ->where('parent',0)
                                                    ->with(['menu','children.post','parent','post'])
                                                    ->orderBy('order')
                                                    ->get();
        }
    	// dd($data['selected_menu_item']);
    	return view('menu.index',$data);
    }

    public function create(Request $request)
    {
    	$this->validate($request, [
    		'title'=>'required',
    		'menu_type'=>'required'
    	]);
    	$menu = new Menu();
    	$menu->title = $request->title;
        $menu->menu_type = $request->menu_type;
    	$menu->is_active = 0;
    	$menu->save();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Menu created successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

    	return redirect()->back();
    }

    public function change(Request $request)
    {
    	if ($request->action=='select_menu') {
            $this->validate($request,[
                'menu_id_change'=>'required'
            ]);
    		$affected = DB::table('menus')->where('is_selected', '=', 1)->update(array('is_selected' => 0));
	    	$menu_id_change = $request->menu_id_change;
	    	$menu = Menu::find($menu_id_change);
	    	$menu->is_selected = 1;
	    	$menu->save();
    	}
    	elseif ($request->action=='save_menu_name') {
            $this->validate($request,[
                'menu_title'=>'required'
            ]);
    		$selected_menu = Menu::find($request->selected_menu_id);
    		$selected_menu->title = $request->menu_title;
    		$selected_menu->save();
    	}
    	elseif($request->action=='add_pages'){
    		$this->validate($request,[
    			'page_id'=>'required'
    		]);
    		foreach ($request->page_id as $key => $value) {
	    		$menu_post = new MenuPost();
	    		$menu_post->post_id = $value;
    			$menu_post->menu_id = $request->selected_menu_id;
                $menu_post->parent = 0;
    			$menu_post->save();
    		}
    	}
    	elseif($request->action=='add_link'){
            $this->validate($request,[
                'link_name'=>'required',
                'custom_link'=>'required',
            ]);

            $post_slug = $request->link_name;
            $post_slug = strtolower($post_slug);
            $post_slug = str_replace(' ','-',$post_slug);

    	    $post = new Post();
    	    $post->post_title = $request->link_name;
    	    $post->post_url = $request->custom_link;
            $post->post_slug = $post_slug;
            $post->post_status = 'publish';
            $post->post_type = 'custom_link';
            $post->post_parent = 0;
            $post->unique_key = str_random(100);
            $post->created_by = Auth::user()->id;
            $post->post_url = $request->custom_link;
            $post->save();

            $pm = new MenuPost();
            $pm->post_id = $post->id;
            $pm->menu_id = $request->selected_menu_id;
            $pm->parent = 0;
            $pm->save();
        }
        elseif($request->action=='add_downloads'){
            $this->validate($request,[
                'downloads_id'=>'required'
            ]);
            foreach ($request->downloads_id as $value) {
                $downloads = DownloadCategory::find($value);
                $post = new Post();
                $post->post_title = $downloads->title;
                $post->downloads_id = $downloads->downloads_category_id;
                $post->post_slug = $downloads->slug;
                $post->post_url = url('downloads/'.$downloads->slug);
                $post->post_status = 'publish';
                $post->post_type = 'downloads';
                $post->post_parent = 0;
                $post->unique_key = str_random(100);
                $post->created_by = Auth::user()->id;
                $post->save();

                $menu_post = new MenuPost();
                $menu_post->post_id = $post->id;
                $menu_post->menu_id = $request->selected_menu_id;
                $menu_post->parent = 0;
                $menu_post->save();
            }
        }
    	return redirect()->back();
    }

    public function remove_item($menu_post_id)
    {
        $pm = MenuPost::where('menu_post_id',$menu_post_id)->first();
        $pm->delete();

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Menu removed successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);
        return redirect()->back();
    }

    public function edit_menu(Request $request)
    {
            $this->validate($request,[
                'menu_display_name'=>'max:255',
                'menu_icon'=>'max:45',
                'menu_description'=>'max:255',
            ]);
            $menupost = MenuPost::find($request->menu_post_id);
            $menupost->menu_display_name = $request->menu_display_name;
            $menupost->menu_icon = $request->menu_icon;
            $menupost->menu_description = $request->menu_description;
            $menupost->save();

            $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Menu edited successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

            return redirect()->back();
    }

    public function saveOrder(Request $request)
    {
        $input = $request->data;
        $dataCount = 1;
        $childCount = 1;
        $subchildCount = 1;
        foreach ($input as $data) {
            if(isset($data['children'])){
                foreach($data['children'] as $child){
                    if(isset($child['children'])){
                        foreach ($child['children'] as $subchild) {
                            $menusub = MenuPost::find($subchild['id']);
                            $menusub->parent = $child['id'];
                            $menusub->order = $subchildCount;
                            $menusub->save();
                            $subchildCount++;
                        }
                        
                    }
                    $menuch = MenuPost::find($child['id']);
                    $menuch->parent = $data['id'];
                    $menuch->order = $childCount;
                    $menuch->save();
                    $childCount++;
                    // dd('child'.$child['id']);
                }
            }
            $menu = MenuPost::find($data['id']);
            $menu->parent = 0;
            $menu->order = $dataCount;
            $menu->save();
            $dataCount++;
            
        }
        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Menu order saved successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

        return 'true';
    }

    public function change_status($menuId,$action)
    {
        $menu = Menu::find($menuId);
        if ($action=='activate'){
            $menu_type = $menu->menu_type;
            Menu::where('menu_type',$menu_type)->update([
               'is_active' => 0
            ]);
            $menu->is_active=1;
            $menu->save();
        }
        elseif($action=='deactivate'){
            $menu->is_active=0;
            $menu->save();
        }

        $flashMessage = [
            'heading'=>'success',
            'type'=>'success',
            'message'=>'Menu status changed successfully.'
        ];
        \Session::flash('flash_message', $flashMessage);

        return redirect()->back();
    }


}
