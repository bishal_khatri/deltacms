<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    protected $dates = [
        'deadline'
    ];
    public function user()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function application()
    {
        return $this->hasMany('App\JobApplication','job_id');
    }
}
