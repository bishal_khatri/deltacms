<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Seo extends Model
{
    protected $primaryKey = 'seo_id';
    protected $table = 'seo';
    protected $fillable = ['page_id'];
}
