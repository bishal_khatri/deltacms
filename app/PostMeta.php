<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model
{
    protected $primaryKey = 'post_meta_id';
    protected $table = 'post_meta';

    protected $fillable = [
        'post_id','slug',
    ];
}
