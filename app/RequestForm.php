<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RequestForm extends Model
{
    protected $table = 'request_form';
    protected $primaryKey = 'form_id';
}
