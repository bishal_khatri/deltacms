<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuthPermissionUser extends Model
{
    protected $table = 'auth_permission_user';
    public $timestamps = false;
}
