<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GalleryImage extends Model
{
    protected $table = 'gallery_images';
    public $timestamps = false;

    public function gallery()
    {
        return $this->belongsTo('App\Gallery');
    }
    
}
