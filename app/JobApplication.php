<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobApplication extends Model
{
    protected $table = 'job_application';

    public function site_user()
    {
        return $this->belongsTo('App\SiteUser','site_user_id');
    }
}
