<?php
namespace App\Exports;

use App\RequestForm;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class RequestFormExport implements FromCollection
{
    private $id;

    public function __construct($id)
    {
        $this->id = $id;
    }

    public function collection()
    {
        $returnData = new Collection();
        $returnData->push(['client code','previous code','client minor']);
        if ($this->id=='all'){
            $data = RequestForm::all();
            foreach ($data as $value)
            {
              $returnData->push([
                  'ccode'=>$value->ccode,
                  'prvccode'=>$value->prvccode,
                  'cminor'=>$value->cminor=='on' ? 'checked' : '',

              ]);

            }
        }else{
            $data = RequestForm::find($this->id);
            $returnData->push([
                'ccode'=>$data->ccode,
                'prvccode'=>$data->prvccode,
                'cminor'=>$data->cminor=='on' ? 'checked' : '',

            ]);
        }
        return $returnData;
    }
}
