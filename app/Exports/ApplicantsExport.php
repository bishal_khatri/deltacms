<?php

namespace App\Exports;

use App\JobApplication;
use App\SiteUser;
use Illuminate\Database\Eloquent\Collection;
use Maatwebsite\Excel\Concerns\FromCollection;

class ApplicantsExport implements FromCollection
{
    private $job_id;

    public function __construct($job_id)
    {
        $this->job_id = $job_id;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $applicants =  JobApplication::where('job_id',$this->job_id)->with('site_user')->get();
//        dd($applicants);
        $applicant_users = new Collection();
        foreach ($applicants as $applicant) {
            $applicant_users[] = SiteUser::where('id',$applicant->site_user_id)->first();
        }
//        dd($applicant_users);
        return $applicant_users;
    }
}
