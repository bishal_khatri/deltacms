<?php

namespace App\Exports;

use App\JobApplication;
use App\SiteUser;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class SiteUsersExport implements FromCollection
{
    use Exportable;

    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return SiteUser::all();
    }

//    public function application($job_id)
//    {
//        $application = JobApplication::where('job_id',$job_id)->with('site_user')->get();
//        foreach ($application as $val){
//            $users[] = $val->site_user->all();
//        }
//        dd($users);
//        return $users;
//    }

}
