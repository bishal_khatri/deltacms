<?php

namespace App\Providers;

use App\AuthPermission;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerUserPolicies();

        //
    }

    public function registerUserPolicies()
    {
        Gate::before(function($user, $ability) {
            if($user->is_super == 1) {
                return true;
            }
        });

        $permissions = AuthPermission::all();
//dd($permissions);
        foreach ($permissions as $prem){
            $role = $prem->code_name;
//            dd($role);
            Gate::define($role, function($user) use($role){
//            return false;
//            dd($user->hasAccess($role));
                return $user->hasAccess($role); // returns true or false.
            });

        }
    }
}
