<?php

namespace App\Providers;

use App\Setting;
use Illuminate\Support\ServiceProvider;

class SettingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $setting_image_size = Setting::where('slug','image_post_max_size')->first();
        $setting_file_size = Setting::where('slug','image_post_max_size')->first();
        if (is_null($setting_image_size)) {
            $max_image_size = 2000;
        }else{
            $max_image_size = $setting_image_size->value;
        }
        if (is_null($setting_file_size)) {
            $max_file_size = 2000;
        }else{
            $max_file_size = $setting_file_size->value;
        }
        config()->set(['lfm.max_image_size' => $max_image_size]);
        config()->set(['lfm.max_file_size' => $max_file_size]);
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
