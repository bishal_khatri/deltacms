<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WeblinkPost extends Model
{
    protected $table = 'weblink_post';
    protected $primaryKey = 'weblink_post_id';

    public function weblink()
    {
        return $this->belongsTo('App\Weblink', 'weblink_id');
    }

    public function post()
    {
        return $this->belongsTo('App\Post', 'post_id');
    }
}
