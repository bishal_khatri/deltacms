<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MenuPost extends Model
{
    protected $table = 'menu_post';
    protected $primaryKey = "menu_post_id";

    public function parent()
    {
        return $this->belongsTo('App\MenuPost', 'parent');
    }

    /**
     * Return the child menu.
     */
    public function children()
    {
        return $this->hasMany('App\MenuPost', 'parent')->orderBy('order','asc');
    }

    public function menu()
    {
        return $this->belongsTo('App\Menu', 'menu_id');
    }

    public function post()
    {
        return $this->belongsTo('App\Post', 'post_id');
    }
}
