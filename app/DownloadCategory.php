<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DownloadCategory extends Model
{
    protected $table = 'downloads_categories';
    protected $primaryKey = 'downloads_category_id';

    public function user()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function downloadItem()
    {
        return $this->hasMany('App\DownloadItem','category_id');
    }


}
