<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Weblink extends Model
{
    protected $primaryKey = 'weblink_id';

    public function user()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function post()
    {
        return $this->hasMany('App\Post','id');
    }

    public function weblink_post()
    {
        return $this->hasMany('App\WeblinkPost','weblink_id');
    }
}
