<?php

namespace App\Helpers;


use App\Menu;
use App\MenuPost;
use App\Post;
use App\Setting;
use App\Slider;

class Helper
{
    public static function createPostSlug($postTitle)
    {
        $replaceable = array(' ', '/', '?');
        return str_replace($replaceable,'-',strtolower($postTitle));
    }

    public static function bytesToHuman($bytes)
    {
        $units = ['B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB'];
        for ($i = 0; $bytes > 1024; $i++) {
            $bytes /= 1024;
        }
        return round($bytes, 2) . ' ' . $units[$i];
    }

    public static function image_post_max_size()
    {
        // dd(config('lfm.max_image_size'));
        $data = Setting::where('slug','image_post_max_size')->first();
        if (isset($data)){
            $value = $data->value;
        }
        else{
            $value = 2*1024;
        }
        return $value;
    }

    public static function file_post_max_size()
    {
        $data = Setting::where('slug','file_post_max_size')->first();
        if (isset($data)){
            $value = $data->value;
        }
        else{
            $value = 2*1024;
        }
        return $value;
    }

    public static function site_logo()
    {
        return Setting::where('slug','site_logo')->first();
    }

    public static function menu()
    {
        $nav_menu = Menu::where('menu_type','nav_menu')->where('is_active',1)->first();
        $footer_menu = Menu::where('menu_type','footer_menu')->where('is_active',1)->first();
        if (!empty($nav_menu)) {
            $data['nav_menus'] = MenuPost::where('menu_id',$nav_menu->id)
                ->where('parent',0)
                ->with(['menu','children.post','parent','post'])
                ->orderBy('order')
                ->get();
        }
        if (!empty($footer_menu)) {
            $footer = MenuPost::where('menu_id',$footer_menu->id)
                ->where('parent',0)
                ->with(['menu','post'])
                ->orderBy('order')
                ->get();
            $data['footer_menus'] = $footer->chunk(5, true);
        }
        $data['prenav_menus'] = self::getMenu('prenav_menu');
        $data['pre_nav_contact_info'] = Setting::where('slug','pre_nav_contact_info')->first();
//        $data['sliders'] = Slider::where('is_active',1)->get();
        $data['site_logo'] = Helper::site_logo();
        return $data;
    }

    public static function get_popup()
    {
        $data = Post::where('post_type','popup')->where('post_status','published')->get();
        return $data;
    }

    private static function getMenu($type='')
    {
        return \DB::table('menu_post as mp')->select('mp.menu_description','mp.menu_icon','mp.menu_display_name','m.id as menu_id','m.title as menu_title','p.post_title','p.post_url','p.post_slug','p.post_type')
            ->join('posts as p','p.id','mp.post_id')
            ->join('menus as m','m.id','mp.menu_id')
            ->where('m.menu_type',$type)
            ->where('m.is_active',1)
            ->get();
    }
}
