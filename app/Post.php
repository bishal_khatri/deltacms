<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function seo()
    {
        return $this->hasOne('App\Seo','page_id');
    }

    public function gallery()
    {
        return $this->hasMany('App\PostMeta','post_id');
    }
}
