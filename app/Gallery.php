<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
    protected $table = 'gallery';
    public $timestamps = false;
    protected $dates = [
        'created_at'
    ];

    public function user()
    {
        return $this->belongsTo('App\User','created_by');
    }

    public function image()
    {
        return $this->hasMany('App\GalleryImage','gallery_id')->take(3)->orderBy('created_at','desc');
    }

    public function images()
    {
        return $this->hasMany('App\GalleryImage','gallery_id')->orderBy('created_at','desc');
    }
}
