<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SliderType extends Model
{
    protected $table = 'slider_type';
    protected $fillable = ['name','slug','status'];

    public function slider()
    {
        return $this->hasMany('App\Slider','type_id');
    }
}
