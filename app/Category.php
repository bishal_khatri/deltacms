<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'post_category';

    public function user()
    {
    	return $this->belongsTo('App\User','created_by');
    }

    public function posts()
    {
        return $this->hasMany('App\Post', 'category_id')->orderBy('created_at','desc');
    }
}
