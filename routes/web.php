<?php

//defines
if(!defined('STATIC_DIR')) define('STATIC_DIR','public/');
if(!defined('ROOT_DIR')) define('ROOT_DIR','admin/');
if(!defined('ROOT_DIR_NAME')) define('ROOT_DIR_NAME','');
if(!defined('DEFAULT_USER')) define('DEFAULT_USER','assets/icons/default-user.png');
if(!defined('DEFAULT_IMAGE')) define('DEFAULT_IMAGE','assets/icons/default-image.png');
if(!defined('LOGO_DARK')) define('LOGO_DARK','assets/icons/delta-dark.png');
if(!defined('LOGO_WHITE')) define('LOGO_WHITE','assets/icons/delta-white.png');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/**
 * BACK END ROUTES
 */
Route::get(ROOT_DIR.'login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post(ROOT_DIR.'login', 'Auth\LoginController@login');
Route::post(ROOT_DIR.'logout', 'Auth\LoginController@logout')->name('logout');
Route::post(ROOT_DIR.'password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
Route::get(ROOT_DIR.'password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.request');
Route::post(ROOT_DIR.'password/reset', 'Auth\ResetPasswordController@reset')->name('password.update');
Route::get(ROOT_DIR.'password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('password.reset');
Route::get(ROOT_DIR, 'HomeController@index')->name('home');

Route::group(['prefix' =>ROOT_DIR.'user', 'middleware' => 'auth', 'as' => 'user.'], function () {
    Route::get('/register', ['uses'=>'Auth\RegisterController@showRegistrationForm','as'=>'register'])->middleware('can:user-add');
    Route::post('/register', 'Auth\RegisterController@register')->middleware('can:user-add');
    Route::get('/edit/{id}', ['uses'=>'Auth\RegisterController@showEditForm','as'=>'edit'])->middleware('can:user-edit');
    Route::post('/update', ['uses'=>'Auth\RegisterController@update','as'=> 'update'])->middleware('can:user-edit');
    Route::get('/list', ['uses'=>'Auth\RegisterController@index', 'as'=>'index'])->middleware('can:user-list');
    Route::post('/permission_view', ['uses'=>'PermissionController@permission_view', 'as'=>'permission_view']);
    Route::get('/assign_permission/{id}', ['uses'=>'PermissionController@assign_permission', 'as'=>'assign_permission'])->middleware('can:user-permission-assign');
    Route::post('/assign_permission/store', ['uses'=>'PermissionController@assign_permission_store', 'as'=>'assign_permission_store'])->middleware('can:user-permission-assign');
    Route::post('/delete', ['uses'=>'Auth\RegisterController@delete', 'as'=>'delete'])->middleware('can:user-delete');
});

Route::group(['prefix' =>ROOT_DIR.'permission', 'middleware' => 'auth', 'as' => 'permission.'], function () {
    Route::get('/list', ['uses'=>'PermissionController@index','as'=>'index'])->middleware('can:permission-add');
    Route::post('/add', ['uses'=>'PermissionController@create','as'=>'add'])->middleware('can:permission-add');
    Route::post('/delete', ['uses'=>'PermissionController@delete','as'=>'delete'])->middleware('can:permission-add');
});

Route::group(['prefix' =>ROOT_DIR.'setting', 'middleware' => 'auth', 'as' => 'setting.'], function () {
    Route::get('/list', ['uses'=>'SettingController@index', 'as'=>'index'])->middleware('can:setting-list');
    Route::post('/store_max_image_size', ['uses'=>'SettingController@store_max_image_size', 'as'=>'store_max_image_size'])->middleware('can:setting-change');
    Route::post('/store_max_file_size', ['uses'=>'SettingController@store_max_file_size', 'as'=>'store_max_file_size'])->middleware('can:setting-change');
    Route::post('/logo', ['uses'=>'SettingController@store_logo', 'as'=>'store_logo'])->middleware('can:setting-change');
    Route::post('/contact_info', ['uses'=>'SettingController@store_contact_info', 'as'=>'contact_info'])->middleware('can:setting-change');
});

Route::group(['prefix' =>ROOT_DIR.'profile', 'middleware' => 'auth', 'as' => 'profile.'], function () {
    Route::get('/view/{id}', ['uses'=>'ProfileController@view','as'=>'view']);
    Route::post('/update/{type}', ['uses'=>'ProfileController@update','as'=>'update']);
});

Route::group(['prefix' =>ROOT_DIR.'slider', 'middleware' => 'auth', 'as' => 'slider.'], function () {
    Route::get('/list', ['uses'=>'SliderController@index','as'=>'index'])->middleware('can:slider-list');
    Route::post('/add', ['uses'=>'SliderController@create','as'=>'add'])->middleware('can:slider-add');
    Route::get('/edit/{slider_id}', ['uses'=>'SliderController@edit','as'=>'edit'])->middleware('can:slider-add');
    Route::post('/update', ['uses'=>'SliderController@update','as'=>'update'])->middleware('can:slider-add');
    Route::get('/activate/{id}/{action}', ['uses'=>'SliderController@set_active','as'=>'activate'])->middleware('can:slider-add');
    Route::post('/delete', ['uses'=>'SliderController@delete','as'=>'delete']);
});

Route::group(['prefix' =>ROOT_DIR.'pages', 'middleware' => 'auth', 'as' => 'pages.'], function () {
    Route::get('/list', ['uses'=>'PostController@index','as'=>'index'])->middleware('can:pages-list');
    Route::get('/add', ['uses'=>'PostController@create','as'=>'add'])->middleware('can:pages-add');
    Route::post('/add', ['uses'=>'PostController@store'])->middleware('can:pages-add');
    Route::get('/edit/{id}', ['uses'=>'PostController@edit', 'as'=> 'edit'])->middleware('can:pages-edit');
    Route::post('/update', ['uses'=>'PostController@update', 'as'=> 'update'])->middleware('can:pages-edit');
    Route::post('/create_url', ['uses'=>'PostController@create_url', 'as'=>'create_url'])->middleware('can:pages-add');
    Route::post('/recycle', ['uses'=>'PostController@recycle', 'as'=>'recycle'])->middleware('can:pages-delete');
    Route::post('/make_featured', ['uses'=>'PostController@make_featured', 'as'=>'make_featured']);
});

Route::group(['prefix' =>ROOT_DIR.'menu', 'middleware' => 'auth', 'as' => 'menu.'], function () {
    Route::get('/list', ['uses'=>'MenuController@index','as'=>'index'])->middleware('can:menu-list');
    Route::post('/add', ['uses'=>'MenuController@create','as'=>'add'])->middleware('can:menu-add');
    Route::post('/change', ['uses'=>'MenuController@change','as'=>'change'])->middleware('can:menu-add');
    Route::get('/remove_item/{menu_post_id}', ['uses'=>'MenuController@remove_item','as'=>'remove_item'])->middleware('can:menu-add');
    Route::post('/edit_menu', ['uses'=>'MenuController@edit_menu','as'=>'edit_menu'])->middleware('can:menu-add');
    Route::post('/saveOrder', ['uses'=>'MenuController@saveOrder','as'=>'saveOrder'])->middleware('can:menu-add');
    Route::get('/change_status/{menu_id}/{action}', ['uses'=>'MenuController@change_status','as'=>'change_status'])->middleware('can:menu-add');
});

Route::group(['prefix' =>ROOT_DIR.'gallery', 'middleware' => 'auth', 'as' => 'gallery.'], function () {
    Route::get('/list', ['uses'=>'GalleryController@index','as'=>'index'])->middleware('can:gallery-list');
    Route::post('/add', ['uses'=>'GalleryController@create','as'=>'add'])->middleware('can:gallery-add');
    Route::get('/view/{id}', ['uses'=>'GalleryController@display','as'=>'view'])->middleware('can:gallery-list');
    Route::post('/edit', ['uses'=>'GalleryController@edit','as'=>'edit'])->middleware('can:gallery-edit');
    Route::post('/delete_gallery', ['uses'=>'GalleryController@delete_gallery','as'=>'delete_gallery'])->middleware('can:gallery-delete');
    Route::group(['prefix' =>'image', 'middleware' => 'auth', 'as' => 'image.'], function () {
        Route::post('/add', ['uses'=>'GalleryController@add_image','as'=>'add'])->middleware('can:gallery-edit');
        Route::post('/delete', ['uses'=>'GalleryController@delete_image','as'=>'delete'])->middleware('can:gallery-delete');
    });
});

Route::group(['prefix' =>ROOT_DIR.'category', 'middleware' => 'auth', 'as' => 'category.'], function () {
    Route::get('/list', ['uses'=>'CategoryController@index','as'=>'index'])->middleware('can:category-list');
    Route::get('/list_item/{id}', ['uses'=>'CategoryController@list_item','as'=>'list_item'])->middleware('can:category-list');
    Route::post('/store', ['uses'=>'CategoryController@store', 'as'=>'store'])->middleware('can:category-add');
    Route::post('/delete', ['uses'=>'CategoryController@delete_category','as'=>'delete'])->middleware('can:category-list');
    Route::post('/update', ['uses'=>'CategoryController@update_category','as'=>'update'])->middleware('can:category-list');

    // Items
    Route::get('/add_item/{category_id}', ['uses'=>'CategoryController@add_item', 'as'=>'add_item'])->middleware('can:category-add');
    Route::post('/store_item', ['uses'=>'CategoryController@store_item', 'as'=>'store_item'])->middleware('can:category-add');
    Route::post('/update_item', ['uses'=>'CategoryController@update_item', 'as'=>'update_item'])->middleware('can:category-add');
    Route::get('/edit_item/{item_id}', ['uses'=>'CategoryController@edit_item', 'as'=>'edit_item'])->middleware('can:category-add');
    Route::post('/delete_item', ['uses'=>'CategoryController@delete_item', 'as'=>'delete_item'])->middleware('can:category-add');
});

Route::group(['prefix' =>ROOT_DIR.'downloads', 'middleware' => 'auth', 'as' => 'downloads.'], function () {
    Route::get('/list', ['uses'=>'DownloadController@index', 'as'=>'index'])->middleware('can:pages-add');
    Route::post('/add', ['uses'=>'DownloadController@store', 'as'=>'add'])->middleware('can:pages-add');
    Route::post('/edit', ['uses'=>'DownloadController@edit', 'as'=>'edit'])->middleware('can:pages-add');
    Route::post('/delete', ['uses'=>'DownloadController@delete', 'as'=>'delete'])->middleware('can:pages-add');
    Route::get('/download/{id}', ['uses'=>'DownloadController@download', 'as'=>'download'])->middleware('can:pages-add');
    Route::group(['prefix' =>'item', 'middleware' => 'auth', 'as' => 'item.'], function () {
        Route::get('/view_item/{id}', ['uses'=>'DownloadController@view_item', 'as'=>'view_item'])->middleware('can:pages-add');
        Route::post('/add', ['uses'=>'DownloadController@store_item', 'as'=>'add'])->middleware('can:pages-add');
        Route::post('/delete', ['uses'=>'DownloadController@delete_item', 'as'=>'delete'])->middleware('can:pages-add');
    });
});

// post route
Route::group(['prefix' =>ROOT_DIR.'post', 'middleware' => 'auth', 'as' => 'post.'], function () {
    Route::get('/list', ['uses'=>'PostController@get_post_index','as'=>'index'])->middleware('can:post-list');
    Route::get('/add', ['uses'=>'PostController@get_post_create','as'=>'add'])->middleware('can:post-add');
    Route::post('/add', ['uses'=>'PostController@post_store'])->middleware('can:post-add');
    Route::get('/edit/{id}', ['uses'=>'PostController@get_post_edit', 'as'=> 'edit'])->middleware('can:post-add');
    Route::post('/update', ['uses'=>'PostController@post_update', 'as'=> 'update'])->middleware('can:post-add');
    Route::post('/recycle', ['uses'=>'PostController@post_recycle', 'as'=>'recycle'])->middleware('can:post-add');
});

// weblink route
Route::group(['prefix' =>ROOT_DIR.'weblink', 'middleware' => 'auth', 'as' => 'weblink.'], function () {
    Route::get('/list', ['uses'=>'WeblinkController@index','as'=>'index'])->middleware('can:weblink-list');
    Route::post('/add', ['uses'=>'WeblinkController@store','as'=>'add'])->middleware('can:weblink-add');
    Route::post('/delete', ['uses'=>'WeblinkController@delete','as'=>'delete'])->middleware('can:weblink-delete');
    // items
    Route::get('/view/{weblink_id}', ['uses'=>'WeblinkController@display','as'=>'view'])->middleware('can:weblink-item-list');
    Route::post('/item/update', ['uses'=>'WeblinkController@updateItem','as'=>'item.update'])->middleware('can:weblink-item-edit');
    Route::post('/item/addPost', ['uses'=>'WeblinkController@addPost','as'=>'item.addPost'])->middleware('can:weblink-item-add');
    Route::get('/item/remove/{weblink_post_id}', ['uses'=>'WeblinkController@removeItem','as'=>'item.remove'])->middleware('can:weblink-item-delete');
});

// job portal
Route::group(['prefix' =>ROOT_DIR.'jobs', 'middleware' => 'auth', 'as' => 'jobs.'], function () {
    Route::get('/list', ['uses'=>'JobController@index','as'=>'index'])->middleware('can:jobs-list');
    Route::get('/add', ['uses'=>'JobController@create','as'=>'add'])->middleware('can:jobs-add');
    Route::post('/add', ['uses'=>'JobController@store'])->middleware('can:jobs-add');
    Route::get('/edit/{job_id}', ['uses'=>'JobController@edit','as'=>'edit'])->middleware('can:jobs-edit');
    Route::post('/update', ['uses'=>'JobController@update','as'=>'update'])->middleware('can:jobs-edit');
    Route::post('/delete', ['uses'=>'JobController@delete','as'=>'delete'])->middleware('can:jobs-delete');
    Route::get('/applications/{job_id}', ['uses'=>'JobController@view_application', 'as'=>'application'])->middleware('can:jobs-list-application');
    Route::get('/application/export/{job_id}/{type}', ['uses'=>'JobController@export_users_application','as'=>'application.export_users'])->middleware('can:jobs-user-list');

    Route::group(['prefix' =>'user', 'middleware' => 'auth', 'as' => 'user.'], function () {
        Route::get('/list', ['uses'=>'JobController@get_users','as'=>'index'])->middleware('can:jobs-user-list');
        Route::get('/user/export/{type}', ['uses'=>'JobController@export_users','as'=>'export_users'])->middleware('can:jobs-user-list');
    });
});

// popup
Route::group(['prefix' =>ROOT_DIR.'popup', 'middleware' => 'auth', 'as' => 'popup.'], function () {
    Route::get('/list', ['uses'=>'PostController@popup_index','as'=>'index'])->middleware('can:popup-list');
    Route::post('/add', ['uses'=>'PostController@popup_store','as'=>'add'])->middleware('can:popup-list');
    Route::post('/delete', ['uses'=>'PostController@popup_delete','as'=>'delete'])->middleware('can:popup-list');
    Route::post('/update', ['uses'=>'PostController@popup_update','as'=>'update'])->middleware('can:popup-list');
});

// Request form
Route::group(['prefix' =>ROOT_DIR.'form', 'middleware' => 'auth', 'as' => 'form.'], function () {
    Route::get('/list/{status?}', ['uses'=>'RequestFormController@index','as'=>'index'])->middleware('can:form-list');
    Route::get('/get_email', ['uses'=>'RequestFormController@get_email','as'=>'get_email'])->middleware('can:form-list');
    Route::post('/update_email', ['uses'=>'RequestFormController@update_email','as'=>'update_email'])->middleware('can:form-list');
    Route::post('/approve', ['uses'=>'RequestFormController@approveForm','as'=>'approve'])->middleware('can:form-list');
    Route::get('/editForm/{form_id}', ['uses'=>'RequestFormController@editForm','as'=>'editForm'])->middleware('can:form-list');
    Route::get('/update', ['uses'=>'RequestFormController@update','as'=>'update'])->middleware('can:form-list');
    Route::get('/display', ['uses'=>'RequestFormController@display','as'=>'display'])->middleware('can:form-list');
    Route::get('/export/{type}/{id}', ['uses'=>'RequestFormController@export','as'=>'export'])->middleware('can:form-list');
});

/**
 * FRONT END ROUTES
 */
Route::get('/', 'frontend\HomeController@index')->name('index');
Route::get('/login', ['uses'=>'Frontend\Auth\LoginController@ShowLoginForm', 'as'=>'user_login']);
Route::post('/login', ['uses'=>'Frontend\Auth\LoginController@login']);
Route::get('/profile', ['uses'=>'Frontend\Auth\ProfileController@index', 'as'=>'user_profile']);
Route::get('/view_profile/{user_id}', ['uses'=>'Frontend\Auth\ProfileController@view_profile', 'as'=>'view_user_profile']);
Route::get('/change_password', ['uses'=>'Frontend\Auth\ProfileController@ShowChangePasswordForm', 'as'=>'change_password']);
Route::post('/change_password', ['uses'=>'Frontend\Auth\ProfileController@ChangePassword', 'as'=>'change_password']);
Route::get('/register', ['uses'=>'Frontend\Auth\RegisterController@ShowRegisterForm','as'=>'user_register']);
Route::post('/register', ['uses'=>'Frontend\Auth\RegisterController@register','as'=>'user_register']);
// CAREER ROUTE
Route::group(['prefix' =>'career', 'as' => 'career.'], function () {
    Route::get('/', ['uses'=>'frontend\CareerController@index', 'as'=>'career']);
    Route::get('/apply/{job_id}', ['uses'=>'frontend\CareerController@apply', 'as'=>'apply']);
});

// Request form
Route::group(['prefix' =>'ac_form', 'as' => 'ac_form.'], function () {
    Route::post('/store', ['uses'=>'RequestFormController@store', 'as'=>'store']);
    Route::get('/{id?}', ['uses'=>'RequestFormController@page1', 'as'=>'page1']);
    Route::get('/page2/{id}', ['uses'=>'RequestFormController@page2', 'as'=>'page2']);
    Route::get('/page3/{id}', ['uses'=>'RequestFormController@page3', 'as'=>'page3']);
    Route::get('/page4/{id}', ['uses'=>'RequestFormController@page4', 'as'=>'page4']);
    Route::get('/page5/{id}', ['uses'=>'RequestFormController@page5', 'as'=>'page5']);
    Route::get('/page6/{id}', ['uses'=>'RequestFormController@page6', 'as'=>'page6']);
});

Route::get('downloads/{page}', 'frontend\HomeController@downloads');
Route::get('mail', 'frontend\HomeController@mailTest');
Route::get('/{page}', 'frontend\HomeController@index')->name('front_page');
